namespace :db do
  desc "fill database with some sample data"

  task populate: :environment do
    role_entities = %w(User Profile)
    role_entities.each do |role_entity|
      Role.create!(name: "#{role_entity}Viewer",
                description: "#{role_entity}Viewer")
      Role.create!(name: "#{role_entity}Manager",
                description: "#{role_entity}Manager")
    end


    User.create!(username: "admin",
              password: "admin123",
              password_confirmation: "admin123")
  end
end