namespace :db do
  desc "fill database with some sample data"

  task provision: :environment do
    role_types = %w(User Profile EBill AuditTrail)
    role_types.each do |role_type|
      RoleType.create!(name: role_type, description: role_type)
    end

    user_viewer_role = Role.create!(name: "UserViewer", description: "UserViewer",
                      role_type: RoleType.find_by(name: "User"))
    user_manager_role = Role.create!(name: "UserManager", description: "UserManager",
                      role_type: RoleType.find_by(name: "User"))
    profile_viewer_role = Role.create!(name: "ProfileViewer", description: "ProfileViewer",
                      role_type: RoleType.find_by(name: "Profile"))
    profile_manager_role = Role.create!(name: "ProfileManager", description: "ProfileManager",
                      role_type: RoleType.find_by(name: "Profile"))


    view_e_bill_role = Role.create!(name: "ViewEBill", description: "View EBill",
                role_type: RoleType.find_by(name: "EBill"))
    create_e_bill_role = Role.create!(name: "CreateEBill", description: "CreateEBill",
                role_type: RoleType.find_by(name: "EBill"))
    view_own_e_bill_role = Role.create!(name: "ViewOwnEBill", description: "View own EBill",
                role_type: RoleType.find_by(name: "EBill"))

    manage_e_bill_role = Role.create!(name: "ManageEBill", description: "ManageEBill",
                role_type: RoleType.find_by(name: "EBill"))
    manage_own_e_bill_role = Role.create!(name: "ManageOwnEBill", description: "ManageOwnEBill",
                role_type: RoleType.find_by(name: "EBill"))

    view_audit_trail_role = Role.create!(name: "ViewAuditTrail", description: "ViewAuditTrail",
                role_type: RoleType.find_by(name: "AuditTrail"))


    admin_profile = Profile.create!(name: Profile::ADMIN, description: "Can manage everything",
                roles: [user_viewer_role, user_manager_role, profile_viewer_role,
                  profile_manager_role, view_e_bill_role, create_e_bill_role,
                  manage_e_bill_role, view_audit_trail_role])

    merchant_profile = Profile.create!(name: Profile::MERCHANT, description: "Merchant",
              roles: [create_e_bill_role, view_own_e_bill_role, manage_own_e_bill_role])

    service_desk_profile = Profile.create!(name: Profile::SERVICE_DESK, description: "Service desk",
              roles: [view_e_bill_role, view_audit_trail_role])

    User.create!(username: "admin",
              password: "admin123",
              password_confirmation: "admin123",
              first_name: "admin",
              last_name: "admin",
              msisdn: "123456789",
              profiles: [admin_profile])

    User.create!(username: "merchant",
              password: "merchant123",
              password_confirmation: "merchant123",
              first_name: "merchant",
              last_name: "merchant",
              msisdn: "123456780",
              profiles: [merchant_profile])

    User.create!(username: "servicedesk",
                password: "servicedesk123",
                password_confirmation: "servicedesk123",
                first_name: "service",
                last_name: "service",
                msisdn: "23434545",
                profiles: [service_desk_profile])
  end
end