# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20181024114950) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pg_trgm"

  create_table "audit_trails", force: true do |t|
    t.string   "client_transaction_id"
    t.string   "from"
    t.string   "to"
    t.string   "bill_id"
    t.string   "payer_id"
    t.text     "context"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "status",                default: "success"
    t.string   "action"
    t.text     "params"
    t.string   "operator"
    t.text     "error_messages"
    t.text     "search_field"
  end

  add_index "audit_trails", ["search_field"], name: "audit_trails_trgm_gin", using: :gin

  create_table "batch_files", force: true do |t|
    t.string   "file"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "state"
    t.integer  "provisioned", default: 0
    t.integer  "succeeded",   default: 0
    t.integer  "failed",      default: 0
  end

  create_table "countries", force: true do |t|
    t.string   "name"
    t.string   "currency_name"
    t.string   "currency_code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "currencies", force: true do |t|
    t.string   "code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "delayed_jobs", force: true do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree

  create_table "e_bills", force: true do |t|
    t.integer  "user_id"
    t.string   "payer_email"
    t.string   "payer_msisdn"
    t.integer  "amount"
    t.string   "currency"
    t.string   "state"
    t.datetime "expired_at"
    t.datetime "schedule_at"
    t.string   "external_reference"
    t.string   "additional_info"
    t.string   "description"
    t.string   "reason"
    t.boolean  "sms",                         default: false
    t.boolean  "email",                       default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "sms_message"
    t.string   "merchant_internal_ref"
    t.datetime "sent_at"
    t.datetime "paid_at"
    t.datetime "cancelled_at"
    t.datetime "processed_at"
    t.datetime "failed_at"
    t.datetime "expire_at"
    t.string   "short_description"
    t.date     "due_date"
    t.string   "bill_id"
    t.integer  "batch_file_id"
    t.string   "payer_name"
    t.string   "payer_address"
    t.string   "payer_city"
    t.boolean  "accept_partial_payment",      default: false
    t.integer  "minimum_amount"
    t.integer  "amount_paid",                 default: 0
    t.datetime "partially_paid_at"
    t.datetime "overdue_at"
    t.integer  "payment_system_id"
    t.string   "ps_transaction_id"
    t.integer  "traffic_fee_centimes",        default: 0,     null: false
    t.string   "traffic_fee_currency",        default: "XAF", null: false
    t.text     "search_field"
    t.string   "payer_id"
    t.string   "payer_code"
    t.string   "data0"
    t.string   "data1"
    t.string   "data2"
    t.string   "data3"
    t.string   "data4"
    t.string   "data5"
    t.string   "data6"
    t.string   "data7"
    t.string   "data8"
    t.string   "data9"
    t.integer  "ps_transaction_fee_centimes", default: 0,     null: false
    t.string   "ps_transaction_fee_currency", default: "XAF", null: false
    t.integer  "last_paid_amount_centimes",   default: 0,     null: false
    t.string   "last_paid_amount_currency",   default: "XAF", null: false
    t.integer  "expiry_period",               default: 0
  end

  add_index "e_bills", ["bill_id"], name: "index_e_bills_on_bill_id", unique: true, using: :btree
  add_index "e_bills", ["search_field"], name: "e_bills_trgm_gin", using: :gin
  add_index "e_bills", ["user_id"], name: "index_e_bills_on_user_id", using: :btree

  create_table "ebilling_payment_accounts", force: true do |t|
    t.integer  "payment_system_id"
    t.string   "payment_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fee_buckets", force: true do |t|
    t.integer  "currency_id"
    t.integer  "min_transactions"
    t.integer  "max_transactions"
    t.integer  "fee_value"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "fee_strategies", force: true do |t|
    t.string   "type"
    t.integer  "fixed_amount_centimes",   default: 0,     null: false
    t.string   "fixed_amount_currency",   default: "XAF", null: false
    t.float    "percentage"
    t.integer  "maximum_amount_centimes", default: 0,     null: false
    t.string   "maximum_amount_currency", default: "XAF", null: false
    t.integer  "merchant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "disable_recurrent",       default: false
  end

  create_table "merchant_global_stats", force: true do |t|
    t.integer  "created_count",        default: 0
    t.integer  "ready_count",          default: 0
    t.integer  "paid_count",           default: 0
    t.integer  "partially_paid_count", default: 0
    t.integer  "cancelled_count",      default: 0
    t.integer  "failed_count",         default: 0
    t.integer  "processed_count",      default: 0
    t.integer  "expired_count",        default: 0
    t.integer  "merchant_id",                      null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "overdue_count",        default: 0
  end

  create_table "merchant_monthly_stats", force: true do |t|
    t.integer  "created_count",                         default: 0
    t.integer  "ready_count",                           default: 0
    t.integer  "paid_count",                            default: 0
    t.integer  "partially_paid_count",                  default: 0
    t.integer  "cancelled_count",                       default: 0
    t.integer  "failed_count",                          default: 0
    t.integer  "processed_count",                       default: 0
    t.integer  "expired_count",                         default: 0
    t.integer  "merchant_id",                                           null: false
    t.integer  "year",                                                  null: false
    t.integer  "month",                                                 null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "overdue_count",                         default: 0
    t.integer  "traffic_fee_centimes",        limit: 8, default: 0,     null: false
    t.string   "traffic_fee_currency",                  default: "XAF", null: false
    t.integer  "revenue_centimes",            limit: 8, default: 0,     null: false
    t.string   "revenue_currency",                      default: "XAF", null: false
    t.integer  "recurrent_fee_centimes",      limit: 8, default: 0,     null: false
    t.string   "recurrent_fee_currency",                default: "XAF", null: false
    t.integer  "ps_transaction_fee_centimes",           default: 0,     null: false
    t.string   "ps_transaction_fee_currency",           default: "XAF", null: false
  end

  add_index "merchant_monthly_stats", ["merchant_id", "year", "month"], name: "index_merchant_monthly_stats_on_merchant_id_and_year_and_month", unique: true, using: :btree

  create_table "merchant_payment_system_mappings", force: true do |t|
    t.integer  "merchant_id"
    t.integer  "payment_system_id"
    t.string   "merchant_ps_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "ps_percentage",       default: 0.0
    t.string   "search_field"
    t.float    "ebilling_percentage", default: 0.0
  end

  create_table "mobile_traces", force: true do |t|
    t.string   "device_id"
    t.text     "stack_trace"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "mobile_traces", ["user_id"], name: "index_mobile_traces_on_user_id", using: :btree

  create_table "mobiles", force: true do |t|
    t.string   "imei"
    t.integer  "user_id"
    t.string   "otp"
    t.datetime "otp_last_sent_at"
    t.datetime "otp_confirmed_at"
    t.string   "pin_hash"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payment_system_monthly_items", force: true do |t|
    t.integer  "paid_count",                               default: 0
    t.integer  "partially_paid_count",                     default: 0
    t.integer  "processed_count",                          default: 0
    t.integer  "revenue_centimes",               limit: 8, default: 0,     null: false
    t.string   "revenue_currency",                         default: "XAF", null: false
    t.integer  "payment_system_monthly_stat_id"
    t.integer  "merchant_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "transaction_fee_centimes",       limit: 8, default: 0,     null: false
    t.string   "transaction_fee_currency",                 default: "XAF", null: false
  end

  create_table "payment_system_monthly_stats", force: true do |t|
    t.integer  "payment_system_id", null: false
    t.integer  "year",              null: false
    t.integer  "month",             null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "payment_system_monthly_stats", ["payment_system_id", "year", "month"], name: "payment_system_monthly_stats_unique", unique: true, using: :btree

  create_table "profiles", force: true do |t|
    t.string   "name",        null: false
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "profiles", ["name"], name: "index_profiles_on_name", unique: true, using: :btree

  create_table "profiles_roles", id: false, force: true do |t|
    t.integer "profile_id"
    t.integer "role_id"
  end

  create_table "profiles_users", id: false, force: true do |t|
    t.integer "profile_id"
    t.integer "user_id"
  end

  create_table "role_types", force: true do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "roles", force: true do |t|
    t.string   "name",         null: false
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "role_type_id"
    t.string   "display_name"
  end

  add_index "roles", ["name"], name: "index_roles_on_name", unique: true, using: :btree

  create_table "settings", force: true do |t|
    t.string   "var",                   null: false
    t.text     "value"
    t.integer  "thing_id"
    t.string   "thing_type", limit: 30
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "settings", ["thing_type", "thing_id", "var"], name: "index_settings_on_thing_type_and_thing_id_and_var", unique: true, using: :btree

  create_table "users", force: true do |t|
    t.string   "username",                       default: "",    null: false
    t.string   "encrypted_password",             default: "",    null: false
    t.string   "email",                          default: ""
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                  default: 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "msisdn"
    t.string   "city"
    t.string   "address"
    t.integer  "e_bills_count",                  default: 0
    t.string   "notification_url"
    t.string   "notification_params",            default: [],                 array: true
    t.boolean  "notification_post",              default: true
    t.string   "legal_info"
    t.string   "greeting_message"
    t.boolean  "email_notification",             default: false
    t.string   "website"
    t.string   "logo"
    t.string   "signature"
    t.string   "additional_notification_params", default: ""
    t.string   "currency"
    t.integer  "payment_system_id"
    t.text     "search_field"
    t.string   "shared_key"
    t.float    "tax_rate"
    t.string   "zipcode"
    t.boolean  "charge_payer",                   default: false
  end

  add_index "users", ["payment_system_id"], name: "index_users_on_payment_system_id", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

end
