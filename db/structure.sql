--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: pg_trgm; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public;


--
-- Name: EXTENSION pg_trgm; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION pg_trgm IS 'text similarity measurement and index searching based on trigrams';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: audit_trails; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE audit_trails (
    id integer NOT NULL,
    client_transaction_id character varying(255),
    "from" character varying(255),
    "to" character varying(255),
    bill_id character varying(255),
    payer_id character varying(255),
    context text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    status character varying(255) DEFAULT 'success'::character varying,
    action character varying(255),
    params text,
    operator character varying(255),
    error_messages text,
    search_field text
);


--
-- Name: audit_trails_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE audit_trails_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: audit_trails_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE audit_trails_id_seq OWNED BY audit_trails.id;


--
-- Name: batch_files; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE batch_files (
    id integer NOT NULL,
    file character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    user_id integer,
    state character varying(255),
    provisioned integer DEFAULT 0,
    succeeded integer DEFAULT 0,
    failed integer DEFAULT 0
);


--
-- Name: batch_files_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE batch_files_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: batch_files_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE batch_files_id_seq OWNED BY batch_files.id;


--
-- Name: currencies; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE currencies (
    id integer NOT NULL,
    code character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: currencies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE currencies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: currencies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE currencies_id_seq OWNED BY currencies.id;


--
-- Name: data_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE data_migrations (
    version character varying(255) NOT NULL
);


--
-- Name: delayed_jobs; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE delayed_jobs (
    id integer NOT NULL,
    priority integer DEFAULT 0 NOT NULL,
    attempts integer DEFAULT 0 NOT NULL,
    handler text NOT NULL,
    last_error text,
    run_at timestamp without time zone,
    locked_at timestamp without time zone,
    failed_at timestamp without time zone,
    locked_by character varying(255),
    queue character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: delayed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE delayed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: delayed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE delayed_jobs_id_seq OWNED BY delayed_jobs.id;


--
-- Name: e_bills; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE e_bills (
    id integer NOT NULL,
    user_id integer,
    payer_email character varying(255),
    payer_msisdn character varying(255),
    amount integer,
    currency character varying(255),
    state character varying(255),
    expired_at timestamp without time zone,
    schedule_at timestamp without time zone,
    external_reference character varying(255),
    additional_info character varying(255),
    description character varying(255),
    reason character varying(255),
    sms boolean DEFAULT false,
    email boolean DEFAULT false,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    sms_message character varying(255),
    merchant_internal_ref character varying(255),
    sent_at timestamp without time zone,
    paid_at timestamp without time zone,
    cancelled_at timestamp without time zone,
    processed_at timestamp without time zone,
    failed_at timestamp without time zone,
    expire_at timestamp without time zone,
    short_description character varying(255),
    due_date date,
    bill_id character varying(255),
    batch_file_id integer,
    payer_name character varying(255),
    payer_address character varying(255),
    payer_city character varying(255),
    accept_partial_payment boolean DEFAULT false,
    minimum_amount integer,
    amount_paid integer DEFAULT 0,
    partially_paid_at timestamp without time zone,
    overdue_at timestamp without time zone,
    payment_system_id integer,
    ps_transaction_id character varying(255),
    traffic_fee_centimes integer DEFAULT 0 NOT NULL,
    traffic_fee_currency character varying(255) DEFAULT 'XAF'::character varying NOT NULL,
    search_field text,
    payer_id character varying(255),
    payer_code character varying(255),
    data0 character varying(255),
    data1 character varying(255),
    data2 character varying(255),
    data3 character varying(255),
    data4 character varying(255),
    data5 character varying(255),
    data6 character varying(255),
    data7 character varying(255),
    data8 character varying(255),
    data9 character varying(255),
    ps_transaction_fee_centimes integer DEFAULT 0 NOT NULL,
    ps_transaction_fee_currency character varying(255) DEFAULT 'XAF'::character varying NOT NULL
);


--
-- Name: e_bills_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE e_bills_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: e_bills_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE e_bills_id_seq OWNED BY e_bills.id;


--
-- Name: ebilling_payment_accounts; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE ebilling_payment_accounts (
    id integer NOT NULL,
    payment_system_id integer,
    payment_id character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: ebilling_payment_accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE ebilling_payment_accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: ebilling_payment_accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE ebilling_payment_accounts_id_seq OWNED BY ebilling_payment_accounts.id;


--
-- Name: fee_buckets; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE fee_buckets (
    id integer NOT NULL,
    currency_id integer,
    min_transactions integer,
    max_transactions integer,
    fee_value integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: fee_buckets_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE fee_buckets_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: fee_buckets_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE fee_buckets_id_seq OWNED BY fee_buckets.id;


--
-- Name: fee_strategies; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE fee_strategies (
    id integer NOT NULL,
    type character varying(255),
    fixed_amount_centimes integer DEFAULT 0 NOT NULL,
    fixed_amount_currency character varying(255) DEFAULT 'XAF'::character varying NOT NULL,
    percentage double precision,
    maximum_amount_centimes integer DEFAULT 0 NOT NULL,
    maximum_amount_currency character varying(255) DEFAULT 'XAF'::character varying NOT NULL,
    merchant_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    disable_recurrent boolean DEFAULT false
);


--
-- Name: fee_strategies_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE fee_strategies_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: fee_strategies_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE fee_strategies_id_seq OWNED BY fee_strategies.id;


--
-- Name: merchant_global_stats; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE merchant_global_stats (
    id integer NOT NULL,
    created_count integer DEFAULT 0,
    ready_count integer DEFAULT 0,
    paid_count integer DEFAULT 0,
    partially_paid_count integer DEFAULT 0,
    cancelled_count integer DEFAULT 0,
    failed_count integer DEFAULT 0,
    processed_count integer DEFAULT 0,
    expired_count integer DEFAULT 0,
    merchant_id integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    overdue_count integer DEFAULT 0
);


--
-- Name: merchant_global_stats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE merchant_global_stats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: merchant_global_stats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE merchant_global_stats_id_seq OWNED BY merchant_global_stats.id;


--
-- Name: merchant_monthly_stats; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE merchant_monthly_stats (
    id integer NOT NULL,
    created_count integer DEFAULT 0,
    ready_count integer DEFAULT 0,
    paid_count integer DEFAULT 0,
    partially_paid_count integer DEFAULT 0,
    cancelled_count integer DEFAULT 0,
    failed_count integer DEFAULT 0,
    processed_count integer DEFAULT 0,
    expired_count integer DEFAULT 0,
    merchant_id integer NOT NULL,
    year integer NOT NULL,
    month integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    overdue_count integer DEFAULT 0,
    traffic_fee_centimes bigint DEFAULT 0 NOT NULL,
    traffic_fee_currency character varying(255) DEFAULT 'XAF'::character varying NOT NULL,
    revenue_centimes bigint DEFAULT 0 NOT NULL,
    revenue_currency character varying(255) DEFAULT 'XAF'::character varying NOT NULL,
    recurrent_fee_centimes bigint DEFAULT 0 NOT NULL,
    recurrent_fee_currency character varying(255) DEFAULT 'XAF'::character varying NOT NULL
);


--
-- Name: merchant_monthly_stats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE merchant_monthly_stats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: merchant_monthly_stats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE merchant_monthly_stats_id_seq OWNED BY merchant_monthly_stats.id;


--
-- Name: merchant_payment_system_mappings; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE merchant_payment_system_mappings (
    id integer NOT NULL,
    merchant_id integer,
    payment_system_id integer,
    merchant_ps_id character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    ps_percentage double precision DEFAULT 0
);


--
-- Name: merchant_payment_system_mappings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE merchant_payment_system_mappings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: merchant_payment_system_mappings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE merchant_payment_system_mappings_id_seq OWNED BY merchant_payment_system_mappings.id;


--
-- Name: mobile_traces; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE mobile_traces (
    id integer NOT NULL,
    device_id character varying(255),
    stack_trace text,
    user_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: mobile_traces_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE mobile_traces_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mobile_traces_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE mobile_traces_id_seq OWNED BY mobile_traces.id;


--
-- Name: mobiles; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE mobiles (
    id integer NOT NULL,
    imei character varying(255),
    user_id integer,
    otp character varying(255),
    otp_last_sent_at timestamp without time zone,
    otp_confirmed_at timestamp without time zone,
    pin_hash character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: mobiles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE mobiles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mobiles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE mobiles_id_seq OWNED BY mobiles.id;


--
-- Name: payment_system_monthly_items; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE payment_system_monthly_items (
    id integer NOT NULL,
    paid_count integer DEFAULT 0,
    partially_paid_count integer DEFAULT 0,
    processed_count integer DEFAULT 0,
    revenue_centimes integer DEFAULT 0 NOT NULL,
    revenue_currency character varying(255) DEFAULT 'XAF'::character varying NOT NULL,
    payment_system_monthly_stat_id integer,
    merchant_id integer,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: payment_system_monthly_items_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payment_system_monthly_items_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payment_system_monthly_items_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payment_system_monthly_items_id_seq OWNED BY payment_system_monthly_items.id;


--
-- Name: payment_system_monthly_stats; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE payment_system_monthly_stats (
    id integer NOT NULL,
    payment_system_id integer NOT NULL,
    year integer NOT NULL,
    month integer NOT NULL,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: payment_system_monthly_stats_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE payment_system_monthly_stats_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: payment_system_monthly_stats_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE payment_system_monthly_stats_id_seq OWNED BY payment_system_monthly_stats.id;


--
-- Name: profiles; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE profiles (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: profiles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE profiles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: profiles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE profiles_id_seq OWNED BY profiles.id;


--
-- Name: profiles_roles; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE profiles_roles (
    profile_id integer,
    role_id integer
);


--
-- Name: profiles_users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE profiles_users (
    profile_id integer,
    user_id integer
);


--
-- Name: role_types; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE role_types (
    id integer NOT NULL,
    name character varying(255),
    description character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: role_types_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE role_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: role_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE role_types_id_seq OWNED BY role_types.id;


--
-- Name: roles; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE roles (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    role_type_id integer,
    display_name character varying(255)
);


--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE roles_id_seq OWNED BY roles.id;


--
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE schema_migrations (
    version character varying(255) NOT NULL
);


--
-- Name: settings; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE settings (
    id integer NOT NULL,
    var character varying(255) NOT NULL,
    value text,
    thing_id integer,
    thing_type character varying(30),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


--
-- Name: settings_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE settings_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: settings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE settings_id_seq OWNED BY settings.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: -; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    username character varying(255) DEFAULT ''::character varying NOT NULL,
    encrypted_password character varying(255) DEFAULT ''::character varying NOT NULL,
    email character varying(255) DEFAULT ''::character varying,
    reset_password_token character varying(255),
    reset_password_sent_at timestamp without time zone,
    remember_created_at timestamp without time zone,
    sign_in_count integer DEFAULT 0,
    current_sign_in_at timestamp without time zone,
    last_sign_in_at timestamp without time zone,
    current_sign_in_ip character varying(255),
    last_sign_in_ip character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    first_name character varying(255),
    last_name character varying(255),
    msisdn character varying(255),
    city character varying(255),
    address character varying(255),
    e_bills_count integer DEFAULT 0,
    notification_url character varying(255),
    notification_params character varying(255)[] DEFAULT '{}'::character varying[],
    notification_post boolean DEFAULT true,
    legal_info character varying(255),
    greeting_message character varying(255),
    email_notification boolean DEFAULT false,
    website character varying(255),
    logo character varying(255),
    signature character varying(255),
    additional_notification_params character varying(255),
    currency character varying(255),
    payment_system_id integer,
    search_field text,
    shared_key character varying(255),
    tax_rate double precision,
    zipcode character varying(255),
    charge_payer boolean DEFAULT false
);


--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY audit_trails ALTER COLUMN id SET DEFAULT nextval('audit_trails_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY batch_files ALTER COLUMN id SET DEFAULT nextval('batch_files_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY currencies ALTER COLUMN id SET DEFAULT nextval('currencies_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY delayed_jobs ALTER COLUMN id SET DEFAULT nextval('delayed_jobs_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY e_bills ALTER COLUMN id SET DEFAULT nextval('e_bills_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY ebilling_payment_accounts ALTER COLUMN id SET DEFAULT nextval('ebilling_payment_accounts_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY fee_buckets ALTER COLUMN id SET DEFAULT nextval('fee_buckets_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY fee_strategies ALTER COLUMN id SET DEFAULT nextval('fee_strategies_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY merchant_global_stats ALTER COLUMN id SET DEFAULT nextval('merchant_global_stats_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY merchant_monthly_stats ALTER COLUMN id SET DEFAULT nextval('merchant_monthly_stats_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY merchant_payment_system_mappings ALTER COLUMN id SET DEFAULT nextval('merchant_payment_system_mappings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY mobile_traces ALTER COLUMN id SET DEFAULT nextval('mobile_traces_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY mobiles ALTER COLUMN id SET DEFAULT nextval('mobiles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment_system_monthly_items ALTER COLUMN id SET DEFAULT nextval('payment_system_monthly_items_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY payment_system_monthly_stats ALTER COLUMN id SET DEFAULT nextval('payment_system_monthly_stats_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY profiles ALTER COLUMN id SET DEFAULT nextval('profiles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY role_types ALTER COLUMN id SET DEFAULT nextval('role_types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY settings ALTER COLUMN id SET DEFAULT nextval('settings_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Name: audit_trails_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY audit_trails
    ADD CONSTRAINT audit_trails_pkey PRIMARY KEY (id);


--
-- Name: batch_files_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY batch_files
    ADD CONSTRAINT batch_files_pkey PRIMARY KEY (id);


--
-- Name: currencies_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY currencies
    ADD CONSTRAINT currencies_pkey PRIMARY KEY (id);


--
-- Name: delayed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY delayed_jobs
    ADD CONSTRAINT delayed_jobs_pkey PRIMARY KEY (id);


--
-- Name: e_bills_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY e_bills
    ADD CONSTRAINT e_bills_pkey PRIMARY KEY (id);


--
-- Name: ebilling_payment_accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY ebilling_payment_accounts
    ADD CONSTRAINT ebilling_payment_accounts_pkey PRIMARY KEY (id);


--
-- Name: fee_buckets_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY fee_buckets
    ADD CONSTRAINT fee_buckets_pkey PRIMARY KEY (id);


--
-- Name: fee_strategies_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY fee_strategies
    ADD CONSTRAINT fee_strategies_pkey PRIMARY KEY (id);


--
-- Name: merchant_global_stats_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY merchant_global_stats
    ADD CONSTRAINT merchant_global_stats_pkey PRIMARY KEY (id);


--
-- Name: merchant_monthly_stats_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY merchant_monthly_stats
    ADD CONSTRAINT merchant_monthly_stats_pkey PRIMARY KEY (id);


--
-- Name: merchant_payment_system_mappings_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY merchant_payment_system_mappings
    ADD CONSTRAINT merchant_payment_system_mappings_pkey PRIMARY KEY (id);


--
-- Name: mobile_traces_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY mobile_traces
    ADD CONSTRAINT mobile_traces_pkey PRIMARY KEY (id);


--
-- Name: mobiles_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY mobiles
    ADD CONSTRAINT mobiles_pkey PRIMARY KEY (id);


--
-- Name: payment_system_monthly_items_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY payment_system_monthly_items
    ADD CONSTRAINT payment_system_monthly_items_pkey PRIMARY KEY (id);


--
-- Name: payment_system_monthly_stats_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY payment_system_monthly_stats
    ADD CONSTRAINT payment_system_monthly_stats_pkey PRIMARY KEY (id);


--
-- Name: profiles_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY profiles
    ADD CONSTRAINT profiles_pkey PRIMARY KEY (id);


--
-- Name: role_types_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY role_types
    ADD CONSTRAINT role_types_pkey PRIMARY KEY (id);


--
-- Name: roles_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: settings_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY settings
    ADD CONSTRAINT settings_pkey PRIMARY KEY (id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: -; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: audit_trails_trgm_gin; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX audit_trails_trgm_gin ON audit_trails USING gin (search_field gin_trgm_ops);


--
-- Name: delayed_jobs_priority; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX delayed_jobs_priority ON delayed_jobs USING btree (priority, run_at);


--
-- Name: e_bills_trgm_gin; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX e_bills_trgm_gin ON e_bills USING gin (search_field gin_trgm_ops);


--
-- Name: index_e_bills_on_bill_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_e_bills_on_bill_id ON e_bills USING btree (bill_id);


--
-- Name: index_e_bills_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_e_bills_on_user_id ON e_bills USING btree (user_id);


--
-- Name: index_merchant_monthly_stats_on_merchant_id_and_year_and_month; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_merchant_monthly_stats_on_merchant_id_and_year_and_month ON merchant_monthly_stats USING btree (merchant_id, year, month);


--
-- Name: index_mobile_traces_on_user_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_mobile_traces_on_user_id ON mobile_traces USING btree (user_id);


--
-- Name: index_profiles_on_name; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_profiles_on_name ON profiles USING btree (name);


--
-- Name: index_roles_on_name; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_roles_on_name ON roles USING btree (name);


--
-- Name: index_settings_on_thing_type_and_thing_id_and_var; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_settings_on_thing_type_and_thing_id_and_var ON settings USING btree (thing_type, thing_id, var);


--
-- Name: index_users_on_payment_system_id; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE INDEX index_users_on_payment_system_id ON users USING btree (payment_system_id);


--
-- Name: index_users_on_reset_password_token; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_reset_password_token ON users USING btree (reset_password_token);


--
-- Name: index_users_on_username; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX index_users_on_username ON users USING btree (username);


--
-- Name: payment_system_monthly_stats_unique; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX payment_system_monthly_stats_unique ON payment_system_monthly_stats USING btree (payment_system_id, year, month);


--
-- Name: unique_data_migrations; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX unique_data_migrations ON data_migrations USING btree (version);


--
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: -; Tablespace: 
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- PostgreSQL database dump complete
--

SET search_path TO "$user",public;

INSERT INTO schema_migrations (version) VALUES ('20130815135214');

INSERT INTO schema_migrations (version) VALUES ('20130817053556');

INSERT INTO schema_migrations (version) VALUES ('20130817055824');

INSERT INTO schema_migrations (version) VALUES ('20130817060724');

INSERT INTO schema_migrations (version) VALUES ('20130818135105');

INSERT INTO schema_migrations (version) VALUES ('20130904163117');

INSERT INTO schema_migrations (version) VALUES ('20130909133524');

INSERT INTO schema_migrations (version) VALUES ('20130911124510');

INSERT INTO schema_migrations (version) VALUES ('20130911145723');

INSERT INTO schema_migrations (version) VALUES ('20130913151837');

INSERT INTO schema_migrations (version) VALUES ('20130913152925');

INSERT INTO schema_migrations (version) VALUES ('20130913164138');

INSERT INTO schema_migrations (version) VALUES ('20131027023721');

INSERT INTO schema_migrations (version) VALUES ('20131027031119');

INSERT INTO schema_migrations (version) VALUES ('20131027163245');

INSERT INTO schema_migrations (version) VALUES ('20131028151532');

INSERT INTO schema_migrations (version) VALUES ('20131030152833');

INSERT INTO schema_migrations (version) VALUES ('20131030160432');

INSERT INTO schema_migrations (version) VALUES ('20131030160819');

INSERT INTO schema_migrations (version) VALUES ('20131031160145');

INSERT INTO schema_migrations (version) VALUES ('20131102124522');

INSERT INTO schema_migrations (version) VALUES ('20131103143811');

INSERT INTO schema_migrations (version) VALUES ('20131103161744');

INSERT INTO schema_migrations (version) VALUES ('20131106155315');

INSERT INTO schema_migrations (version) VALUES ('20131118135426');

INSERT INTO schema_migrations (version) VALUES ('20131118153055');

INSERT INTO schema_migrations (version) VALUES ('20131118163055');

INSERT INTO schema_migrations (version) VALUES ('20131119145456');

INSERT INTO schema_migrations (version) VALUES ('20131120163041');

INSERT INTO schema_migrations (version) VALUES ('20131125135013');

INSERT INTO schema_migrations (version) VALUES ('20131125165104');

INSERT INTO schema_migrations (version) VALUES ('20131127153000');

INSERT INTO schema_migrations (version) VALUES ('20131212235504');

INSERT INTO schema_migrations (version) VALUES ('20131228162119');

INSERT INTO schema_migrations (version) VALUES ('20140216034123');

INSERT INTO schema_migrations (version) VALUES ('20140222061124');

INSERT INTO schema_migrations (version) VALUES ('20140305140613');

INSERT INTO schema_migrations (version) VALUES ('20140305144128');

INSERT INTO schema_migrations (version) VALUES ('20140306161248');

INSERT INTO schema_migrations (version) VALUES ('20140308093220');

INSERT INTO schema_migrations (version) VALUES ('20140310150646');

INSERT INTO schema_migrations (version) VALUES ('20140311134601');

INSERT INTO schema_migrations (version) VALUES ('20140311134721');

INSERT INTO schema_migrations (version) VALUES ('20140619122953');

INSERT INTO schema_migrations (version) VALUES ('20150211095806');

INSERT INTO schema_migrations (version) VALUES ('20150524070505');

INSERT INTO schema_migrations (version) VALUES ('20150621013728');

INSERT INTO schema_migrations (version) VALUES ('20150701115929');

INSERT INTO schema_migrations (version) VALUES ('20150705111016');

INSERT INTO schema_migrations (version) VALUES ('20150705112137');

INSERT INTO schema_migrations (version) VALUES ('20150707132132');

INSERT INTO schema_migrations (version) VALUES ('20150713120349');

INSERT INTO schema_migrations (version) VALUES ('20150713121106');

INSERT INTO schema_migrations (version) VALUES ('20150713122741');

INSERT INTO schema_migrations (version) VALUES ('20150717114512');

INSERT INTO schema_migrations (version) VALUES ('20150717114906');

INSERT INTO schema_migrations (version) VALUES ('20150720105736');

INSERT INTO schema_migrations (version) VALUES ('20150721112910');

INSERT INTO schema_migrations (version) VALUES ('20150725101051');

INSERT INTO schema_migrations (version) VALUES ('20150727114823');

INSERT INTO schema_migrations (version) VALUES ('20150727115626');

INSERT INTO schema_migrations (version) VALUES ('20150728114303');

INSERT INTO schema_migrations (version) VALUES ('20150728131757');

INSERT INTO schema_migrations (version) VALUES ('20150730111640');

INSERT INTO schema_migrations (version) VALUES ('20150801134106');

INSERT INTO schema_migrations (version) VALUES ('20150802111458');

INSERT INTO schema_migrations (version) VALUES ('20150802113712');

INSERT INTO schema_migrations (version) VALUES ('20150803111255');

INSERT INTO schema_migrations (version) VALUES ('20150804122242');

INSERT INTO schema_migrations (version) VALUES ('20150808125015');

INSERT INTO schema_migrations (version) VALUES ('20150808130233');

INSERT INTO schema_migrations (version) VALUES ('20150810114053');

INSERT INTO schema_migrations (version) VALUES ('20150816132659');

INSERT INTO schema_migrations (version) VALUES ('20150914104005');

INSERT INTO schema_migrations (version) VALUES ('20150914104229');

INSERT INTO schema_migrations (version) VALUES ('20150926103917');

INSERT INTO schema_migrations (version) VALUES ('20150929110344');

INSERT INTO schema_migrations (version) VALUES ('20150929121230');

INSERT INTO schema_migrations (version) VALUES ('20150929121549');

INSERT INTO schema_migrations (version) VALUES ('20150929121831');

INSERT INTO schema_migrations (version) VALUES ('20150930120256');

INSERT INTO schema_migrations (version) VALUES ('20151006121333');

INSERT INTO schema_migrations (version) VALUES ('20151007110947');

INSERT INTO schema_migrations (version) VALUES ('20151015213902');

INSERT INTO schema_migrations (version) VALUES ('20151024105054');

INSERT INTO schema_migrations (version) VALUES ('20151109112902');

INSERT INTO schema_migrations (version) VALUES ('20160210090657');

INSERT INTO schema_migrations (version) VALUES ('20160213003227');

INSERT INTO schema_migrations (version) VALUES ('20160225104327');

INSERT INTO schema_migrations (version) VALUES ('20160225105604');

INSERT INTO schema_migrations (version) VALUES ('20160328003905');
