class AddCountersAndStateToBatchFiles < ActiveRecord::Migration
  def change
    add_column :batch_files, :state, :string
    add_column :batch_files, :provisioned, :integer, :default => 0
    add_column :batch_files, :succeeded, :integer, :default => 0
    add_column :batch_files, :failed, :integer, :default => 0

    BatchFile.all.each do |batch_file|
      batch_file.state = "finished"
      batch_file.provisioned = 0;
      batch_file.succeeded = 0;
      batch_file.failed = 0;
      batch_file.save!
    end
  end
end
