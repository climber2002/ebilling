class CreateProfilesUsersJoinTable < ActiveRecord::Migration
  def change
    create_table :profiles_users, id: false do |t|
      t.integer :profile_id
      t.integer :user_id
    end
  end
end
