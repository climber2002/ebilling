class CreatePgTrgmIndex < ActiveRecord::Migration
  def up
    execute <<-SQL
      create extension pg_trgm;
      create index e_bills_trgm_gin on e_bills using gin (search_field gin_trgm_ops);
      create index audit_trails_trgm_gin on audit_trails using gin (search_field gin_trgm_ops);
    SQL
  end

  def down
    execute <<-SQL
      drop index e_bills_trgm_gin;
      drop index audit_trails_trgm_gin;
    SQL
  end
end
