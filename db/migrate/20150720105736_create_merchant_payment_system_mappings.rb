class CreateMerchantPaymentSystemMappings < ActiveRecord::Migration
  def change
    create_table :merchant_payment_system_mappings do |t|
      t.references :merchant
      t.references :payment_system
      t.string     :merchant_ps_id
      t.timestamps
    end
  end
end
