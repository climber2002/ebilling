class AddAttributesForReceiptsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :legal_info, :string
    add_column :users, :greeting_message, :string
    add_column :users, :email_notification, :boolean, :default => false
    add_column :users, :website, :string
  end
end
