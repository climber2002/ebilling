class ChangeClientTransactionIdMaxLength < ActiveRecord::Migration
  def change
    change_column :audit_trails, :client_transaction_id, :string, :limit => 255
  end
end
