class CreateMobiles < ActiveRecord::Migration
  def change
    create_table :mobiles do |t|
      t.string :imei
      t.references :user
      t.string :otp
      t.datetime :otp_last_sent_at
      t.datetime :otp_confirmed_at
      t.string :pin_hash
      t.timestamps
    end
  end
end
