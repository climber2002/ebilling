class AddNotificationInfoToUsers < ActiveRecord::Migration
  def change
    add_column :users, :notification_url, :string
    add_column :users, :notification_params, :string, array: true, default: []
    add_column :users, :notification_post, :boolean, default: true
  end
end
