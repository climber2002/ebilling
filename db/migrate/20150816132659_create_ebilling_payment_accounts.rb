class CreateEbillingPaymentAccounts < ActiveRecord::Migration
  def change
    create_table :ebilling_payment_accounts do |t|
      t.references :payment_system
      t.string     :payment_id
      t.timestamps
    end
  end
end
