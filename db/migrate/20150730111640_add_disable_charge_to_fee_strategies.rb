class AddDisableChargeToFeeStrategies < ActiveRecord::Migration
  def change
    add_column :fee_strategies, :disable_charge, :boolean, default: false
  end
end
