class AddFeeToMerchantMonthlyStats < ActiveRecord::Migration
  def change
    add_money :merchant_monthly_stats, :fee
  end
end
