class CreatePaymentSystemMonthlyStats < ActiveRecord::Migration
  def change
    create_table :payment_system_monthly_stats do |t|
      t.references :payment_system, null: false
      t.integer :year, null: false
      t.integer :month, null: false
      t.timestamps
    end

    add_index :payment_system_monthly_stats, [:payment_system_id, :year, :month], unique: true,
      name: 'payment_system_monthly_stats_unique'
  end
end
