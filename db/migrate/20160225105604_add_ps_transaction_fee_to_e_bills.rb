class AddPsTransactionFeeToEBills < ActiveRecord::Migration
  def change
    add_money :e_bills, :ps_transaction_fee
  end
end
