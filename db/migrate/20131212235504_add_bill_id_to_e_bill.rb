class AddBillIdToEBill < ActiveRecord::Migration
  def change
    add_column :e_bills, :bill_id, :string

    EBill.all.each do |e_bill|
      e_bill.generate_bill_id
      e_bill.save!
    end
  end

end
