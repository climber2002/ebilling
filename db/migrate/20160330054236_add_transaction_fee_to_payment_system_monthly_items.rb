class AddTransactionFeeToPaymentSystemMonthlyItems < ActiveRecord::Migration
  def change
    add_money :payment_system_monthly_items, :transaction_fee
  end
end
