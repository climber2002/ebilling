class AddRecurrentFeeToMerchantMonthlyStats < ActiveRecord::Migration
  def change
    add_money :merchant_monthly_stats, :recurrent_fee
  end
end
