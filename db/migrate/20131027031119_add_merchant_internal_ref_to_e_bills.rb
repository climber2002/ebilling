class AddMerchantInternalRefToEBills < ActiveRecord::Migration
  def change
    add_column :e_bills, :merchant_internal_ref, :string
  end
end
