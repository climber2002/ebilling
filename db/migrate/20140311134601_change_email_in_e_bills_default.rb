class ChangeEmailInEBillsDefault < ActiveRecord::Migration
  def change
    change_column_default :e_bills, :email, true
  end
end
