class AddSearchFieldToEBills < ActiveRecord::Migration
  def change
    add_column :e_bills, :search_field, :text
  end
end
