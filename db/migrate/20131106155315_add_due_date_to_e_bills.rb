class AddDueDateToEBills < ActiveRecord::Migration
  def change
    add_column :e_bills, :due_date, :date
  end
end
