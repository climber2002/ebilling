class CreateFeeStrategies < ActiveRecord::Migration
  def change
    create_table :fee_strategies do |t|
      t.string :type
      t.money :fixed_amount
      t.float   :percentage
      t.money :maximum_amount
      t.references :merchant
      t.timestamps
    end


  end
end
