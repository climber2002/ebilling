class CreateMobileTraces < ActiveRecord::Migration
  def change
    create_table :mobile_traces do |t|
      t.string :imei
      t.text   :trace
      t.belongs_to :user, :index => true
      t.timestamps
    end
  end
end
