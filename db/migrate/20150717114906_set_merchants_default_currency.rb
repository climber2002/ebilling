class SetMerchantsDefaultCurrency < ActiveRecord::Migration
  def up
    User.joins(:profiles).where("profiles.name = ?", Profile::MERCHANT).update_all(currency: 'XAF')
  end

  def down
    
  end
end
