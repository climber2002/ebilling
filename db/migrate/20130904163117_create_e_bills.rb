class CreateEBills < ActiveRecord::Migration
  def change
    create_table :e_bills do |t|
      t.belongs_to :user
      t.string :payer_email
      t.string :payer_msisdn
      t.integer :amount
      t.string :currency
      t.string :state
      t.timestamp :expired_at
      t.timestamp :schedule_at
      t.string :external_reference
      t.string :additional_info
      t.string :description
      t.string :reason
      t.boolean :sms
      t.boolean :email
      t.timestamps
    end

    add_index :e_bills, :user_id
  end
end
