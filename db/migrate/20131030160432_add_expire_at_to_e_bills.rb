class AddExpireAtToEBills < ActiveRecord::Migration
  def change
    add_column :e_bills, :expire_at, :datetime
  end
end
