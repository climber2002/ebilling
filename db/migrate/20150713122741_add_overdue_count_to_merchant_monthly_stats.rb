class AddOverdueCountToMerchantMonthlyStats < ActiveRecord::Migration
  def change
    add_column :merchant_monthly_stats, :overdue_count, :integer, default: 0
  end
end
