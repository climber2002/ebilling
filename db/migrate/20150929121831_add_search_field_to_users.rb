class AddSearchFieldToUsers < ActiveRecord::Migration
  def change
    add_column :users, :search_field, :text
  end
end
