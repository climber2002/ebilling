class AddPartialUpdateAttributesToEbills < ActiveRecord::Migration
  def change
    add_column :e_bills, :accept_partial_payment, :boolean, default: false
    add_column :e_bills, :minimum_amount, :integer
    add_column :e_bills, :amount_paid, :integer, default: 0
  end
end
