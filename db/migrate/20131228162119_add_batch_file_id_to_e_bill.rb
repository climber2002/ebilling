class AddBatchFileIdToEBill < ActiveRecord::Migration
  def change
    add_column :e_bills, :batch_file_id, :integer
  end
end
