class AddEBillsCountToUsers < ActiveRecord::Migration
  def change
    add_column :users, :e_bills_count, :integer, :default => 0

    User.reset_column_information

    User.all.each do |user|
      User.update(user.id, :e_bills_count => user.e_bills.length)
    end
  end
end
