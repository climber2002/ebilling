class AddViewAuditTrailRole < ActiveRecord::Migration
  def change
    view_audit_trail_role = Role.find_by :name => "ViewAuditTrail"
    unless view_audit_trail_role
      view_audit_trail_role = Role.create! name: "ViewAuditTrail",
            description: "ViewAuditTrail",
            role_type: RoleType.find_by(name: "AuditTrail")
    end

    Profile.find_by(name: Profile::ADMIN).roles << view_audit_trail_role
    Profile.find_by(name: Profile::SERVICE_DESK).roles << view_audit_trail_role
  end
end
