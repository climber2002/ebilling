class ChangeSmsInEBillsDefault < ActiveRecord::Migration
  def change
    change_column_default :e_bills, :sms, true
  end
end
