class AddOverdueCountToMerchantGlobalStats < ActiveRecord::Migration
  def change
    add_column :merchant_global_stats, :overdue_count, :integer, default: 0
  end
end
