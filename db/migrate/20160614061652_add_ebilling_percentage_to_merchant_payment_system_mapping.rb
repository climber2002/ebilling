class AddEbillingPercentageToMerchantPaymentSystemMapping < ActiveRecord::Migration
  def change
    add_column :merchant_payment_system_mappings, :ebilling_percentage, :float, default: 0
  end
end
