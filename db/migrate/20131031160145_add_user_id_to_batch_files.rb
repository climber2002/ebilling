class AddUserIdToBatchFiles < ActiveRecord::Migration
  def change
    add_column :batch_files, :user_id, :integer
  end
end
