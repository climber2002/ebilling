class AddSearchFieldToMerchantPaymentSystemMappings < ActiveRecord::Migration
  def change
    add_column :merchant_payment_system_mappings, :search_field, :string

    MerchantPaymentSystemMapping.all.each do |mapping|
      mapping.send(:store_search_field)
      mapping.save!
    end
  end
end
