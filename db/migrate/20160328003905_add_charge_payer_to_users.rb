class AddChargePayerToUsers < ActiveRecord::Migration
  def change
    add_column :users, :charge_payer, :boolean, default: false
  end
end
