class AddPaymentSystemOperatorProfile < ActiveRecord::Migration
  def change
    unless view_payment_system_e_bill_role = Role.find_by(name: "ViewPaymentSystemEBill") 
      view_payment_system_e_bill_role = Role.create!(name: "ViewPaymentSystemEBill", description: "View payment system EBill",
              role_type: RoleType.find_by(name: "EBill"))
    end

    unless Profile.find_by(name: Profile::PAYMENT_SYSTEM_OPERATOR)
      payment_system_operator_profile = Profile.create!(name: Profile::PAYMENT_SYSTEM_OPERATOR, description: "Payment System Operator",
              roles: [view_payment_system_e_bill_role])  
    end
    
    add_reference :users, :payment_system, index: true
  end
end
