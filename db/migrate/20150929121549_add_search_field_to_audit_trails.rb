class AddSearchFieldToAuditTrails < ActiveRecord::Migration
  def change
    add_column :audit_trails, :search_field, :text
  end
end
