class AddAdditionalNotificationParamsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :additional_notification_params, :string, default: ""
  end
end
