class ChangeUsersTaxRateType < ActiveRecord::Migration
  def up
    remove_column :users, :tax_rate
    add_column :users, :tax_rate, :float
  end

  def down
    remove_column :users, :tax_rate
    add_column :users, :tax_rate, :string
  end
end
