class CreateAuditTrails < ActiveRecord::Migration
  def change
    create_table :audit_trails do |t|
      t.string :client_transaction_id, :limit => 16
      t.string :from
      t.string :to
      t.string :bill_id
      t.string :payer_id
      t.string :context
      t.timestamps
    end
  end
end
