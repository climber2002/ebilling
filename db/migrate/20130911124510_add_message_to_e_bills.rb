class AddMessageToEBills < ActiveRecord::Migration
  def change
    add_column :e_bills, :message, :string, :length => 250
  end
end
