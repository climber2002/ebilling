class AddAttributesForReceiptsToEBills < ActiveRecord::Migration
  def change
    add_column :e_bills, :payer_name, :string
    add_column :e_bills, :payer_address, :string
    add_column :e_bills, :payer_city, :string
  end
end
