class AddPartiallyPaidAtToEBills < ActiveRecord::Migration
  def change
    add_column :e_bills, :partially_paid_at, :datetime
  end
end
