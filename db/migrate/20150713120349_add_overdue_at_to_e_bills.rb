class AddOverdueAtToEBills < ActiveRecord::Migration
  def change
    add_column :e_bills, :overdue_at, :datetime
  end
end
