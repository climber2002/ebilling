class AddFeeToEBills < ActiveRecord::Migration
  def change
    add_money :e_bills, :fee
  end
end
