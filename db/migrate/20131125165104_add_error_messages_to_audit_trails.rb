class AddErrorMessagesToAuditTrails < ActiveRecord::Migration
  def change
    add_column :audit_trails, :error_messages, :text
  end
end
