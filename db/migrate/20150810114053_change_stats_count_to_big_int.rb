class ChangeStatsCountToBigInt < ActiveRecord::Migration
  def up
    change_column :merchant_monthly_stats, :traffic_fee_centimes, :bigint
    change_column :merchant_monthly_stats, :revenue_centimes, :bigint
    change_column :merchant_monthly_stats, :recurrent_fee_centimes, :bigint
  end

  def down
    change_column :merchant_monthly_stats, :traffic_fee_centimes, :integer
    change_column :merchant_monthly_stats, :revenue_centimes, :integer
    change_column :merchant_monthly_stats, :recurrent_fee_centimes, :integer
  end
end
