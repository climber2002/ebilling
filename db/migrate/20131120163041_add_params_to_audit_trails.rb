class AddParamsToAuditTrails < ActiveRecord::Migration
  def change
    add_column :audit_trails, :params, :text
  end
end
