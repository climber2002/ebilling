class CreateMerchantGlobalStats < ActiveRecord::Migration
  def change
    create_table :merchant_global_stats do |t|
      t.integer :created_count, default: 0
      t.integer :ready_count, default: 0
      t.integer :paid_count, default: 0
      t.integer :partially_paid_count, default: 0
      t.integer :cancelled_count, default: 0
      t.integer :failed_count, default: 0
      t.integer :processed_count, default: 0
      t.integer :expired_count, default: 0

      t.references :merchant, null: false, unique: true
      t.timestamps
    end
  end
end
