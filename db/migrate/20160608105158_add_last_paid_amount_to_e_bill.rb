class AddLastPaidAmountToEBill < ActiveRecord::Migration
  def change
    add_money :e_bills, :last_paid_amount
  end
end
