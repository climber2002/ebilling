class AddOperatorToAuditTrails < ActiveRecord::Migration
  def change
    add_column :audit_trails, :operator, :string
  end
end
