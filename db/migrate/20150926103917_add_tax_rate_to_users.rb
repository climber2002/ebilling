class AddTaxRateToUsers < ActiveRecord::Migration
  def change
    add_column :users, :tax_rate, :string
  end
end
