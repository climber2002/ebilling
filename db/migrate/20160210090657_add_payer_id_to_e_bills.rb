class AddPayerIdToEBills < ActiveRecord::Migration
  def change
    add_column :e_bills, :payer_id, :string
    add_column :e_bills, :payer_code, :string
    add_column :e_bills, :data0, :string
    add_column :e_bills, :data1, :string
    add_column :e_bills, :data2, :string
    add_column :e_bills, :data3, :string
    add_column :e_bills, :data4, :string
    add_column :e_bills, :data5, :string
    add_column :e_bills, :data6, :string
    add_column :e_bills, :data7, :string
    add_column :e_bills, :data8, :string
    add_column :e_bills, :data9, :string
  end
end
