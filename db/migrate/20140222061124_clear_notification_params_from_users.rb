class ClearNotificationParamsFromUsers < ActiveRecord::Migration
  def change
    User.all.each do |user|
      user.update_column(:notification_params, [])
    end
  end
end
