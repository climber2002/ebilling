class AddStatusToAuditTrails < ActiveRecord::Migration
  def change
    add_column :audit_trails, :status, :string, default: 'success'

    AuditTrail.all.each do |audit_trail|
      audit_trail.status = 'success'
      audit_trail.save!
    end
  end
end
