class AddPsTransactionFeeToMerchantMonthlyStat < ActiveRecord::Migration
  def change
    add_money :merchant_monthly_stats, :ps_transaction_fee
  end
end
