class AddActionToAuditTrails < ActiveRecord::Migration
  def change
    add_column :audit_trails, :action, :string

    AuditTrail.all.each do |audit_trail|
      if audit_trail.from.blank?
        audit_trail.action = "createEBill"
      else
        audit_trail.action = "updateEBill"
      end
      audit_trail.save!
    end
  end
end
