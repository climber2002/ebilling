class CreateRolesForApi < ActiveRecord::Migration
  def change
  	verify_e_bill_role = Role.find_by(name: "VerifyEBill")
  	unless verify_e_bill_role
  		verify_e_bill_role = Role.create!(name: "VerifyEBill", description: "VerifyEBill for API",
                role_type: RoleType.find_by(name: "EBill"))
  	end

    update_e_bill_to_pay_role = Role.find_by(name: "UpdateEBillToPay")
    unless update_e_bill_to_pay_role
      update_e_bill_to_pay_role = Role.create!(name: "UpdateEBillToPay", description: "UpdateEBill for API",
        role_type: RoleType.find_by(name: "EBill"))
    end

    payment_system_profile = Profile.create!(name: "PaymentSystem", description: "PaymentSystem", 
      roles: [verify_e_bill_role, update_e_bill_to_pay_role])

    payment_user = User.create(username: "payment",
                               password: "payment123",
                               password_confirmation: "payment123",
                               first_name: "payment",
                               last_name: "payment",
                               msisdn: "323456780",
                               profiles: [payment_system_profile])
  end
end
