class CreateMerchantMonthlyStats < ActiveRecord::Migration
  def change
    create_table :merchant_monthly_stats do |t|
      t.integer :created_count, default: 0
      t.integer :ready_count, default: 0
      t.integer :paid_count, default: 0
      t.integer :partially_paid_count, default: 0
      t.integer :cancelled_count, default: 0
      t.integer :failed_count, default: 0
      t.integer :processed_count, default: 0
      t.integer :expired_count, default: 0

      t.references :merchant, null: false
      t.integer :year, null: false
      t.integer :month, null: false
      t.timestamps
    end

    add_index :merchant_monthly_stats, [:merchant_id, :year, :month], unique: true
  end
end
