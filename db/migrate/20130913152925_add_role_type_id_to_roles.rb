class AddRoleTypeIdToRoles < ActiveRecord::Migration
  def change
    add_column :roles, :role_type_id, :integer
  end
end
