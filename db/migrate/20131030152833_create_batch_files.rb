class CreateBatchFiles < ActiveRecord::Migration
  def change
    create_table :batch_files do |t|
      t.string :file
      t.timestamps
    end
  end
end
