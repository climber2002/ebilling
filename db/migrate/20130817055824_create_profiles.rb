class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.string :name, null: false
      t.string :description

      t.timestamps
    end

    add_index :profiles, :name, unique: true
  end
end
