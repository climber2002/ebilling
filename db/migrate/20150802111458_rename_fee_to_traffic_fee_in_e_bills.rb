class RenameFeeToTrafficFeeInEBills < ActiveRecord::Migration
  def change
    rename_column :e_bills, :fee_centimes, :traffic_fee_centimes
    rename_column :e_bills, :fee_currency, :traffic_fee_currency
  end
end
