class AddRevenueToMerchantMonthlyStats < ActiveRecord::Migration
  def change
    add_money :merchant_monthly_stats, :revenue
  end
end
