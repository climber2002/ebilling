class CreateFeeBuckets < ActiveRecord::Migration
  def change
    create_table :fee_buckets do |t|
      t.references  :currency
      t.integer     :min_transactions
      t.integer     :max_transactions
      t.integer     :fee_value
      t.timestamps
    end
  end
end
