class ChangeStackTraceColumn < ActiveRecord::Migration
  def change
    rename_column :mobile_traces, :imei, :device_id
    rename_column :mobile_traces, :trace, :stack_trace
  end
end
