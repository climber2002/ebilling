class AddBillIdIndex < ActiveRecord::Migration
  def change
    add_index(:e_bills, [:bill_id], unique: true)
  end
end
