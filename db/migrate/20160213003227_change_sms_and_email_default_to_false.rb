class ChangeSmsAndEmailDefaultToFalse < ActiveRecord::Migration
  def up
    change_column :e_bills, :sms, :boolean, :default => false
    change_column :e_bills, :email, :boolean, :default => false
  end

  def down
    change_column :e_bills, :sms, :boolean, :default => true
    change_column :e_bills, :email, :boolean, :default => true
  end
end
