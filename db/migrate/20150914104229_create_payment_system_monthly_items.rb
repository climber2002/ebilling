class CreatePaymentSystemMonthlyItems < ActiveRecord::Migration
  def change
    create_table :payment_system_monthly_items do |t|
      t.integer :paid_count, default: 0
      t.integer :partially_paid_count, default: 0
      t.integer :processed_count, default: 0
      t.money   :revenue
      t.references :payment_system_monthly_stat
      t.references :merchant
      t.timestamps
    end
  end
end
