class AddStateEventDatetimeToEBills < ActiveRecord::Migration
  def change
    add_column :e_bills, :sent_at, :datetime
    add_column :e_bills, :paid_at, :datetime
    add_column :e_bills, :cancelled_at, :datetime
    add_column :e_bills, :processed_at, :datetime
    add_column :e_bills, :failed_at, :datetime
  end
end
