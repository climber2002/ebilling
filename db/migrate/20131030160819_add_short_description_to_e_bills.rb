class AddShortDescriptionToEBills < ActiveRecord::Migration
  def change
    add_column :e_bills, :short_description, :string
  end
end
