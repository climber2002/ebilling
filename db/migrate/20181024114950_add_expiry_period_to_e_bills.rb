class AddExpiryPeriodToEBills < ActiveRecord::Migration
  def change
    add_column :e_bills, :expiry_period, :integer, default: 0
  end
end
