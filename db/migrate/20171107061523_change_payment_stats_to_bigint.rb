class ChangePaymentStatsToBigint < ActiveRecord::Migration
  
  def up
    change_column :payment_system_monthly_items, :transaction_fee_centimes, :bigint
    change_column :payment_system_monthly_items, :revenue_centimes, :bigint
  end

  def down
    change_column :payment_system_monthly_items, :transaction_fee_centimes, :integer
    change_column :payment_system_monthly_items, :revenue_centimes, :integer
  end

end
