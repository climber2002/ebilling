class AddPsPercentageToMerchantPaymentSystemMappings < ActiveRecord::Migration
  def change
    add_column :merchant_payment_system_mappings, :ps_percentage, :float, default: 0
  end
end
