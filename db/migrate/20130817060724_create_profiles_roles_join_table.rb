class CreateProfilesRolesJoinTable < ActiveRecord::Migration
  def change
    create_table :profiles_roles, id: false do |t|
      t.integer :profile_id
      t.integer :role_id
    end
  end
end
