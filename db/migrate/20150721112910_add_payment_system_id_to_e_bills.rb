class AddPaymentSystemIdToEBills < ActiveRecord::Migration
  def change
    add_column :e_bills, :payment_system_id, :integer
    add_column :e_bills, :ps_transaction_id, :string
  end
end
