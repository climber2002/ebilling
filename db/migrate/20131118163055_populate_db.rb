class PopulateDb < ActiveRecord::Migration
  def change
    return if Profile.find_by name: Profile::ADMIN
    Rake::Task['db:provision'].invoke
  end
end