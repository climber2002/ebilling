class RenameFeeToTrafficFeeInMerchantMonthlyStats < ActiveRecord::Migration
  def change
    rename_column :merchant_monthly_stats, :fee_centimes, :traffic_fee_centimes
    rename_column :merchant_monthly_stats, :fee_currency, :traffic_fee_currency
  end
end
