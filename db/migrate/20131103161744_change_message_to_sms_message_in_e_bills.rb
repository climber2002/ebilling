class ChangeMessageToSmsMessageInEBills < ActiveRecord::Migration
  def change
    rename_column :e_bills, :message, :sms_message
  end
end
