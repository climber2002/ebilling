class MakeCreatorCanReadEbills < ActiveRecord::Migration
  def change
    profile = Profile.find_by name: Profile::EBILL_CREATOR
    read_ebill_role = Role.find_by name: 'ViewEBill'
    if profile.present? && read_ebill_role.present?
      profile.roles << read_ebill_role
    end
  end
end
