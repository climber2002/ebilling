class ChangeAuditTrailContextToText < ActiveRecord::Migration
  def up
    change_column :audit_trails, :context, :text
  end

  def down
    change_column :audit_trails, :context, :string
  end
end
