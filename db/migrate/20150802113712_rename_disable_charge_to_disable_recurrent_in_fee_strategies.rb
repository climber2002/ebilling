class RenameDisableChargeToDisableRecurrentInFeeStrategies < ActiveRecord::Migration
  def change
    rename_column :fee_strategies, :disable_charge, :disable_recurrent

    FeeStrategy.where(type: 'FeeStrategy::RecurrentStrategy').update_all(type: 'Fee::FixedStrategy')
  end
end
