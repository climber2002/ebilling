# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
role_types = %w(User Profile EBill AuditTrail)
role_types.each do |role_type|
  RoleType.create!(name: role_type, description: role_type)
end

user_viewer_role = Role.create!(name: "UserViewer", description: "UserViewer",
                  role_type: RoleType.find_by(name: "User"))
user_manager_role = Role.create!(name: "UserManager", description: "UserManager",
                  role_type: RoleType.find_by(name: "User"))
profile_viewer_role = Role.create!(name: "ProfileViewer", description: "ProfileViewer",
                  role_type: RoleType.find_by(name: "Profile"))
profile_manager_role = Role.create!(name: "ProfileManager", description: "ProfileManager",
                  role_type: RoleType.find_by(name: "Profile"))


view_e_bill_role = Role.create!(name: "ViewEBill", description: "View EBill",
            role_type: RoleType.find_by(name: "EBill"))
create_e_bill_role = Role.create!(name: "CreateEBill", description: "CreateEBill",
            role_type: RoleType.find_by(name: "EBill"))
view_own_e_bill_role = Role.create!(name: "ViewOwnEBill", description: "View own EBill",
            role_type: RoleType.find_by(name: "EBill"))

manage_e_bill_role = Role.create!(name: "ManageEBill", description: "ManageEBill",
            role_type: RoleType.find_by(name: "EBill"))
manage_own_e_bill_role = Role.create!(name: "ManageOwnEBill", description: "ManageOwnEBill",
            role_type: RoleType.find_by(name: "EBill"))

view_audit_trail_role = Role.create!(name: "ViewAuditTrail", description: "ViewAuditTrail",
            role_type: RoleType.find_by(name: "AuditTrail"))

view_payment_system_e_bill_role = Role.create!(name: "ViewPaymentSystemEBill", description: "View payment system EBill",
            role_type: RoleType.find_by(name: "EBill"))


admin_profile = Profile.create!(name: Profile::ADMIN, description: "Can manage everything",
            roles: [user_viewer_role, user_manager_role, profile_viewer_role,
              profile_manager_role, view_e_bill_role, create_e_bill_role,
              manage_e_bill_role, view_audit_trail_role])

merchant_profile = Profile.create!(name: Profile::MERCHANT, description: "Merchant",
          roles: [create_e_bill_role, view_own_e_bill_role, manage_own_e_bill_role])

service_desk_profile = Profile.create!(name: Profile::SERVICE_DESK, description: "Service desk",
          roles: [view_e_bill_role, view_audit_trail_role])

payment_system_operator_profile = Profile.create!(name: Profile::PAYMENT_SYSTEM_OPERATOR, description: "Merchant",
          roles: [view_payment_system_e_bill_role])

User.create!(username: "admin",
          password: "admin123",
          password_confirmation: "admin123",
          first_name: "admin",
          last_name: "admin",
          msisdn: "123456789",
          profiles: [admin_profile],
          currency: "XAF")

User.create!(username: "merchant",
          password: "merchant123",
          password_confirmation: "merchant123",
          first_name: "merchant",
          last_name: "merchant",
          msisdn: "123456780",
          profiles: [merchant_profile],
          currency: "XAF")

User.create!(username: "servicedesk",
            password: "servicedesk123",
            password_confirmation: "servicedesk123",
            first_name: "service",
            last_name: "service",
            msisdn: "23434545",
            profiles: [service_desk_profile],
            currency: "XAF")
