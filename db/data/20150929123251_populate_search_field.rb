class PopulateSearchField < ActiveRecord::Migration
  def self.up
    [User, EBill, AuditTrail].each do |clazz|
      clazz.find_each do |obj|
        obj.send(:store_search_field)
        obj.save
        puts "saved #{clazz} #{obj}"
      end
    end
  end

  def self.down
    raise IrreversibleMigration
  end
end
