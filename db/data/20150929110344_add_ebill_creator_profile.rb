class AddEbillCreatorProfile < ActiveRecord::Migration
  def self.up
    unless Profile.find_by name: Profile::EBILL_CREATOR
      profile = Profile.new(name: Profile::EBILL_CREATOR, description: "EBill Creator")
      profile.roles << Role.find_by(name: 'CreateEBill')
      profile.save!
    end
    
  end

  def self.down
    
  end
end
