class AddPartialUpdateAttributesToEbills < ActiveRecord::Migration
  def self.up
    EBill.where(state: ['paid', 'processed']).each do |e_bill|
      e_bill.update_attribute(:amount_paid, e_bill.amount)
    end
  end

  def self.down
  end
end
