FROM climber2002/ebilling-base

RUN mkdir -p /var/log/supervisor
# COPY config/supervisord/*.conf /etc/supervisor/conf.d/

#Cache bundle install
WORKDIR /tmp
ADD ./Gemfile Gemfile
ADD ./Gemfile.lock Gemfile.lock
RUN bundle install

RUN mkdir /app
ADD . /app
WORKDIR /app

RUN DB_ADAPTER=nulldb RAILS_ENV=production bundle exec rake assets:precompile

CMD ["./bin/start_prod_server"]
