class EmailSender < ActionMailer::Base
  default from: "from@example.com"

  def e_bill_invoice e_bill
    deliver_attachment e_bill, :invoice
  end

  def e_bill_receipt e_bill
    deliver_attachment e_bill, :receipt
  end

private 
  def deliver_attachment e_bill, type
    if e_bill.payer_email.blank?
      logger.warn("The ebill has no email: #{e_bill}")
      return
    end

    audit_trail_params = {
      :action => "send#{type.to_s.camelize}Email", :bill_id => e_bill.bill_id,
      :params => {'email' => e_bill.payer_email }.to_s
    }

    generator = ReceiptGenerator.new(e_bill, File.join(Rails.root, "/public/templates/#{type}_template.odt"))

    generator.generate do |pdf_path|
      logger.debug "mail pdf file : ${pdf_path}"
      attachments["#{type}_#{e_bill.bill_id}.pdf"] = File.read(pdf_path)
    end

    mail_vars = { to: e_bill.payer_email, from: Settings.sender_id, subject: Settings.send("#{type}_email_subject_for", e_bill) }

    if e_bill.user.email_notification? && !e_bill.user.email.blank?
      mail_vars[:bcc] = e_bill.user.email
    end

    @message = Settings.send("#{type}_email_content_for", e_bill)
    mail(mail_vars)
    
    AuditTrail.create(audit_trail_params.merge(status: 'success'))
  rescue => e
    Rails.logger.warn "can't send email: #{e.message}"
    AuditTrail.create(audit_trail_params.merge(status: 'failed', error_messages: e.message))
  end
end
