class MobileRegistration < ActionMailer::Base
  default from: "from@example.com"

  def register_mobile(mobile)
    @mobile = mobile
    @user = mobile.user
    mail(to: @user.email, from: Settings.sender_id, subject: 'Welcome to register your mobile to eBilling')
  end

  def forget_pin(mobile)
    @mobile = mobile
    @user = mobile.user
    mail(to: @user.email, from: Settings.sender_id, subject: 'Reset your pin')
  end
end
