class UserMailer < Devise::Mailer
  def default_url_options
    {
      host: host
    }
  end

  private

  def host
    @host ||= Settings.email_default_host || 'https://billing-easy.com'
  end
end
