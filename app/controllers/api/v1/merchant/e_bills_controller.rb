module Api
  module V1
    module Merchant
      class EBillsController < BaseController
        include ApiSupport
        
        #  Explaination: 
        #  When the user views a form to create, update, or destroy a resource,
        #  the Rails app creates a random authenticity_token, stores this token in the session, 
        #  and places it in a hidden field in the form. 
        #  When the user submits the form, Rails looks for the authenticity_token, 
        #  compares it to the one stored in the session,
        #  and if they match the request is allowed to continue.
        #  1. Don't want to check the authenticity token for actions in this controller (But why ?)
        skip_before_filter :verify_authenticity_token

        #  1. Authenticates user before all actions in this controller
        before_filter :base_authenticate

        #  1. Checks if user has permission to create ebill
        #  2. Builds ebill model from params 
        #  3. Prepares ebill for audit by attaching action and operator
        #  4. Creates ebill from model and checks if it created.
        #  5. Saves the ebill and renders ebill
        def create
          authorize! :create, EBill
          @e_bill = current_user.e_bills.build create_e_bill_params    
          assign_attributes_to_e_bill_for_audit
          @e_bill.create true
          respond_with(@e_bill)
        end

        #for send
        state_event_action :trigger, "ready"

        # for process
        state_event_action :handle, "processed", { :event_action => "process" }

        #  1. Read per_page param from request, else set it to 20 ebills per page by default
        #  2. Get ebills accessible by current user and paginate them 
        #  3. Returns ebills and renders them
        def index
          per_page = params[:per_page] || 20
          @e_bills = EBill.accessible_by(current_ability).order("id desc").paginate :page => params[:page], :per_page => per_page
          respond_with(@e_bills) do |format|
            format.json do
              render :json => {
                :current_page => @e_bills.current_page,
                :per_page => @e_bills.per_page,
                :total_entries => @e_bills.total_entries,
                :entries => @e_bills
              }
            end
          end
        end

        def bulk_create
          authorize! :create, EBill
          bulk_creator = BulkEbillsCreator.new(current_user)
          bulk_creator.bulk_create(params)
          @e_bills = bulk_creator.e_bills
        rescue ActiveRecord::RecordInvalid
          @e_bill = bulk_creator.failed_ebill
          respond_with(@e_bill)
        rescue ArgumentError => e
          render json: { :error => e.message }, status: 422
        end


      private

        #  1. Filters ebill params with following params and returns them and this is used for create method 
        def create_e_bill_params
          params.permit(:client_transaction_id, :payer_email, :payer_msisdn, :amount,
                                        :due_date, :schedule_at, :expire_at, :expiry_period, :email,
                                        :sms, :external_reference, 
                                        :additional_info, :short_description, :description,
                                        :payer_name, :payer_address, :payer_city,
                                        :accept_partial_payment, :minimum_amount, :payer_id,
                                        :payer_code, :data0, :data1, :data2, :data3, :data4,
                                        :data5, :data6, :data7, :data8, :data9)
        end

        #  1. Filters ebills for triggering them to ready state with following param and returns those ebills 
        def trigger_e_bill_params
          params.permit(:client_transaction_id)
        end
        
        #  1. Filters ebills for processing them to processed state with following param and returns those ebills
        def process_e_bill_params
          params.permit(:client_transaction_id)
        end

      end
    end
  end
end