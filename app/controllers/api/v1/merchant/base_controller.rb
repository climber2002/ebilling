module Api
  module V1
    module Merchant
      class BaseController < ApplicationController

        private

          #  1. Username and password are obtained from http basic authentication params 
          #  2. Fetches user by username
          #  3. Raises unauthorised if user is not present or password is not matched
          #  4. User is assigned to current_api_user
          def base_authenticate
             authenticate_or_request_with_http_basic do |username, password|
              u = User.find_by username: username
              if u.present? && u.shared_key == password
                @current_api_user = u
                true
              else
                false
              end
            end

            unless @current_api_user.present?
              self.response_body = {'error' => 'HTTP Basic: Access denied.', 
               'message' => 'Auth invalid' }.to_json
            end
          end
          
          #  1. Returns current api user
          def current_user
            @current_api_user
          end

          def unauthorized
            render "api/errors/unauthorized", :status => 401 and return
          end
      end
    end
  end
end