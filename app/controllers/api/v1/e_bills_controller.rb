module Api
  module V1
    class EBillsController < ApplicationController
      self.responder = EBillResponder

      #  1. Authenticate user before every action in the this controller 
      before_filter :authenticate_user!

      #  1. Respond to only json request
      respond_to :json

      #  1. Checks if user has permission to create ebill
      #  2. Creates new ebill model with parameters passed in request
      #  3. Prepares ebill for audit trail
      #  4. Saves ebill model in database 
      #  5. Renders ebill
      def create
        authorize! :create, EBill
        @e_bill = EBill.new create_e_bill_params    
        assign_attributes_to_e_bill_for_audit
        @e_bill.create true
        respond_with(@e_bill)
      end

      #  1. Fetches ebill from id param in request
      #  2. Checks if user has permission to read ebill
      #  3. Filters attributes for ebill
      #  4. Renders ebill 
      def show
        fetch_e_bill
        authorize! :read, @e_bill
        @e_bill.assign_attributes show_e_bill_params
        respond_with(@e_bill)
      end

      #  1. Fetches ebill from id param received in request
      #  2. Checks if user has permission to update the ebill
      #  3. Filters ebill for trigger action
      #  4. Prepares ebill for audit trail
      #  5. Triggers ebill
      #  6. Logs ebill to audit trail if trigger fails
      def trigger
        fetch_e_bill
        authorize! :update, @e_bill
        @e_bill.assign_attributes trigger_e_bill_params
        assign_attributes_to_e_bill_for_audit

        unless @e_bill.trigger
          @e_bill.generate_failed_update_audit_trail @e_bill.state, "ready"
        end
        respond_with(@e_bill)
      end

      #  1. Fetches ebill from id param received in request
      #  2. Checks if user has permission to update the ebill
      #  3. Filters ebill for process action
      #  4. Prepares ebill for audit trail
      #  5. Processes ebill
      #  6. Logs ebill to audit trail if trigger fails
      def handle
        fetch_e_bill
        authorize! :update, @e_bill
        @e_bill.assign_attributes process_e_bill_params
        assign_attributes_to_e_bill_for_audit

        unless @e_bill.process
          @e_bill.generate_failed_update_audit_trail @e_bill.state, "processed"
        end
        respond_with(@e_bill)
      end

    private

      #  1. Filters ebill params with following params for creating ebill
      def create_e_bill_params
        params.permit(:client_transaction_id, :payer_email, :payer_msisdn, :amount,
                                      :due_date, :expired_at, :expiry_period, :schedule_at, :expire_at, :email,
                                      :sms, :merchant_internal_ref,:payee_id, :payer_id,
                                      :additional_info, :short_description, :description,
                                      :payer_code, :data0, :data1, :data2, :data3,
                                      :data4, :data5, :data6, :data7, :data8, :data9 )
      end

      #  1. Filters ebill params with following params for triggering ebill
      def trigger_e_bill_params
        params.permit(:client_transaction_id)
      end

      #  1. Filters ebill params with following params for showing ebill
      def show_e_bill_params
        params.permit(:client_transaction_id)
      end

      #  1. Filters ebill params with following params for processing ebill
      def process_e_bill_params
        params.permit(:client_transaction_id)
      end

      #  1. Returns ebill from ebill table based on bill id 
      #  2. Raises exception if not found   
      def fetch_e_bill
        @e_bill = EBill.find_by :bill_id => params[:id]
        raise ActiveRecord::RecordNotFound, "Could not find bill_id #{params[:id]}" unless @e_bill
      end
      
      #  1. Checks if ebill exists
      #  2. Assigns action to ebill
      #  3. Assigns operator to ebill
      def assign_attributes_to_e_bill_for_audit
        if @e_bill
          @e_bill.params = send("#{action_name}_e_bill_params").to_s
          @e_bill.operator = current_user.username
        end
      end

      rescue_from CanCan::AccessDenied do |exception|
        render :json => { error: I18n.t("api.errors.no_right") }, :status => :forbidden
      end

      rescue_from ActiveRecord::RecordNotFound do |exception|
        render :json => { error: I18n.t("api.errors.not_found") }, :status => :not_found
      end
    end
  end
end