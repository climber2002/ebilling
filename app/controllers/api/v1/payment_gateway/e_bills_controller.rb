module Api
  module V1
    module PaymentGateway
      class EBillsController < ApplicationController
        include ApiSupport

        #  1. Don't want to check the authenticity token for actions in this controller.
        skip_before_filter :verify_authenticity_token

        #  1. Authenticates user before every action in this controller 
        before_filter :authenticate_user!

        #  1. Searches ebill in the ebill as gateway pay table by id passed in request
        #  2. Checks if user has permission to update ebill through api
        #  3. Make ebill ready for audit trail by assigning it action and operator
        #  4. Updates ebill with params in request
        #  5. If updation fails, it is logged in audit trail database
        #  6. Returns ebill and renders it
        def update
          fetch_e_bill_as_gateway_pay

          authorize! :update_for_api, @e_bill
          assign_attributes_to_e_bill_for_audit
          unless @e_bill.update_attributes(update_e_bill_params)
            @e_bill.generate_failed_update_audit_trail @e_bill.state, 'paid'
          end

          respond_with(@e_bill)
        end

        #  1. Updates ebill
        def pay
          update
        end

        #  1. Searches ebill in the ebill as gateway query table by id passed in request
        #  2. Checks if user has permission to query ebill through api
        #  3. Assigns client transaction id from request to ebill
        #  4. Make ebill ready for audit trail by assigning it action and operator 
        #  5. Checks if ebill is valid against nil constraint 
        #  6. Prepares result from ebill in json format 
        #  7. Renders result with status ok
        #  8. If ebill is not valid, then errors in json format are rendered 
        def show
          fetch_e_bill_as_gateway_query
          authorize! :verify_for_api, @e_bill
          @e_bill.assign_attributes show_e_bill_params
          assign_attributes_to_e_bill_for_audit

          if @e_bill.valid?
            result = { bill_id: @e_bill.bill_id, payee_id: @e_bill.payee_id, 
              payee_name: @e_bill.user.username, amount: @e_bill.amount_left, 
              currency: @e_bill.currency, state: @e_bill.state, 
              short_description: @e_bill.short_description,
              accept_partial_payment: @e_bill.accept_partial_payment, minimum_amount: @e_bill.minimum_amount_left,
              amount_paid: @e_bill.amount_paid,
              ebilling_payment_id:  @e_bill.payment_id, 
              transaction_fee: @e_bill.transaction_fee.to_f,
              ps_transaction_fee: @e_bill.ps_transaction_fee.to_f,
              referent_id: nil,
              referent_transaction_fee: nil,
              charge_payer: @e_bill.user.charge_payer,
              transaction_percent: transaction_percent,
              ps_transaction_percent: ps_transaction_percent,
              referent_transaction_percent: nil
            }

            audit_trail_params = {
                :action => "showForGateway", :bill_id => @e_bill.bill_id,
                :params => result.to_json
              }
            AuditTrail.create(audit_trail_params.merge(status: 'success'))
            render json: result, status: :ok
          else
            render json: EBillResponder.new(self, [@e_bill], params).json_resource_errors, status: status_code
          end
        end

        private

        def transaction_percent
          if @e_bill.amount_paid > 0 
            (@e_bill.transaction_fee.to_f / @e_bill.amount_paid).round(3)
          else
            0.0
          end
        end

        def ps_transaction_percent
          if @e_bill.amount_paid > 0
            (@e_bill.ps_transaction_fee.to_f / @e_bill.amount_paid).round(3)
          else
            0.0
          end
        end

        def status_code
          @e_bill.error_code.present? ? @e_bill.error_code : 422
        end

         #  1. Filters ebill params with client transaction id for query request to show ebill
        def show_e_bill_params
          params.permit(:client_transaction_id).merge(:payment_system_id => current_user.id)
        end

        #  1. Filters ebill params with client transaction id and paid amount for update request 
        def update_e_bill_params
          params.permit(:client_transaction_id, :paid_amount).merge(payment_system_id: current_user.id)
        end

        #  1. Filters ebill params with following parameters for cancel request
        def cancel_e_bill_params
          params.permit(:client_transaction_id, :merchant_internal_ref, :reason)
        end

        #  1. Filters ebill params with following parameters for expire event
        def expire_e_bill_params
          params.permit(:client_transaction_id, :merchant_internal_ref, :reason)
        end

        #  1. Filters ebill params with following params for invalidating ebill
        def invalidate_e_bill_params
          params.permit(:client_transaction_id, :merchant_internal_ref, :reason)
        end

        #  1. Filters ebill params with following params for verifying ebill
        def verify_e_bill_params
          params.permit(:client_transaction_id, :payee_id, :amount)
        end

        #  1. Returns ebill from ebill as gateway pay table based on bill id 
        #  2. Raises exception if not found
        def fetch_e_bill_as_gateway_pay
          @e_bill = EBill::AsGatewayPay.find_by :bill_id => params[:id]
          raise ActiveRecord::RecordNotFound, "Could not find bill_id #{params[:id]}" unless @e_bill
        end
        
        #  1. Returns ebill from ebill as gateway query table based on bill id 
        #  2. Raises exception if not found 
        def fetch_e_bill_as_gateway_query
          @e_bill = EBill::AsGatewayQuery.find_by :bill_id => params[:id]
          raise ActiveRecord::RecordNotFound, "Could not find bill_id #{params[:id]}" unless @e_bill
        end
      end
    end
  end
end