class Api::Mobileapp::EBillsController < Api::Mobileapp::BaseController

  include ApiSupport

  #  Authenticates username and password with basic authentication
  before_action :base_authenticate, except: [:by_external_reference, :download_receipt]
  before_action :base_authenticate_for_by_external_reference, only: [:by_external_reference, :download_receipt]
  before_action :only_admin_merchant_payment_system_can_access, only: [:by_external_reference, :download_receipt]

  #  1. Returns searched ebills 
  #  2. Searches ebills of user by billid or merchant internal reference or external reference 
  #  or msisdn or state or amount
  #  3. Paginates results with 20 results per page and also returns page number. 
  def search
     ability = Ability.new(current_api_user)
     @e_bills = EBill.accessible_by(ability)

    if params[:bill_id].present?
       @e_bills = @e_bills.where(:bill_id => params[:bill_id])
    end

    if params[:merchant_internal_ref].present?
      @e_bills = @e_bills.where(:merchant_internal_ref => params[:merchant_internal_ref])
    end

    if params[:external_reference].present?
      @e_bills = @e_bills.where(:merchant_internal_ref => params[:external_reference])
    end

    if params[:payer_msisdn].present?
      @e_bills = @e_bills.where(:payer_msisdn => params[:payer_msisdn])
    end

    if params[:state].present?
      @e_bills = @e_bills.where(:state => params[:state])
    end

    if params[:amount].present?
      @e_bills = @e_bills.where(:amount => params[:amount].to_i)
    end   

    if params[:ps_transaction_id].present?
      @e_bills = @e_bills.where(:ps_transaction_id => params[:ps_transaction_id])
    end   
    
    @e_bills = @e_bills.paginate(:page => params[:page], :per_page => 20)
  end 

  def by_external_reference
    unless params[:external_reference].present? || params[:payer_id].present?
      render json: { error: I18n.t("api.errors.external_reference_mandatory") }, status: 422
      return
    end

    @e_bills = EBill.all

    if params[:external_reference].present?
      @e_bills = @e_bills.where("merchant_internal_ref = ?", params[:external_reference])
    end

    if params[:payer_id].present?
      @e_bills = @e_bills.where("payer_id = ?", params[:payer_id])
    end

    @e_bills = @e_bills.where("state in (?)", ['ready', 'partially_paid', 'overdue'])

    if params[:payer_code].present?
      @e_bills = @e_bills.where("payer_code = ?", params[:payer_code])
    end

    if params[:payee_name].present?
      @e_bills = @e_bills.joins(:user).where("users.username = ?", params[:payee_name])
    end

    if current_api_user.has_profile?(Profile::MERCHANT) 
      @e_bills = @e_bills.where("user_id = ?", current_api_user.id)
    end

  end 

  def download_receipt
    unless params[:ps_transaction_id].present?
      render json: { error: I18n.t("api.errors.ps_transaction_id_mandatory") }, status: 422
      return
    end

    @e_bills = EBill.where("ps_transaction_id = ?", params[:ps_transaction_id])
    if current_api_user.has_profile?(Profile::MERCHANT) 
      @e_bills = @e_bills.where("user_id = ?", current_api_user.id)
    end

    @e_bill = @e_bills.first

    if @e_bill.present?
      @generator = ReceiptGenerator.new(@e_bill, File.join(Rails.root, "/public/templates/receipt_template.odt"))
      @generator.generate do |pdf_filepath|
        send_data IO.binread(pdf_filepath), filename: "e_bill_receipt.pdf", type: 'application/pdf', disposition: 'attachment', status: 200
      end
    else
      render json: { error: I18n.t("api.errors.ps_transaction_id_not_found") }, status: 404
    end
  end

  def history
    @result = {}

    set_all_currencies
    set_global_counts
    set_monthly_stats
    set_top10
    set_merchants_current_month
    set_merchants_global
    
    render json: @result
  end

  def create
    authorize! :create, EBill
    @e_bill = current_user.e_bills.build create_e_bill_params    
    assign_attributes_to_e_bill_for_audit
    @e_bill.create true
    respond_with(@e_bill)
  end

  private
    def create_e_bill_params
      params.permit(:client_transaction_id, :payer_email, :payer_msisdn, :amount,
                                    :due_date, :schedule_at, :expire_at, :expiry_period, :email,
                                    :sms, :external_reference, 
                                    :additional_info, :short_description, :description,
                                    :payer_name, :payer_address, :payer_city,
                                    :accept_partial_payment, :minimum_amount)
    end

    def base_authenticate_for_by_external_reference
      authenticate_or_request_with_http_basic do |username, password|
        u = User.find_by username: username
        unless u.present?
          unauthorized
          return
        end

        if (u.has_profile?(Profile::MERCHANT) || u.has_profile?(Profile::EBILL_CREATOR))
          if u.shared_key != password
            unauthorized 
            return
          end
        elsif !u.valid_password?(password)
          unauthorized 
          return
        end

        @current_api_user = u
      end
    end

    def only_admin_merchant_payment_system_can_access
      unless current_api_user.has_profile?(Profile::MERCHANT) || 
           current_api_user.has_profile?(Profile::PAYMENT_SYSTEM) ||
           current_api_user.has_profile?(Profile::EBILL_CREATOR)
        raise CanCan::AccessDenied.new("Not authorized!", :read, EBill)   
      end
    end

    def set_all_currencies
      @result[:all_currencies] = if current_api_user.has_profile?(Profile::ADMIN)
        User.by_profile(Profile::MERCHANT).select('distinct currency').map(&:currency)
      elsif current_api_user.has_profile?(Profile::MERCHANT)
        [current_api_user.currency]
      elsif current_api_user.has_profile?(Profile::PAYMENT_SYSTEM_OPERATOR)
        payment_system_id = current_api_user.payment_system_id
        PaymentSystemMonthlyItem.joins(:payment_system_monthly_stat).
          select('distinct revenue_currency as currency').
          where("payment_system_monthly_stats.payment_system_id = ?", payment_system_id).
          map(&:currency)
      end 
    end

    def set_global_counts
      @result[:global_counts] = {}

      global_counts = if current_api_user.has_profile?(Profile::ADMIN)
        MerchantGlobalStat.total_count
      elsif current_api_user.has_profile?(Profile::MERCHANT)
        MerchantGlobalStat.total_count(merchant_id: current_api_user.id)
      end

      if global_counts.present?
        [:created_count, :ready_count, :paid_count, :partially_paid_count, :cancelled_count, 
          :failed_count, :processed_count, :expired_count, :overdue_count].each do |count_name|
          @result[:global_counts][count_name] = global_counts.send(count_name)
        end
        @result[:global_counts][:total_count] = @result[:global_counts].values.inject(0, :+)
      end

      if current_api_user.has_profile?(Profile::PAYMENT_SYSTEM_OPERATOR)
        payment_system_id = current_api_user.payment_system_id
        global_counts = PaymentSystemMonthlyItem.total_counts_for(payment_system_id)
        [:partially_paid_count, :paid_count, :processed_count].each do |count_name|
          @result[:global_counts][count_name] = global_counts.send(count_name) || 0
        end
      end

      currency_stats = if current_user.has_profile? Profile::ADMIN
        GlobalCurrencyStatsFetcher.get_global_currencies
      elsif current_user.has_profile? Profile::MERCHANT
        GlobalCurrencyStatsFetcher.get_global_currencies(merchant_id: current_user.id)
      end

      if currency_stats.present?
        [:revenues, :traffic_fees, :recurrent_fees].each do |key|
          @result[:global_counts][key] = currency_stats[key].each_with_object({}) { |(currency, money), h| h[currency] = money.to_f }
        end        
      end

      if current_api_user.has_profile?(Profile::PAYMENT_SYSTEM_OPERATOR)
        payment_system_id = current_api_user.payment_system_id
        @result[:global_counts][:revenues] = {}

        currency_stats = PaymentSystemMonthlyItem.total_revenue_for(payment_system_id)
        currency_stats.each do |stat|
          @result[:global_counts][:revenues][stat.revenue_currency] = stat.revenue.to_f
        end
      end
    end
    
    def set_monthly_stats
      date = Date.today
        
      merchant_monthly_stats = if current_user.has_profile? Profile::ADMIN
        MerchantMonthlyStat::MonthlyHistoryForAdmin.get_stats_for(date, 6)
      elsif current_user.has_profile? Profile::MERCHANT
        MerchantMonthlyStat::MonthlyHistoryForMerchant.get_stats_for(date, 6, current_user.id)
      elsif current_user.has_profile? Profile::PAYMENT_SYSTEM_OPERATOR
        payment_system_id = current_api_user.payment_system_id
        PaymentSystemMonthlyItem::AsStats.get_stats_for(date, 6, payment_system_id)
      end

      return unless merchant_monthly_stats.present?

      @result[:monthly_history] = []
      merchant_monthly_stats.each do |stat|
        total_count = stat.respond_to?(:created_count) ? stat.created_count : (stat.paid_count + stat.partially_paid_count)
        stat_result = { year: stat.year, month: stat.month, 
          total_count: total_count, paid_count: stat.paid_count, partially_paid_count: stat.partially_paid_count,
          processed_count: stat.processed_count,
          revenues: stat.revenues.each_with_object({}) { |(currency, money), h| h[currency] = money.to_f }
        }

        # only merchant and admin profile has traffic_fees and recurrent_fees
        if stat.respond_to? :traffic_fees
          stat_result.merge!({
              traffic_fees: stat.traffic_fees.each_with_object({}) { |(currency, money), h| h[currency] = money.to_f },
              recurrent_fees: stat.recurrent_fees.each_with_object({}) { |(currency, money), h| h[currency] = money.to_f }   
            })
        end
        @result[:monthly_history] << stat_result
      end
    end

    def set_top10
      if current_user.has_profile? Profile::ADMIN
        @result[:top10] = {}

        top10 = MerchantGlobalStat.top10
        @result[:top10][:total] = top10.map do |stat|
          { 
            merchant_name: stat.merchant.username, 
            count: stat.count
          }
        end
        top10_by_paid = MerchantGlobalStat.top10_paid
        @result[:top10][:paid] = top10_by_paid.map do |stat|
          { 
            merchant_name: stat.merchant.username, 
            count: stat.paid_count
          }
        end
      elsif current_user.has_profile? Profile::PAYMENT_SYSTEM_OPERATOR
        @result[:top10] = {}
        payment_system_id = current_user.payment_system_id
        top10_by_paid = PaymentSystemMonthlyItem::AsStats.top10_by_paid(payment_system_id)
        @result[:top10][:paid] = top10_by_paid.map do |stat|
          { 
            merchant_name: stat.merchant_name, 
            count: stat.paid_count
          }
        end
      end
    end

    def set_merchants_current_month
      date = Date.today
      all_merchants_current_month = if current_user.has_profile? Profile::ADMIN
        MerchantMonthlyStat.joins(:merchant).where('year = ? and month = ?', date.year, date.month)
      elsif current_user.has_profile? Profile::MERCHANT
        MerchantMonthlyStat.joins(:merchant).where('year = ? and month = ? and merchant_id = ?', date.year, date.month, current_user.id)  
      elsif current_user.has_profile? Profile::PAYMENT_SYSTEM_OPERATOR
        payment_system_id = current_user.payment_system_id
        PaymentSystemMonthlyItem::AsStats.joins(:merchant, :payment_system_monthly_stat).
          where('year = ? and month = ? and payment_system_monthly_stats.payment_system_id = ?', date.year, date.month, payment_system_id)
      end
      return unless all_merchants_current_month.present?

      @result[:merchants_current_month] = all_merchants_current_month.map do |stat|
        {
          merchant_name: stat.merchant.username,
          partially_paid_count: stat.partially_paid_count,
          paid_count: stat.paid_count,
          processed_count: stat.processed_count,
          revenue: stat.revenue.to_f,
          currency: stat.revenue_currency
        }
      end
    end

    def set_merchants_global
      merchants_global = if current_user.has_profile? Profile::ADMIN
        MerchantMonthlyStat.select_sum('merchant_id').group("merchant_id")
      elsif current_user.has_profile? Profile::MERCHANT
        MerchantMonthlyStat.select_sum('merchant_id').where("merchant_id = ?", current_user.id).group("merchant_id")
      elsif current_user.has_profile? Profile::PAYMENT_SYSTEM_OPERATOR
        payment_system_id = current_user.payment_system_id
        PaymentSystemMonthlyItem::AsStats.joins(:payment_system_monthly_stat).
          select('sum(partially_paid_count) as partially_paid_count, sum(paid_count) as paid_count, sum(processed_count) as processed_count, sum(revenue_centimes) as revenue_centimes, merchant_id').
          where("payment_system_monthly_stats.payment_system_id = ?", payment_system_id).
          group("merchant_id")
      end

      return unless merchants_global.present?

      @result[:merchants_global] = merchants_global.map do |stat|
        {
          merchant_name: stat.merchant.username,
          partially_paid_count: stat.partially_paid_count,
          paid_count: stat.paid_count,
          processed_count: stat.processed_count,
          revenue: Money.new(stat.revenue_centimes, stat.merchant.currency).to_f,
          currency: stat.merchant.currency
        }
      end
    end
end
