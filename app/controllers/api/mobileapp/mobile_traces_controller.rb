#  1. The class can create mobile trace
class Api::Mobileapp::MobileTracesController < Api::Mobileapp::BaseController

  #  1. The parameters received for stack trace are DEVICE_ID and STACKTRACE
  #  2. This line assigns content of parameters to the lowercase param names before creating the mobile trace in database
  before_action :set_upcase_params

  #  1. Ensures that imei of mobile exists in the mobiles table of database before creating the trace.
  before_action :ensure_mobile_exists

  #  Explanation : 
  #  class UsersController < ApplicationController
  #   wrap_parameters format: [:json, :xml]
  #  end

  #  If you enable ParamsWrapper for :json format, instead of having to send JSON parameters like this:
  #  {"user": {"name": "Konata"}}

  #  You can send parameters like this:
  #  {"name": "Konata"}
  #  And it will be wrapped into a nested hash with the key name matching the controller's name. For example, if you're posting to UsersController, your new params hash will look like this:
  #  {"name" => "Konata", "user" => {"name" => "Konata"}}
  
  #  You can also specify the key in which the parameters should be wrapped to, and also the list of attributes it should wrap by using either :include or :exclude options
  wrap_parameters :mobile_trace, format: [:json], include: [:device_id, :stack_trace, :DEVICE_ID, :STACK_TRACE]

  #  1. Creates new mobile stack trace with parameters passed 
  #  2. Returns saved mobile trace, status as created if trace saved successfully in database.
  #  3. Redirects to show mobile traces
  #  4. What is invalid resource ? 
  def create
    @mobile_trace = MobileTrace.new(mobile_trace_params)
    if @mobile_trace.save
      respond_with(@mobile_trace, status: :created, template: 'api/mobileapp/mobile_traces/show')
    else
      invalid_resource!(@mobile_trace)
    end
  end

  private
  
    #  1. Filters device_id and stack_trace params from received params in the request
    def mobile_trace_params
      params.require(:mobile_trace).permit(:device_id, :stack_trace)
    end

    #  1. Converts param names from Upper case to lower case for consistency.
    #  2. Converts devise id value to string because it is received as big number which can not be held in ruby data types 
    def set_upcase_params
      params[:mobile_trace][:device_id] = params[:mobile_trace][:DEVICE_ID].to_s if params[:mobile_trace][:DEVICE_ID].present?
      params[:mobile_trace][:stack_trace] = params[:mobile_trace][:STACK_TRACE] if params[:mobile_trace][:STACK_TRACE].present?
    end

    #  1. Searches imei in mobiles table which is received in request 
    #  2. Returns not_found message if search returns nothing
    def ensure_mobile_exists
      unless Mobile.find_by imei: params[:mobile_trace][:device_id]
        not_found
      end
    end
end
