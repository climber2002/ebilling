class Api::Mobileapp::MobilesController < Api::Mobileapp::BaseController

  #  1. Does basic authentication before every method which is required
  before_action :base_authenticate

  #  1. Wraps parameters into json format and assigns that as value of controller name as key resulting in hash
  #  2. Can include one or more params out of : imei, otp and pin 
  wrap_parameters :mobile, format: [:json], include: [:imei, :otp, :pin]

  #  1. Creates record of mobile in the table mobiles as register from imei received in request  
  #  2. Why user_id is required ? May be for linking record with users? 
  #  3. Returns invalid message with status code for create
  def create
    @mobile = Mobile::AsRegister.new(create_mobile_params.merge(user_id: current_api_user.id))
    if @mobile.save
      respond_with(@mobile, status: :created, template: 'api/mobileapp/mobiles/show')
    else
      invalid_resource!(@mobile, status_code_for_create)
    end
  end

  #  1. Returns mobiles of currenct user
  def index
    @mobiles = current_api_user.mobiles
  end

  #  1. User id from basic authentication and imei in the request are used to find mobile record from mobiles as update pin table 
  #  2. If the mobile confirmed field in the record found is false, unauthorized error is shown
  #  3. Otherwise mobile record is updated with params in the request.
  #  4. Returns created mobile record with status ok , if record is updated successfully
  #  5. Redirects to show mobile records 
  def update
    @mobile = Mobile::AsUpdatePin.find_by! user_id: current_api_user.id, imei: params[:id]
    unauthorized unless @mobile.confirmed?
    if @mobile.update_attributes(update_mobile_params)
      respond_with(@mobile, status: :ok, template: 'api/mobileapp/mobiles/show')
    else
      invalid_resource!(@mobile)
    end
  end

  #  1. Imei from request is required to search mobile record to be deleted.
  #  2. Deletes record which is found
  #  3. Exception is not handled if record is not found ?
  #  4. Returns record with status ok 
  #  5. Redirects to show mobiles 
  def destroy
    @mobile = Mobile.find_by!(imei: params[:id])
    @mobile.destroy
    respond_with(@mobile, status: :ok, template: 'api/mobileapp/mobiles/show')
  end

  #  1. Record of mobile for received imei and user id is fetched from mobiles as reset pin table (What is AsResetPin)
  #  2. Reset pin is invloked on that record
  #  3. Returns record with status Ok (Is this needed ? )
  #  4. Redirects to show mobiles 
  #  5. exception not handled
  def forget_pin
    @mobile = Mobile::AsResetPin.find_by! user_id: current_api_user.id, imei: params[:id]
    @mobile.reset_otp
    respond_with(@mobile, status: :ok, template: 'api/mobileapp/mobiles/show')
  end

  #  1. Record of mobile for received imei and user id is fetched from mobiles as verify table (What is AsVerify? )
  #  2. Record is updated with received params which have confirmation otp
  #  3. If record updation is successful, it returns record with status ok 
  #  4. Redirects to show mobiles
  def verify
    @mobile = Mobile::AsVerify.find_by! user_id: current_api_user.id, imei: params[:id]
    if @mobile.update_attributes(verify_mobile_params)
      respond_with(@mobile, status: :ok, template: 'api/mobileapp/mobiles/show')
    else
      invalid_resource!(@mobile)
    end
  end

  private

    #  1. Filters received params to get imei param and returns it 
    def create_mobile_params
      params.require(:mobile).permit(:imei)
    end

    #  1. Adds "imei is invalid " message to errors array
    #  2. Iterates on each error.
    #  3. If error has string "already been taken", and if the imei belongs to current user, 423 is returned.
    #  4. If imei does not belong to current user, 422 is returned
    #  5. Else 499 is returned for unknown error
    def status_code_for_create
      errors = @mobile.errors[:imei]
      errors.each do |error|
        if error.include?('already been taken')
          if current_api_user.mobiles.find_by_imei(params[:imei])
            return 423
          else
            return 422
          end
        end
      end
      return 499
    end

    #  1. While verifying mobile, if the param mobile exists, otp is copied to otp_confirmation param
    #  2. Filters and returns otp_confirmation param from received request
    def verify_mobile_params
      if params[:mobile]
        params[:mobile][:otp_confirmation] = params[:mobile][:otp]
      end

      params.require(:mobile).permit(:otp_confirmation)
    end
    
    #  1. Filters and returns pin param from request
    def update_mobile_params
      params.require(:mobile).permit(:pin)
    end
end