class Api::Mobileapp::BaseController < ActionController::Base

  #  1. Parameters wraped to json format
  wrap_parameters format: [:json]

  #  1. Responds to only json format requests 
  respond_to :json

  private

    #  1.  Renders invalid resource with passed resource and status
    def invalid_resource!(resource, status=:unprocessable_entity)
      @resource = resource
      render "api/errors/invalid_resource", :status => status
    end

    #  1. Renders unauthorised error with status 401
    def unauthorized
      render "api/errors/unauthorized", :status => 401 and return
    end

    #  1. Renders not found error with status 404
    def not_found
      render "api/errors/not_found", :status => 404 and return
    end

    #  1. Renders invalid login error with status 401
    def invalid_login
      render "api/errors/invalid_login", :status => 401 and return
    end

    #  1. Returns api_token from request header or from request params 
    def api_token
      request.headers["X-Xhoppe-Token"] || params[:token]
    end
    helper_method :api_token

    #  1. Checks if api token is blank. If it is unauthorised error is raised
    #  2. If it is not, then user is fetched based on token from users table
    #  3. Raises unauthorised error if user fetched is not present or sms is not confirmed on that user
    def authenticate_user
      if api_token.blank?
        unauthorized
      else
        @current_api_user = User.find_by(authentication_token: api_token)
        unauthorized unless @current_api_user.present? && @current_api_user.confirmed_sms?
      end
    end

    #  1. Username and password are obtained from http basic authentication params 
    #  2. Fetches user by username
    #  3. Raises unauthorised if user is not present or password is not matched
    #  4. User is assigned to current_api_user
    def base_authenticate
      authenticate_or_request_with_http_basic do |username, password|
        u = User.find_by username: username
        unless u.present?
          unauthorized
          return
        end
        unauthorized unless u.valid_password? password
        @current_api_user = u
      end
    end
    
    #  1. Returns current api user
    def current_api_user
      @current_api_user
    end
    helper_method :current_api_user
end