class SharedKeysController < ApplicationController

  def update
    load_user
    @user.generate_shared_key
    @user.save
  end

  private 

    def load_user
      if current_user.has_profile?(Profile::ADMIN) && params[:user_id].present?
        @user = User.find(params[:user_id])
      else
        @user = current_user
      end
    end

end
