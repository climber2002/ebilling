class CurrenciesController < ApplicationController

  before_action :authenticate_user!
  before_action :check_authorization

  def index
    @currencies = Currency.all.order('code')
  end

  def new
    @currency = Currency.new
  end

  def create
    @currency = Currency.new(currency_params)
    if @currency.save
      flash[:notice] = "Currency #{@currency.code} is created"
      redirect_to currencies_url
    else
      render 'new'
    end
  end
  
  private

    def currency_params
      params.require(:currency).permit(:code)
    end

    def check_authorization
      authorize! :manage, Currency
    end
end
