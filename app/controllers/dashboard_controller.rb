class DashboardController < ApplicationController

  #  1. Authenticates user before every action in this controller 
  before_filter :authenticate_user!

  #  1. Gets the latest updated record from merchant global stat table.
  #  2. Sets global counts , monthly stats mechants of current month, global merchants
  #  3. If current user is admin then top 10 global monthly stats and top 10 paid gloabl monthly stats are fetched
  def index
    # for timestamp
    # @total_count =  EBill.accessible_by(current_ability).all.count
    # @created_count = EBill.accessible_by(current_ability).created.count
    # @ready_count = EBill.accessible_by(current_ability).ready.count
    # @overdue_count = EBill.accessible_by(current_ability).overdue.count
    # @paid_count = EBill.accessible_by(current_ability).paid.count
    # @cancelled_count = EBill.accessible_by(current_ability).cancelled.count
    # @expired_count = EBill.accessible_by(current_ability).expired.count
    # @processed_count = EBill.accessible_by(current_ability).processed.count
    # @partially_paid_count = EBill.accessible_by(current_ability).partially_paid.count
    
    @stat = MerchantGlobalStat.order("updated_at desc").first

    set_global_counts
    set_monthly_stats
    set_all_merchants_current_month
    set_all_merchants_global

    if current_user.has_profile? Profile::ADMIN
      @top10 = MerchantGlobalStat.top10
      @top10_by_paid = MerchantGlobalStat.top10_paid
    elsif current_user.has_profile? Profile::PAYMENT_SYSTEM_OPERATOR
      payment_system_id = current_user.payment_system_id
      @top10_by_paid = PaymentSystemMonthlyItem::AsStats.top10_by_paid(payment_system_id)
      @top10_by_transaction_fee = PaymentSystemMonthlyItem::AsStats.top10_by_transaction_fee(payment_system_id)
    end
  end

  private

    #  1. Sets all the counts from merchant global stat table 
    #  2. If profile is admin, sum of all users is shown, else counts related to specific merchant is shown  
    #  3. If count is present, it is shown. Else 0 is shown.
    def set_global_counts
      @total_count = 0
      @created_count = 0
      @ready_count = 0
      @overdue_count = 0
      @paid_count = 0
      @cancelled_count = 0
      @expired_count = 0
      @processed_count = 0
      @partially_paid_count = 0
      count = if current_user.has_profile? Profile::ADMIN
        MerchantGlobalStat.total_count
      elsif current_user.has_profile? Profile::MERCHANT
        MerchantGlobalStat.find_by merchant_id: current_user.id
      elsif current_user.has_profile? Profile::PAYMENT_SYSTEM_OPERATOR
        if current_user.has_profile?(Profile::PAYMENT_SYSTEM_OPERATOR)
          payment_system_id = current_user.payment_system_id
          PaymentSystemMonthlyItem.total_counts_for(payment_system_id)
        end
      end

      if count.present?
        @paid_count = count.paid_count || 0
        @processed_count = count.processed_count || 0
        @partially_paid_count = count.partially_paid_count || 0

        if current_user.has_profile?(Profile::ADMIN) || current_user.has_profile?(Profile::MERCHANT)
          @created_count = count.created_count || 0
          @ready_count = count.ready_count || 0
          @cancelled_count = count.cancelled_count || 0
          @expired_count = count.expired_count || 0
          @overdue_count = count.overdue_count || 0
          @total_count = @created_count + @ready_count +
            @paid_count + @cancelled_count + @expired_count +
            @processed_count + @partially_paid_count
        end
      end
    end

    #  1. If param year is not nil , do nothing 
    #  2. If it is nil, then
    #  3. Fetches current date
    #  4. Puts date in an array called year_month
    #  5. Puts dates of last eleven months correspoing to today's date in the year_month array.
    #  6. Creates a map of year and month from every date in year_month and stores in year_month
    #  7. What does select_sum do? 
    #  8. If current profile is admin, then monthly stats contains sum of counts from merchant monhtly stat table for each user
    #  9. If the profile is merchant, then monthly stats are selected by merchant id.
    def set_monthly_stats
      if params[:year].present?
      else
        date = Date.today
        
        @merchant_monthly_stats = if current_user.has_profile? Profile::ADMIN
          MerchantMonthlyStat::MonthlyHistoryForAdmin.get_stats_for(date, 24)
        elsif current_user.has_profile? Profile::MERCHANT
          MerchantMonthlyStat::MonthlyHistoryForMerchant.get_stats_for(date, 24, current_user.id)
        elsif current_user.has_profile? Profile::PAYMENT_SYSTEM_OPERATOR
          payment_system_id = current_user.payment_system_id
          PaymentSystemMonthlyItem::AsStats.get_stats_for(date, 24, payment_system_id)
        end
      end
    end


    #  1. If profile is admin, the current month merchant stats are collected 
    def set_all_merchants_current_month
      date = Date.today
      @all_merchants_current_month = if current_user.has_profile? Profile::ADMIN
        MerchantMonthlyStat.joins(:merchant).where('year = ? and month = ?', date.year, date.month)
      elsif current_user.has_profile? Profile::MERCHANT
        MerchantMonthlyStat.joins(:merchant).where('year = ? and month = ? and merchant_id = ?', date.year, date.month, current_user.id)  
      elsif current_user.has_profile? Profile::PAYMENT_SYSTEM_OPERATOR
        payment_system_id = current_user.payment_system_id
        PaymentSystemMonthlyItem::AsStats.joins(:merchant, :payment_system_monthly_stat).
          where('year = ? and month = ? and payment_system_monthly_stats.payment_system_id = ?', date.year, date.month, payment_system_id)
      end
    end

    
    # 1. If profile is admin, select sum gathers all counts and sums them for individual user and groups them according to merchant id.
    def set_all_merchants_global
      @all_merchants_global = if current_user.has_profile? Profile::ADMIN
        MerchantMonthlyStat.select_sum('merchant_id').group("merchant_id")
      elsif current_user.has_profile? Profile::MERCHANT
        MerchantMonthlyStat.select_sum('merchant_id').where("merchant_id = ?", current_user.id).group("merchant_id")
      elsif current_user.has_profile? Profile::PAYMENT_SYSTEM_OPERATOR
        payment_system_id = current_user.payment_system_id
        PaymentSystemMonthlyItem::AsStats.joins(:payment_system_monthly_stat).
          select('sum(partially_paid_count) as partially_paid_count, sum(paid_count) as paid_count, sum(processed_count) as processed_count, sum(revenue_centimes) as revenue_centimes, sum(transaction_fee_centimes) as transaction_fee_centimes, merchant_id').
          where("payment_system_monthly_stats.payment_system_id = ?", payment_system_id).
          group("merchant_id")
      end
    end
end
