class MonthlyStatsController < ApplicationController

  #  1. Authenticates user before every action in this controller
  before_filter :authenticate_user!


  #  1. Gets year and month params from the request
  #  2. Initiates counts to zero
  #  3. Gets the array of all counts corresding to year and month for current user if they are either admin or merchant
  #  4. If the array exists, all values are assigned to respective variables
  def index
    @year = params[:year] 
    @month = params[:month]

    @total_count = 0
    @created_count = 0
    @ready_count = 0
    @overdue_count = 0
    @paid_count = 0
    @cancelled_count = 0
    @expired_count = 0
    @processed_count = 0
    @partially_paid_count = 0

    count = if current_user.has_profile? Profile::ADMIN
      MerchantMonthlyStat.total_count_for(@year, @month)
    elsif current_user.has_profile? Profile::MERCHANT
      MerchantMonthlyStat.find_by merchant_id: current_user.id, year: @year, month: @month
    end

    if count.present?
      @created_count = count.created_count || 0
      @ready_count = count.ready_count || 0
      @paid_count = count.paid_count || 0
      @cancelled_count = count.cancelled_count || 0
      @expired_count = count.expired_count || 0
      @processed_count = count.processed_count || 0
      @partially_paid_count = count.partially_paid_count || 0
      @total_count = @created_count + @ready_count +
        @paid_count + @cancelled_count + @expired_count +
        @processed_count + @partially_paid_count
    end
  end
end
