class CountriesController < ApplicationController

  before_filter :authenticate_user!

  def index
    authorize! :read, Country

    @countries = Country.all.order("name")
  end

  def new
    authorize! :create, Country
    @country = Country.new
  end

  def create
    authorize! :create, Country
    @country = Country.new(country_params)
    if @country.save
      redirect_to countries_url
    else
      render :new
    end
  end

  def destroy
    authorize! :create, Country
    @country = Country.find(params[:id])
    @country.destroy
    redirect_to countries_url
  end
  
  private

    def country_params
      params.require(:country).permit(:name, :currency_name, :currency_code)
    end
end
