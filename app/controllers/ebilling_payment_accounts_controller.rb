class EbillingPaymentAccountsController < ApplicationController

  before_action :authenticate_user!
  before_action :check_authorization

  def index
    @ebilling_payment_accounts = EbillingPaymentAccount.includes(:payment_system).all
  end

  def new
    @ebilling_payment_account = EbillingPaymentAccount.new
  end

  def create
    @ebilling_payment_account = EbillingPaymentAccount.new(ebilling_payment_account_params)
    if @ebilling_payment_account.save
      flash[:notice] = "EBilling Payment Account created for #{@ebilling_payment_account.payment_system.username}"
      redirect_to ebilling_payment_accounts_url
    else
      render 'new'
    end
  end

  def edit
    @ebilling_payment_account = EbillingPaymentAccount.find params[:id]
  end

  def update
    @ebilling_payment_account = EbillingPaymentAccount.find params[:id]
    if @ebilling_payment_account.update_attributes(ebilling_payment_account_params)
      flash[:notice] = "EBilling Payment Account created for #{@ebilling_payment_account.payment_system.username}"
      redirect_to ebilling_payment_accounts_url
    else
      render 'edit'
    end
  end

  def destroy
    @ebilling_payment_account = EbillingPaymentAccount.find params[:id]
    @ebilling_payment_account.destroy
    flash[:notice] = "EBilling Payment Account deleted for #{@ebilling_payment_account.payment_system.username}"
    redirect_to ebilling_payment_accounts_url
  end

  private

    def ebilling_payment_account_params
      params.require(:ebilling_payment_account).permit(:payment_system_id, :payment_id)
    end

    def check_authorization
      authorize! :manage, EbillingPaymentAccount
    end
end
