class BatchFilesController < ApplicationController

  before_filter :authenticate_user!
  #  1. Creates a new batch file model
  def new
    @batch_file = BatchFile.new
  end

  #  1. Creates a new batch files model with params filtered
  #  2. Saves batch file model in database 
  #  3. If save is successful, it creates a delay job to call action provision
  #  4. Flashes a notice which says that the file is provisioned in background
  #  5. Redirects to batch files view with html format
  #  6. If save fails, then new is rendered
  def create
    @batch_file = current_user.batch_files.new create_batch_file_params

    if @batch_file.save
      @batch_file.delay.provision
      flash[:notice] = I18n.t("batch_files.notice.provisioned_in_background")
      respond_to do |format|
        format.html { redirect_to batch_files_path }
        format.js
      end
    else
      respond_to do |format|
        format.html { render 'new' }
        format.js
      end

    end
  end

  #  1. Fetches all batch files accessible by current user and pagintes them 20 per page
  #  2. Returns those records with page number requested
  def index
    @batch_files = BatchFile.order("created_at desc")
                      .accessible_by(current_ability)
                      .paginate(:page => params[:page], :per_page => 10)
  end

  private
  
  #  1. Filters request params to have only file parameter
  def create_batch_file_params
    params.require(:batch_file).permit(:file)
  end

end
