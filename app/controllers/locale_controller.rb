class LocaleController < ApplicationController

  #  1. Saves locale from request to cookies 
  def update
    cookies[:locale] = params["locale"] if params["locale"].present?
    redirect_to root_path
  end

end
