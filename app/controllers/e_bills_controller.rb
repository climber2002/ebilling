class EBillsController < ApplicationController

  KEPT_PARAMS_IN_INDEX = [:by_state, :by_user_id, :overdue]

  #  1. Hash of actions for states
  STATE_TO_STATE_EVENTS = ActiveSupport::HashWithIndifferentAccess.new paid: 'pay',
                              failed: 'invalidate', expired: 'expire', cancelled: 'cancel'

  #  1. For update method authenticity token is ignored
  skip_before_filter :verify_authenticity_token, only: [:update]

  #  1. Authenticates user 
  before_filter :authenticate_user!

  #  1. Authorises user has permission to create ebill
  #  2. Creates a new model for ebill with currency XAF and returns it
  def new
    authorize! :create, EBill
    @e_bill = EBill.new currency: "XAF"
  end


  #  1. Authorises if user has permission to create new ebill
  #  2. If current profile is admin, user_id is deleted from params 
  #  3. Else assigns current_user value to user 
  #  4. Creates new ebill model for user with filtered params 
  #  5. Assigns operator to ebill
  #  6. Saves ebill 
  #  7. Checks if ebill saved is ready, if it is , then accordingly notice is shown
  #  8. Else only shows created notice.  
  #  9. Redirects to show created ebill
  #  10. If ebill is not saved then new is rendered
  def create
    authorize! :create, EBill
    if current_user.has_profile? Profile::ADMIN
      user = User.find params[:e_bill].delete :user_id
    else
      user = current_user
    end

    @e_bill = user.e_bills.new create_e_bill_params
    @e_bill.operator = current_user.username
    if @e_bill.create
      if @e_bill.ready?
        flash[:notice] = I18n.t("e_bills.notice.created_and_sent")
      else
        flash[:notice] = I18n.t("e_bills.notice.created")
      end

      redirect_to @e_bill
    else
      render 'new'
    end
  end

  #  1. Searches ebill by id param
  #  2. Checks if user has permission to process the ebill
  #  3. Process ebill if it can with permission
  #  4. Else shows message that it can't process the ebill
  #  5. Enqueues the delayed job for sending notification for that ebill 
  #  6. Redirects to show processed ebill
  def proces
    @e_bill = EBill.find params[:id]
    authorize! :process, @e_bill
    if @e_bill.can_process?
      @e_bill.process
    else
      flash[:notice] = I18n.t("e_bills.notice.cant_process")
    end
    redirect_to @e_bill
  end

  #  1. Fetches ebill by id from e_bills table
  #  2. Checks if user has permission to read ebill
  def show
    @e_bill = EBill.find params[:id]
    authorize! :read, @e_bill
  end

  #  1. Checks if user has permission to read ebill
  #  2. Retrieves ebills accessible by current user 
  #  3. Searches those ebills by state or by query or by user id or batch file id or overdue status 
  #  4. Paginates the resulted ebill with 20 per page
  #  5. Returns the ebills with page number
  def index
    authorize! :read, EBill
    @e_bills = EBill.includes(:user).accessible_by(current_ability)

    if params[:by_state]
      @by_state = params[:by_state]
      @e_bills = @e_bills.by_state(params[:by_state])
    end

    if params[:query]
      @query = params[:query]
      @e_bills = @e_bills.search(@query)
    end

    if params[:by_user_id]
      @by_user = User.find params[:by_user_id]
      @e_bills = @e_bills.by_user_id params[:by_user_id]
    end

    if params[:batch_file_id]
      @e_bills = @e_bills.batch_file_id params[:batch_file_id]
    end

    if params[:overdue]
      @overdue = "true"
      @e_bills = @e_bills.overdue
    end

    @kept_params = {}
    KEPT_PARAMS_IN_INDEX.each do |param_key|
      @kept_params[param_key] = params[param_key] unless params[param_key].blank?
    end

    @e_bills = @e_bills.order("id desc").paginate(:page => params[:page], :per_page => 20)
  end

  def edit
    @e_bill = EBill.find params[:id]
    @state_event = params[:state_event]
  end

  def export
    @e_bill = EBill.find params[:id]
    authorize! :read, @e_bill

    dir = Dir.mktmpdir
    exporter = PdfExporter.new(@e_bill)
    exporter.export do |pdf_path|
      dest = File.join(dir, "#{@e_bill.bill_id}_#{exporter.generate_type}.pdf")
      FileUtils.cp pdf_path, dest
      send_file(dest, disposition: 'attachment')
    end
  end

  #  1. Finds wbill by id 
  #  2. Checks if user has permission to update ebill
  #  3. Deletes state_event from params and stores that state event
  #  4. If the state event is nil, state is also deleted from params and stored that state in variable
  #  5. Gets state_event from state
  #  6. Updates ebill with filtered params from request for update action
  #  7. If state event exists, the state is set for ebill
  #  8. If state can not be set , message is shown and edit page is rendered
  #  9. Saves ebill, if succesful, shows ebill page or shows error page
  def update
    @e_bill = EBill.find params[:id]
    authorize! :update, @e_bill

    @state_event = params[:e_bill].delete :state_event
    if @state_event.nil?
      state = params[:e_bill].delete :state
      if state
        @state_event = STATE_TO_STATE_EVENTS[state.downcase]
      end
    end

    @e_bill.update_attributes update_e_bill_params
    if @state_event
      if @e_bill.send("can_#{@state_event}?")
        @e_bill.send(@state_event, false)
      else
        respond_to do |format|
          format.html do
            flash[:alert] = I18n.t("e_bill.alert.cant_set_state")
            render 'edit'
          end
          format.json { render json: { errors: "can't set the state" } }
        end
        return
      end
    end

    if @e_bill.save
      respond_to do |format|
        format.html { redirect_to @e_bill }
        format.json { render json: @e_bill }
      end
    else
      respond_to do |format|
        format.html { render 'edit' }
        format.json { render json: { errors: @e_bill.errors.full_messages } }
      end

    end
  end

  #  1. Finds ebill by id
  #  2. Retrieves state event from request
  #  3. Checks if user has permission to perform action stated in state event on ebill
  def pre_trigger
    @e_bill = EBill.find params[:id]
    @e_bill.state_event = params[:state_event]
    authorize! @e_bill.state_event.to_sym, @e_bill
  end

  #  1. Finds ebill by id 
  #  2. Retrieves state event from request
  #  3. Checks if user has permission to perform action stated in state event on ebill
  #  4. Enqueues the delayed job to assign state event as action to the ebill for current user 
  #  5. Notices about it
  #  6. Redirects to show the ebill
  def trigger
    @e_bill = EBill.find params[:id]
    @state_event = params[:state_event]

    authorize! @state_event.to_sym, @e_bill

    Delayed::Job.enqueue PerformEBillJob.new(@state_event, @e_bill.id, current_user.username)
    flash[:notice] = I18n.t("e_bills.notice.sent_in_background")
    redirect_to @e_bill
  end

  #  1. Sends receipt with delayed job
  #  2. Flashes notice for the same 
  def send_receipt
    Delayed::Job.enqueue DeliverReceiptJob.new(params[:id])
    flash[:notice] = I18n.t("e_bills.notice.receipt_sent_in_background")
  end

  #  1. Sends invoice with delayed job
  #  2. Flashes notice for the same 
  def send_invoice
    Delayed::Job.enqueue DeliverInvoiceJob.new(params[:id])
    flash[:notice] = I18n.t("e_bills.notice.invoice_sent_in_background")
  end

  private

  #  1. Filters request params for create action
  def create_e_bill_params
    params.require(:e_bill).permit(:payer_email, :payer_msisdn, :amount,
                                  :due_date, :expired_at, :expiry_period, :schedule_at, :expire_at, :email,
                                  :sms, :merchant_internal_ref,
                                  :additional_info, :short_description, :description,
                                  :payer_name, :payer_address, :payer_city, 
                                  :accept_partial_payment, :minimum_amount,
                                  :payer_id, :payer_code, :data0, :data1, :data2,
                                  :data3, :data4, :data5, :data6, :data7, 
                                  :data8, :data9)
  end

  #  1. Filters request params for trigger action
  def trigger_e_bill_params
    params.require(:e_bill).permit(:client_transaction_id, :sms_message)
  end

  #  1. Filters request params for update action
  def update_e_bill_params
    params[:e_bill].permit(:client_transaction_id, :state_event, :reason,
      :merchant_internal_ref, :external_reference, :failure_reason)
  end
end
