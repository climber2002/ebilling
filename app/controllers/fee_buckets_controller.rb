class FeeBucketsController < ApplicationController

  before_action :authenticate_user!
  before_action :check_authorization
  before_action :load_currency

  def new
    @fee_bucket = @currency.fee_buckets.build
  end

  def create
    @fee_bucket = @currency.fee_buckets.build(fee_bucket_params)
    if @fee_bucket.save
      flash[:notice] = "Fee bucket created for Currency #{@currency.code}"
      redirect_to currencies_url
    else
      render 'new'
    end
  end

  def edit
    @fee_bucket = @currency.fee_buckets.find params[:id]
  end

  def update
    @fee_bucket = @currency.fee_buckets.find params[:id]
    if @fee_bucket.update_attributes(fee_bucket_params)
      flash[:notice] = "Fee bucket updated for Currency #{@currency.code}"
      redirect_to currencies_url
    else
      render 'edit'
    end
  end

  def destroy
    @fee_bucket = @currency.fee_buckets.find params[:id]
    @fee_bucket.destroy
    flash[:notice] = "A fee bucket deleted for Currency #{@currency.code}"
    redirect_to currencies_url
  end

  private

    def fee_bucket_params
      params.require(:fee_bucket).permit(:min_transactions, :max_transactions, :fee_value)
    end

    def load_currency
      @currency = Currency.find(params[:currency_id])
    end

    def check_authorization
      authorize! :manage, Currency
    end
end
