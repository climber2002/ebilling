class MerchantPaymentSystemMappingsController < ApplicationController

  #  1. Authenticates user before every action in this controller
  before_filter :authenticate_user!

  #  1. Retrieves and returns all mappings with params merchant and payment system
  def index
    @query = params[:query]
    @merchant_payment_system_mappings = MerchantPaymentSystemMapping.includes([:merchant, :payment_system])
    @merchant_payment_system_mappings = @merchant_payment_system_mappings.search(@query) unless @query.blank?
  end

  #  1. Checks if user has permission to create mapping
  #  2. Returns empty new model for mapping
  def new
    authorize! :create, MerchantPaymentSystemMapping
    @merchant_payment_system_mapping = MerchantPaymentSystemMapping.new
  end

  #  1. Creates new mapping with filtered params 
  #  2. Checks permission to create 
  #  3. Saves the mapping
  #  4. If saved, message is shown else new page is rendered for mapping.
  def create
    @merchant_payment_system_mapping = MerchantPaymentSystemMapping.new(merchant_payment_system_mapping_params)
    authorize! :create, @merchant_payment_system_mapping


    if @merchant_payment_system_mapping.save
      flash[:notice] = "Merchant PaymentSystem mapping created. "
      redirect_to merchant_payment_system_mappings_url
    else
      render 'new'
    end
  end

  #  1. Finds mapping by id in request
  #  2. Checks if user has permission to update
  def edit
    @merchant_payment_system_mapping = MerchantPaymentSystemMapping.find(params[:id])
    authorize! :update, @merchant_payment_system_mapping
  end


  #  1. Finds mapping by id
  #  2. Checks permission to update
  #  3. Updates mapping with filtered params 
  #  4. Displays message
  #  5. Redirects to mappings url
  #  6. If mapping is not updated, redirects to edit page
  def update
    @merchant_payment_system_mapping = MerchantPaymentSystemMapping.find(params[:id])
    authorize! :update, @merchant_payment_system_mapping

    if @merchant_payment_system_mapping.update_attributes(merchant_payment_system_mapping_params)
      flash[:notice] = "Merchant PaymentSystem mapping updated. "
      redirect_to merchant_payment_system_mappings_url
    else
      render 'edit'
    end
  end


  #  1. Finds a mapping by id 
  #  2. Checks permission to delete
  #  3. Deletes the mapping 
  #  4. Shows message for the same
  #  5. Redirects to mappings url where mappings are shown
  def destroy
    @merchant_payment_system_mapping = MerchantPaymentSystemMapping.find(params[:id])
    authorize! :delete, @merchant_payment_system_mapping
    
    @merchant_payment_system_mapping.destroy
    flash[:notice] = "Merchant PaymentSystem mapping deleted. "
    redirect_to merchant_payment_system_mappings_url
  end

  private

    #  1. Filters request params the following way.
    def merchant_payment_system_mapping_params
      params.require(:merchant_payment_system_mapping).permit(:merchant_id, :payment_system_id,
        :merchant_ps_id, :ps_percentage, :ebilling_percentage)
    end
end
