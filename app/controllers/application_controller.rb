class ApplicationController < ActionController::Base

  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  #  1. Sets locale before all actions in this controller and those who extend this controller.
  before_filter :set_locale

  private

  #  1. Obtains locale from cookies if present
  def set_locale
    I18n.locale = cookies[:locale] if cookies[:locale].present?
  end

  def authenticate_user!
    super
    unless request.path.start_with?('/api') || request.path.start_with?('/soap_ebills')
      if current_user && current_user.has_profile?(Profile::PAYMENT_SYSTEM)
        flash[:alert] = "PaymentSystem profile can't access the system."
        sign_out_and_redirect(current_user)
      end
    end
  end
end
