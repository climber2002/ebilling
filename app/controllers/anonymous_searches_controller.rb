class AnonymousSearchesController < ApplicationController

  def new
    @search_type = get_search_type
  end

  def create
    @search_type = get_search_type
    if @search_type == 'by_bill_id'
      search_for_merchant
    elsif @search_type == 'by_ps_transaction_id'
      search_for_payment_system
    end
  end

  private

    def search_types
      @search_types ||= ['by_bill_id', 'by_ps_transaction_id']
    end

    def get_search_type
      search_types.include?(params[:search_type]) ? params[:search_type] : 'by_bill_id'
    end

    def search_for_merchant
      @e_bill = EBill.where("bill_id = ? and payer_msisdn = ?", params[:bill_id], params[:payer_msisdn]).first
    end

    def search_for_payment_system
      @e_bill = EBill.where('ps_transaction_id = ?', params[:ps_transaction_id]).first
    end
end
