class PaymentSystemOperatorMerchantsController < ApplicationController

  #  1. Authenticates user before every action in this controller
  before_filter :authenticate_user!

  def index
    unless current_user.has_profile? Profile::PAYMENT_SYSTEM_OPERATOR
      raise CanCan::AccessDenied.new("Not authorized!", :read, MerchantPaymentSystemMapping) 
    end

    @mappings = MerchantPaymentSystemMapping.includes([:merchant])
    if params[:query].present?
      @query = params[:query]
      @mappings = @mappings.search(@query)
    end
    @mappings = @mappings.where("merchant_payment_system_mappings.payment_system_id = ?", current_user.payment_system.id).
      paginate(:page => params[:page], :per_page => 10)
  end
end