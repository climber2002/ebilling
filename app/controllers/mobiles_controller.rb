class MobilesController < ApplicationController

  #  1. Authenticates user before every action in this controller
  before_filter :authenticate_user!

  #  1. Retrieves all mobiles accessible by current user 
  #  2. Paginates the mobiles resulted with 20 per page
  #  3. Returns the mobiles with page number
  def index
    @mobiles = Mobile.accessible_by(current_ability).includes(:user).order('id desc')
      .paginate(:page => params[:page], :per_page => 10)
  end


  #  1. Finds mobile by id in request
  #  2. Checks permission to delete
  #  3. Deletes trace
  #  4. Dsiplays message 
  #  5. Redirects to mobiles url to show mobiles
  def destroy
    @mobile = Mobile.find params[:id]
    authorize! :delete, @mobile

    @mobile.destroy
    flash[:notice] = t("mobiles.edit.deleted")
    redirect_to mobiles_url
  end

end
