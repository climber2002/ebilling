# It's named OutlineController instead of ProfileController
# because the name collision with ProfilesController
class OutlineController < ApplicationController

  #  1. Authenticates user before every action in this controller
  before_action :authenticate_user!

  #  1. Returns current user
  def show
    @user = current_user
  end

  def edit
    @user = current_user
  end

  def update
    @user = current_user
    if @user.update_attributes(update_user_params)
      redirect_to outline_url, notice: 'Profile is updated'
    else
      render 'edit'
    end
  end

  #  1. Returns current user
  def edit_password
    @user = current_user
  end


  #  1. Gets current user
  #  2. Updates password with password params filtered
  #  3. Signs in user 
  #  4. If password is blank , notice is shown. Else password updated message is shown
  #  5. Redirects to show curent user
  #  6. If update fails, redirects to edit password page
  def update_password
    @user = current_user
    if(@user.update_with_password(update_password_params))
      sign_in(@user, bypass: true)
      if(params.require(:user)[:password].blank?)
        flash[:notice] = "Password is not updated"
      else
        flash[:notice] = "Password updated successfully"
      end
      redirect_to action: :show
    else
      render 'edit_password'
    end
  end

  private

    #  1. Filters params for updating password
    def update_password_params
      params.require(:user).permit(:current_password, :password, :password_confirmation)
    end

    #  1. If current user is Usermanager, profile ids are included in filtered params for udate action
    #  2. Else the profile ids are not included.
    def update_user_params
      params.require(:user).permit(:first_name,
          :last_name, :email, :msisdn, :city, :address, :zipcode, :notification_url, :notification_post, 
          :greeting_message, :tax_rate, :legal_info, :email_notification, :website, :logo, :logo_cache, :currency,
          :signature, :signature_cache, :additional_notification_params, :notification_params => [])
      
    end
end
