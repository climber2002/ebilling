class UsersController < ApplicationController

  #  1. Authenticates user before every action in this controller
  before_filter :authenticate_user!

  #  1. Creates new user model with params for create action
  before_action :load_user, only: [:create]

  #  1. Checks appropriate permission of user for all actions 
  load_and_authorize_resource

  #  1. Retrieves query from request params 
  #  2. Searches users by query param if query is not blank
  #  3. Paginates the results with 10 per page
  #  4. Returns results with page number param 
  def index
    @query = params[:query]
    @users = @users.search(@query) unless @query.blank?
    @users = @users.paginate(page: params[:page], per_page: 10)
  end

  def new

  end

  #  1. Saves user model with params 
  #  2. Notice is shown about creation of user
  #  3. If save is successful, user page is shown
  #  4. Else new user page is shown
  def create
    if @user.save
      flash[:notice] = "User #{@user.username} created. "
      redirect_to @user
    else
      render 'users/new'
    end
  end

  def edit

  end

  #  1. If password is blank , password related params are removed from request params 
  #  2. Updates user with params from request which are filtered
  #  3. If update is successful, redirects to user page
  #  4. If update fails, edit user page is shown
  def update
    if params[:user][:password].blank?
      params[:user].delete(:password)
      params[:user].delete(:password_confirmation)
    end

    if(@user.update_attributes(update_user_params))
      redirect_to @user
    else
      render 'users/edit'
    end
  end

  #  1. If current user is same as user model passed, message is shown that it cant delete self
  #  2. Otherwise the user is deleted (Profile should be admin ? )
  #  3. Message is shown accordingly
  #  4. Redirects to users page
  def destroy
    if current_user == @user
      flash[:alert] = t("users.edit.cant_delete_self")
    elsif @user.destroy
      flash[:notice] = t("users.edit.deleted", :user => @user.username)
    end
    redirect_to users_url
  end

  private

  #  1. New user with params is created
  def load_user
    @user = User.new(create_user_params)
  end

  #  1. Filters request params for user 
  def create_user_params
    params.require(:user).permit(:username, :password, :password_confirmation, :first_name,
        :last_name, :email, :msisdn, :city, :address, :zipcode, :notification_url, :notification_post, :greeting_message,
        :legal_info, :tax_rate, :email_notification, :charge_payer, :website, :logo, :logo_cache, :signature, :signature_cache, :currency,
        :additional_notification_params, :payment_system_id, :profile_ids => [], :notification_params => [])
  end

  #  1. If current user is Usermanager, profile ids are included in filtered params for udate action
  #  2. Else the profile ids are not included.
  def update_user_params
    if current_user.has_role?("UserManager")
      params.require(:user).permit(:password, :password_confirmation, :first_name,
        :last_name, :email, :msisdn, :city, :address, :zipcode, :notification_url, :notification_post, 
        :greeting_message, :tax_rate, :legal_info, :email_notification, :charge_payer, :website, :logo, :logo_cache,
        :signature, :signature_cache, :additional_notification_params, :currency, :payment_system_id,
        :notification_params => [],  :profile_ids => [])
    else
      params.require(:user).permit(:password, :password_confirmation, :first_name,
        :last_name, :email, :msisdn, :city, :address, :zipcode, :notification_url, :notification_post, 
        :greeting_message, :tax_rate, :legal_info, :email_notification, :charge_payer, :website, :logo, :logo_cache, :currency,
        :signature, :signature_cache, :additional_notification_params, :notification_params => [])
    end
  end

end
