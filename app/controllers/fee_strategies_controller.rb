class FeeStrategiesController < ApplicationController

  before_action :authenticate_user!
  before_action :check_authorization
  before_action :load_user

  def new
    
  end

  def create
    @fee_strategy = FeeStrategy.new(fee_strategy_params.merge(merchant_id: @user.id))
    if @fee_strategy.save
      flash[:notice] = 'Fee Strategy is set'
      redirect_to user_url(@user)
    else
      render 'new'
    end
  end

  def edit
    @fee_strategy = @user.fee_strategy
  end

  def update
    @fee_strategy = @user.fee_strategy
    if @fee_strategy.update_attributes(fee_strategy_params)
      flash[:notice] = 'Fee Strategy is updated'
      redirect_to user_url(@user)
    else
      render 'edit'
    end
  end

  private

    def fee_strategy_params
      params.require(:fee_strategy).permit(:type, :fixed_amount, :fixed_amount_currency, 
        :percentage, :maximum_amount, :maximum_amount_currency, :disable_recurrent)
    end

    def load_user
      @user = User.find(params[:user_id])
    end

    def check_authorization
      authorize! :manage, FeeStrategy
    end
end
