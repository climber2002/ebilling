class AuditTrailsController < ApplicationController
  #  1. Authenticates user before all actions in this controller
  before_filter :authenticate_user!
  #  1. Checks if user has permission to read audit trails 
  #  2. Fetches all audit trails
  #  3. Gets query from request params 
  #  4. Searches fetched audit trails by query if query is not blank.
  #  5. Paginates the searched audit trails with 20 audit trails per page by page number param from request
  #  6. Returns those audit trails
  def index
    authorize! :read, AuditTrail

    @audit_trails = AuditTrail.order("id desc")
    @query = params[:query]
    @audit_trails = @audit_trails.search(@query) unless @query.blank?
    @audit_trails = @audit_trails.paginate(:page => params[:page], :per_page => 20)
  end
  #  1. Searches audit trail by id 
  #  2. Checks if audit trail is readable by user
  def show
    @audit_trail = AuditTrail.find params[:id]
    authorize! :read, @audit_trail
  end
end
