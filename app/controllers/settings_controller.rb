class SettingsController < ApplicationController
  
  #  1. Authenticates user before every action in this controller
  before_filter :authenticate_user!

  #  1. Checks permission to read settings
  def show
    authorize! :read, Settings
  end

  def edit
    authorize! :read, Settings
  end

  #  1. Sets each param value to appropriate column of settings table 
  #  2. Redirects to show page 
  def update
    authorize! :read, Settings
    
    [:sms_template, :sms_instructions_template, :paid_sms_template, 
      :receipt_email_subject_template, :receipt_email_template,
      :invoice_email_subject_template, :invoice_email_template, 
      :disable_sms_and_email].each do |setting|
      Settings.send("#{setting}=", params[setting])
    end

    redirect_to action: :show
  end
end
