class SystemConfigsController < ApplicationController

  before_filter :authenticate_user!

  def show
    authorize! :read, Settings
  end

  def edit
    authorize! :edit, Settings
  end

  def update
    authorize! :edit, Settings

    [:sms_server_url, :sms_username, :sms_password, :sms_params, 
      :smtp_address, :smtp_port, :smtp_domain, :smtp_username, 
      :smtp_password, :sender_id, :max_export_pdf, :max_export_excel, :bill_id_length].each do |setting|
      Settings.send("#{setting}=", params[setting])
    end

    save_templates

    redirect_to action: :show
  end

  def regenerate_stats
    authorize! :edit, Settings

    Delayed::Job.enqueue StatsGenerationJob.new
  end

  private

    def save_templates
      [:invoice_template, :receipt_template].each do |template_name|
        if params[template_name].present?
          File.open("#{Rails.root}/public/templates/#{template_name}.odt", 'wb') do |file|
            file.write(params[template_name].read)
          end
        end
      end
    end
  
end
