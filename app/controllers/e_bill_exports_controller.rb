class EBillExportsController < ApplicationController

  before_filter :authenticate_user!

  def new
    @export_request = ExportRequest.new(export_type: params[:export_type] || 'excel')
  end

  def create
    @export_request = ExportRequest.new(export_request_params)
    @export_request.current_user = current_user

    if @export_request.valid?
      @e_bills = @export_request.to_e_bill_scope
      if @export_request.excel?
        render xlsx: 'create', filename: 'export'
      elsif @export_request.pdf?
        BatchPdfExporter.new(@e_bills).export do |archive|
          send_file(archive, disposition: 'attachment')
        end
      else
        render 'new'
      end
    else
      render 'new'
    end
  end

  private

    def export_request_params
      params.require(:export_request).permit(:from, :to, :state, :export_type, :target_date, :payment_system_id)
    end
end
