module ApiSupport
  extend ActiveSupport::Concern

  included do
    self.responder = EBillResponder

    respond_to :json

    rescue_from CanCan::AccessDenied do |exception|
      render :json => { message: I18n.t("api.errors.no_right") }, :status => :forbidden
    end

    rescue_from ActiveRecord::RecordNotFound do |exception|
      render :json => { message: I18n.t("api.errors.not_found") }, :status => :not_found
    end

    # define the action for state change, the class which calls this method must
    # define a "#{event_action_name}_e_bill_params" method to define the parameters
    # permitted
    def self.state_event_action event_action_name, to_state, options = {}
      event_action = options[:event_action] || event_action_name
      role = options[:role] || :update

      define_method event_action_name do
        fetch_e_bill
        authorize! role, @e_bill
        @e_bill.assign_attributes(send("#{event_action_name}_e_bill_params"))
        assign_attributes_to_e_bill_for_audit
        event = event_action || event_action_name
        unless @e_bill.send(event)
          @e_bill.generate_failed_update_audit_trail @e_bill.state, to_state
        end
        respond_with(@e_bill)
      end
    end
  end

  def show
    fetch_e_bill
    authorize! :read, @e_bill
    @e_bill.assign_attributes show_e_bill_params
    respond_with(@e_bill)
  end

  private

  def assign_attributes_to_e_bill_for_audit
    if @e_bill
      @e_bill.params = send("#{action_name}_e_bill_params").to_s
      @e_bill.operator = current_user.username
    end
  end

  def fetch_e_bill
    @e_bill = EBill.find_by :bill_id => params[:id]
    raise ActiveRecord::RecordNotFound, "Could not find bill_id #{params[:id]}" unless @e_bill
  end

  def show_e_bill_params
    params.permit(:client_transaction_id)
  end
end