class MobileTracesController < ApplicationController

  #  1. Authenticates user before every action in this controller
  before_filter :authenticate_user!

  #  1. Retrieves all mobile traces accessible by current user 
  #  2. Paginates the traces resulted with 20 per page
  #  3. Returns the traces with page number
  def index
    @mobile_traces = MobileTrace.accessible_by(current_ability).order('id desc')
      .paginate(:page => params[:page], :per_page => 10)
  end

  #  1. Finds trace by id in request
  #  2. Checks permission to delete
  #  3. Deletes trace
  #  4. Dsiplays message 
  #  5. Redirects to traces url to show traces
  def destroy
    @mobile_trace = MobileTrace.find params[:id]
    authorize! :delete, @mobile_trace

    @mobile_trace.destroy
    flash[:notice] = t("mobile_traces.edit.deleted")
    redirect_to mobile_traces_url
  end
end
