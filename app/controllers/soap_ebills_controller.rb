class SoapEbillsController < ApplicationController
  STATE_TO_STATE_EVENTS = ActiveSupport::HashWithIndifferentAccess.new paid: 'pay',
                              failed: 'invalidate', expired: 'expire', cancelled: 'cancel'

  EBILLS_STATE_ERROR_CODES = { "paid" => ["ERR-010", "api.errors.already_paid"],
                               "expired" => ["ERR-011","api.errors.expired"],
                               "cancelled" => ["ERR-012","api.errors.cancelled"],
                               'created' => ["ERR-013","api.errors.not_yet_sent"],
                               "failed" => ["ERR-014","api.errors.error_state"]
                                }

  skip_before_filter :verify_authenticity_token
  before_filter :set_locale_from_params
  before_filter :authenticate_user!

  #  1. Fetches ebill from request params 
  #  2. Displays not found error if ebill is nil
  #  3. Checks permission to verify the ebill through api
  #  4. Prepares ebill for audit trail by adding client transaction id and operator to ebill
  #  5. Copies params in request to ebill params 
  def verify
    fetch_e_bill
    unless @e_bill
      render json: { errors: {error_message: I18n.t("api.errors.not_found") } }, status: 404
      return
    end

     authorize! :verify_for_api, @e_bill

    @e_bill.client_transaction_id = params[:e_bill][:client_transaction_id]
    @e_bill.params = params[:e_bill].clone
    @e_bill.operator = current_user.username


    unless @e_bill.ready?
      @e_bill.generate_verify_audit_trail false, I18n.t(EBILLS_STATE_ERROR_CODES[@e_bill.state][1])
      render json: {
         client_transaction_id: @e_bill.client_transaction_id,
         server_transaction_id: @e_bill.server_transaction_id,
         errors: {
              error_code:  EBILLS_STATE_ERROR_CODES[@e_bill.state][0],
              error_message: I18n.t(EBILLS_STATE_ERROR_CODES[@e_bill.state][1])}}, status: 405
      return
    end

    unless @e_bill.user.msisdn.casecmp(params[:e_bill][:payee_id]) == 0
      err_msg = "api.errors.payee_not_match"
       @e_bill.generate_verify_audit_trail false, I18n.t(err_msg)
       render json: {
          client_transaction_id: @e_bill.client_transaction_id,
          server_transaction_id: @e_bill.server_transaction_id,
          errors: {
               error_code:  "ERR-102",
               error_message: I18n.t(err_msg)}}, status: 406
       return
    end

    unless @e_bill.amount.to_s == params[:e_bill][:amount]
      err_msg = "api.errors.amount_not_match"
      @e_bill.generate_verify_audit_trail false, I18n.t(err_msg)
      render json: {
         client_transaction_id: @e_bill.client_transaction_id,
         server_transaction_id: @e_bill.server_transaction_id,
         errors: {
              error_code:  "ERR-103",
              error_message: I18n.t(err_msg)}}, status: 406
      return
    end

    @e_bill.generate_verify_audit_trail true
    render json: @e_bill.to_json_for_soap

  end

  #  1. Fetches ebill 
  #  2. If ebill is blank, not found error is rendered
  #  3. Else it is checked if user has permission to update ebill through api
  #  4. Request params are cloned into ebill params
  #  5. Assigns current username to ebill operator 
  #  6. State is deleted from params
  #  7. If state existed, state event is obtained from state
  #  8. Assigns ebill state to origin_state variable
  #  9. Assigns attributes to ebill from filtered request params
  #  10. State event is set for ebill 
  #  11. If it fails, audit trail for update action and origin_state is created
  #  12. Displays errors for already paid code with status 405
  #  13. Saves ebill
  #  14. If it is saved, ebill is returned in json soap format
  #  15. If it does not save, audit trail is generated and error messages are shown
  def update
    fetch_e_bill
    unless @e_bill
      render json: { errors: {error_message: I18n.t("api.errors.not_found") } }, status: 404
      return
    end

    authorize! :update_for_api, @e_bill

    @e_bill.params = params[:e_bill].clone
    @e_bill.operator = current_user.username

    state = params[:e_bill].delete :state
    if state
      @state_event = STATE_TO_STATE_EVENTS[state.downcase]
    end
    origin_state = @e_bill.state
    @e_bill.assign_attributes update_e_bill_params


    if @state_event
      if @e_bill.send("can_#{@state_event}?")

        @e_bill.send(@state_event, false)
      else
        @e_bill.generate_failed_update_audit_trail origin_state, state
        render json: {
           client_transaction_id: @e_bill.client_transaction_id,
           server_transaction_id: @e_bill.server_transaction_id,
           errors: {
                error_code:  EBILLS_STATE_ERROR_CODES[@e_bill.state][0],
                error_message: I18n.t(EBILLS_STATE_ERROR_CODES[@e_bill.state][1])}}, status: 405
        return
      end
    end

    if @e_bill.save
      render json: @e_bill.to_json_for_soap
    else
      @e_bill.generate_failed_update_audit_trail origin_state, state
      render json: { client_transaction_id: @e_bill.client_transaction_id,
           server_transaction_id: @e_bill.server_transaction_id,
           errors: @e_bill.errors.full_messages }, status: 406
    end
  end

  private

  #  1. Filters ebill update request params to suite update table columns
  def update_e_bill_params
    params[:e_bill].permit(:client_transaction_id, :state_event, :reason,
      :merchant_internal_ref, :external_reference, :failure_reason)
  end

  #  1. Fetches ebill by bill id param and returns it
  def fetch_e_bill
    @e_bill = EBill.find_by_bill_id params[:id]
  end

  #  1. If locale param is not blank, it is assigned to I18n locale
  def set_locale_from_params
    I18n.locale = params[:locale] unless params[:locale].blank?
  end
end
