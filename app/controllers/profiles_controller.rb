class ProfilesController < ApplicationController

  #  1. Authenticates user before every action in this controller
  before_filter :authenticate_user!

  #  1. Creates new profile model with params for create action
  before_action :load_profile, only: [:create]

  #  1. Checks appropriate permission of user for all actions 
  load_and_authorize_resource

  #  1. Paginates the profiles with 30 profiles per page 
  #  2. Returns those profiles
  #  3. How are profiles obtained
  def index
    @profiles = @profiles.paginate(page: params[:page], :per_page => 30)
  end

  def new

  end

  #  1. Saves profile. 
  #  2. If profile is saved, message is shown accordingly
  #  3. Redirects to profile page
  #  4. Renders new profile page if it could not save profile
  def create
    if @profile.save
      flash[:notice] = t("profiles.notice.created", name: @profile.name)
      redirect_to @profile
    else
      render 'profiles/new'
    end
  end

  def show
  end

  def edit
  end

  #  1. Profile is updated with filtered params for profile
  #  2. Redirects to profile page if update is successful
  #  3. Else redirects to edit page 
  def update
    if @profile.update_attributes(profile_params)
      redirect_to @profile
    else
      render 'profiles/edit'
    end
  end

  #  1. Deletes profile
  #  2. What does respond to json do ?
  def destroy
    @profile.destroy

    respond_to do |format|
      format.js
    end
  end

  private

  #  1. Creates a new profile model with profile params from request
  def load_profile
    @profile = Profile.new(profile_params)
  end

  #  1. Filters params for profile 
  def profile_params
    params.require(:profile).permit(:name, :description, :role_ids => [])
  end
end
