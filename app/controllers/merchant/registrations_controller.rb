class Merchant::RegistrationsController < ApplicationController

  def new
    @user = User::AsMerchantRegister.new
  end

  def create
    @user = User::AsMerchantRegister.new(user_params)
    if @user.save
      render 'create'
    else
      render 'new'
    end
  end

  private

    def user_params
      params.require(:user).permit(:username, :email, :last_name, :msisdn, :address, :city, :zipcode,
        :currency)
    end
end
