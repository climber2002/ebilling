module EBillExportsHelper

  def state_options_for_export
    options = []
    options << ['All', '']
    options << ['All paid', 'all_paid']
    options << ['Paid', 'paid']
    options << ['Partially Paid', 'partially_paid']
    options << ['Processed', 'processed']
    if current_user.has_profile?(Profile::ADMIN) || current_user.has_profile?(Profile::MERCHANT)
      options << ['Created', 'created']
      options << ['Ready', 'ready']
      options << ['Overdue', 'overdue']
      options << ['Expired', 'expired']
      options << ['Cancelled', 'cancelled']
    end
    options_for_select(options)
  end

  def payment_system_options
    options = []
    options << ['All', '']
    User.payment_systems.each do |ps|
      options << [ps.username, ps.id]
    end
    options_for_select(options)
  end
end