module ApplicationHelper

  # Generates a menu item li
  def menu_item_li url, name, icon, li_class = "menu_item", options = {}
    link_tag = link_to(url) do
                  content = content_tag(:i, "", class: icon)
                  content << ' '
                  content << content_tag(:span, I18n.t("menu.#{name}"), class: "hidden-tablet")
               end
    if options[:active]
      if controller.instance_eval &options[:active]
        content_tag(:li, link_tag, class: "active #{li_class}")
      else
        content_tag(:li, link_tag, class: "#{li_class}")
      end
    else
      if controller.controller_name.casecmp(name.gsub(/\s+/, "")) == 0
        content_tag(:li, link_tag, class: "active #{li_class}")
      else
        content_tag(:li, link_tag, class: "#{li_class}")
      end
    end

  end

  # Generate a box
  def big_box title, icon
    content_tag(:div, class: 'row-fluid') do
      content_tag(:div, class: 'box span12') do
        content = content_tag(:div, class: 'box-header well') do
                    content_tag(:h2) do
                      header = content_tag(:i, "", class: icon)
                      header << " "
                      header << title
                    end
                  end

        content << content_tag(:div, class: 'box-content') do
                    yield
                  end

        content << content_tag(:div, "", class: 'clearfix')
      end
    end
  end

end
