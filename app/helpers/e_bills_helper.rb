module EBillsHelper

  # the send and fail methods already exist in ruby class, so need to use
  # trigger and invalidate to represent them internally. But for display
  # we still use send and fail
  STATE_EVENTS_FOR_DISPLAY = { :trigger => :send, :invalidate => :fail }

  ICONS_FOR_STATE_EVENT = { :trigger => "icon-share-alt",
                            :pay => "icon-file",
                            :cancel => "icon-remove",
                            :invalidate => "icon-minus-sign",
                            :process => "icon-hand-up",
                            :expire => "icon-time"
                             }

  # Get all options for select currency
  def currency_options
    options = []
    Country.all.order(:name).each do |country|
      options << ["#{country.name} - #{country.currency_code}", country.currency_code]
    end
    options
  end

  def state_events_for_display state_event
    STATE_EVENTS_FOR_DISPLAY[state_event] || state_event
  end

  def state_event_path_for e_bill, state_event
    link_class = "btn #{btn_class_for_state_event state_event}"

    content = "<i class='icon-white #{icon_for_state_event state_event}'></i>"
    content << I18n.t("e_bills.state_event.#{state_events_for_display(state_event)}")

    case state_event
    when :trigger, :resend
      link_to raw(content), trigger_e_bill_path(e_bill, state_event: state_event),
                  method: :put,
                  class: link_class
    when :cancel, :fail
      link_to raw(content), edit_e_bill_path(e_bill, state_event: state_event), class: link_class
    when :process
      link_to raw(content), process_e_bill_path(e_bill), method: :put, class: link_class if can? :process, e_bill
    else
      link_to raw(content), e_bill_path(e_bill, state_event: state_event), class: link_class
    end
  end

  def state_label state
    raw "<span class='label big-label label-#{state}'>#{state}</span>"
  end

  def icon_for_state_event state_event
    ICONS_FOR_STATE_EVENT[state_event]
  end

  def btn_class_for_state_event state_event
    case state_event
    when :cancel
      "btn-warning"
    when :invalidate
      "btn-danger"
    when :process
      "btn-inverse"
    when :trigger
      "btn-info"
    else
      "btn-success"
    end
  end
end
