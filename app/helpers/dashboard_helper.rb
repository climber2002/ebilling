module DashboardHelper

  def format_month(year, month)
    Date.new(year, month).strftime("%b %Y")
  end
end
