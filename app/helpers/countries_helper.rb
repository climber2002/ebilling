module CountriesHelper

  def countries
    ISO3166::Country.all.select {|c| c.currency.present? }.sort {|a, b| a.name <=> b.name }
  end

  def country_options
    countries.map do |country|
      [country.name, country.name]
    end
  end

  def currency_options_for_select
   countries.map do |country|
      "{ \"name\": \"#{country.name}\", \"currency_code\": \"#{country.currency.try(:code)}\", \"currency_name\": \"#{country.currency.try(:name)}\" }"
    end.join(",")
  end
end
