// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require kindeditor
//= require turbolinks
//= require jquery.turbolinks
//= require js/jquery-ui-1.8.21.custom.min
//= require js/jquery.uniform.min
//= require js/bootstrap
//= require js/jquery.cookie
//= require js/jquery.dataTables.min
//= require js/jquery.iphone.toggle
//= require jquery-fileupload/basic
//= require_tree .

$(document).ready(function() {

  // animating menus on hover
  $('ul.main-menu li:not(.nav-header)').hover(function(){
    $(this).animate({'margin-left':'+=5'},300);
  },
  function(){
    $(this).animate({'margin-left':'-=5'},300);
  });

  // uniform elemnts
  $('input.check-box').uniform();

  //enable datetimepicker
  $('div.datetimepicker').datetimepicker({
        language: 'en'
  });

  $('div.datepicker').datetimepicker({
        language: 'en',
        pickTime: false
  });
});

//This method is not used now as we don't add right buttons
//to the dialog box yet
function styleDialogRightButtons() {
  //right button for dialog
  $('.btn-close').click(function(e){
    e.preventDefault();
    $(this).parent().parent().parent().fadeOut();
  });
  $('.btn-minimize').click(function(e){
    e.preventDefault();
    var $target = $(this).parent().parent().next('.box-content');
    if($target.is(':visible')) $('i',$(this)).removeClass('icon-chevron-up').addClass('icon-chevron-down');
    else             $('i',$(this)).removeClass('icon-chevron-down').addClass('icon-chevron-up');
    $target.slideToggle();
  });
  $('.btn-setting').click(function(e){
    e.preventDefault();
    $('#myModal').modal('show');
  });
}