$(document).ready(function(){

  $("#new_batch_file").fileupload({
    dataType: 'script',
    add: function (e, data) {
                $('#progress .progress-bar').css('width', '0%');
                data.submit();
            },
    progressall: function (e, data) {
                var progress = parseInt(data.loaded / data.total * 100, 10);
                $('#progress .progress-bar').css(
                    'width',
                    progress + '%'
                );
            }
  });

});

