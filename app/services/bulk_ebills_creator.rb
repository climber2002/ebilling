class BulkEbillsCreator

  attr_reader :current_user
  attr_accessor :failed_ebill
  attr_accessor :e_bills

  def initialize(current_user)
    @current_user = current_user
    @e_bills = []
  end

  def bulk_create(params)
    EBill.transaction do
      sms = params[:sms]
      email = params[:email]
      merchant_name = params[:merchant_name]
      expiry_period = params[:expiry_period]

      ebills_params = params[:e_bills]
      ebills_params.each do |ebill_params|
        self.e_bills << create(ebill_params, sms, email, merchant_name, expiry_period)
      end
    end

    perform_after_save_tasks
  end

  private

    def create(ebill_params, sms, email, merchant_name, expiry_period)
      params = ebill_params.merge(sms: sms, email: email, expiry_period: expiry_period)
      if current_user.has_profile?(Profile::MERCHANT)
        params.merge!(user_id: current_user.id)
      else
        merchant = User.find_by_username(merchant_name)
        unless merchant.present? && merchant.has_profile?(Profile::MERCHANT)
          self.failed_ebill = EBill.new
          raise ArgumentError.new("merchant_name not valid, it must be a merchant's name")
        end
        params.merge!(user_id: merchant.id)
      end
      e_bill = EBill.new(params)
      e_bill.params = params.to_s
      e_bill.operator = current_user.username
      e_bill.save!
      e_bill
    rescue ActiveRecord::RecordInvalid => e
      self.failed_ebill = e_bill
      raise
    end

    def perform_after_save_tasks
      e_bills.each do |e_bill|
        e_bill.trigger_after_save_tasks
      end
    end
end
