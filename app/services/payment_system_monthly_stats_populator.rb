class PaymentSystemMonthlyStatsPopulator

  def populate_for_yesterday
    populate(Date.yesterday)
  end

  def populate(date)
    year = date.year
    month = date.month
    beginning_time = date.beginning_of_month.beginning_of_day
    end_time = date.end_of_month.end_of_day

    timestamp_columns_to_count = { 'partially_paid_at' => 'partially_paid_count', 
      'processed_at' => 'processed_count', 'paid_at' => 'paid_count' }

    # the key is [payment_system_id, merchant_id], the value is
    # PaymentSystemMonthlyItem
    stats_hash = {}

    timestamp_columns_to_count.each do |timestamp_column, count_column|
      stats = EBill.select("count(*) as count, e_bills.payment_system_id as payment_system_id, e_bills.user_id as merchant_id").group("payment_system_id, merchant_id").
        where("#{timestamp_column} >= ? and #{timestamp_column} <= ?", beginning_time, end_time)
      stats.each do |stat|
        payment_system_id = stat.payment_system_id
        merchant_id = stat.merchant_id
        count = stat.count
        monthly_stat = PaymentSystemMonthlyStat.find_or_create_by(payment_system_id: payment_system_id, year: year, month: month)

        unless ps_monthly_item = stats_hash[[payment_system_id, merchant_id]]
          ps_monthly_item = PaymentSystemMonthlyItem.find_or_create_by(payment_system_monthly_stat_id: monthly_stat.id, merchant_id: merchant_id)
          stats_hash[[payment_system_id, merchant_id]] = ps_monthly_item
        end
        ps_monthly_item.send("#{count_column}=", count)
      end
    end

    fee_stats = EBill.select("sum(amount_paid) * 100 as amount_paid_in_cents, sum(ps_transaction_fee_centimes) as ps_transaction_fee_centimes,e_bills.payment_system_id as payment_system_id, e_bills.user_id as user_id").group("payment_system_id, user_id").
      where("paid_at >= ? and paid_at <= ?", beginning_time, end_time)

    fee_stats.each do |fee_stat|
      payment_system_id = fee_stat.payment_system_id
      user_id = fee_stat.user_id
      if (ps_monthly_item = stats_hash[[payment_system_id, user_id]])
        ps_monthly_item.revenue = Money.new(fee_stat.amount_paid_in_cents, ps_monthly_item.merchant.currency)
        ps_monthly_item.transaction_fee = Money.new(fee_stat.ps_transaction_fee_centimes, ps_monthly_item.merchant.currency)
      end
    end

    stats_hash.values.each do |monthly_item|
      monthly_item.save
    end
  end
end