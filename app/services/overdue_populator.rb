class OverduePopulator

  # scope :overdue, ->() { where("due_date < ? and (state = 'created' or state = 'ready')", Date.today ) }
  def populate_overdue
    now = DateTime.now
    EBill.where("due_date < ? and (state = 'created' or state = 'ready')", Date.today).
      update_all(state: 'overdue', overdue_at: now)    
  end

end