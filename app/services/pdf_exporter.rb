class PdfExporter

  attr_reader :e_bill
  attr_reader :generator
  attr_reader :generate_type

  def initialize(e_bill)
    @e_bill = e_bill
    if ['created', 'ready', 'overdue', 'expired', 'cancelled'].include?(e_bill.state)
      @generator = ReceiptGenerator.new(e_bill, File.join(Rails.root, "/public/templates/invoice_template.odt"))
      @generate_type = 'invoice'
    else
      @generator = ReceiptGenerator.new(e_bill, File.join(Rails.root, "/public/templates/receipt_template.odt"))
      @generate_type = 'receipt'
    end
  end

  def export
    generator.generate do |pdf_path|
      yield pdf_path
    end
  end

end