class MerchantMonthlyStatsPopulator

  def populate_for_yesterday
    populate(Date.yesterday)
  end

  def populate(date)
    year = date.year
    month = date.month
    beginning_time = date.beginning_of_month.beginning_of_day
    end_time = date.end_of_month.end_of_day

    timestamp_columns_to_count = { 'created_at' => 'created_count', 'sent_at' => 'ready_count', 
      'partially_paid_at' => 'partially_paid_count', 'cancelled_at' => 'cancelled_count',
      'failed_at' => 'failed_count', 'processed_at' => 'processed_count', 
      'expired_at' => 'expired_count', 'overdue_at' => 'overdue_count', 'paid_at' => 'paid_count' }

    stats_hash = {}
    
    timestamp_columns_to_count.each do |timestamp_column, count_column|
      stats = EBill.select("count(*) as count, e_bills.user_id as user_id").group("user_id").
        where("#{timestamp_column} >= ? and #{timestamp_column} <= ?", beginning_time, end_time)
      stats.each do |stat|
        user_id = stat.user_id
        count = stat.count
        unless merchant_monthly_stat = stats_hash[user_id]
          merchant_monthly_stat = MerchantMonthlyStat.find_or_create_by merchant_id: user_id, year: year, month: month
          stats_hash[user_id] = merchant_monthly_stat
        end
        merchant_monthly_stat.send("#{count_column}=", count)
      end
    end

    fee_stats = EBill.select("sum(amount_paid) * 100 as amount_paid_in_cents, sum(traffic_fee_centimes) as traffic_fee_stats_in_cents, sum(ps_transaction_fee_centimes) as ps_transaction_fee_in_cents, e_bills.user_id as user_id").group("user_id").
      where("paid_at >= ? and paid_at <= ?", beginning_time, end_time)

    fee_stats.each do |fee_stat|
      user_id = fee_stat.user_id
      user = fee_stat.user
      traffic_fee_stats_in_cents = fee_stat.traffic_fee_stats_in_cents
      if (merchant_monthly_stat = stats_hash[user_id])
        if user.fee_strategy.present?
          merchant_monthly_stat.recurrent_fee = user.fee_strategy.calculate_monthly_recurrent_fee(merchant_monthly_stat.paid_count)
        end

        merchant_monthly_stat.traffic_fee = Money.new(traffic_fee_stats_in_cents, user.currency)

        merchant_monthly_stat.revenue = Money.new(fee_stat.amount_paid_in_cents, merchant_monthly_stat.merchant.currency)
        merchant_monthly_stat.ps_transaction_fee = Money.new(fee_stat.ps_transaction_fee_in_cents, merchant_monthly_stat.merchant.currency)
      end
    end    

    stats_hash.values.each do |merchant_monthly_stat|
      merchant_monthly_stat.save
    end
  end
end