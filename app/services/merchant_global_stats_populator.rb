class MerchantGlobalStatsPopulator

  def populate
    stats = EBill.select("count(*) as count, e_bills.user_id as user_id, e_bills.state as state").group("user_id, state")

    # the key is user_id, the value is a hash, in the hash the key is state, the value is count
    stats_hash = Hash.new { |h, k| h[k] = {} }
    stats.each do |stat|
      stat_hash = stats_hash[stat.user_id]
      stat_hash[stat.state] = stat.count
    end

    stats_hash.each do |user_id, stat_hash|
      merchant_stat = MerchantGlobalStat.find_or_create_by(merchant_id: user_id)
      attrs = map_stat_hash_to_attributes(stat_hash)
      merchant_stat.update_attributes(attrs)
    end
  end

  private

    def map_stat_hash_to_attributes(stat_hash)
      stat_hash.each_with_object({}) {|(state,count),o| o["#{state}_count"] = count }      
    end

end