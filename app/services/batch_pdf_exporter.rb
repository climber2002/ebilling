require 'fileutils'
require 'tmpdir'
require 'zip/zip'
require 'zip/zipfilesystem'

class BatchPdfExporter

  attr_reader :e_bills
  attr_reader :dir

  def initialize(e_bills)
    @e_bills = e_bills
  end

  def export
    create_tmpdir
    generate_pdfs
    archive = generate_zip
    yield archive if block_given?
  end

  private 

    def create_tmpdir
      @dir = Dir.mktmpdir  
    end

    def generate_pdfs
      e_bills.each do |e_bill|
        generate_for(e_bill)
      end  
    end

    # for created, ready, overdue, expired, cancelled state, export invoice
    # otherwise export receipt
    def generate_for(e_bill)
      bill_exporter = PdfExporter.new(e_bill)
    
      bill_exporter.export do |pdf_path|
        Rails.logger.debug "pdf file : ${pdf_path}"
        dest = File.join(dir, "#{e_bill.bill_id}_#{bill_exporter.generate_type}.pdf")
        FileUtils.cp pdf_path, dest
        Rails.logger.debug "copied to #{dest}"
      end
    end

    def generate_zip
      archive = File.join(dir, "export.zip")
      Zip::ZipFile.open(archive, 'w') do |zipfile|
        Dir["#{dir}/*.pdf"].reject{|f| f == archive }.each do |file|
          zipfile.add(File.basename(file), file)
        end
      end
      archive
    end

end