class GlobalCurrencyStatsFetcher

  def self.get_global_currencies(options={})     
    stats = MerchantMonthlyStat.joins(:merchant).select_sum('users.currency as currency').group('currency')
    if options[:merchant_id].present?
      stats = stats.where("merchant_id = ?", options[:merchant_id])
    end

    result = {}
    [:revenues, :traffic_fees, :recurrent_fees].each do |key|
      result[key] = {}
    end

    stats.each do |stat|
      currency = stat.currency
      result[:revenues][currency] ||= Money.new(0, currency)
      result[:revenues][currency] += Money.new(stat.revenue_centimes, currency)

      result[:traffic_fees][currency] ||= Money.new(0, currency)
      result[:traffic_fees][currency] += Money.new(stat.traffic_fee_centimes, currency)

      result[:recurrent_fees][currency] ||= Money.new(0, currency)
      result[:recurrent_fees][currency] += Money.new(stat.recurrent_fee_centimes, currency)
    end

    result
  end
end