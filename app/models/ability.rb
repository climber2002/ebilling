class Ability
  include CanCan::Ability

  def initialize(user)
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user
    # permission to do.
    # If you pass :manage it will apply to every action. Other common actions
    # here are :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on.
    # If you pass :all it will apply to every resource. Otherwise pass a Ruby
    # class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the
    # objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details:
    # https://github.com/ryanb/cancan/wiki/Defining-Abilities

    user ||= User.new

    user.roles.each { |role| send(role.name.underscore, user) if respond_to?(role.name.underscore)}

    manage_normal_resources(user)
    manage_admin_only_resources(user)
  end

  def manage_normal_resources(user)
    [Mobile, MobileTrace].each do |resource|
      if user.has_profile?(Profile::ADMIN)
        can :manage, resource
      elsif user.has_profile?(Profile::MERCHANT)
        can :manage, resource, :user_id => user.id
      end
    end
  end

  def manage_admin_only_resources(user)
    [MerchantPaymentSystemMapping, FeeStrategy, Currency, EbillingPaymentAccount, Country].each do |resource|
      if user.has_profile?(Profile::ADMIN)
        can :manage, resource
      end
    end
  end

  def user_viewer user
    can :read, User
  end

  def user_manager user
    user_viewer user
    can :manage, User
  end

  def profile_viewer user
    can :read, Profile
  end

  def profile_manager user
    profile_viewer user
    can :manage, Profile
  end

  def view_e_bill user
    can :read, EBill
  end

  def view_own_e_bill user
    can :read, EBill, :user_id => user.id
  end

  def view_payment_system_e_bill user
    can :read, EBill, :payment_system_id => user.payment_system_id
  end  

  def create_e_bill user
    can :create, EBill
  end

  def view_audit_trail user
    can :read, AuditTrail
  end

  def manage_e_bill user
    [:update, :trigger, :cancel, :pay, :invalidate, :resend, :process].each do |state_event|
      can state_event, EBill
    end

    can [:read, :create], BatchFile
    can [:read, :update], Settings
  end

  def manage_own_e_bill user
    [:update, :trigger, :cancel, :pay, :invalidate, :resend, :process].each do |state_event|
      can state_event, EBill, :user_id => user.id
    end

    can :create, BatchFile
    can :read, BatchFile, :user_id => user.id
  end

  def verify_e_bill user
    view_e_bill user
    can :verify_for_api, EBill
  end

  def update_e_bill_to_pay user
    view_e_bill user
    can :update_for_api, EBill
  end
end
