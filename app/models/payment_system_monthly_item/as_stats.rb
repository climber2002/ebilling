# == Schema Information
#
# Table name: payment_system_monthly_items
#
#  id                             :integer          not null, primary key
#  paid_count                     :integer          default(0)
#  partially_paid_count           :integer          default(0)
#  processed_count                :integer          default(0)
#  revenue_centimes               :integer          default(0), not null
#  revenue_currency               :string(255)      default("XAF"), not null
#  payment_system_monthly_stat_id :integer
#  merchant_id                    :integer
#  created_at                     :datetime
#  updated_at                     :datetime
#  transaction_fee_centimes       :integer          default(0), not null
#  transaction_fee_currency       :string(255)      default("XAF"), not null
#

class PaymentSystemMonthlyItem::AsStats < PaymentSystemMonthlyItem

  attr_accessor :year, :month

  extend MerchantMonthlyStat::DateUtil

  attr_accessor :revenues, :transaction_fees

  after_initialize :reset_revenues, :reset_transaction_fees

  # Get month_count from start_date, for example, when date is August 15, 2015, month_count is 6
  # it will get stats of August, July, June, May, April, March, 6 months in total
  def self.get_stats_for(start_date, month_count, payment_system_id)
    year_months = get_year_months(start_date, month_count)

    select_part = ['paid_count', 'partially_paid_count', 'processed_count', 'revenue_centimes', 'transaction_fee_centimes'].map do |count_name|
      "sum(#{count_name}) as #{count_name}"
    end.join(', ')

    select_part = [select_part, 'year as stat_year', 'month as stat_month', 'users.currency as currency'].join(', ')

    stats = self.joins(:payment_system_monthly_stat, :merchant).select(select_part).
      where("(year, month) in (#{year_months.join(',')}) and payment_system_monthly_stats.payment_system_id = ?", payment_system_id).group("year, month, users.currency").order('year desc, month desc')

    # the key is array [year, month], the value is an instance of MerchantMonthlyStat::MonthlyHistoryForAdmin
    year_month_to_stat = {}
    stats.each do |stat|
      year_month = [stat.stat_year, stat.stat_month]
      year_month_to_stat[year_month] ||= self.new(year: stat.stat_year, month: stat.stat_month)
      
      [:paid_count, :partially_paid_count, :processed_count].each do |count_type|
        origin = year_month_to_stat[year_month].send(count_type)
        year_month_to_stat[year_month].send("#{count_type}=", origin + stat.send(count_type))
      end

      currency = stat.currency
      year_month_to_stat[year_month].revenues[currency] ||= Money.new(0, currency)
      year_month_to_stat[year_month].revenues[currency] += Money.new(stat.revenue_centimes, currency)
      year_month_to_stat[year_month].transaction_fees[currency] ||= Money.new(0, currency)
      year_month_to_stat[year_month].transaction_fees[currency] += Money.new(stat.transaction_fee_centimes, currency)
    end
    year_month_to_stat.values
  end

  def self.top10_by_paid(payment_system_id)
    self.joins(:payment_system_monthly_stat,:merchant).select("sum(paid_count) as paid_count, users.username as merchant_name").
      where("payment_system_monthly_stats.payment_system_id = ?", payment_system_id).group('merchant_name').order('paid_count desc').limit(10)
  end

  def self.top10_by_transaction_fee(payment_system_id)
    self.joins(:payment_system_monthly_stat,:merchant).select("sum(transaction_fee_centimes) as transaction_fee_centimes, users.username as merchant_name").
      where("payment_system_monthly_stats.payment_system_id = ?", payment_system_id).group('merchant_name').order('transaction_fee_centimes desc').limit(10)
  end

  def username
    try(:merchant_name)
  end

  private

    def reset_revenues
      self.revenues = {}
    end

    def reset_transaction_fees
      self.transaction_fees = {}
    end
end
