# == Schema Information
#
# Table name: roles
#
#  id           :integer          not null, primary key
#  name         :string(255)      not null
#  description  :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#  role_type_id :integer
#  display_name :string(255)
#

class Role < ActiveRecord::Base
  
  #  Validates presence and uniqueness of name 
  validates :name, presence: true, uniqueness: true

  #  Shows one to one relationship with role type class 
  belongs_to :role_type

  #  Validates presence of role type id 
  validates :role_type_id, presence: true

  #  Returns name of role 
  def display_name
    self[:display_name] || name
  end
end
