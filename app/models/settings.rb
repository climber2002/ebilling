# == Schema Information
#
# Table name: settings
#
#  id         :integer          not null, primary key
#  var        :string(255)      not null
#  value      :text
#  thing_id   :integer
#  thing_type :string(30)
#  created_at :datetime
#  updated_at :datetime
#

class Settings < RailsSettings::CachedSettings

  def self.receipt_email_subject_for e_bill
    e_bill.transform_message self.receipt_email_subject_template.to_s
  end

  def self.receipt_email_content_for e_bill
    e_bill.transform_message self.receipt_email_template.to_s
  end

  def self.invoice_email_subject_for e_bill
    e_bill.transform_message self.invoice_email_subject_template.to_s
  end

  def self.invoice_email_content_for e_bill
    e_bill.transform_message self.invoice_email_template.to_s
  end

  def self.disable_sms_and_email?
    self.disable_sms_and_email.to_s.downcase == 'true'
  end
end
