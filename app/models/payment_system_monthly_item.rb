# == Schema Information
#
# Table name: payment_system_monthly_items
#
#  id                             :integer          not null, primary key
#  paid_count                     :integer          default(0)
#  partially_paid_count           :integer          default(0)
#  processed_count                :integer          default(0)
#  revenue_centimes               :integer          default(0), not null
#  revenue_currency               :string(255)      default("XAF"), not null
#  payment_system_monthly_stat_id :integer
#  merchant_id                    :integer
#  created_at                     :datetime
#  updated_at                     :datetime
#  transaction_fee_centimes       :integer          default(0), not null
#  transaction_fee_currency       :string(255)      default("XAF"), not null
#

# the stats for a particular merchant which belongs to 
# a PaymentSystemMonthlyStat
class PaymentSystemMonthlyItem < ActiveRecord::Base

  belongs_to :payment_system_monthly_stat
  belongs_to :merchant, class_name: 'User'

  monetize :revenue_centimes, with_model_currency: :revenue_currency
  monetize :transaction_fee_centimes, with_model_currency: :transaction_fee_currency

  validates_presence_of :payment_system_monthly_stat_id, :merchant_id

  def self.total_counts_for(payment_system_id)
    select_part = [:partially_paid_count, :paid_count, :processed_count].map do |count_name|
      "sum(#{count_name}) as #{count_name}"
    end.join(', ')
    select(select_part).joins(:payment_system_monthly_stat).
      where("payment_system_monthly_stats.payment_system_id = ?", payment_system_id)[0]
  end

  def self.total_revenue_for(payment_system_id)
    select("sum(revenue_centimes) as revenue_centimes, revenue_currency").joins(:payment_system_monthly_stat).
      where("payment_system_monthly_stats.payment_system_id = ?", payment_system_id).
      group("revenue_currency")
  end
  
  def revenue_from_stats
    Money.new(revenue_centimes, merchant.currency)
  end

  def transaction_fee_from_stats
    Money.new(transaction_fee_centimes, merchant.currency)
  end
end
