module NotificationSender

  # Send notification to merchant for e_bill
  def self.send_notification_to_merchant(e_bill, additional_params = {})
    return unless e_bill.user || e_bill.user.notification_url.blank?

    merchant = e_bill.user
    
    notification_params = e_bill.get_notification_params(merchant.notification_params)

    audit_trail_params = {
      :action => "sendNotification", :bill_id => e_bill.bill_id,
      :params => {'notification_url' => merchant.notification_url, 'notification_params' => notification_params }.to_s
    }

    notification_params = notification_params.merge(additional_params)
    
    response = send_notification(merchant.notification_url, 
            notification_params,
            merchant.notification_post)

    if e_bill.can_process?
      e_bill.process
      e_bill.save
    end

    Rails.logger.info("Send the notification to merchant, #{response.to_s}")

    AuditTrail.create(audit_trail_params.merge(status: 'success', context: response.to_s[0, 64000]))
    
  rescue => e
    Rails.logger.warn "can't send notification: #{e.message}"
    AuditTrail.create(audit_trail_params.merge(status: 'failed', error_messages: e.message))
  end

  private

  def self.send_notification(notification_url, notification_params, post)
    if post
      RestClient.post notification_url, notification_params
    else
      RestClient.get notification_url, { :params => notification_params }
    end
  end

end