class EmailValidator < ActiveModel::EachValidator

  #  1. Valid characters inside email
  EMAIL_REGEX = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i

  #  1. Checks if email matches the pattern, else shows error message that email is not valid
  def validate_each(record, attribute, value)
    unless value =~ EMAIL_REGEX
      record.errors[attribute] << (options[:message] || "is not an email")
    end 
  end
end