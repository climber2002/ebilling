# == Schema Information
#
# Table name: merchant_payment_system_mappings
#
#  id                  :integer          not null, primary key
#  merchant_id         :integer
#  payment_system_id   :integer
#  merchant_ps_id      :string(255)
#  created_at          :datetime
#  updated_at          :datetime
#  ps_percentage       :float            default(0.0)
#  search_field        :string(255)
#  ebilling_percentage :float            default(0.0)
#

class MerchantPaymentSystemMapping < ActiveRecord::Base

  belongs_to :merchant, class_name: 'User'
  belongs_to :payment_system, class_name: 'User'

  validates :merchant_id, presence: true
  validates :payment_system_id, presence: true
  validates_uniqueness_of :merchant_id, :scope => :payment_system_id

  validates :merchant_ps_id, presence: true #, uniqueness: true

  validate :merchant_should_have_merchant_profile
  validate :payment_system_should_have_payment_system_profile

  include Searchable
  searchable :merchant_name, :payment_system_name, :merchant_ps_id

  def merchant_name
    self.merchant.username
  end

  def payment_system_name
    self.payment_system.username
  end

  def total_percentage
    ps_percentage + ebilling_percentage
  end

  private

    def merchant_should_have_merchant_profile
      if merchant && !merchant.has_profile?(Profile::MERCHANT)
        errors.add(:merchant_id, 'should have merchant profile')
      end
    end

    def payment_system_should_have_payment_system_profile
      if payment_system && !payment_system.has_profile?(Profile::PAYMENT_SYSTEM)
        errors.add(:payment_system_id, 'should have payment system profile')
      end
    end

    def store_search_field
      self.search_field = [merchant.try(:username), payment_system.try(:username), merchant_ps_id].map(&:downcase).join(' ')
    end
end
