class ExportRequest < ActiveType::Object

  attribute :target_date
  attribute :from, :date
  attribute :to, :date
  attribute :state, :string
  attribute :current_user, User
  attribute :export_type, :string
  attribute :payment_system_id

  validates :target_date, presence: true
  validates :from, presence: true
  validates :to, presence: true
  validates :export_type, inclusion: { in: %w(pdf excel),
    message: "export_type should be pdf or excel" }

  validate :not_reach_maximum_count

  def to_e_bill_scope
    if target_date == 'paid_date'
      scope = EBill.where("paid_at >= ? and paid_at < ?", from, to.next_day)
    else
      scope = EBill.where("created_at >= ? and created_at < ?", from, to.next_day)
    end
    if state.present?
      if state == 'all_paid'
        scope = scope.where("state in (?)", ['paid', 'partially_paid', 'processed'])
      else 
        scope = scope.where("state = ?", state)
      end
    end

    if current_user.present?
      if current_user.has_profile?(Profile::MERCHANT)
        scope = scope.where('user_id = ?', current_user.id)
      elsif current_user.has_profile?(Profile::PAYMENT_SYSTEM_OPERATOR)
        payment_system_id = current_user.payment_system_id
        scope = scope.where("payment_system_id = ?", payment_system_id)
      end
    end

    if self.payment_system_id.present?
      scope = scope.where("payment_system_id = ?", self.payment_system_id)
    end
    scope
  end

  def count
    to_e_bill_scope.count
  end

  def excel?
    export_type == 'excel'
  end

  def pdf?
    export_type == 'pdf'
  end

  private

    def not_reach_maximum_count
      if from.present? && to.present?
        maximum = Settings.send("max_export_#{export_type}").present? ? 
          Settings.send("max_export_#{export_type}").to_i : 1000

        if count > maximum
          errors.add(:count, "Export size #{count} is more than the maximum #{maximum}")
        end
      end
    end
end