module MerchantMonthlyStat::DateUtil

  def get_year_months(date, month_count)
    year_months = []
    month_count.times do 
      year_months << date
      date = date.last_month
    end
    year_months = year_months.map { |date| "(#{date.year}, #{date.month})" }
  end
end