# == Schema Information
#
# Table name: merchant_monthly_stats
#
#  id                          :integer          not null, primary key
#  created_count               :integer          default(0)
#  ready_count                 :integer          default(0)
#  paid_count                  :integer          default(0)
#  partially_paid_count        :integer          default(0)
#  cancelled_count             :integer          default(0)
#  failed_count                :integer          default(0)
#  processed_count             :integer          default(0)
#  expired_count               :integer          default(0)
#  merchant_id                 :integer          not null
#  year                        :integer          not null
#  month                       :integer          not null
#  created_at                  :datetime
#  updated_at                  :datetime
#  overdue_count               :integer          default(0)
#  traffic_fee_centimes        :integer          default(0), not null
#  traffic_fee_currency        :string(255)      default("XAF"), not null
#  revenue_centimes            :integer          default(0), not null
#  revenue_currency            :string(255)      default("XAF"), not null
#  recurrent_fee_centimes      :integer          default(0), not null
#  recurrent_fee_currency      :string(255)      default("XAF"), not null
#  ps_transaction_fee_centimes :integer          default(0), not null
#  ps_transaction_fee_currency :string(255)      default("XAF"), not null
#

class MerchantMonthlyStat::MonthlyHistoryForMerchant < MerchantMonthlyStat

  extend DateUtil

  def self.get_stats_for(start_date, month_count, merchant_id)
    year_months = get_year_months(start_date, month_count)
    self.where(merchant_id: merchant_id).where("(year, month) in (#{year_months.join(',')})").order('year desc, month desc')
  end

  # the revenues have only one currency since it's for one merchant, but we
  # make it compatible with the interfaces of MonthlyHistoryForAdmin
  [:revenues, :traffic_fees, :recurrent_fees, :total_transaction_fees, :net_revenues].each do |method_name|
    define_method method_name do 
      result = {}
      result[merchant.currency] = self.send(method_name.to_s.singularize)
      result  
    end
  end

end
