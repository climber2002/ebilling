# == Schema Information
#
# Table name: merchant_monthly_stats
#
#  id                          :integer          not null, primary key
#  created_count               :integer          default(0)
#  ready_count                 :integer          default(0)
#  paid_count                  :integer          default(0)
#  partially_paid_count        :integer          default(0)
#  cancelled_count             :integer          default(0)
#  failed_count                :integer          default(0)
#  processed_count             :integer          default(0)
#  expired_count               :integer          default(0)
#  merchant_id                 :integer          not null
#  year                        :integer          not null
#  month                       :integer          not null
#  created_at                  :datetime
#  updated_at                  :datetime
#  overdue_count               :integer          default(0)
#  traffic_fee_centimes        :integer          default(0), not null
#  traffic_fee_currency        :string(255)      default("XAF"), not null
#  revenue_centimes            :integer          default(0), not null
#  revenue_currency            :string(255)      default("XAF"), not null
#  recurrent_fee_centimes      :integer          default(0), not null
#  recurrent_fee_currency      :string(255)      default("XAF"), not null
#  ps_transaction_fee_centimes :integer          default(0), not null
#  ps_transaction_fee_currency :string(255)      default("XAF"), not null
#

class MerchantMonthlyStat::MonthlyHistoryForAdmin < MerchantMonthlyStat

  extend DateUtil

  # Get month_count from start_date, for example, when date is August 15, 2015, month_count is 6
  # it will get stats of August, July, June, May, April, March, 6 months in total
  def self.get_stats_for(start_date, month_count)
    year_months = get_year_months(start_date, month_count)
    stats = self.joins(:merchant).select_sum('year, month, users.currency as currency').where("(year, month) in (#{year_months.join(',')})").group("year, month, users.currency").order('year desc, month desc')

    # the key is array [year, month], the value is an instance of MerchantMonthlyStat::MonthlyHistoryForAdmin
    year_month_to_stat = {}
    stats.each do |stat|
      year_month = [stat.year, stat.month]
      year_month_to_stat[year_month] ||= self.new(year: stat.year, month: stat.month)
      
      [:created_count, :ready_count, :paid_count, :partially_paid_count, :cancelled_count, 
        :failed_count, :processed_count, :expired_count, :overdue_count].each do |count_type|
        origin = year_month_to_stat[year_month].send(count_type)
        year_month_to_stat[year_month].send("#{count_type}=", origin + stat.send(count_type))
      end

      currency = stat.currency
      year_month_to_stat[year_month].revenues[currency] ||= Money.new(0, currency)
      year_month_to_stat[year_month].revenues[currency] += Money.new(stat.revenue_centimes, currency)

      year_month_to_stat[year_month].traffic_fees[currency] ||= Money.new(0, currency)
      year_month_to_stat[year_month].traffic_fees[currency] += Money.new(stat.traffic_fee_centimes, currency)

      year_month_to_stat[year_month].recurrent_fees[currency] ||= Money.new(0, currency)
      year_month_to_stat[year_month].recurrent_fees[currency] += Money.new(stat.recurrent_fee_centimes, currency)

      year_month_to_stat[year_month].ps_transaction_fees[currency] ||= Money.new(0, currency)
      year_month_to_stat[year_month].ps_transaction_fees[currency] += Money.new(stat.ps_transaction_fee_centimes, currency)

      year_month_to_stat[year_month].net_revenues[currency] ||= Money.new(0, currency)
      year_month_to_stat[year_month].net_revenues[currency] += Money.new(stat.revenue_centimes - stat.ps_transaction_fee_centimes - stat.traffic_fee_centimes, currency)
    end

    year_month_to_stat.values
  end

  attr_accessor :revenues, :traffic_fees, :recurrent_fees, :ps_transaction_fees, :net_revenues

  after_initialize :reset_revenues_and_fees

  private

    def reset_revenues_and_fees
      self.revenues = {}
      self.traffic_fees = {}
      self.recurrent_fees = {}
      self.ps_transaction_fees = {}
      self.net_revenues = {}
    end
end
