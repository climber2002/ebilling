# == Schema Information
#
# Table name: fee_strategies
#
#  id                      :integer          not null, primary key
#  type                    :string(255)
#  fixed_amount_centimes   :integer          default(0), not null
#  fixed_amount_currency   :string(255)      default("XAF"), not null
#  percentage              :float
#  maximum_amount_centimes :integer          default(0), not null
#  maximum_amount_currency :string(255)      default("XAF"), not null
#  merchant_id             :integer
#  created_at              :datetime
#  updated_at              :datetime
#  disable_recurrent       :boolean          default(FALSE)
#

class FeeStrategy < ActiveRecord::Base

  belongs_to :merchant, class_name: 'User'
  
  monetize :fixed_amount_centimes, with_model_currency: :fixed_amount_currency
  monetize :maximum_amount_centimes, with_model_currency: :maximum_amount_currency

  validates :fixed_amount, :numericality => {
      greater_than: 0.0,
  }, :if => :fixed_strategy?

  validates :percentage, :numericality => {
      greater_than: 0.0,
  }, :if => :variable_strategy?

  validates :maximum_amount, :numericality => {
      greater_than: 0.0,
  }, :if => :variable_strategy?


  def calculate_monthly_traffic_fee(paid_count, fee_stats_in_cents)
    return Money.new(fee_stats_in_cents, merchant.currency)
  end

  def calculate_monthly_recurrent_fee(paid_count)
    currency = Currency.find_by_code(merchant.currency)
    if currency.present? && (!disable_recurrent)
      fee_bucket = currency.fee_buckets.where("min_transactions <= ? and max_transactions >= ?", paid_count, paid_count).first
      if fee_bucket.present?
        return Money.new(fee_bucket.fee_value * 100, merchant.currency)
      end
    end
    Money.new(0, merchant.currency)
  end

  private

    def fixed_strategy?
      self.type == 'FeeStrategy::FixedStrategy'
    end

    def variable_strategy?
      self.type == 'FeeStrategy::VariableStrategy'
    end
end
