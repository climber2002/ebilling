# == Schema Information
#
# Table name: batch_files
#
#  id          :integer          not null, primary key
#  file        :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#  user_id     :integer
#  state       :string(255)
#  provisioned :integer          default(0)
#  succeeded   :integer          default(0)
#  failed      :integer          default(0)
#

class BatchFile < ActiveRecord::Base


  #  Hash for parameters to be extracted from excel file
  KEY_MAPPINGS = {:merchant_name_m => :username,
                    :payer_msisdn_m => :payer_msisdn, :amount_m => :amount,
                    :short_description_m => :short_description, 
                    :payer_name_o => :payer_name, :payer_address_o => :payer_address,
                    :payer_city_o => :payer_city, 
                    :expiration_date_o => :expire_at, :external_reference_o => :merchant_internal_ref,
                    :payer_email_m => :payer_email, :description_o => :description, 
                    :accept_partial_payment_o => :accept_partial_payment,
                    :minimum_amount_o => :minimum_amount,
                    :payer_id_o => :payer_id,
                    :payer_code_o => :payer_code,
                    :email_o => :email,
                    :sms_o => :sms,
                    :due_date_o => :due_date,
                    :additional_info_o => :additional_info,
                    :data0_o => :data0,
                    :data1_o => :data1,
                    :data2_o => :data2,
                    :data3_o => :data3,
                    :data4_o => :data4,
                    :data5_o => :data5,
                    :data6_o => :data6,
                    :data7_o => :data7,
                    :data8_o => :data8,
                    :data9_o => :data9
                     };


  #  Mounts the given uploader on the given column. 
  #  This means that assigning and reading from the column will upload and retrieve files.
  mount_uploader :file, FileUploader

  #  Specifies a one-to-one association with user class.
  belongs_to :user

  #  Specifies a one-to-many association with ebills class
  has_many :e_bills

  #  State machine for start and finish events for provision state
  state_machine initial: :created do
    event :start do
      transition :created => :in_process
    end

    event :finish do
      transition :in_process => :finished
    end
  end

  #  Increments succeeded and provisioned count by 1
  def inc_succeeded
    self.succeeded += 1
    self.provisioned += 1
  end

  #  Increments failed and provisioned count by 1 
  def inc_failed
    self.failed += 1
    self.provisioned +=1
  end


  #  1. Checks if user has permission to provision the file 
  #  2. Start 
  #  3. Keeps old provisioned number
  #  4. Initiates provisioned count to 0
  #  5. Initiates excel to nil
  #  6. Checks extension of current file, and initiates a roo object on the current file  
  #  7. Returns back if excel is still nil
  #  8. Reads the last column number to get column count
  #  9. Defines two arrays to hold names of columns and converted names of columns to match database column names 
  #  10. Iterates over column names and stores them 
  #  11. Gets number of rows and scans rows one by one to prepare ebill if rows are greater than 1 in number
  #  12. Saves ebill if provision is correct, else shows error message 
  #  13. Saves every 100 provisions  
  def provision

    unless user.can_provision?
      logger.error("The user can't provision. #{user}")
      return
    end
    start
    logger.info("Begin provision #{file.current_path}")

    old_provisioned = provisioned # keep old provisioned number
    provisioned_count = 0

    excel = nil
    if file.current_path.ends_with? ".xlsx"
      excel = Roo::Excelx.new(file.current_path)
    elsif file.current_path.ends_with? ".xls"
      excel = Roo::Excel.new(file.current_path)
    end

    unless excel
      finish
      return
    end

    # Read header
    column_count = excel.last_column

    header = []
    original_header = []
    1.upto(column_count) do |column|
      column_name = excel.cell(1, column)
      header << KEY_MAPPINGS[column_name.delete(" ").downcase.to_sym]
      original_header << column_name
    end
    last_row = excel.last_row
    if last_row > 1
      2.upto(last_row) do |row|       # start from the second row
        attributes = {}
        # original attributes is just for audit trail
        original_attributes = {}

        1.upto(column_count) do |column|
          value = excel.cell(row, column)
          if value.class == Float
            value = value.to_i.to_s
          end
          attributes[header[column - 1]] = value
          original_attributes[original_header[column - 1]] = value
          if(user.has_profile? Profile::MERCHANT)
            attributes.merge(username: user.username)
          end
        end
        e_bill = e_bills.build attributes
        e_bill.params = original_attributes
        e_bill.operator = self.user.username

        if e_bill.create true
          inc_succeeded
        else
          logger.error("provision not correct: #{e_bill.errors.full_messages}")
          e_bills.delete e_bill
          inc_failed
        end

        # save every 100 provision
        provisioned_count += 1
        if provisioned_count % 100 == 0
          save
        end
      end
    end

    save
    logger.debug("batch file valid: #{errors.full_messages}")
    logger.info("finish batch file #{file}")
    finish
  end


end
