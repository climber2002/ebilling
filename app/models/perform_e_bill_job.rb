class PerformEBillJob < Struct.new(:action, :e_bill_id, :operator)

  #  1. Finds ebill by id 
  #  2. Assigns operator to ebill
  #  3. Assigns action to ebill and triggers action 
  def perform
    e_bill = EBill.find e_bill_id
    e_bill.operator = operator
    e_bill.send(action, true)
  end
end