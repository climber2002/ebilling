class ReceiptGenerator

  attr_reader :odf_filepath

  def initialize ebill, template
    @ebill = ebill
    @template = template
  end

  def generate
    report = ODFReport::Report.new(@template) do |r|
      r.add_field :billing_id, @ebill.bill_id
      r.add_field :amount, @ebill.amount

      r.add_field :created_at, @ebill.created_at.strftime("%d/%m/%Y")
      r.add_field :due_at, @ebill.due_date? ? @ebill.due_date.strftime("%d/%m/%Y") : ""
      r.add_field :expire_at, @ebill.expire_at? ? @ebill.expire_at.strftime("%d/%m/%Y") : ""

      r.add_field :cur, @ebill.user.currency.to_s
      r.add_field :short_description, @ebill.short_description.to_s
      r.add_field :description, @ebill.description.to_s
      r.add_field :ext_ref, @ebill.external_reference.to_s
      r.add_field :add_info, @ebill.additional_info.to_s

      r.add_field :paid_amount, @ebill.amount_paid.to_s
      r.add_field :amount_to_pay, (@ebill.amount.to_i - @ebill.amount_paid.to_i).to_s
      r.add_field :paid_at, @ebill.paid_at.present? ? @ebill.paid_at.strftime("%d/%m/%Y %H:%M") : ""

      r.add_field :payment_system, @ebill.payment_system.try(:username).to_s
      r.add_field :ps_transaction, @ebill.ps_transaction_id.to_s

      r.add_image :merchant_logo, @ebill.user.logo.file.path if @ebill.user.logo?
      r.add_field :merchant_name, @ebill.user.try(:last_name).to_s
      r.add_field :merchant_first, @ebill.user.try(:first_name).to_s
      r.add_field :merchant_last, @ebill.user.try(:last_name).to_s
      r.add_field :merchant_address, @ebill.user.address.to_s
      r.add_field :merchant_msisdn, @ebill.user.msisdn.to_s
      r.add_field :merchant_email, @ebill.user.email.to_s
      r.add_field :merchant_website, @ebill.user.website.to_s
      r.add_field :merchant_greetings, @ebill.user.greeting_message.to_s
      r.add_image :merchant_signature, @ebill.user.signature.file.path if @ebill.user.signature?
      r.add_field :merchant_legal_info, @ebill.user.legal_info
      r.add_field :merchant_city, @ebill.user.city.to_s
      r.add_field :merchant_tax_rate, @ebill.user.tax_rate.to_s
      r.add_field :last_paid_amount, @ebill.last_paid_amount.to_f
      
      r.add_field :customer_name, @ebill.payer_name
      r.add_field :customer_msisdn, @ebill.payer_msisdn.to_s
      r.add_field :customer_email, @ebill.payer_email.to_s
      r.add_field :customer_address, @ebill.payer_address.to_s
      r.add_field :customer_city, @ebill.payer_city.to_s  

      r.add_field :payer_id, @ebill.payer_id.to_s
      r.add_field :payer_code, @ebill.payer_code.to_s
      r.add_field :data0, @ebill.data0.to_s
      r.add_field :data1, @ebill.data1.to_s
      r.add_field :data2, @ebill.data2.to_s
      r.add_field :data3, @ebill.data3.to_s
      r.add_field :data4, @ebill.data4.to_s
      r.add_field :data5, @ebill.data5.to_s
      r.add_field :data6, @ebill.data6.to_s
      r.add_field :data7, @ebill.data7.to_s
      r.add_field :data8, @ebill.data8.to_s
      r.add_field :data9, @ebill.data9.to_s

      r.add_field :today, Date.today.strftime("%d/%m/%Y")

    end

    report.generate do |odfpath|
      @odf_filepath = odfpath

      puts "odt file : #{odfpath}, output_dir #{output_dir}"

      # system("soffice --headless --invisible --convert-to pdf --outdir #{output_dir} #{@odfpath}")
      command = "soffice --headless --invisible --convert-to pdf --outdir #{output_dir} #{odfpath}"
      puts "command #{command}"
      system(command)

      yield pdf_filepath
    end
  end

  def pdf_filepath
    @odf_filepath[0, @odf_filepath.rindex(?.)] << ".pdf"
  end

  def output_dir
    Pathname.new(@odf_filepath).dirname.to_s
  end

end