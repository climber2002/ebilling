module SmsSender

  def self.send_sms to, message
    if Settings.disable_sms_and_email?
      Rails.logger.info "SMS sending is disabled"
      return
    end

    url = generate_sms to, message
    resource = RestClient::Resource.new url,
                :user => Settings.sms_username,
                :password => Settings.sms_password
    resource.get
    Rails.logger.info "SMS is sent: #{url}"
  rescue => e
    Rails.logger.error "Error when sending sms #{url}, exception: #{e}"
    raise e
  end

  def self.generate_sms to, message
    url = Settings.sms_server_url
    params = {}
    params['to'] = to
    params['text'] = message

    if Settings.sms_params.present?
      Settings.sms_params.split('&').each do |key_value|
        splitted = key_value.split('=')
        if splitted.size == 2
          params[splitted[0]] = splitted[1]
        end
      end
    end

    full_url = "#{url}?"
    params.each do |key, value|
      full_url << "#{key}=#{Rack::Utils.escape(value)}&"
    end

    full_url = full_url[0..-2]
    full_url
  end

end