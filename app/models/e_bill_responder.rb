class EBillResponder < ActionController::Responder

  #  1. Returns hash containing client transaction id , server transaction id, Errors and not processable message
  def json_resource_errors
    { :client_transaction_id => resource.client_transaction_id,
      :server_transaction_id => resource.server_transaction_id,
      :message => I18n.t("api.errors.not_processable"),
      :errors => resource.errors }
  end

  def api_behavior(error)
    raise error unless resourceful?

    if get?
      display resource
    elsif post?
      display resource, :status => :created, :location => api_location
    elsif put? || patch?
      display resource, :status => :ok  
    else
      head :no_content
    end
  end

  def display_errors
    controller.render format => resource_errors, :status => error_code
  end

  private

    def error_code
      resource.try(:error_code).present? ? resource.error_code : 422
    end
end