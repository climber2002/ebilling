# == Schema Information
#
# Table name: mobile_traces
#
#  id          :integer          not null, primary key
#  device_id   :string(255)
#  stack_trace :text
#  user_id     :integer
#  created_at  :datetime
#  updated_at  :datetime
#

class MobileTrace < ActiveRecord::Base

  #  One to one relationship with user class 
  belongs_to :user

  #  Validates presence of device id 
  validates :device_id, presence: true
  
end
