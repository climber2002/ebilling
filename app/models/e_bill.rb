# == Schema Information
#
# Table name: e_bills
#
#  id                          :integer          not null, primary key
#  user_id                     :integer
#  payer_email                 :string(255)
#  payer_msisdn                :string(255)
#  amount                      :integer
#  currency                    :string(255)
#  state                       :string(255)
#  expired_at                  :datetime
#  schedule_at                 :datetime
#  external_reference          :string(255)
#  additional_info             :string(255)
#  description                 :string(255)
#  reason                      :string(255)
#  sms                         :boolean          default(FALSE)
#  email                       :boolean          default(FALSE)
#  created_at                  :datetime
#  updated_at                  :datetime
#  sms_message                 :string(255)
#  merchant_internal_ref       :string(255)
#  sent_at                     :datetime
#  paid_at                     :datetime
#  cancelled_at                :datetime
#  processed_at                :datetime
#  failed_at                   :datetime
#  expire_at                   :datetime
#  short_description           :string(255)
#  due_date                    :date
#  bill_id                     :string(255)
#  batch_file_id               :integer
#  payer_name                  :string(255)
#  payer_address               :string(255)
#  payer_city                  :string(255)
#  accept_partial_payment      :boolean          default(FALSE)
#  minimum_amount              :integer
#  amount_paid                 :integer          default(0)
#  partially_paid_at           :datetime
#  overdue_at                  :datetime
#  payment_system_id           :integer
#  ps_transaction_id           :string(255)
#  traffic_fee_centimes        :integer          default(0), not null
#  traffic_fee_currency        :string(255)      default("XAF"), not null
#  search_field                :text
#  payer_id                    :string(255)
#  payer_code                  :string(255)
#  data0                       :string(255)
#  data1                       :string(255)
#  data2                       :string(255)
#  data3                       :string(255)
#  data4                       :string(255)
#  data5                       :string(255)
#  data6                       :string(255)
#  data7                       :string(255)
#  data8                       :string(255)
#  data9                       :string(255)
#  ps_transaction_fee_centimes :integer          default(0), not null
#  ps_transaction_fee_currency :string(255)      default("XAF"), not null
#  last_paid_amount_centimes   :integer          default(0), not null
#  last_paid_amount_currency   :string(255)      default("XAF"), not null
#

class EBill < ActiveRecord::Base

  MSISDN_REGEX = /\A[+]?[0-9]{7,14}\z/
  MSISDN_MESSAGE = "Not a valid MSISDN, the number should be between 7 to 14 characters"
  

  validates :payer_email, presence: { message: "Must specify Email as you select the email notification"},
                    email: { message: "Not a valid Email", allow_blank: true},
                    :if => Proc.new { |e_bill| e_bill.email? }

  validates :payer_msisdn, presence: true, msisdn: { with: true, allow_blank: true }

  before_validation :set_default_due_date_if_necessary
  validates :due_date, presence: true

  validates :amount, presence: true, numericality: true
  validates :description, length: { maximum: 200 }

  validates :client_transaction_id, length: { maximum: 35, allow_blank: true }
  validates :short_description, presence: true

  # disable this for batch provisioning
  # validate :must_have_notification_method

  # user is merchant
  belongs_to :user,:counter_cache => true
  validates :user_id, presence: true

  belongs_to :payment_system, class_name: 'User'

  validates :minimum_amount, presence: true, :if => :accept_partial_payment?
  validates :minimum_amount, numericality: { only_integer: true }, :if => :accept_partial_payment?, :allow_blank => true
  validate :minimum_amount_within_amount, :if => ->(ebill) { ebill.accept_partial_payment? && ebill.amount.present? }

  belongs_to :batch_file

  monetize :traffic_fee_centimes, with_model_currency: :traffic_fee_currency
  monetize :ps_transaction_fee_centimes, with_model_currency: :ps_transaction_fee_currency
  monetize :last_paid_amount_centimes, with_model_currency: :last_paid_amount_currency

  before_validation :set_merchant_currency_to_e_bill

  after_create :generate_bill_id

  include Searchable
  searchable :payer_email, :payer_msisdn, :merchant_internal_ref, 
    :additional_info, :description, :bill_id, after_create: true
  after_create :save_additional_fields

  def self.top10 state = nil
    if state.to_s == 'paid'
      count_part = "count(case when state='paid' or state='processed' then 1 else null end)"
    elsif state
      count_part = "count(case when state='#{state}' then 1 else null end)"
    else
      count_part = "count(1)"
    end

    select("e_bills.user_id as user_id, users.username as username,
       #{count_part} as count")
      .joins(:user => :profiles)
      .where("profiles.name = ?", Profile::MERCHANT)
      .group('username, e_bills.user_id').order("count desc")
  end

  def can_send_receipt?
    state == "paid" || state == "processed"
  end

  def external_reference= ref
    self.merchant_internal_ref = ref
  end

  def external_reference
    self.merchant_internal_ref
  end

  def failure_reason= failure_reason
    self.reason = failure_reason
  end

  ## remove the first 555 and convert to integer
  def self.find_by_bill_id bill_id
    find bill_id[3, 13].to_i
  end

  def payment_system_name
    payment_system.try(:last_name)
  end

  scope :by_state, ->(state) do
    where("state = ?", state)
  end
  scope :by_user_id, ->(user_id) { where("user_id = ?", user_id) }
  scope :batch_file_id, ->(batch_file_id) { where("batch_file_id = ?", batch_file_id) }
  scope :overdue, ->() { where("due_date < ? and (state = 'created' or state = 'ready')", Date.today ) }

  def must_have_notification_method
    if (!self.sms? && !self.email?)
      errors[:notification_method] << "At least one notification method must be selected."
    end
  end

  before_validation :set_default_currency_if_necessary

  attr_accessor :client_transaction_id, :server_transaction_id, :params, :operator

  def operator_for_audit
    unless self.operator.blank?
      operator
    else
      user.username
    end
  end

  def payee_id= payee_id
    self.user = User.find_by msisdn:payee_id
  end

  def payee_id
    user.present? ? user.msisdn : ''
  end

  def merchant_name
    user ? user.username : ""
  end

  def get_notification_params param_names
    result = param_names.inject({}) do |hash, param_name|
      attr_name = User::NOTIFICATION_PARAMS_MAPPING[param_name]
      if attr_name
        result = send(attr_name)
        hash[param_name] = result.instance_of?(Money) ? result.to_f : result.to_s
      end
      hash
    end

    user.additional_notification_params.to_s.split(/&|;/).each do |param|
      name, value = param.strip.split('=')
      value ||= ''
      result[name] = value
    end
    result  
  end

  EBILLS_STATE_ERROR_CODES = { "paid" => ["ERR-010", "api.errors.already_paid"],
                               "expired" => ["ERR-011","api.errors.expired"],
                               "cancelled" => ["ERR-012","api.errors.cancelled"],
                               'created' => ["ERR-013","api.errors.not_yet_sent"],
                               "failed" => ["ERR-014","api.errors.error_state"]
                                }

  def payment_system_name
    payment_system.try(:username) || ""
  end

  def self.log_audit_trail(e_bill, transition = nil, success = true)
    status = success ? "success" : "failed"

    if transition && transition.event == :resend
      action = "resendEBill"
    else
      action = transition ?
                          "updateEBillTo#{transition.to_name.capitalize}"
                          : "createEBill"
    end

    audit_trail = AuditTrail.create(:client_transaction_id => e_bill.client_transaction_id,
                      :from => transition ? transition.from_name : "",
                      :to => transition ? transition.to_name : e_bill.state,
                      :bill_id => e_bill.bill_id,
                      :payer_id => e_bill.payer_id,
                      :context => e_bill.context_message,
                      :action => action,
                      :operator => e_bill.operator_for_audit,
                      :status => status,
                      :params => e_bill.params ? e_bill.params.to_s : "",
                      :error_messages => e_bill.errors.empty? ? nil : e_bill.errors.messages.to_s )

    e_bill.server_transaction_id = audit_trail.server_transaction_id
  end

  def generate_failed_update_audit_trail from, to
    audit_trail = AuditTrail.create(:client_transaction_id => self.client_transaction_id,
      :from => from, :to => to, :bill_id => self.bill_id, :payer_id => self.payer_id,
      :status => "failed", :action => "updateEBillTo#{to.capitalize}",
      :params => self.params ? self.params.to_s : "",
      :error_messages => self.errors.messages.to_s )
    self.server_transaction_id = audit_trail.server_transaction_id
  end

  def generate_verify_audit_trail success, error_messages = nil
    status = success ? "success" : "failed"
    audit_trail = AuditTrail.create(:client_transaction_id => self.client_transaction_id,
      :action => "queryEBill", :status => status, :bill_id => self.bill_id,
      :params => self.params ? self.params.to_s : "",
      :error_messages => error_messages,
      :operator => self.operator)
    self.server_transaction_id = audit_trail.server_transaction_id
  end

  def generate_bill_id
    length = Settings.bill_id_length.to_i
    length = 10 if length <= 0
    self.bill_id = "555%0#{length - 3}d" % id if id
  end

  def sms_message
    sms = Settings.sms_template.to_s
    transform_message sms
  end

  def sms_instructions_message
    sms = Settings.sms_instructions_template.to_s
    transform_message sms
  end

  def send_paid_message
    send_paid_sms_message if self.sms?
    send_receipt_email_message if self.email?
  end

  def send_paid_sms_message
    audit_trail_params = {
        :action => "sendReceiptSMS", :bill_id => bill_id,
        :params => {'msisdn' => self.payer_msisdn, 
          'sms' => paid_sms_message }.to_s
        }
    SmsSender.send_sms self.payer_msisdn, paid_sms_message
    AuditTrail.create(audit_trail_params.merge(status: 'success'))
  rescue => e
    Rails.logger.warn "can't send sms: #{e.message}"
    audit_trail_params = {
        :action => "sendReceiptSMS", :bill_id => bill_id,
        :params => {'msisdn' => self.payer_msisdn, 
          'sms' => paid_sms_message }.to_s
        }
    AuditTrail.create(audit_trail_params.merge(status: 'failed', error_messages: e.message))
  end

  def send_receipt_email_message
    if Settings.disable_sms_and_email?
      Rails.logger.info "Email sending is disabled"
      return
    end

    email = EmailSender.e_bill_receipt self
    email.deliver
  end

  def send_sms_message
    sms = self.sms_message
    SmsSender.send_sms self.payer_msisdn, sms
  end

  def send_sms_instructions_message
    sms = self.sms_instructions_message
    SmsSender.send_sms self.payer_msisdn, sms
  end

  def email_message
    email = Settings.email_template.to_s
    transform_message email
  end

  def email_subject
    subject = Settings.email_subject_template.to_s
    transform_message subject
  end

  def paid_sms_message
    transform_message Settings.paid_sms_template.to_s
  end

  def paid_email_subject
    message = transform_message Settings.paid_email_subject_template.to_s
    Rails.logger.debug "paid email subject #{message}"
    message
  end

  def paid_email_message
    message = transform_message Settings.paid_email_template.to_s
    Rails.logger.debug "paid email message #{message}"
    message
  end

  def context_message
    case self.state
    when "ready"
      "Sent message: #{self.sms_message}"
    else
      ""
    end
  end

  def ui_state_events
    self.state_events - [:expire, :pay, :invalidate, :pay_partial, :overdue]
  end

  def username= username
    self.user = User.find_by username: username
  end

  def username
    self.user.username if self.user
  end

  def payee_name
    self.user.present? ? self.user.username : ''
  end

  def to_json_for_soap
    {client_transaction_id: client_transaction_id,
      server_transaction_id: server_transaction_id,
      e_bill: {id: bill_id, payer_id: payer_msisdn,
          payee_id: payee_id, payee_name: user.username,
          amount: amount,
          currency: currency, state: state,
          created_at: created_at, expire_at: expire_at, expiry_period: expiry_period,
          schedule_at: schedule_at, updated_at: updated_at,
          external_reference: external_reference,
          additional_info: additional_info, description: description,
          reason: reason, message: sms_message}}
  end

  def as_json(options = {})
    e_bill_hash = { bill_id: bill_id, payer_msisdn: payer_msisdn,
          payer_email: payer_email,
          payee_id: payee_id, payee_name: user.username,
          amount: amount,
          currency: currency, state: state,
          created_at: created_at, expire_at: expire_at, expiry_period: expiry_period,
          schedule_at: schedule_at, updated_at: updated_at,
          short_description: short_description, due_date: due_date,
          external_reference: external_reference,
          additional_info: additional_info, description: description,
          reason: reason, payer_name: payer_name,
          payer_address: payer_address, payer_city: payer_city, 
          accept_partial_payment: accept_partial_payment, minimum_amount: minimum_amount,
          amount_paid: amount_paid, ps_transaction_id: ps_transaction_id,
          payment_system_name: payment_system_name,
          payer_id: payer_id, payer_code: payer_code, data0: data0,
          data1: data1, data2: data2, data3: data3, data4: data4,
          data5: data5, data6: data6, data7: data7, data8: data8,
          data9: data9 }
    if server_transaction_id
      {
        client_transaction_id: client_transaction_id,
        server_transaction_id: server_transaction_id,
        e_bill: e_bill_hash
      }
    else
      e_bill_hash
    end
  end

  def send_invoice_sms
    audit_trail_params = {
        :action => "sendInvoiceSMS", :bill_id => bill_id,
        :params => {'msisdn' => self.payer_msisdn, 
          'sms' => sms_message }.to_s
        }
    send_sms_message
    send_sms_instructions_message
    AuditTrail.create(audit_trail_params.merge(status: 'success'))
  rescue => e
    Rails.logger.warn "can't send sms: #{e.message}"
    audit_trail_params = {
        :action => "sendInvoiceSMS", :bill_id => bill_id,
        :params => {'msisdn' => self.payer_msisdn, 
          'sms' => sms_message }.to_s
        }
    AuditTrail.create(audit_trail_params.merge(status: 'failed', error_messages: e.message))
  end

  #  1. Finds mapping from database with help of merchant id and payment system id 
  #  2. Returns the mapping object
  def merchant_payment_system_mapping
    @merchant_ps_mapping ||= MerchantPaymentSystemMapping.find_by(merchant_id: user_id, 
          payment_system_id: payment_system_id)
  end

  def calculate_ps_transaction_fee
    if (mapping = merchant_payment_system_mapping) && (mapping.ps_percentage.present?)
      Money.new(self.amount_paid * mapping.ps_percentage, user.currency)
    else
      Money.new(0, user.currency)
    end
  end

  def calculate_ebilling_fee
    if (mapping = merchant_payment_system_mapping) && (mapping.ebilling_percentage.present?)
      Money.new(self.amount_paid * mapping.ebilling_percentage, user.currency)
    else
      Money.new(0, user.currency)
    end
  end
  

  state_machine initial: :created do
    after_transition do |e_bill, transition|
      EBill.log_audit_trail(e_bill, transition)
    end

    before_transition any => :ready do |e_bill, transition|
      begin
        if Settings.disable_sms_and_email?
            Rails.logger.info "Email and sms sending is disabled"
        else
          if e_bill.sms?
            e_bill.send_invoice_sms
          end

          if e_bill.email?
            Rails.logger.debug "send email to #{e_bill.payer_email}"
            email = EmailSender.e_bill_invoice e_bill
            email.deliver
          end
        end
      rescue => e 
        e_bill.errors[:send] = e.message
        EBill.log_audit_trail e_bill, transition, false
        EBill.connection.commit_db_transaction
        false
      end
    end

    before_transition any => :ready do |e_bill, transition|
      e_bill.sent_at = DateTime.now.to_time
    end

    before_transition any => :paid do |e_bill, transition|
      e_bill.paid_at = DateTime.now.to_time
    end

    before_transition any => :partially_paid do |e_bill, transition|
      e_bill.partially_paid_at = DateTime.now.to_time
      e_bill.paid_at = DateTime.now.to_time
    end

    before_transition any => :cancelled do |e_bill, transition|
      e_bill.cancelled_at = DateTime.now.to_time
    end

    before_transition any => :failed do |e_bill, transition|
      e_bill.failed_at = DateTime.now.to_time
    end

    before_transition any => :processed do |e_bill, transition|
      e_bill.processed_at = DateTime.now.to_time
    end

    before_transition any => :expired do |e_bill, transition|
      e_bill.expired_at = DateTime.now.to_time
    end


    event :trigger do
      transition :created => :ready
    end

    event :pay_partial do 
      transition [:ready, :failed, :partially_paid] => :partially_paid
    end

    event :pay do
      transition [:ready, :failed, :partially_paid, :overdue] => :paid
    end

    event :cancel do
      transition [:overdue, :ready] => :cancelled
    end

    event :invalidate do
      transition :ready => :failed
    end

    event :expire do
      transition [:ready, :failed, :created] => :expired
    end

    event :process do
      transition :paid => :processed
    end

    event :resend do
      transition [:overdue, :ready] => :ready
    end

    event :overdue do 
      transition [:created, :ready] => :overdue
    end
  end

  def self.all_states
    state_machine.states.map &:name
  end
  # for states, the client can call EBill.created, EBill.ready
  # to get the corresponding ebills by state
  all_states.each do |state|
    define_singleton_method state do
      by_state state
    end
  end

  def create audit_failure = false
    saved = self.save
    if saved
      trigger_after_save_tasks
      EBill.log_audit_trail(self, nil)
    elsif audit_failure
      EBill.log_audit_trail(self, nil, false)
    end
    saved
  end

  def trigger_after_save_tasks
    # unless self.expire_at.nil?
    #   Delayed::Job.enqueue PerformEBillJob.new(:expire, self.id, self.operator), :run_at => self.expire_at
    # end
    if self.expiry_period && self.expiry_period > 0
      Delayed::Job.enqueue PerformEBillJob.new(:expire, self.id, self.operator), :run_at => Time.now + self.expiry_period.minutes
    end

    if self.schedule_at.nil?
      Delayed::Job.enqueue PerformEBillJob.new(:trigger, self.id, self.operator)
    else
      Delayed::Job.enqueue PerformEBillJob.new(:trigger, self.id, self.operator), :run_at => self.schedule_at
    end
  end

  def transform_message message
    result = message.gsub("{0}", username.to_s)
    result = result.gsub("{1}", bill_id.to_s)
    result = result.gsub("{2}", amount.to_s)

    if short_description
      result = result.gsub("{3}", short_description.to_s) 
    else
      result = result.gsub("{3}", "")
    end

    if due_date
      result = result.gsub("{4}", due_date.to_s(:db))
    else
      result = result.gsub("{4}", "")
    end

    result = result.gsub("{5}", external_reference.to_s)
    result = result.gsub("{6}", payee_id.to_s)
    result = result.gsub("{7}", payer_msisdn.to_s)

    result = result.gsub("{8}", client_transaction_id.to_s)
    if user
      result = result.gsub("{9}", user.additional_notification_params.to_s)
    else
      result = result.gsub("{9}", "")
    end
    result = result.gsub("{10}", amount_paid.to_s)

    result
  end

  private
  def set_default_due_date_if_necessary
    unless self.due_date.present?
      self.due_date = Date.today
    end
  end

  def set_default_currency_if_necessary
    self.currency = "XAF" if self.currency.blank?
  end

  def minimum_amount_within_amount
    return unless minimum_amount.present?

    if minimum_amount <= 0
      errors.add(:minimum_amount, 'should be more than 0')
    elsif minimum_amount > amount
      errors.add(:minimum_amount, 'should be less than or equal to amount')
    end
  end

  def set_merchant_currency_to_e_bill
    if user.present?
      self.currency = user.currency
      self.traffic_fee_currency = user.currency
    end
  end

  def save_additional_fields
    save
  end
end
