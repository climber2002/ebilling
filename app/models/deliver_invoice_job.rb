class DeliverInvoiceJob < Struct.new(:e_bill_id)

  #  1. Checks if sms and email are disabled 
  #  2. Finds ebill from id 
  #  3. Prepares invoice from ebill 
  #  4. Delivers invoice through email sender 
  def perform
    if Settings.disable_sms_and_email?
      Rails.logger.info "Email sending is disabled"
      return
    end
    
    e_bill = EBill.find e_bill_id
    sender = EmailSender.e_bill_invoice e_bill
    sender.deliver
  end
end