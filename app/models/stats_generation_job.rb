class StatsGenerationJob

  def perform
    MerchantMonthlyStatsPopulator.new.populate_for_yesterday
    PaymentSystemMonthlyStatsPopulator.new.populate_for_yesterday
  end
end