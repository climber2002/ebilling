# == Schema Information
#
# Table name: mobiles
#
#  id               :integer          not null, primary key
#  imei             :string(255)
#  user_id          :integer
#  otp              :string(255)
#  otp_last_sent_at :datetime
#  otp_confirmed_at :datetime
#  pin_hash         :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#

class Mobile::AsResetPin < Mobile

  def reset_otp
    generate_otp
    reset_otp_confirmed_at
    save
    send_reset_pin_email
  end

  private

    def generate_otp
      self.otp = "%06d" % SecureRandom.random_number(999999)
      self.otp_last_sent_at = Time.now
    end

    def reset_otp_confirmed_at
      self.otp_confirmed_at = nil
    end

    def send_reset_pin_email
      email = MobileRegistration.forget_pin self
      email.deliver
    end

    handle_asynchronously :send_reset_pin_email
end
