# == Schema Information
#
# Table name: mobiles
#
#  id               :integer          not null, primary key
#  imei             :string(255)
#  user_id          :integer
#  otp              :string(255)
#  otp_last_sent_at :datetime
#  otp_confirmed_at :datetime
#  pin_hash         :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#

class Mobile::AsVerify < Mobile

  validate :otp_match
  validate :otp_within_period

  attr_accessor :otp_confirmation

  before_save :set_otp_confirmed

  private

    def otp_match
      if self.otp != self.otp_confirmation
        errors.add(:otp, "doesn't match")
      end
    end

    def otp_within_period
      if self.otp_last_sent_at + 15.minutes < Time.now
        errors.add(:otp, "already expired")
      end
    end

    def set_otp_confirmed
      self.otp_confirmed_at = Time.now
      self.otp = nil
      self.otp_last_sent_at = nil
    end
end
