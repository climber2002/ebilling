# == Schema Information
#
# Table name: mobiles
#
#  id               :integer          not null, primary key
#  imei             :string(255)
#  user_id          :integer
#  otp              :string(255)
#  otp_last_sent_at :datetime
#  otp_confirmed_at :datetime
#  pin_hash         :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#

class Mobile::AsUpdatePin < Mobile

  attr_reader :pin

  validates :pin, format: { with: /\A\d\d\d\d\z/,  message: 'must be 4 digits' }

  def pin=(pin)
    @pin = pin
    self.pin_hash = BCrypt::Password.create(pin)
  end


end
