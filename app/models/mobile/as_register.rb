# == Schema Information
#
# Table name: mobiles
#
#  id               :integer          not null, primary key
#  imei             :string(255)
#  user_id          :integer
#  otp              :string(255)
#  otp_last_sent_at :datetime
#  otp_confirmed_at :datetime
#  pin_hash         :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#

class Mobile::AsRegister < Mobile

  before_create :generate_otp
  after_create :send_confirmation_email

  private

    def generate_otp
      self.otp = "%06d" % SecureRandom.random_number(999999)
      self.otp_last_sent_at = Time.now
    end

    def send_confirmation_email
      email = MobileRegistration.register_mobile self
      email.deliver
    end

    handle_asynchronously :send_confirmation_email

end
