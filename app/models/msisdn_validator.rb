class MsisdnValidator < ActiveModel::EachValidator

  #  pattern for validating msisdn 
  MSISDN_REGEX = /\A[+]?[0-9]{7,14}\z/

  #  Validates msisdn with pattern ,else shows invalid msisdn message
  def validate_each(record, attribute, value)
    unless value =~ MSISDN_REGEX
      record.errors[attribute] << (options[:message] || I18n.t("errors.messages.invalid_msisdn"))
    end
  end
end