module Searchable

  OR_CONDITION = " OR "

  extend ActiveSupport::Concern

  included do 
    scope :search, ->(term) {
        where('search_field like ?', "%#{term.to_s.downcase}%")
      }
  end

  module ClassMethods
    
    def searchable *columns
      options = columns.extract_options!

      define_method :store_search_field do 
        if respond_to?(:"search_field=")
          self.search_field = columns.map { |column_name| read_attribute(column_name)}.map(&:to_s).map(&:downcase).join(' ')
        end
      end
      
      if options[:after_create]     
        after_create :store_search_field
      else
        before_save :store_search_field
      end
    end
  end

end