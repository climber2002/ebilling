# == Schema Information
#
# Table name: merchant_global_stats
#
#  id                   :integer          not null, primary key
#  created_count        :integer          default(0)
#  ready_count          :integer          default(0)
#  paid_count           :integer          default(0)
#  partially_paid_count :integer          default(0)
#  cancelled_count      :integer          default(0)
#  failed_count         :integer          default(0)
#  processed_count      :integer          default(0)
#  expired_count        :integer          default(0)
#  merchant_id          :integer          not null
#  created_at           :datetime
#  updated_at           :datetime
#  overdue_count        :integer          default(0)
#

class MerchantGlobalStat < ActiveRecord::Base

  belongs_to :merchant, class_name: 'User'

  def self.total_count(options={})
    select_part = ['created_count', 'ready_count', 'paid_count', 'partially_paid_count', 'cancelled_count', 
     'failed_count', 'processed_count', 'expired_count', 'overdue_count'].map do |count_name|
      "sum(#{count_name}) as #{count_name}"
    end.join(', ')

    relation = select(select_part)
    if options[:merchant_id].present?
      relation = relation.where(:merchant_id => options[:merchant_id])
    end

    relation[0]
  end

  def self.top10
    select_part = ['created_count', 'ready_count', 'paid_count', 'partially_paid_count', 'cancelled_count', 
   'failed_count', 'processed_count', 'expired_count', 'overdue_count'].join(' + ')
    select_part = "(#{select_part}) as count, users.username, merchant_id"

    joins(:merchant).select(select_part).order('count desc').limit(10)
  end

  def self.top10_paid
    select_part = ['paid_count', 'processed_count'].join(' + ')
    select_part = "(#{select_part}) as paid_count, users.username, merchant_id"
    joins(:merchant).select(select_part).order('paid_count desc').limit(10)
  end

end
