# == Schema Information
#
# Table name: payment_system_monthly_stats
#
#  id                :integer          not null, primary key
#  payment_system_id :integer          not null
#  year              :integer          not null
#  month             :integer          not null
#  created_at        :datetime
#  updated_at        :datetime
#

class PaymentSystemMonthlyStat < ActiveRecord::Base

  belongs_to :payment_system, class_name: 'User'
  validates_presence_of :payment_system_id, :year, :month

  has_many :payment_system_monthly_items
end
