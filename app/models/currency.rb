# == Schema Information
#
# Table name: currencies
#
#  id         :integer          not null, primary key
#  code       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

class Currency < ActiveRecord::Base

  validates :code, uniqueness: true, presence: true,
    format: { with: /\A[a-zA-Z]{3}\z/, message: "Not valid currency" }

  has_many :fee_buckets

end
