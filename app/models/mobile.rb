# == Schema Information
#
# Table name: mobiles
#
#  id               :integer          not null, primary key
#  imei             :string(255)
#  user_id          :integer
#  otp              :string(255)
#  otp_last_sent_at :datetime
#  otp_confirmed_at :datetime
#  pin_hash         :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#

class Mobile < ActiveRecord::Base

  #  Validates presence and uniqueness of imei
  validates :imei, presence: true, uniqueness: true

  #  One to one relationship with user class
  belongs_to :user

  #  Validates presence of user id 
  validates :user_id, presence: true

  #  Returns true if otp is confirmed for the mobile 
  def confirmed?
    self.otp_confirmed_at.present?
  end
end
