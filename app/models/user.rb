# == Schema Information
#
# Table name: users
#
#  id                             :integer          not null, primary key
#  username                       :string(255)      default(""), not null
#  encrypted_password             :string(255)      default(""), not null
#  email                          :string(255)      default("")
#  reset_password_token           :string(255)
#  reset_password_sent_at         :datetime
#  remember_created_at            :datetime
#  sign_in_count                  :integer          default(0)
#  current_sign_in_at             :datetime
#  last_sign_in_at                :datetime
#  current_sign_in_ip             :string(255)
#  last_sign_in_ip                :string(255)
#  created_at                     :datetime
#  updated_at                     :datetime
#  first_name                     :string(255)
#  last_name                      :string(255)
#  msisdn                         :string(255)
#  city                           :string(255)
#  address                        :string(255)
#  e_bills_count                  :integer          default(0)
#  notification_url               :string(255)
#  notification_params            :string(255)      default([]), is an Array
#  notification_post              :boolean          default(TRUE)
#  legal_info                     :string(255)
#  greeting_message               :string(255)
#  email_notification             :boolean          default(FALSE)
#  website                        :string(255)
#  logo                           :string(255)
#  signature                      :string(255)
#  additional_notification_params :string(255)
#  currency                       :string(255)
#  payment_system_id              :integer
#  search_field                   :text
#  shared_key                     :string(255)
#  tax_rate                       :float
#  zipcode                        :string(255)
#  charge_payer                   :boolean          default(FALSE)
#

class User < ActiveRecord::Base
  NOTIFICATION_PARAMS_MAPPING = { 'billingid' => 'bill_id', 'merchantid' => 'payee_id',
        'customerid' => 'payer_id', 'transactionid' => 'ps_transaction_id', 'reference' => 'merchant_internal_ref',
        'payer_id' => 'payer_id', 'payer_code' => 'payer_code', 'paymentsystem' => 'payment_system_name', 'data0' => 'data0', 'data1' => 'data1',
        'data2' => 'data2', 'data3' => 'data3', 'data4' => 'data4', 'data5' => 'data5', 'data6' => 'data6',
        'data7' => 'data7', 'data8' => 'data8', 'data9' => 'data9', 'amount' => 'last_paid_amount' }
        
  NOTIFICATION_PARAMS = NOTIFICATION_PARAMS_MAPPING.keys

  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable
  # comment :registerable,
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable, :timeoutable

  validates :username, presence: true, uniqueness: true, format: { without: /\s/ }
  validates :last_name, presence: true
  validates :msisdn, presence: true, msisdn: { with: true, allow_blank: true}
  validates :currency, presence: true, format: { with: /\A[a-zA-Z]{3}\z/, message: "Not valid currency" }, :if => :has_merchant_profile?


  has_and_belongs_to_many :profiles, uniq: true
  has_many :roles, through: :profiles

  has_many :e_bills, :dependent => :delete_all
  has_many :batch_files, :dependent => :delete_all

  has_many :mobiles, :dependent => :delete_all
  has_many :mobile_traces, :dependent => :delete_all

  scope :by_profile, ->(profile_name) { joins(:profiles).where("profiles.name = ?", profile_name) }
  scope :order_by_username, -> { order("username") }

  include Searchable
  searchable :username, :msisdn, :first_name, :last_name, :city, :address

  mount_uploader :logo, LogoUploader
  mount_uploader :signature, SignatureUploader

  has_one :fee_strategy, foreign_key: 'merchant_id', autosave: true
  accepts_nested_attributes_for :fee_strategy

  has_one :ebilling_payment_account, foreign_key: 'payment_system_id'

  belongs_to :payment_system, class_name: 'User'
  validates_presence_of :payment_system_id, :if => :has_payment_system_operator_profile?

  before_validation :reset_payment_system_id_if_not_payment_system_operator

  before_create :generate_shared_key

  def self.merchants
    by_profile(Profile::MERCHANT).order_by_username
  end

  def self.payment_systems
    by_profile(Profile::PAYMENT_SYSTEM).order_by_username
  end

  # for devise
  def email_required?
    false
  end

  def has_role?(role_name)
    ! roles.find_by(name: role_name).nil?
  end

  def has_profile?(profile_name)
    profiles.any? { |profile| profile.name == profile_name}
  end

  # Only Admin or Merchant can provision
  def can_provision?
    return true if has_profile? Profile::ADMIN or has_profile? Profile::MERCHANT
    return false
  end

  def generate_shared_key
    self.shared_key = SecureRandom.uuid if respond_to?(:"shared_key=")
  end

  private 

    def has_merchant_profile?
      has_profile?(Profile::MERCHANT) && respond_to?(:currency)
    end

    def has_payment_system_operator_profile?
      has_profile?(Profile::PAYMENT_SYSTEM_OPERATOR)
    end

    def reset_payment_system_id_if_not_payment_system_operator
      unless has_profile?(Profile::PAYMENT_SYSTEM_OPERATOR)
        self.payment_system_id = nil if respond_to?(:"payment_system_id=")
      end
    end
end
