# == Schema Information
#
# Table name: merchant_monthly_stats
#
#  id                          :integer          not null, primary key
#  created_count               :integer          default(0)
#  ready_count                 :integer          default(0)
#  paid_count                  :integer          default(0)
#  partially_paid_count        :integer          default(0)
#  cancelled_count             :integer          default(0)
#  failed_count                :integer          default(0)
#  processed_count             :integer          default(0)
#  expired_count               :integer          default(0)
#  merchant_id                 :integer          not null
#  year                        :integer          not null
#  month                       :integer          not null
#  created_at                  :datetime
#  updated_at                  :datetime
#  overdue_count               :integer          default(0)
#  traffic_fee_centimes        :integer          default(0), not null
#  traffic_fee_currency        :string(255)      default("XAF"), not null
#  revenue_centimes            :integer          default(0), not null
#  revenue_currency            :string(255)      default("XAF"), not null
#  recurrent_fee_centimes      :integer          default(0), not null
#  recurrent_fee_currency      :string(255)      default("XAF"), not null
#  ps_transaction_fee_centimes :integer          default(0), not null
#  ps_transaction_fee_currency :string(255)      default("XAF"), not null
#

class MerchantMonthlyStat < ActiveRecord::Base

  belongs_to :merchant, class_name: 'User'

  monetize :traffic_fee_centimes, with_model_currency: :traffic_fee_currency
  monetize :recurrent_fee_centimes, with_model_currency: :recurrent_fee_currency
  monetize :revenue_centimes, with_model_currency: :revenue_currency
  monetize :ps_transaction_fee_centimes, with_model_currency: :ps_transaction_fee_currency

  def self.select_sum(additional_columns=nil)
    select_part = ['created_count', 'ready_count', 'paid_count', 'partially_paid_count', 'cancelled_count', 
     'failed_count', 'processed_count', 'expired_count', 'overdue_count', 'traffic_fee_centimes', 'recurrent_fee_centimes', 'revenue_centimes', 'ps_transaction_fee_centimes'].map do |count_name|
      "sum(#{count_name}) as #{count_name}"
    end.join(', ')
    select_part << ", #{additional_columns}" if additional_columns.present?
    select("#{select_part}")
  end

  def self.total_count_for(year, month)
    select_sum('year, month').where("year = ? and month = ?", year, month)[0]
  end

  def recurrent_fee_from_stats
    Money.new(recurrent_fee_centimes, merchant.currency)
  end

  def traffic_fee_from_stats
    Money.new(traffic_fee_centimes, merchant.currency)
  end

  def revenue_from_stats
    Money.new(revenue_centimes, merchant.currency)
  end

  def total_transaction_fee
    ps_transaction_fee + traffic_fee
  end

  def net_revenue
    revenue - total_transaction_fee
  end

end
