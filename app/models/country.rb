# == Schema Information
#
# Table name: countries
#
#  id            :integer          not null, primary key
#  name          :string(255)
#  currency_name :string(255)
#  currency_code :string(255)
#  created_at    :datetime
#  updated_at    :datetime
#

class Country < ActiveRecord::Base

  validates :name, presence: true, uniqueness: true
  validates :currency_name, presence: true
  validates :currency_code, presence: true

end
