# == Schema Information
#
# Table name: profiles
#
#  id          :integer          not null, primary key
#  name        :string(255)      not null
#  description :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

class Profile < ActiveRecord::Base
  MERCHANT = "Merchant"
  SERVICE_DESK = "ServiceDesk"
  ADMIN = "Admin"
  PAYMENT_SYSTEM = "PaymentSystem"
  PAYMENT_SYSTEM_OPERATOR = "PaymentSystemOperator"
  EBILL_CREATOR = "EBillCreator"

  #  Validates presence and uniqueness of name 
  validates :name, presence: true, uniqueness: true

  #  Shows one to many relationship with role class
  has_and_belongs_to_many :roles

  #  Validates minimum length of roles to be 1 
  validates :roles, length: { minimum: 1, message: "At least one role must be selected" }

  #  Checks if role is there in the database and returns boolean 
  def has_role? role_name
    !! (roles.find_by name: role_name)
  end
end
