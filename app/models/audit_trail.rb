# == Schema Information
#
# Table name: audit_trails
#
#  id                    :integer          not null, primary key
#  client_transaction_id :string(255)
#  from                  :string(255)
#  to                    :string(255)
#  bill_id               :string(255)
#  payer_id              :string(255)
#  context               :text
#  created_at            :datetime
#  updated_at            :datetime
#  status                :string(255)      default("success")
#  action                :string(255)
#  params                :text
#  operator              :string(255)
#  error_messages        :text
#  search_field          :text
#

class AuditTrail < ActiveRecord::Base
 
  validates :client_transaction_id, length: { maximum: 35 }

  #  Included searchable module, giving access to all the methods of that module
  include Searchable
  searchable :client_transaction_id, :bill_id, :status

  #  Returns latest audit trail
  def self.last_audit
    self.order("created_at desc").first
  end

  #  Returns server transaction id 
  def server_transaction_id
    "%013d" % id
  end
end
