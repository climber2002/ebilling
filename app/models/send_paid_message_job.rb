class SendPaidMessageJob < Struct.new(:e_bill_id, :client_transaction_id, :paid_amount)
  def perform
    e_bill = EBill.find e_bill_id
    e_bill.client_transaction_id = client_transaction_id
    e_bill.send_paid_message  

    send_notification_to_merchant e_bill
    e_bill.save
  end

  private
  def send_notification_to_merchant e_bill
    amount = paid_amount.present? ? paid_amount.to_i : e_bill.amount_paid
    NotificationSender.send_notification_to_merchant(e_bill, amount: amount)
  end
end