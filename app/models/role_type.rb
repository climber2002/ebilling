# == Schema Information
#
# Table name: role_types
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

class RoleType < ActiveRecord::Base

  #  Validates presence and uniqueness of name of role type 
  validates :name, uniqueness: true, presence: true

  #  Shows one to many relationship with roles class 
  has_many :roles
end
