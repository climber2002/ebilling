# == Schema Information
#
# Table name: e_bills
#
#  id                          :integer          not null, primary key
#  user_id                     :integer
#  payer_email                 :string(255)
#  payer_msisdn                :string(255)
#  amount                      :integer
#  currency                    :string(255)
#  state                       :string(255)
#  expired_at                  :datetime
#  schedule_at                 :datetime
#  external_reference          :string(255)
#  additional_info             :string(255)
#  description                 :string(255)
#  reason                      :string(255)
#  sms                         :boolean          default(FALSE)
#  email                       :boolean          default(FALSE)
#  created_at                  :datetime
#  updated_at                  :datetime
#  sms_message                 :string(255)
#  merchant_internal_ref       :string(255)
#  sent_at                     :datetime
#  paid_at                     :datetime
#  cancelled_at                :datetime
#  processed_at                :datetime
#  failed_at                   :datetime
#  expire_at                   :datetime
#  short_description           :string(255)
#  due_date                    :date
#  bill_id                     :string(255)
#  batch_file_id               :integer
#  payer_name                  :string(255)
#  payer_address               :string(255)
#  payer_city                  :string(255)
#  accept_partial_payment      :boolean          default(FALSE)
#  minimum_amount              :integer
#  amount_paid                 :integer          default(0)
#  partially_paid_at           :datetime
#  overdue_at                  :datetime
#  payment_system_id           :integer
#  ps_transaction_id           :string(255)
#  traffic_fee_centimes        :integer          default(0), not null
#  traffic_fee_currency        :string(255)      default("XAF"), not null
#  search_field                :text
#  payer_id                    :string(255)
#  payer_code                  :string(255)
#  data0                       :string(255)
#  data1                       :string(255)
#  data2                       :string(255)
#  data3                       :string(255)
#  data4                       :string(255)
#  data5                       :string(255)
#  data6                       :string(255)
#  data7                       :string(255)
#  data8                       :string(255)
#  data9                       :string(255)
#  ps_transaction_fee_centimes :integer          default(0), not null
#  ps_transaction_fee_currency :string(255)      default("XAF"), not null
#  last_paid_amount_centimes   :integer          default(0), not null
#  last_paid_amount_currency   :string(255)      default("XAF"), not null
#

class EBill::AsGatewayPay < EBill

  include AcceptPartialPaymentConcern

  # 422: others
  # 425: invoice already paid
  # 426: invoice expired
  # 427: invoice cancelled
  # 430: Amount greater than due
  # 431: Amount paid below minimum
  # 432: Partial payment not accepted
  attr_accessor :error_code

  #  1. Get and set methods for paid amount
  attr_accessor :paid_amount

  #  1. Get method for total paid
  attr_reader :total_paid

  #  1. Get and set method for validating count 
  attr_accessor :validate_count

  #  Paid amount should be present in ebill
  validate :paid_amount, presence: true

  #  Paid amount can be only integer and can also be blank
  validate :paid_amount, numericality: { greater_than: 0 }, allow_blank: true
  
  #  Validates if ebill can be paid or partially paid if validation count is 1
  validate :ensure_can_pay, :if => :first_save?

  #  Validates the paid amount if validation count is 1
  validate :paid_amount_is_valid, :if => :first_save?

  before_save :set_last_paid_amount

  #  Resets validation count to 0 after initialization of ebill
  after_initialize :reset_validate_count

  #  Updates validation count by 1 before validation
  before_validation :update_validate_count

  #  Stores toal paid and updates paid status before saving ebill if validation count is 1
  before_save :store_total_paid, :if => :first_save?

  #  Stores client trasaction id to payment system transaction id before saving ebill
  before_save :store_ps_transaction_id
  before_save :store_fee
  
  before_save :store_ps_transaction_fee

  after_commit :send_merchant_notification

  private

    #  1. Sets validate count to zero
    def reset_validate_count
      self.validate_count = 0
    end

    #  1. Increments the validate_count by 1 and returns the same 
    def update_validate_count
      self.validate_count += 1
    end

    def set_last_paid_amount
      self.last_paid_amount = Money.new(Float(self.paid_amount) * 100, user.currency)
    end

    #  1. If count of validation is equal to 1 , it returns true, else false
    def first_save?
      self.validate_count == 1
    end

    #  1. Checks if can_pay or can_pay_partial is true
    #  2. If either of them are false, invoice not payable errors are added to errors 
    def ensure_can_pay
      unless can_pay? || can_pay_partial?
        errors.add(:state, 'Invoice not payable')

        if self.paid_at.present?
          self.error_code = 425
        elsif self.expired_at.present?
          self.error_code = 426
        elsif self.cancelled_at.present?
          self.error_code = 427
        end
      end
    end

    #  1. Returns if paid amount is not convertible to integer
    #  2. Calculates total paid amount by adding paid amount just now and amount paid already
    #  3. Checks partial payment if accept partial payment is true 
    #  4. Else checks for full payment 
    def paid_amount_is_valid
      return unless (Integer(paid_amount.to_i) rescue false)
      @total_paid = Integer(paid_amount.to_i) + amount_paid
      if accept_partial_payment?
        check_partial_payment
      else
        check_full_payment
      end
    end

    #  1. If total amount paid just now is less than minimum amount, error is shown
    #  2. If total amount paid is greater than amount of ebill, error is shown
    def check_partial_payment
      if total_paid < minimum_amount
        errors.add(:paid_amount, 'Amount paid below minimum')
        self.error_code = 431
      elsif total_paid > amount
        errors.add(:paid_amount, 'Amount paid greater than due')
        self.error_code = 430
      end 
    end


    #  1. If addition of already paid amount and amount paid just now is not equal to total amount of ebill, 
    #  "partial payment not accepted" error is shown
    def check_full_payment
      if (Integer(paid_amount.to_i) + amount_paid) != amount
        errors.add(:paid_amount, 'Amount paid not equal to invoice amount')
        self.error_code = 432
      end
    end

    #  1. Assigns total_paid to amount_paid
    #  2. Checks if total_paid is equal to amount of ebill
    #  3. If it is equal, transitions ebill from ready to paid state
    #  4. If it is not equal, transitions ebill to partially_paid state
    def store_total_paid
      self.amount_paid = total_paid
      if fully_paid?
        pay
      else
        pay_partial
      end
    end

    #  Checks if total_paid is equal to amount of ebill
    def fully_paid?
      self.amount == self.total_paid
    end

    #  Assigns client transaction id to payment system transaction id
    def store_ps_transaction_id
      self.ps_transaction_id = self.client_transaction_id
    end

    def store_fee
      self.traffic_fee = calculate_ebilling_fee
    end

    def store_ps_transaction_fee
      self.ps_transaction_fee = calculate_ps_transaction_fee
    end

    def send_merchant_notification
      Delayed::Job.enqueue SendNotificationJob.new(id) if paid? || partially_paid?
    end
end
