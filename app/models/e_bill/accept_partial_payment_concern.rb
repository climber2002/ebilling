module EBill::AcceptPartialPaymentConcern

  # if the ebill is already paritial_paid, return accept_partial_payment as false
  # to disable partial payment twice 
  def accept_partial_payment
    if self.state == 'partially_paid'
      false
    else
      super
    end
  end

  def accept_partial_payment?
    self.accept_partial_payment
  end
end