# == Schema Information
#
# Table name: e_bills
#
#  id                          :integer          not null, primary key
#  user_id                     :integer
#  payer_email                 :string(255)
#  payer_msisdn                :string(255)
#  amount                      :integer
#  currency                    :string(255)
#  state                       :string(255)
#  expired_at                  :datetime
#  schedule_at                 :datetime
#  external_reference          :string(255)
#  additional_info             :string(255)
#  description                 :string(255)
#  reason                      :string(255)
#  sms                         :boolean          default(FALSE)
#  email                       :boolean          default(FALSE)
#  created_at                  :datetime
#  updated_at                  :datetime
#  sms_message                 :string(255)
#  merchant_internal_ref       :string(255)
#  sent_at                     :datetime
#  paid_at                     :datetime
#  cancelled_at                :datetime
#  processed_at                :datetime
#  failed_at                   :datetime
#  expire_at                   :datetime
#  short_description           :string(255)
#  due_date                    :date
#  bill_id                     :string(255)
#  batch_file_id               :integer
#  payer_name                  :string(255)
#  payer_address               :string(255)
#  payer_city                  :string(255)
#  accept_partial_payment      :boolean          default(FALSE)
#  minimum_amount              :integer
#  amount_paid                 :integer          default(0)
#  partially_paid_at           :datetime
#  overdue_at                  :datetime
#  payment_system_id           :integer
#  ps_transaction_id           :string(255)
#  traffic_fee_centimes        :integer          default(0), not null
#  traffic_fee_currency        :string(255)      default("XAF"), not null
#  search_field                :text
#  payer_id                    :string(255)
#  payer_code                  :string(255)
#  data0                       :string(255)
#  data1                       :string(255)
#  data2                       :string(255)
#  data3                       :string(255)
#  data4                       :string(255)
#  data5                       :string(255)
#  data6                       :string(255)
#  data7                       :string(255)
#  data8                       :string(255)
#  data9                       :string(255)
#  ps_transaction_fee_centimes :integer          default(0), not null
#  ps_transaction_fee_currency :string(255)      default("XAF"), not null
#  last_paid_amount_centimes   :integer          default(0), not null
#  last_paid_amount_currency   :string(255)      default("XAF"), not null
#

class EBill::AsGatewayQuery < EBill

  include AcceptPartialPaymentConcern

  # 405: no payment account found
  # 422: others
  # 425: invoice already paid
  # 426: invoice expired
  # 427: invoice cancelled
  attr_accessor :error_code

  #  1. Validates if ebill can be paid or can be paid partially
  validate :ensure_state_can_pay_or_pay_partial

  #  1. Validates if merchant payment system id exists 
  validate :merchant_ps_id_exist

  #  1. Returns merchant payment system id if present or returns nil if not present 
  def payee_id
    merchant_payment_system_mapping.try(:merchant_ps_id)
  end

  def payment_id
    ebilling_payment_account.try(:payment_id)
  end

  def transaction_fee
    if self.state == 'partially_paid'
      Money.new(0, user.currency)
    else
      user.fee_strategy.try(:calculate_traffic_fee_for_e_bill, self)
    end
  end

  def ps_transaction_fee
    if self.state == 'partially_paid'
      Money.new(0, user.currency)
    else
      calculate_ps_transaction_fee
    end
  end

  def amount_left
    amount - amount_paid
  end

  def minimum_amount_left
    partially_paid? ? 0 : minimum_amount
  end

  private

    def ebilling_payment_account
      @ebilling_payment_account ||= EbillingPaymentAccount.find_by payment_system_id: payment_system_id
    end

    #  1. Generates successful audit trail for verify if can_pay or can_pay_partial is true
    #  2. Generates failed audit trail for verify if can_pay or can_pay_partial is false
    def ensure_state_can_pay_or_pay_partial
      unless can_pay? || can_pay_partial?
        self.errors['state'] = 'Invoice not payable'
        generate_verify_audit_trail false, 'Invoice not payable'
        
        if self.paid_at.present?
          self.error_code = 425
        elsif self.expired_at.present?
          self.error_code = 426
        elsif self.cancelled_at.present?
          self.error_code = 427
        end
             
        return
      end

      generate_verify_audit_trail true
    end

    #  1. Checks if merchant payment system mapping exists, else adds error to errors 
    def merchant_ps_id_exist
      unless merchant_payment_system_mapping.present?
        errors.add(:payee_id, 'No payee_id found')
        self.error_code = 405
      end
    end
end
