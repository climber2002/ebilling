# == Schema Information
#
# Table name: ebilling_payment_accounts
#
#  id                :integer          not null, primary key
#  payment_system_id :integer
#  payment_id        :string(255)
#  created_at        :datetime
#  updated_at        :datetime
#

class EbillingPaymentAccount < ActiveRecord::Base

  belongs_to :payment_system, class_name: 'User'

  validates :payment_id, presence: true
  validates :payment_system_id, uniqueness: true, presence: true
  validate  :payment_system_must_have_specific_role

  private

    def payment_system_must_have_specific_role
      return unless payment_system.present?

      unless payment_system.has_profile?(Profile::PAYMENT_SYSTEM)
        errors.add(:payment_system_id, 'payment system does not have specific role')
      end
    end
end
