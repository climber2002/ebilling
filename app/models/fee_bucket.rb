# == Schema Information
#
# Table name: fee_buckets
#
#  id               :integer          not null, primary key
#  currency_id      :integer
#  min_transactions :integer
#  max_transactions :integer
#  fee_value        :integer
#  created_at       :datetime
#  updated_at       :datetime
#

class FeeBucket < ActiveRecord::Base

  validates :min_transactions, presence: true
  validates :max_transactions, presence: true
  validates :fee_value, presence: true
  validates :currency_id, presence: true

  belongs_to :currency

end
