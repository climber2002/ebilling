class SendNotificationJob < Struct.new(:e_bill_id)

  #  Finds ebill by id and sends ebill by notification to merchant 
  def perform
    e_bill = EBill.find e_bill_id
    amount = e_bill.last_paid_amount.to_f
    NotificationSender.send_notification_to_merchant(e_bill, amount: amount)
  end
end