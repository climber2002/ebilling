object @mobile => :mobile

attributes :imei

node(:confirmed) { |mobile| mobile.confirmed? }

node(:profile_name) { |_| current_api_user.profiles.first.try(:name) }

node(:currency) { |_| current_api_user.currency }

node(:username) { |_| current_api_user.username }

node(:email) { |_| current_api_user.email }

node(:msisdn) { |_| current_api_user.msisdn }