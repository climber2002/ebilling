object @e_bill

attributes :bill_id, :payer_id, :payer_code,
          :payer_email, :payer_msisdn, 
          :amount,
          :currency, :state,
          :created_at, :expire_at,
          :schedule_at, :updated_at,
          :short_description, :due_date,
          :external_reference,
          :additional_info, :description,
          :reason, :payer_name,
          :payer_address, :payer_city, 
          :accept_partial_payment, :minimum_amount, :amount_paid,
          :ps_transaction_id, :payment_system_name

node(:payee_id) { |e_bill| e_bill.user.msisdn }
node(:payee_name) { |e_bill| e_bill.user.username }