object false

node(:total_pages) {|m| @e_bills.total_pages }
node(:current_page) {|m| @e_bills.current_page }
node(:per_page) { |m| @e_bills.per_page }
node(:total_entries) { |m| @e_bills.total_entries }

child(@e_bills, object_root: false) do
  extends "api/mobileapp/e_bills/show"
end