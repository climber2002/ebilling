require 'spec_helper'

describe MerchantMonthlyStatsPopulator, type: :request do

  let(:merchant1) { FactoryGirl.create(:merchant) }
  let(:merchant2) { FactoryGirl.create(:merchant2) }
  let(:merchant3) { FactoryGirl.create(:merchant3) }
  let!(:payment) { FactoryGirl.create(:payment) }
  let(:url) { "/api/v1/payment_gateway/e_bills" }


  context 'when merchant has variable strategy' do 
    before :each do 
      c = Currency.create(code: merchant3.currency)
      c.fee_buckets.create(min_transactions: 0, max_transactions: 30, fee_value: 10)
      c.fee_buckets.create(min_transactions: 31, max_transactions: 500, fee_value: 25)
    end

    context 'when there are 10 ebills' do 
      context 'when disable_recurrent is false' do

        before do 
          # merchant3 has recurrent strategy
          merchant3.fee_strategy = FeeStrategy::VariableStrategy.create(percentage: 5, 
            maximum_amount: 25, maximum_amount_currency: merchant3.currency)
          merchant3.save

          (1..10).each do |i|
            e_bill = FactoryGirl.create(:e_bill, 
                user: merchant3, amount: 100 * i, accept_partial_payment: false)
            e_bill.trigger
            put "#{url}/#{e_bill.bill_id}.json", { :client_transaction_id => "12345", 
              :paid_amount => 100 * i }.to_json, payment_headers
            expect(response.status).to eq 200
          end

          described_class.new.populate(Date.today)
        end

        it 'should have correct paid count' do 
          today = Date.today
          stat = MerchantMonthlyStat.find_by(merchant_id: merchant3.id, year: today.year, month: today.month)
          expect(stat.paid_count).to eq 10
        end

        it 'should have correct traffic fee' do 
          today = Date.today
          stat = MerchantMonthlyStat.find_by(merchant_id: merchant3.id, year: today.year, month: today.month)
          expect(stat.traffic_fee).to eq Money.new(2000 * 10, merchant3.currency)
        end

        it 'should have correct recurrent fee' do 
          today = Date.today
          stat = MerchantMonthlyStat.find_by(merchant_id: merchant3.id, year: today.year, month: today.month)
          expect(stat.recurrent_fee).to eq Money.new(10 * 100, merchant3.currency)
        end

        it 'should have correct revenue' do 
          today = Date.today
          stat = MerchantMonthlyStat.find_by(merchant_id: merchant3.id, year: today.year, month: today.month)
          expect(stat.revenue).to eq Money.new(5500 * 100, merchant3.currency)
        end
      end

      context 'when disable_recurrent is true' do 
        before do 
          # merchant3 has recurrent strategy
          merchant3.fee_strategy = FeeStrategy::VariableStrategy.create(percentage: 5, 
            maximum_amount: 25, maximum_amount_currency: merchant3.currency, disable_recurrent: true)
          merchant3.save

          (1..10).each do |i|
            e_bill = FactoryGirl.create(:e_bill, 
                user: merchant3, amount: 100 * i, accept_partial_payment: false)
            e_bill.trigger
            put "#{url}/#{e_bill.bill_id}.json", { :client_transaction_id => "12345", 
              :paid_amount => 100 * i }.to_json, payment_headers
            expect(response.status).to eq 200
          end

          described_class.new.populate(Date.today)
        end

        it 'should have correct paid count' do 
          today = Date.today
          stat = MerchantMonthlyStat.find_by(merchant_id: merchant3.id, year: today.year, month: today.month)
          expect(stat.paid_count).to eq 10
        end

        it 'should have correct traffic fee' do 
          today = Date.today
          stat = MerchantMonthlyStat.find_by(merchant_id: merchant3.id, year: today.year, month: today.month)
          expect(stat.traffic_fee).to eq Money.new(2000 * 10, merchant3.currency)
        end

        it 'should have correct recurrent fee' do 
          today = Date.today
          stat = MerchantMonthlyStat.find_by(merchant_id: merchant3.id, year: today.year, month: today.month)
          expect(stat.recurrent_fee).to eq Money.new(0, merchant3.currency)
        end

        it 'should have correct revenue' do 
          today = Date.today
          stat = MerchantMonthlyStat.find_by(merchant_id: merchant3.id, year: today.year, month: today.month)
          expect(stat.revenue).to eq Money.new(5500 * 100, merchant3.currency)
        end
      end  
    end

    
  end

  context 'when merchant has fixed strategy' do

    let!(:mapping) do
      MerchantPaymentSystemMapping.create!(merchant: merchant2, payment_system: payment, ps_percentage: 10, merchant_ps_id: 'abcde')
    end

    before :each do 
      # merchant2 has recurrent strategy
      merchant2.fee_strategy = FeeStrategy::FixedStrategy.create(fixed_amount: Money.new(200, merchant2.currency))
      merchant2.save

      (1..10).each do |i|
        e_bill = FactoryGirl.create(:e_bill, 
            user: merchant2, amount: 100 * i, accept_partial_payment: false)
        e_bill.trigger
        put "#{url}/#{e_bill.bill_id}.json", { :client_transaction_id => "12345", 
          :paid_amount => 100 * i }.to_json, payment_headers
        expect(response.status).to eq 200
      end

      described_class.new.populate(Date.today)

    end

    it 'should have correct paid count' do 
      today = Date.today
      stat = MerchantMonthlyStat.find_by(merchant_id: merchant2.id, year: today.year, month: today.month)
      expect(stat.paid_count).to eq 10
    end

    it 'should have correct fee' do 
      today = Date.today
      stat = MerchantMonthlyStat.find_by(merchant_id: merchant2.id, year: today.year, month: today.month)
      expect(stat.traffic_fee).to eq Money.new(200 * 10, merchant2.currency)
    end

    it 'should have correct ps_transaction_fee' do 
      today = Date.today
      stat = MerchantMonthlyStat.find_by(merchant_id: merchant2.id, year: today.year, month: today.month)
      expect(stat.ps_transaction_fee).to eq Money.new(10 * 10, merchant2.currency)
    end
  end

  # set the JSON headers include basic auth
  def json_headers username, password
    { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json', 
            'HTTP_AUTHORIZATION' =>  ActionController::HttpAuthentication::Basic.encode_credentials(username, password) }
  end  

  def payment_headers
    json_headers payment.username, payment.password
  end
end