require 'spec_helper'

describe PaymentSystemMonthlyStatsPopulator do
  let!(:merchant1) { FactoryGirl.create(:merchant, username: 'merchant1') }
  let!(:merchant2) { FactoryGirl.create(:merchant, username: 'merchant2', currency: 'USD') }
  let!(:payment_system1) { FactoryGirl.create(:payment, username: 'payment_system1') }
  let!(:payment_system2) { FactoryGirl.create(:payment, username: 'payment_system2') }

  let!(:ebill11_1) { FactoryGirl.create(:e_bill, user: merchant1, amount: 340) } 
  let!(:ebill11_2) { FactoryGirl.create(:e_bill, user: merchant1, amount: 120) } 
  let!(:ebill21_1) { FactoryGirl.create(:e_bill, user: merchant2, amount: 287) } 
  let!(:ebill21_2) { FactoryGirl.create(:e_bill, user: merchant2, amount: 320) } 
  let!(:ebill12_1) { FactoryGirl.create(:e_bill, user: merchant1, amount: 376) } 
  let!(:ebill12_2) { FactoryGirl.create(:e_bill, user: merchant1, amount: 500) } 
  let!(:ebill22_1) { FactoryGirl.create(:e_bill, user: merchant2, amount: 209) } 
  let!(:ebill22_2) { FactoryGirl.create(:e_bill, user: merchant2, amount: 700) } 

  before do 
    [ebill11_1, ebill11_2, ebill21_1, ebill21_2].each do |e_bill|
      e_bill.trigger
      as_gateway = EBill::AsGatewayPay.find(e_bill.id)
      as_gateway.update_attributes(payment_system_id: payment_system1.id, paid_amount: e_bill.amount)
      e_bill.reload
    end

    [ebill12_1, ebill12_2, ebill22_1, ebill22_2].each do |e_bill|
      e_bill.trigger
      as_gateway = EBill::AsGatewayPay.find(e_bill.id)
      as_gateway.update_attributes(payment_system_id: payment_system2.id, paid_amount: e_bill.amount)
      e_bill.reload
    end

    [ebill11_1, ebill11_2, ebill21_1, ebill21_2].each do |e_bill|
      expect(e_bill.payment_system).to eq payment_system1
    end

    [ebill12_1, ebill12_2, ebill22_1, ebill22_2].each do |e_bill|
      expect(e_bill.payment_system).to eq payment_system2
    end

    [ebill11_1, ebill11_2, ebill21_1, ebill21_2, ebill12_1, ebill12_2, ebill22_1, ebill22_2].each do |e_bill|
      expect(e_bill).to be_valid
      e_bill.save
      expect(e_bill.paid_at).to_not be_nil
    end
  end

  describe 'populate' do 
    before do 
      PaymentSystemMonthlyStatsPopulator.new.populate(Date.today)
    end

    it 'should have two PaymentSystemMonthlyStat' do
      expect(PaymentSystemMonthlyStat.count).to eq 2
      expect(PaymentSystemMonthlyStat.find_by(payment_system_id: payment_system1.id)).to_not be_nil
      expect(PaymentSystemMonthlyStat.find_by(payment_system_id: payment_system2.id)).to_not be_nil
    end

    it 'each stat should have two items' do 
      stat = PaymentSystemMonthlyStat.find_by(payment_system_id: payment_system1.id)
      expect(stat.payment_system_monthly_items.count).to eq 2
      item11 = stat.payment_system_monthly_items.find_by(merchant_id: merchant1.id)
      expect(item11.revenue).to eq Money.new(460 * 100, merchant1.currency)
      expect(item11.partially_paid_count).to eq 0
      expect(item11.paid_count).to eq 2
      expect(item11.processed_count).to eq 0

      item12 = stat.payment_system_monthly_items.find_by(merchant_id: merchant2.id)
      expect(item12.revenue).to eq Money.new((287 + 320) * 100, merchant2.currency)
      expect(item12.partially_paid_count).to eq 0
      expect(item12.paid_count).to eq 2
      expect(item12.processed_count).to eq 0

      stat2 = PaymentSystemMonthlyStat.find_by(payment_system_id: payment_system2.id)
      expect(stat2.payment_system_monthly_items.count).to eq 2
      item21 = stat2.payment_system_monthly_items.find_by(merchant_id: merchant1.id)
      expect(item21.revenue).to eq Money.new((376 + 500) * 100, merchant1.currency)
      expect(item21.partially_paid_count).to eq 0
      expect(item21.paid_count).to eq 2
      expect(item21.processed_count).to eq 0

      item22 = stat2.payment_system_monthly_items.find_by(merchant_id: merchant2.id)
      expect(item22.revenue).to eq Money.new((209 + 700) * 100, merchant2.currency)
      expect(item22.partially_paid_count).to eq 0
      expect(item22.paid_count).to eq 2
      expect(item22.processed_count).to eq 0
    end    
  end

end