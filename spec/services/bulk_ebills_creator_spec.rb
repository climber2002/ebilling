require 'spec_helper'

describe BulkEbillsCreator do 
  let(:merchant) { FactoryGirl.create(:merchant) }

  subject { BulkEbillsCreator.new(merchant) }

  describe '#bulk_create' do 

    context 'when params are valid' do 
      let(:params) do
        { 
          sms: true, 
          email: true,
          e_bills: [
            {
              payer_msisdn: '12345678',
              payer_name: 'Andy',
              amount: 200,
              short_description: 'short',
              due_date: '16/09/2015',
              payer_email: 'aaa@sina.com'
            },

            {
              payer_msisdn: '22222222',
              payer_name: 'Lee',
              amount: 100,
              short_description: 'short',
              due_date: '16/09/2015',
              payer_email: 'aaa@sina.com'
            },

            {
              payer_msisdn: '55555555',
              payer_name: 'Colin',
              amount: 100,
              short_description: 'short',
              due_date: '16/09/2015',
              payer_email: 'aaa@sina.com'
            }
          ]
        }
      end

      before do 
        subject.bulk_create(params)
      end

      it 'should create 3 e_bills' do 
        expect(EBill.count).to eq 3
      end

      it 'the bills should belong to merchant' do 
        EBill.all.each do |e_bill|
          expect(e_bill.user).to eq merchant
        end
      end

      it 'should set notification method' do 
        EBill.all.each do |e_bill|
          expect(e_bill.sms?).to eq true
          expect(e_bill.email?).to eq true
        end
      end
    end

    context 'when params are invalid' do 
      let(:params) do
        { 
          sms: true, 
          email: true,
          e_bills: [
            {
              payer_msisdn: '12345678',
              payer_name: 'Andy',
              amount: 200,
              short_description: 'short',
              due_date: '16/09/2015',
              payer_email: 'aaa@sina.com'
            },

            {
              payer_msisdn: '22222222',
              payer_name: 'Lee',
              amount: 100,
              short_description: 'short',
              due_date: '16/09/2015'
            },

            {
              payer_msisdn: '55555555',
              payer_name: 'Colin',
              amount: 100,
              short_description: 'short',
              due_date: '16/09/2015',
              payer_email: 'aaa@sina.com'
            }
          ]
        }
      end

      it 'should raise exception' do 
        expect { subject.bulk_create(params) }.to raise_error ActiveRecord::RecordInvalid
        expect(EBill.count).to eq 0
        expect(subject.failed_ebill).to_not be_nil
      end
    end
  end
end