require 'active_support/all'
module ControllerHacks

  def basic_auth(username, password)
    encoded = Base64::encode64("#{username}:#{password}")
    { 'Authorization' => "Basic #{encoded}" }
  end


  def api_get(url, params={}, headers={}, session=nil, flash=nil)
    # api_process(action, params, session, flash, "GET")
    get url, params, 
      { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json'  }.merge(headers)
  end

  def api_post(url, params={}, headers={}, session=nil, flash=nil)
    post url, params.to_json, 
      { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }.merge(headers)
  end

  def api_put(url, params={}, headers={}, session=nil, flash=nil)
    put url, params.to_json, 
      { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }.merge(headers)
  end

  def api_delete(url, params={}, headers={}, session=nil, flash=nil)
    delete url, params.to_json, 
      { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json' }.merge(headers)
  end

  def api_process(action, params={}, headers={}, session=nil, flash=nil, method="get")
    scoping = respond_to?(:resource_scoping) ? resource_scoping : {}
    process(action, method, params.merge(scoping).reverse_merge!(:use_route => :spree, :format => :json), session, flash)
  end
end

RSpec.configure do |config|
  config.include ControllerHacks, :type => :controller
  config.include ControllerHacks, :type => :request
end