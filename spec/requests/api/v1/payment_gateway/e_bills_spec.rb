
require 'spec_helper'

describe "/api/v1/payment_gateway/e_bills" do

  let!(:admin) { FactoryGirl.create(:admin) }
  let!(:service_desk) { FactoryGirl.create(:service_desk) }
  let!(:merchant) { FactoryGirl.create(:merchant) }
  let!(:merchant2) { FactoryGirl.create(:merchant2) }
  let!(:payment) { FactoryGirl.create(:payment) }
  let(:url) { "/api/v1/payment_gateway/e_bills" }

  before do 
    MerchantPaymentSystemMapping.find_or_create_by! merchant_id: merchant.id,
      payment_system_id: payment.id, merchant_ps_id: 'merchant_ps_test'

    EbillingPaymentAccount.find_or_create_by! payment_system_id: payment.id,
      payment_id: '1234567890'

    merchant.fee_strategy = FeeStrategy::FixedStrategy.create(fixed_amount: 20)
  end

  after do 
    MerchantPaymentSystemMapping.find_by(merchant_id: merchant.id,
      payment_system_id: payment.id, merchant_ps_id: 'merchant_ps_test').try(:destroy)
  end

  describe "Update" do

    context 'accept_partial_payment is false' do 
      let(:e_bill) { FactoryGirl.create(:e_bill, user: merchant, amount: 100, accept_partial_payment: false) }

      context "pay successfully but has decimal" do 
        before do
          e_bill.trigger
          put "#{url}/#{e_bill.bill_id}.json", { :client_transaction_id => "12345", 
            :paid_amount => '100.0' }.to_json, payment_headers
          puts "#{response.body}"
        end

        it { expect(response.status).to eq 200 }

        it "should be payed" do
          e_bill.reload
          expect(e_bill.state).to eq "paid"
        end
      end

      context "pay successfully" do
        before do
          e_bill.trigger
          put "#{url}/#{e_bill.bill_id}.json", { :client_transaction_id => "12345", 
            :paid_amount => '100' }.to_json, payment_headers
          puts "#{response.body}"
        end

        it { expect(response.status).to eq 200 }

        it "should be payed" do
          e_bill.reload
          expect(e_bill.state).to eq "paid"
        end

        it 'should set ps_transaction_id' do 
          e_bill.reload
          expect(e_bill.ps_transaction_id).to eq '12345'
        end

        it 'should set payment_system' do 
          e_bill.reload
          expect(e_bill.payment_system).to eq payment
        end

        it "should have a success audit trail" do
          last_audit = AuditTrail.last_audit
          puts "#{last_audit.inspect}"

          expect(last_audit.to).to eq "paid"
          expect(last_audit.status).to eq "success"
        end

        it "should return client_transaction_id" do
          json = JSON.parse(response.body)
          expect(json['client_transaction_id']).to eq "12345"
        end

        it "should have server_transaction_id" do
          puts "response: #{response.body}"
          json = JSON.parse(response.body)
          expect(json['server_transaction_id']).not_to be_nil
        end

        it 'should has correct payee_id' do 
          json = JSON.parse(response.body)
          expect(json['e_bill']['payee_id']).to eq merchant.msisdn
        end
      end

      context 'check fee' do 

        context 'when the fee_strategy is fixed' do 
          before do 
            merchant.fee_strategy = FeeStrategy::FixedStrategy.create(fixed_amount: Money.new(20, merchant.currency))
            merchant.save

            e_bill.trigger
            put "#{url}/#{e_bill.bill_id}.json", { :client_transaction_id => "12345", 
              :paid_amount => '100' }.to_json, payment_headers
          end

          it { expect(response.status).to eq 200 }

          it 'should have correct fee' do 
            e_bill.reload
            expect(e_bill.traffic_fee).to eq Money.new(20, merchant.currency)
          end
        end

        context 'when the fee_strategy is variant' do 

          context 'when maximum is not reached' do 
            before do 
              merchant.fee_strategy = FeeStrategy::VariableStrategy.create(
                maximum_amount: Money.new(500, merchant.currency), percentage: 3)
              merchant.save

              e_bill.trigger
              put "#{url}/#{e_bill.bill_id}.json", { :client_transaction_id => "12345", 
                :paid_amount => '100' }.to_json, payment_headers
            end

            it { expect(response.status).to eq 200 }

            it 'should have correct fee' do 
              e_bill.reload
              expect(e_bill.traffic_fee).to eq Money.new(300, merchant.currency)
            end
          end
          
          context 'when maximum is reached' do 
            before do 
              merchant.fee_strategy = FeeStrategy::VariableStrategy.create(
                maximum_amount: Money.new(500, merchant.currency), percentage: 6)
              merchant.save

              e_bill.trigger
              put "#{url}/#{e_bill.bill_id}.json", { :client_transaction_id => "12345", 
                :paid_amount => '100' }.to_json, payment_headers
            end

            it { expect(response.status).to eq 200 }

            it 'should have correct fee' do 
              e_bill.reload
              expect(e_bill.traffic_fee).to eq Money.new(500, merchant.currency)
            end
          end
        end
      end

      context "invalid state" do
        before do
          e_bill.trigger
          e_bill.pay
          put "#{url}/#{e_bill.bill_id}.json", { :client_transaction_id => "12345", :paid_amount => 100 }.to_json, payment_headers
        end

        it { expect(response.status).to eq 425 }

        it "should have errors" do
          json = JSON.parse(response.body)
          expect(json['errors']['state']).to eq ['Invoice not payable']
        end
      end

      context "amount not sufficient" do
        before do
          put "#{url}/#{e_bill.bill_id}.json", { :client_transaction_id => "12345", :paid_amount => 80 }.to_json, payment_headers
        end

        it { expect(response.status).to eq 432 }

        it "should have errors" do
          json = JSON.parse(response.body)
          expect(json['errors']['paid_amount']).to eq ['Partial payment not accepted']
        end
      end
    end

    context 'accept_partial_payment is true' do 
      let(:e_bill) { FactoryGirl.create(:e_bill, user: merchant, amount: 100, accept_partial_payment: true, minimum_amount: 50) }

      before do 
        merchant.fee_strategy = FeeStrategy::VariableStrategy.create(
          maximum_amount: Money.new(500, merchant.currency), percentage: 3)
        merchant.save
      end

      context "pay successfully partially" do
        before do
          e_bill.trigger
          put "#{url}/#{e_bill.bill_id}.json", { :client_transaction_id => "12345", 
            :paid_amount => '60' }.to_json, payment_headers
          puts "#{response.body}"
        end

        it { expect(response.status).to eq 200 }

        it 'the traffic_fee should be saved' do 
          e_bill.reload
          expect(e_bill.traffic_fee.to_f).to eq 3
        end

        it "should be payed" do
          e_bill.reload
          expect(e_bill.state).to eq "partially_paid"
          expect(e_bill.amount_paid).to eq 60
        end

        it "should have a success audit trail" do
          last_audit = AuditTrail.last_audit
          puts "#{last_audit.inspect}"

          expect(last_audit.to).to eq "partially_paid"
          expect(last_audit.status).to eq "success"
        end

        it "should return client_transaction_id" do
          json = JSON.parse(response.body)
          expect(json['client_transaction_id']).to eq "12345"
        end

        it "should have server_transaction_id" do
          puts "response: #{response.body}"
          json = JSON.parse(response.body)
          expect(json['server_transaction_id']).not_to be_nil
        end

        it 'should set partially_paid_at' do 
          e_bill.reload
          expect(e_bill.partially_paid_at).to_not be_nil
        end
      end

      context "pay successfully fully" do
        before do
          e_bill.trigger
          put "#{url}/#{e_bill.bill_id}.json", { :client_transaction_id => "12345", 
            :paid_amount => 100 }.to_json, payment_headers
          puts "#{response.body}"
        end

        it { expect(response.status).to eq 200 }

        it 'the traffic_fee should be more than 0' do 
          e_bill.reload
          expect(e_bill.traffic_fee.to_f > 0).to eq true
        end

        it "should be payed" do
          e_bill.reload
          expect(e_bill.state).to eq "paid"
          expect(e_bill.amount_paid).to eq 100
        end

        it "should have a success audit trail" do
          last_audit = AuditTrail.last_audit
          puts "#{last_audit.inspect}"

          expect(last_audit.to).to eq "paid"
          expect(last_audit.status).to eq "success"
        end

        it "should return client_transaction_id" do
          json = JSON.parse(response.body)
          expect(json['client_transaction_id']).to eq "12345"
        end

        it "should have server_transaction_id" do
          puts "response: #{response.body}"
          json = JSON.parse(response.body)
          expect(json['server_transaction_id']).not_to be_nil
        end
      end

      context "pay successfully fully twice" do
        before do
          e_bill.trigger
          put "#{url}/#{e_bill.bill_id}.json", { :client_transaction_id => "12345", 
            :paid_amount => 60 }.to_json, payment_headers

          put "#{url}/#{e_bill.bill_id}.json", { :client_transaction_id => "12345", 
            :paid_amount => 40 }.to_json, payment_headers
          puts "#{response.body}"
        end

        it { expect(response.status).to eq 200 }

        it 'the traffic_fee should be more than 0' do 
          e_bill.reload
          expect(e_bill.traffic_fee.to_f > 0).to eq true
        end

        it "should be payed" do
          e_bill.reload
          expect(e_bill.state).to eq "paid"
          expect(e_bill.amount_paid).to eq 100
        end

        it 'should set last_paid_amount' do 
          e_bill.reload
          expect(e_bill.last_paid_amount.to_f).to eq 40.0
        end

        it "should have a success audit trail" do
          last_audit = AuditTrail.last_audit
          puts "#{last_audit.inspect}"

          expect(last_audit.to).to eq "paid"
          expect(last_audit.status).to eq "success"
        end

        it "should return client_transaction_id" do
          json = JSON.parse(response.body)
          expect(json['client_transaction_id']).to eq "12345"
        end

        it "should have server_transaction_id" do
          puts "response: #{response.body}"
          json = JSON.parse(response.body)
          expect(json['server_transaction_id']).not_to be_nil
        end
      end

      context "amount not sufficient" do
        before do
          put "#{url}/#{e_bill.bill_id}.json", { :client_transaction_id => "12345", :paid_amount => 40 }.to_json, payment_headers
        end

        it { expect(response.status).to eq 431 }

        it "should have errors" do
          json = JSON.parse(response.body)
          expect(json['errors']['paid_amount']).to eq ['Amount paid below minimum']
        end
      end

      context "amount too much" do
        before do
          put "#{url}/#{e_bill.bill_id}.json", { :client_transaction_id => "12345", :paid_amount => 120 }.to_json, payment_headers
        end

        it { expect(response.status).to eq 430 }

        it "should have errors" do
          json = JSON.parse(response.body)
          expect(json['errors']['paid_amount']).to eq ['Amount paid greater than due']
        end
      end
    end
      
  end


  describe "query" do
    let(:e_bill) { FactoryGirl.create(:e_bill, user: merchant, accept_partial_payment: true,
      amount: 100, minimum_amount: 50) }
    context "query successfully" do
      before do
        e_bill.trigger

        get "#{url}/#{e_bill.bill_id}.json", 
              { :client_transaction_id => "12345" }.to_json, 
              payment_headers
      end

      it 'should have bill id' do 
        json = JSON.parse(response.body)
        expect(json['bill_id']).to_not be_nil
      end

      it 'should have correct payee_id' do 
        json = JSON.parse(response.body)
        expect(json['payee_id']).to eq 'merchant_ps_test'
      end

      it 'should have ebilling_payment_id' do 
        json = JSON.parse(response.body)
        expect(json['ebilling_payment_id']).to eq '1234567890'
      end

      it 'should have transaction_fee' do 
        json = JSON.parse(response.body)
        expect(json['transaction_fee']).to eq 20
      end

      it "should have no error" do
        puts "#{response.body}"
        json = JSON.parse(response.body)
        expect(json['errors']).to be_nil
      end

      it "should have a audit success" do
        last_audit = AuditTrail.last_audit
        expect(last_audit.status).to eq "success"
      end

      it 'should have short_description' do 
        json = JSON.parse(response.body)
        expect(json['short_description']).to_not be_nil
      end

      it 'should have correct amount' do 
        json = JSON.parse(response.body)
        expect(json['amount']).to eq 100
      end

      it 'should have correct minimum_amount' do 
        json = JSON.parse(response.body)
        expect(json['minimum_amount']).to eq 50
      end

      it 'should have percentage' do 
        json = JSON.parse(response.body)
        expect(json['transaction_percent']).not_to eq nil
        expect(json['ps_transaction_percent']).not_to eq nil
      end
    end

    context 'when the bill_id not exist' do 

      before do 
        get "#{url}/00000.json", 
              { :client_transaction_id => "12345" }.to_json, 
              payment_headers
      end

      it { expect(response.status).to eq 404 }
    end

    context 'query successfully for partial' do

      before do 
        merchant.fee_strategy = FeeStrategy::VariableStrategy.create(
                maximum_amount: Money.new(500, merchant.currency), percentage: 3)
        merchant.save

        e_bill.trigger

        put "#{url}/#{e_bill.bill_id}.json", { :client_transaction_id => "12345", :paid_amount => 60 }.to_json, payment_headers
        get "#{url}/#{e_bill.bill_id}.json", 
              { :client_transaction_id => "12345" }.to_json, 
              payment_headers
      end

      it { expect(response.status).to eq 200 }

      it { expect(json_response['accept_partial_payment']).to eq false }

      it 'should have bill id' do 
        json = JSON.parse(response.body)
        expect(json['bill_id']).to_not be_nil
      end

      it 'should have amount_paid' do 
        json = JSON.parse(response.body)
        expect(json['amount_paid']).to eq 60
      end

      it 'should have correct amount' do 
        json = JSON.parse(response.body)
        expect(json['amount']).to eq 40
      end

      it 'should have correct minimum_amount' do 
        json = JSON.parse(response.body)
        expect(json['minimum_amount']).to eq 0
      end

      it 'transaction fee should be 0' do 
        json = JSON.parse(response.body)
        expect(json['transaction_fee']).to eq 0
      end

      it 'the traffic_fee should already stored' do 
        e_bill.reload
        expect(e_bill.traffic_fee.to_f).to eq 3
      end

      it 'should have percentage' do 
        json = JSON.parse(response.body)
        expect(json['transaction_percent']).to_not eq nil
        expect(json['ps_transaction_percent']).to_not eq nil
      end
    end

    context "get failed" do
      before do
        get "#{url}/#{e_bill.bill_id}.json?client_transaction_id=12345", 
              {}, 
              payment_headers
      end

      it { expect(response.status).to eq 422 }

      it "should has error" do
        puts "#{response.body}"
        json = JSON.parse(response.body)
        expect(json['client_transaction_id']).to eq '12345'
        expect(json['errors']['state']).to eq ['Invoice not payable']
      end
    end

    context 'when already paid' do 

      before do
        e_bill.trigger
        e_bill.pay
        e_bill.save
        get "#{url}/#{e_bill.bill_id}.json?client_transaction_id=12345", 
              {}, 
              payment_headers
      end

      it { expect(response.status).to eq 425 }

      it "should has error" do
        puts "#{response.body}"
        json = JSON.parse(response.body)
        expect(json['client_transaction_id']).to eq '12345'
        expect(json['errors']['state']).to eq ['Invoice not payable']
      end
    end

    context 'when cancelled' do 

      before do
        e_bill.trigger
        e_bill.cancel
        e_bill.save
        get "#{url}/#{e_bill.bill_id}.json?client_transaction_id=12345", 
              {}, 
              payment_headers
      end

      it { expect(response.status).to eq 427 }

      it "should has error" do
        puts "#{response.body}"
        json = JSON.parse(response.body)
        expect(json['client_transaction_id']).to eq '12345'
        expect(json['errors']['state']).to eq ['Invoice not payable']
      end
    end

    context 'when expired' do 

      before do
        e_bill.trigger
        e_bill.expire
        e_bill.save
        get "#{url}/#{e_bill.bill_id}.json?client_transaction_id=12345", 
              {}, 
              payment_headers
      end

      it { expect(response.status).to eq 426 }

      it "should has error" do
        puts "#{response.body}"
        json = JSON.parse(response.body)
        expect(json['client_transaction_id']).to eq '12345'
        expect(json['errors']['state']).to eq ['Invoice not payable']
      end
    end

    context 'when merchant_ps_mapping not exist' do 
      before do
         MerchantPaymentSystemMapping.find_by(merchant_id: merchant.id,
          payment_system_id: payment.id, merchant_ps_id: 'merchant_ps_test').try(:destroy)

        e_bill.trigger

        get "#{url}/#{e_bill.bill_id}.json?client_transaction_id=12345", 
              {}, 
              payment_headers
      end  

      it "should has error" do
        puts "#{response.body}"
        json = JSON.parse(response.body)
        expect(json['client_transaction_id']).to eq '12345'
        expect(json['errors']['payee_id']).to eq ['No payee_id found']
      end    
    end

  end

  # set the JSON headers include basic auth
  def json_headers username, password
    { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json', 
            'HTTP_AUTHORIZATION' =>  ActionController::HttpAuthentication::Basic.encode_credentials(username, password) }
  end  

  def payment_headers
    json_headers payment.username, payment.password
  end
end