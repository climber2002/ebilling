require 'spec_helper'

describe "/api/v1/payment_gateway/e_bills" do

  let!(:admin) { FactoryGirl.create(:admin) }
  let!(:service_desk) { FactoryGirl.create(:service_desk) }
  let!(:merchant) { FactoryGirl.create(:merchant) }
  let!(:merchant2) { FactoryGirl.create(:merchant2) }
  let!(:payment) { FactoryGirl.create(:payment) }
  let(:url) { "/api/v1/payment_gateway/e_bills" }

  before do 
    MerchantPaymentSystemMapping.find_or_create_by! merchant_id: merchant.id,
      payment_system_id: payment.id, merchant_ps_id: 'merchant_ps_test', ps_percentage: 2.5

    EbillingPaymentAccount.find_or_create_by! payment_system_id: payment.id,
      payment_id: '1234567890'

    merchant.fee_strategy = FeeStrategy::FixedStrategy.create(fixed_amount: 20)
  end

  after do 
    MerchantPaymentSystemMapping.find_by(merchant_id: merchant.id,
      payment_system_id: payment.id, merchant_ps_id: 'merchant_ps_test').try(:destroy)
  end

  context "query" do

    let(:e_bill) { FactoryGirl.create(:e_bill, user: merchant, amount: 100, accept_partial_payment: false) }

    before do
      e_bill.trigger
      get "#{url}/#{e_bill.bill_id}.json?client_transaction_id=12345", 
              {}, payment_headers
    end

    it { expect(response.status).to eq 200 }

    it 'should get ps_transaction_fee' do 
      expect(json_response['ps_transaction_fee']).to eq 2.5
    end

    it 'should update the fee after pay' do 
      e_bill.reload
      expect(e_bill.ps_transaction_fee.to_f).to eq 0
      put "#{url}/#{e_bill.bill_id}.json", { :client_transaction_id => "12345", 
            :paid_amount => '100.0' }.to_json, payment_headers

      e_bill.reload
      expect(e_bill.ps_transaction_fee.to_f).to eq 2.5
    end
  end

  # set the JSON headers include basic auth
  def json_headers username, password
    { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json', 
            'HTTP_AUTHORIZATION' =>  ActionController::HttpAuthentication::Basic.encode_credentials(username, password) }
  end

  def payment_headers
    json_headers payment.username, payment.password
  end
end