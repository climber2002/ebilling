require 'spec_helper'

describe "/api/v1/merchant/ebills" do
  let!(:admin) { FactoryGirl.create(:admin) }
  let!(:service_desk) { FactoryGirl.create(:service_desk) }
  let!(:merchant) { FactoryGirl.create(:merchant) }
  let!(:merchant2) { FactoryGirl.create(:merchant2) }
  let!(:ebill_creator) { FactoryGirl.create(:ebill_creator) }
  let(:url) { "/api/v1/merchant/e_bills/bulk_create" }

  context 'when create by a ebill creator' do 
    context 'when params valid' do 
      let(:params) do
        { 
          sms: true, 
          email: true,
          merchant_name: merchant.username,
          e_bills: [
            {
              payer_msisdn: '12345678',
              payer_name: 'Andy',
              amount: 200,
              short_description: 'short',
              due_date: '16/09/2015',
              payer_email: 'aaa@sina.com'
            },

            {
              payer_msisdn: '22222222',
              payer_name: 'Lee',
              amount: 100,
              short_description: 'short',
              due_date: '16/09/2015',
              payer_email: 'aaa@sina.com'
            },

            {
              payer_msisdn: '55555555',
              payer_name: 'Colin',
              amount: 100,
              short_description: 'short',
              due_date: '16/09/2015',
              payer_email: 'aaa@sina.com'
            }
          ]
        }
      end

      before do 
        api_post url, params, basic_auth(ebill_creator.username, ebill_creator.shared_key)
      end

      it { expect(response.status).to eq 200 }

      it 'the ebill should belong to merchant' do 
        EBill.all.each do |e_bill|
          expect(e_bill.user).to eq merchant
        end
      end
    end

    context 'when no merchant_name' do 
      let(:params) do
        { 
          sms: true, 
          email: true,
          e_bills: [
            {
              payer_msisdn: '12345678',
              payer_name: 'Andy',
              amount: 200,
              short_description: 'short',
              due_date: '16/09/2015',
              payer_email: 'aaa@sina.com'
            }
          ]
        }
      end

      before do 
        api_post url, params, basic_auth(ebill_creator.username, ebill_creator.shared_key)
      end

      it { expect(response.status).to eq 422 }
    end
  end

  context 'when create by a merchant' do 
    context 'when params valid' do
      let(:params) do
        { 
          sms: true, 
          email: true,
          e_bills: [
            {
              payer_msisdn: '12345678',
              payer_name: 'Andy',
              amount: 200,
              short_description: 'short',
              due_date: '16/09/2015',
              payer_email: 'aaa@sina.com'
            },

            {
              payer_msisdn: '22222222',
              payer_name: 'Lee',
              amount: 100,
              short_description: 'short',
              due_date: '16/09/2015',
              payer_email: 'aaa@sina.com'
            },

            {
              payer_msisdn: '55555555',
              payer_name: 'Colin',
              amount: 100,
              short_description: 'short',
              due_date: '16/09/2015',
              payer_email: 'aaa@sina.com'
            }
          ]
        }
      end

      before do 
        api_post url, params, basic_auth(merchant.username, merchant.shared_key)
      end

      it { expect(response.status).to eq 200 }
    end

    context 'when params invalid' do 
      let(:params) do
        { 
          sms: true, 
          email: true,
          e_bills: [
            {
              payer_msisdn: '12345678',
              payer_name: 'Andy',
              amount: 200,
              short_description: 'short',
              due_date: '16/09/2015',
              payer_email: 'aaa@sina.com'
            },

            {
              payer_msisdn: '22222222',
              payer_name: 'Lee',
              amount: 100,
              short_description: 'short',
              due_date: '16/09/2015'
            },

            {
              payer_msisdn: '55555555',
              payer_name: 'Colin',
              amount: 100,
              short_description: 'short',
              due_date: '16/09/2015',
              payer_email: 'aaa@sina.com'
            }
          ]
        }
      end

      before do 
        api_post url, params, basic_auth(merchant.username, merchant.shared_key)
      end

      it { expect(response.status).to eq 422 }
    end
  end

  
end