require 'spec_helper'

describe "/api/v1/merchant/ebills" do
  let!(:admin) { FactoryGirl.create(:admin) }
  let!(:service_desk) { FactoryGirl.create(:service_desk) }
  let!(:merchant) { FactoryGirl.create(:merchant) }
  let!(:merchant2) { FactoryGirl.create(:merchant2) }
  let(:url) { "/api/v1/merchant/e_bills" }

  describe "create" do
    it "should not be able to create if invalid authentication" do
      e_bill_attrs = FactoryGirl.attributes_for(:e_bill_for_api).merge!(payee_id: merchant.msisdn)
      post url, e_bill_attrs.to_json, json_headers("invalid", "invalid")

      puts "response code #{response.response_code} #{response.body}"
      puts "response: #{response.inspect}"
      expect(response.response_code).to eq 401
      json = JSON.parse(response.body)
      expect(json['message']).not_to be_nil
    end

    it "should not be able to create if authorization failed" do
      e_bill_attrs = FactoryGirl.attributes_for(:e_bill_for_api).merge!(payee_id: merchant.msisdn)
      post url, e_bill_attrs.to_json, json_headers(service_desk.username, service_desk.shared_key)

      puts "response code #{response.response_code} #{response.body}"
      expect(response.response_code).to eq 403
      json = JSON.parse(response.body)
      expect(json['message']).to eq I18n.t("api.errors.no_right")
    end

    context "create valid ebill" do
      let(:e_bill_attrs) do
        attrs = FactoryGirl.attributes_for(:e_bill_for_api)
        attrs.merge!(:client_transaction_id => '001', :payee_id => merchant.msisdn) 
      end

      before do
        post url, e_bill_attrs.to_json, merchant_headers
        puts "request body: #{e_bill_attrs.to_json}"
      end

      it "should be able to create e_bill" do
        expect(response.response_code).to eq 201
      end

      it "should return a valid ebill" do
        puts "#{response.body}"
        json = JSON.parse response.body
        json['e_bill']["payer_email"].should == e_bill_attrs[:payer_email]
        json['e_bill']["currency"].should == e_bill_attrs[:currency]
        json['e_bill']["state"].should == "created"
        json['e_bill']['external_reference'].should == "internal ref001"
        json['e_bill']['payer_name'].should == e_bill_attrs[:payer_name]
        json['e_bill']['payer_address'].should == e_bill_attrs[:payer_address]
        json['e_bill']['payer_city'].should == e_bill_attrs[:payer_city]
        json['e_bill']['payer_id'].should == 'payer_id'
        json["errors"].should be_nil
      end

      it "should add a new e_bill in db" do
        expect(EBill.all.count).to eq 1
      end

      it "should add a new success audit trail " do
        last_audit = AuditTrail.last_audit
        expect(last_audit.status).to eq "success"
      end

      it "should have an audit trail that the client_transaction_id is not nil" do
        last_audit = AuditTrail.last_audit
        expect(last_audit.client_transaction_id).to eq "001"
      end

      it "should not be nil for the params of audit trail" do
        last_audit = AuditTrail.last_audit
        expect(last_audit.params).not_to be_blank
      end

      it "should not be nil for the operator of audit trail" do
        last_audit = AuditTrail.last_audit
        expect(last_audit.operator).not_to be_blank
      end

    end
    
    context "create invalid e_bill" do
      let(:e_bill_attrs) { FactoryGirl.attributes_for(:e_bill, :client_transaction_id => "12345", :amount => nil) }

      before do
        post url, e_bill_attrs.to_json, merchant_headers
      end

      it "should not be able to create e_bill if validation failed" do
        puts "response code #{response.response_code} #{response.body}"
        expect(response.response_code).to eq 422
      end

      it "should have an error attribute in response" do
        json = JSON.parse(response.body)
        expect(json['errors']).not_to be_blank
      end

      it "should have client_transaction_id" do
        json = JSON.parse(response.body)
        expect(json['client_transaction_id']).to eq "12345"
      end

      it "should have server_transaction_id" do
        json = JSON.parse(response.body)
        expect(json['server_transaction_id']).not_to be_blank
      end

      it "should not create e_bill in db" do
        expect(EBill.all.count).to eq 0
      end

      it "should create a failed audit trail" do
        last_audit = AuditTrail.last_audit
        expect(last_audit.status).to eq "failed"
      end
    end
    
  end

  context 'create valid ebill with accept_partial_payment to true' do
      context 'valid params' do 
        let(:e_bill_attrs) do
          attrs = FactoryGirl.attributes_for(:e_bill_for_api, amount: 100, 
            accept_partial_payment: true, minimum_amount: 40)
          attrs.merge!(:client_transaction_id => '001', :payee_id => merchant.msisdn) 
        end

        before do
          post url, e_bill_attrs.to_json, merchant_headers
          puts "request body: #{e_bill_attrs.to_json}"
        end

        it "should be able to create e_bill" do
          expect(response.response_code).to eq 201
        end

        it "should return a valid ebill" do
          puts "#{response.body}"
          json = JSON.parse response.body
          json['e_bill']["payer_email"].should == e_bill_attrs[:payer_email]
          json['e_bill']["currency"].should == e_bill_attrs[:currency]
          json['e_bill']["state"].should == "created"
          json['e_bill']['external_reference'].should == "internal ref001"
          json['e_bill']['payer_name'].should == e_bill_attrs[:payer_name]
          json['e_bill']['payer_address'].should == e_bill_attrs[:payer_address]
          json['e_bill']['payer_city'].should == e_bill_attrs[:payer_city]
          json['e_bill']['accept_partial_payment'].should == true
          json['e_bill']['minimum_amount'].should == 40
          json["errors"].should be_nil
        end

      end

      context 'when no minimum_amount' do 
        let(:e_bill_attrs) do
          attrs = FactoryGirl.attributes_for(:e_bill_for_api, amount: 100, 
            accept_partial_payment: true)
          attrs.merge!(:client_transaction_id => '001', :payee_id => merchant.msisdn) 
        end

        before do
          post url, e_bill_attrs.to_json, merchant_headers
          puts "request body: #{e_bill_attrs.to_json}"
        end

        it "should be able to create e_bill" do
          expect(response.response_code).to eq 422
        end

        it "should return a valid ebill" do
          puts "#{response.body}"
          json = JSON.parse response.body
          json["errors"].should_not be_nil
          expect(json["errors"]['minimum_amount']).to eq ["can't be blank"]
        end
      end

      context 'when minimum_amount is too big' do 
        let(:e_bill_attrs) do
          attrs = FactoryGirl.attributes_for(:e_bill_for_api, amount: 100, 
            accept_partial_payment: true, minimum_amount: 120)
          attrs.merge!(:client_transaction_id => '001', :payee_id => merchant.msisdn) 
        end

        before do
          post url, e_bill_attrs.to_json, merchant_headers
          puts "request body: #{e_bill_attrs.to_json}"
        end

        it "should be able to create e_bill" do
          expect(response.response_code).to eq 422
        end

        it "should return a valid ebill" do
          puts "#{response.body}"
          json = JSON.parse response.body
          json["errors"].should_not be_nil
          expect(json["errors"]['minimum_amount']).to eq ["should be less than or equal to amount"]
        end
      end
    end

  context "send e_bill" do
    let(:e_bill) { FactoryGirl.create(:e_bill, user: merchant) }
    let(:send_url) { "#{url}/#{e_bill.bill_id}/send.json" }
    context "send e_bill success" do
      before do
        put send_url, {:client_transaction_id => '123'}.to_json, merchant_headers
      end

      it "should set ebill state to ready" do
        puts "send response_code #{response.code}"
        puts "response body: #{response.body}"
        e_bill.reload
        expect(e_bill.state).to eq "ready"
      end

      it "should success" do
        puts "body: #{response.body}"
        json = JSON.parse(response.body)
        expect(json['errors']).to be_nil
        expect(json['error']).to be_nil
      end
    end
    
    context "send e_bill failed" do
      before do
        e_bill.trigger
        put send_url, {:client_transaction_id => '123'}.to_json , merchant_headers
      end

      it "should fail" do
        puts "body: #{response.body}"
        json = JSON.parse(response.body)
        expect(json['errors']).not_to be_nil
      end
    end
  end

  context "get e_bill" do
    let(:e_bill) { FactoryGirl.create(:e_bill, user: merchant) }
    let(:get_url) { "#{url}/#{e_bill.bill_id}.json" }

    before do
      get get_url, "", merchant_headers
    end

    it "should has no error in response" do
      puts "body: #{response.body}"
      json = JSON.parse(response.body)
      expect(json['errors']).to be_nil
      expect(json['error']).to be_nil
    end

    it "should return the ebill" do
      json = JSON.parse(response.body)
      expect(json['bill_id']).to eq e_bill.bill_id
    end
  end

  context "get e_bills" do
    1.upto 5 do |i|
      let!("e_bill#{i}") { FactoryGirl.create(:e_bill, user: merchant) }
    end
    let(:get_url) { "#{url}.json" }

    context "without params" do
      before do 
        get get_url, "", merchant_headers
      end

      it "should return 5 bills per page" do
        puts "body: #{response.body}"
        json = JSON.parse(response.body)

        expect(json['entries'].size).to eq 5
      end

      it "should have 5 ebills total" do
        json = JSON.parse(response.body)
        expect(json['total_entries']).to eq 5        
      end
    end

    context "with params" do
      before do
        get "#{get_url}?page=2&per_page=2", "", merchant_headers
      end

      it "should return 2 ebills" do
        puts "response: #{response.body}"
        json = JSON.parse(response.body)
        expect(json['entries'].size).to eq 2
      end

      it "should have 5 ebills total" do
        json = JSON.parse(response.body)
        expect(json['total_entries']).to eq 5        
      end
    end
    
  end
  
  # set the JSON headers include basic auth
  def json_headers username, password
    { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json', 
            'HTTP_AUTHORIZATION' =>  ActionController::HttpAuthentication::Basic.encode_credentials(username, password) }
  end  

  def merchant_headers
    json_headers merchant.username, merchant.shared_key
  end
end