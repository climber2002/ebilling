require 'spec_helper'

describe "/api/v1/mobileapp/e_bills/search", type: :request do

  let(:url) { "/api/v1/mobileapp/e_bills/search" }

  let!(:admin) { FactoryGirl.create(:admin) }

  let!(:merchant) { FactoryGirl.create(:merchant) }
  let!(:merchant2) { FactoryGirl.create(:merchant, username: 'merchant2') }

  let!(:payment_system_operator) { FactoryGirl.create(:payment_system_operator) }
  let!(:payment_system) { payment_system_operator.payment_system }
  let!(:payment_system_operator2) { FactoryGirl.create(:payment_system_operator, username: "ps_operator_#{rand(10)}") }
  let!(:payment_system2) { payment_system_operator2.payment_system }

  [:e_bill10, :e_bill11, :e_bill12].each_with_index do |bill_name, index|
    let!(bill_name) { FactoryGirl.create(:e_bill, user: merchant, 
      merchant_internal_ref: "ref#{index}", ps_transaction_id: '12345') }
  end

  [:e_bill20, :e_bill21, :e_bill22].each do |bill_name|
    let!(bill_name) { FactoryGirl.create(:e_bill, user: merchant2,
      ps_transaction_id: '12345') }
  end

  before do 
    [e_bill10, e_bill20].each do |e_bill|
      e_bill.update_attributes(state: 'paid', payment_system_id: payment_system.id,
      amount_paid: e_bill.amount)
    end

    [e_bill11, e_bill21].each do |e_bill|
      e_bill.update_attributes(state: 'paid', payment_system_id: payment_system2.id,
      amount_paid: e_bill.amount)
    end
  end

  context 'when search by a merchant' do 
    context 'search' do 
      before do 
        api_get url, {}, basic_auth(merchant.username, merchant.password)
      end

      it { expect(response.status).to eq 200 }
      it do
        expect(json_response[:total_pages]).to eq 1
        expect(json_response[:per_page]).to eq 20
        expect(json_response[:e_bills].size).to eq 3
      end
    end

    context 'search with msisdn' do 

      before do 
        api_get url, { payer_msisdn: e_bill10.payer_msisdn }, basic_auth(merchant.username, merchant.password)
      end

      it { expect(response.status).to eq 200 }
      it do
        expect(json_response[:total_pages]).to eq 1
        expect(json_response[:per_page]).to eq 20
        expect(json_response[:e_bills].size).to eq 1
      end
      it 'should return ps_transaction_id' do 
        expect(json_response[:e_bills].first[:ps_transaction_id]).to eq '12345'
      end
    end

    context 'search with external_reference' do 

      before do 
        api_get url, { external_reference: "ref0" }, basic_auth(merchant.username, merchant.password)
      end

      it { expect(response.status).to eq 200 }
      it do
        expect(json_response[:total_pages]).to eq 1
        expect(json_response[:per_page]).to eq 20
        expect(json_response[:e_bills].size).to eq 1
      end
    end

    context 'search with bill id' do 

      before do 
        api_get url, { bill_id: e_bill10.bill_id }, basic_auth(merchant.username, merchant.password)
      end

      it { expect(response.status).to eq 200 }
      it do
        expect(json_response[:total_pages]).to eq 1
        expect(json_response[:per_page]).to eq 20
        expect(json_response[:e_bills].size).to eq 1
      end
    end
  end

  context 'when search by an admin' do 
    context 'when search without params' do 
      before do 
        api_get url, { }, basic_auth(admin.username, admin.password)
      end

      it { expect(response.status).to eq 200 }
      it 'should return all ebills' do
        expect(json_response[:total_pages]).to eq 1
        expect(json_response[:per_page]).to eq 20
        expect(json_response[:e_bills].size).to eq 6
      end
    end

    context 'search by state' do 
      before do 
        api_get url, { state: 'paid' }, basic_auth(admin.username, admin.password)
      end

      it { expect(response.status).to eq 200 }
      it do
        expect(json_response[:total_pages]).to eq 1
        expect(json_response[:per_page]).to eq 20
        expect(json_response[:e_bills].size).to eq 4
        json_response[:e_bills].each do |e_bill|
          expect(e_bill[:state]).to eq 'paid'
        end
      end
    end
  end


  context 'when search by a payment_system_operator' do 
    context 'when search without params' do 
      before do 
        api_get url, { }, basic_auth(payment_system_operator.username, 
          payment_system_operator.password)
      end

      it { expect(response.status).to eq 200 }

      it 'should return ebills only paid to payment_system' do
        expect(json_response[:total_pages]).to eq 1
        expect(json_response[:per_page]).to eq 20
        expect(json_response[:e_bills].size).to eq 2
        expect(json_response[:e_bills].map {|e_bill| e_bill[:payer_msisdn]}).to eq [e_bill10.payer_msisdn, e_bill20.payer_msisdn]
      end
    end
  end

end