require 'spec_helper'

describe "/api/v1/mobileapp/e_bills/by_external_reference", type: :request do

  let(:url) { "/api/v1/mobileapp/e_bills/by_external_reference" }

  let!(:admin) { FactoryGirl.create(:admin) }

  let!(:merchant) { FactoryGirl.create(:merchant) }
  let!(:merchant2) { FactoryGirl.create(:merchant, username: 'merchant2') }

  let!(:payment_system_operator) { FactoryGirl.create(:payment_system_operator) }
  let!(:payment_system) { payment_system_operator.payment_system }
  let!(:payment_system_operator2) { FactoryGirl.create(:payment_system_operator, username: "ps_operator_#{rand(10)}") }
  let!(:payment_system2) { payment_system_operator2.payment_system }

  [:e_bill10, :e_bill11, :e_bill12].each_with_index do |bill_name, index|
    let!(bill_name) { FactoryGirl.create(:e_bill, user: merchant, 
      merchant_internal_ref: "ref_#{index}", ps_transaction_id: '12345',
      payer_id: "payer_id_#{index}", payer_code: "payer_code_#{index}") }
  end

  [:e_bill20, :e_bill21, :e_bill22].each_with_index do |bill_name, index|
    let!(bill_name) { FactoryGirl.create(:e_bill, user: merchant2,
      merchant_internal_ref: "ref_#{index}",
      ps_transaction_id: '12345', external_reference: "ref_#{index}",
      payer_id: "payer_id_#{index}", payer_code: "payer_code_#{index}") }
  end

  before do 
    [e_bill10, e_bill20].each do |e_bill|
      e_bill.update_attributes(state: 'ready', payment_system_id: payment_system.id)
    end

    [e_bill11, e_bill21].each do |e_bill|
      e_bill.update_attributes(state: 'partially_paid', payment_system_id: payment_system2.id,
      amount_paid: e_bill.amount - 5)
    end
  end

  context 'when invalid login' do 
    before do 
      api_get url, {}, basic_auth(merchant.username, 'invalid')
    end

    it { expect(response.status).to eq 401 }
    it do 
      expect(json_response[:error]).to eq "This action must be authenticated."
    end
  end

  context 'when search by admin' do 
    before do 
      api_get url, { external_reference: 'ref_0' }, basic_auth(admin.username, admin.password)
    end

    it { expect(response.status).to eq 403 }
    it { expect(json_response[:message]).to eq "No right to perform this action" }
      
  end

  context 'when no external_reference' do 
    before do 
      api_get url, {}, basic_auth(merchant.username, merchant.shared_key)
    end

    it { expect(response.status).to eq 422 }
    it { expect(json_response[:error]).to eq "external_reference or payer_id is mandatory" }
  end

  context 'when search by payment_system' do 
    before do 
      api_get url, {external_reference: "ref_0"}, basic_auth(payment_system.username, payment_system.password)
    end

    it { expect(response.status).to eq 200 }
    it { expect(json_response[:e_bills].size).to eq 2 }
    it do 
      json_response[:e_bills].each do |e_bill|
        expect(e_bill['external_reference']).to eq 'ref_0'
      end
    end
  end

  context 'when search by payment_system and status parially_paid' do 
    before do 
      api_get url, { external_reference: "ref_1" }, basic_auth(payment_system.username, payment_system.password)
    end

    it { expect(response.status).to eq 200 }
    it { expect(json_response[:e_bills].size).to eq 2 }
    it do 
      json_response[:e_bills].each do |e_bill|
        expect(e_bill['external_reference']).to eq 'ref_1'
      end
    end
  end

  context 'when search by merchant' do 
    context 'by external_reference' do 
      before do 
        api_get url, { external_reference: "ref_0", payee_name: merchant.username }, basic_auth(merchant.username, merchant.shared_key)
      end

      it { expect(response.status).to eq 200 }
      it 'should only return ebill of that merchant' do 
        expect(json_response[:e_bills].size).to eq 1 
      end
      it do 
        json_response[:e_bills].each do |e_bill|
          expect(e_bill['external_reference']).to eq 'ref_0'
          expect(e_bill['payee_id']).to eq merchant.msisdn
        end
      end
    end
    
    context 'by payer_id' do 
      before do 
        api_get url, { payer_id: "payer_id_0", payee_name: merchant.username }, basic_auth(merchant.username, merchant.shared_key)
      end

      it { expect(response.status).to eq 200 }
      it 'should only return ebill of that merchant' do 
        expect(json_response[:e_bills].size).to eq 1 
      end
      it do 
        json_response[:e_bills].each do |e_bill|
          expect(e_bill['external_reference']).to eq 'ref_0'
          expect(e_bill['payee_id']).to eq merchant.msisdn
        end
      end
    end
  end

  context 'when search by invalid external_reference' do 
    before do 
      api_get url, {external_reference: "ref_132342"}, basic_auth(merchant.username, merchant.shared_key)
    end

    it { expect(response.status).to eq 200 }
    it 'should only return ebill of that merchant' do 
      expect(json_response[:e_bills].size).to eq 0
    end
  end
  
end