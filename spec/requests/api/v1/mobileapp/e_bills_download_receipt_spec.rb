require 'spec_helper'

describe "/api/v1/mobileapp/e_bills/download_receipt", type: :request do

  let(:url) { "/api/v1/mobileapp/e_bills/download_receipt" }

  let!(:admin) { FactoryGirl.create(:admin) }

  let!(:merchant) { FactoryGirl.create(:merchant) }
  let!(:merchant2) { FactoryGirl.create(:merchant, username: 'merchant2') }

  let!(:payment_system_operator) { FactoryGirl.create(:payment_system_operator) }
  let!(:payment_system) { payment_system_operator.payment_system }
  let!(:payment_system_operator2) { FactoryGirl.create(:payment_system_operator, username: "ps_operator_#{rand(10)}") }
  let!(:payment_system2) { payment_system_operator2.payment_system }

  [:e_bill10, :e_bill11, :e_bill12].each_with_index do |bill_name, index|
    let!(bill_name) { FactoryGirl.create(:e_bill, user: merchant, 
      merchant_internal_ref: "ref_#{index}", ps_transaction_id: "tid_#{index}") }
  end

  [:e_bill20, :e_bill21, :e_bill22].each_with_index do |bill_name, index|
    let!(bill_name) { FactoryGirl.create(:e_bill, user: merchant2,
      merchant_internal_ref: "ref_#{index}",
      ps_transaction_id: 'tid2_#{index}', external_reference: "ref_#{index}") }
  end

  before do 
    [e_bill10, e_bill20].each do |e_bill|
      e_bill.update_attributes(state: 'ready', payment_system_id: payment_system.id)
    end

    [e_bill11, e_bill21].each do |e_bill|
      e_bill.update_attributes(state: 'partially_paid', payment_system_id: payment_system2.id,
      amount_paid: e_bill.amount - 5)
    end
  end

  context 'when invalid login' do 
    before do 
      api_get url, {}, basic_auth(merchant.username, 'invalid')
    end

    it { expect(response.status).to eq 401 }
    it do 
      expect(json_response[:error]).to eq "This action must be authenticated."
    end
  end

  context 'when search by admin' do 
    before do 
      api_get url, { ps_transaction_id: 'ref_0' }, basic_auth(admin.username, admin.password)
    end

    it { expect(response.status).to eq 403 }
    it { expect(json_response[:message]).to eq "No right to perform this action" }
      
  end

  context 'when no ps_transaction_id' do 
    before do 
      api_get url, {}, basic_auth(merchant.username, merchant.shared_key)
    end

    it { expect(response.status).to eq 422 }
    it { expect(json_response[:error]).to eq "ps_transaction_id is mandatory" }
  end

  context 'when ps_transaction_id not found' do 
    before do 
      api_get url, {ps_transaction_id: 'notfound'}, basic_auth(merchant.username, merchant.shared_key)
    end

    it { expect(response.status).to eq 404 }
    it { expect(json_response[:error]).to eq "ps_transaction_id not found" }
  end

  context 'when ps_transaction_id valid' do 
    before do 
      api_get url, {ps_transaction_id: 'tid_0'}, basic_auth(merchant.username, merchant.shared_key)
    end

    it do 
      expect(response.status).to eq 200 
    end
  end  
end