require 'spec_helper'

describe "/api/v1/mobileapp/e_bills/history", type: :request do

  let(:url) { "/api/v1/mobileapp/e_bills/history" }
  
  let!(:admin) { FactoryGirl.create(:admin) }

  let!(:merchant) { FactoryGirl.create(:merchant) }
  let!(:merchant2) { FactoryGirl.create(:merchant, username: 'merchant2') }
  let!(:merchant3) { FactoryGirl.create(:merchant, username: 'merchant3', currency: 'USD') }

  let!(:payment_system_operator) { FactoryGirl.create(:payment_system_operator) }
  let!(:payment_system) { payment_system_operator.payment_system }
  let!(:payment_system_operator2) { FactoryGirl.create(:payment_system_operator, username: "ps_operator_#{rand(1000)}") }
  let!(:payment_system2) { payment_system_operator2.payment_system }

  [:e_bill10, :e_bill11, :e_bill12, :e_bill13, :e_bill14].each_with_index do |bill_name, index|
    let!(bill_name) { FactoryGirl.create(:e_bill, user: merchant, merchant_internal_ref: "ref#{index}") }
  end

  [:e_bill20, :e_bill21, :e_bill22, :e_bill23, :e_bill24].each do |bill_name|
    let!(bill_name) { FactoryGirl.create(:e_bill, user: merchant2) }
  end

  [:e_bill30, :e_bill31, :e_bill32, :e_bill33, :e_bill34].each do |bill_name|
    let!(bill_name) { FactoryGirl.create(:e_bill, user: merchant3) }
  end

  before do 
    [e_bill10, e_bill20, e_bill30].each do |e_bill|
      e_bill.update_attributes(state: 'paid', payment_system_id: payment_system.id,
      amount_paid: e_bill.amount, paid_at: Time.now, sent_at: Time.now)
    end

    [e_bill11, e_bill21, e_bill31].each do |e_bill|
      e_bill.update_attributes(state: 'paid', payment_system_id: payment_system2.id,
      amount_paid: e_bill.amount, paid_at: Time.now, sent_at: Time.now)
    end

    [e_bill12, e_bill22, e_bill32].each do |e_bill|
      e_bill.update_attributes(state: 'ready', sent_at: Time.now)
    end

    [e_bill13, e_bill23, e_bill33].each do |e_bill|
      e_bill.update_attributes(state: 'processed', payment_system_id: payment_system.id,
      amount_paid: e_bill.amount, sent_at: Time.now, paid_at: Time.now, processed_at: Time.now)
    end

    MerchantGlobalStatsPopulator.new.populate
    MerchantMonthlyStatsPopulator.new.populate(Date.today)
    PaymentSystemMonthlyStatsPopulator.new.populate(Date.today)
  end

  context 'when get by an admin' do 
    before do 
      api_get url, { }, basic_auth(admin.username, admin.password)
    end

    it { expect(response.status).to eq 200 }

    it 'should return correct all_currencies' do 
      expect(json_response[:all_currencies].sort).to eq ['USD', 'XAF']
    end

    it 'should return correct global counts' do
      expect(json_response[:global_counts][:created_count]).to eq 3
      expect(json_response[:global_counts][:ready_count]).to eq 3
      expect(json_response[:global_counts][:paid_count]).to eq 6
      expect(json_response[:global_counts][:partially_paid_count]).to eq 0
      expect(json_response[:global_counts][:cancelled_count]).to eq 0
      expect(json_response[:global_counts][:failed_count]).to eq 0
      expect(json_response[:global_counts][:processed_count]).to eq 3
      expect(json_response[:global_counts][:expired_count]).to eq 0
      expect(json_response[:global_counts][:overdue_count]).to eq 0
      expect(json_response[:global_counts][:total_count]).to eq 15

      expect(json_response[:global_counts][:revenues]['XAF']).to_not be_nil
      expect(json_response[:global_counts][:revenues]['USD']).to_not be_nil
    end

    it 'should return correct monthly history' do 
      expect(json_response[:monthly_history].size).to eq 1
      expect(json_response[:monthly_history].first[:year]).to eq Date.today.year
      expect(json_response[:monthly_history].first[:month]).to eq Date.today.month
      expect(json_response[:monthly_history].first[:total_count]).to eq 15
      expect(json_response[:monthly_history].first[:paid_count]).to eq 9
      expect(json_response[:monthly_history].first[:partially_paid_count]).to eq 0

      expect(json_response[:monthly_history].first[:revenues]['USD'].class).to eq Float
      expect(json_response[:monthly_history].first[:revenues]['XAF']).to_not be_nil

      expect(json_response[:monthly_history].first[:traffic_fees]['USD']).to_not be_nil
      expect(json_response[:monthly_history].first[:traffic_fees]['XAF']).to_not be_nil

      expect(json_response[:monthly_history].first[:recurrent_fees]['USD']).to_not be_nil
      expect(json_response[:monthly_history].first[:recurrent_fees]['XAF']).to_not be_nil
    end

    it 'should have top10' do 
      expect(json_response[:top10]).to_not be_nil
      expect(json_response[:top10][:total].size).to eq 3
      expect(json_response[:top10][:paid].size).to eq 3
      expect(json_response[:top10][:paid].first[:merchant_name]).to_not be_nil
      expect(json_response[:top10][:paid].first[:count]).to_not be_nil
    end

    it 'should have merchants_current_month' do 
      expect(json_response[:merchants_current_month].size).to eq 3
    end

    it 'should have merchants_global' do 
      expect(json_response[:merchants_global].size).to eq 3
    end
  end

  context 'when get by a payment_system_operator' do 
    before do 
      api_get url, { }, basic_auth(payment_system_operator.username, payment_system_operator.password)
    end

    it { expect(response.status).to eq 200 }

    it 'should return correct all_currencies' do 
      expect(json_response[:all_currencies].sort).to eq ["USD", "XAF"]
    end

    it 'should return correct global counts' do
      expect(json_response[:global_counts][:paid_count]).to eq 6
      expect(json_response[:global_counts][:partially_paid_count]).to eq 0
      expect(json_response[:global_counts][:processed_count]).to eq 3

      expect(json_response[:global_counts][:cancelled_count]).to be_nil
      expect(json_response[:global_counts][:failed_count]).to be_nil
      expect(json_response[:global_counts][:created_count]).to be_nil
      expect(json_response[:global_counts][:ready_count]).to be_nil
      expect(json_response[:global_counts][:expired_count]).to be_nil
      expect(json_response[:global_counts][:overdue_count]).to be_nil
      expect(json_response[:global_counts][:total_count]).to be_nil

      expect(json_response[:global_counts][:revenues]['XAF']).to_not be_nil
      expect(json_response[:global_counts][:revenues]['USD']).to_not be_nil
    end

    it 'should return correct monthly history' do 
      expect(json_response[:monthly_history].size).to eq 1
      expect(json_response[:monthly_history].first[:year]).to eq Date.today.year
      expect(json_response[:monthly_history].first[:month]).to eq Date.today.month
      expect(json_response[:monthly_history].first[:total_count]).to eq 6
      expect(json_response[:monthly_history].first[:paid_count]).to eq 6
      expect(json_response[:monthly_history].first[:partially_paid_count]).to eq 0
      expect(json_response[:monthly_history].first[:processed_count]).to eq 3
      expect(json_response[:monthly_history].first[:revenues]['USD']).to_not be_nil
      expect(json_response[:monthly_history].first[:revenues]['XAF']).to_not be_nil
    end

    it 'should have top10' do 
      expect(json_response[:top10][:paid]).to_not be_nil
    end

    it 'should have merchants_current_month' do 
      expect(json_response[:merchants_current_month].size).to eq 3
      expect(json_response[:merchants_current_month].first[:merchant_name]).to eq merchant.username
    end

    it 'should have merchants_global' do 
      expect(json_response[:merchants_global].size).to eq 3
    end
  end

  context 'when get by a merchant' do 
    before do 
      api_get url, { }, basic_auth(merchant.username, merchant.password)
    end

    it { expect(response.status).to eq 200 }

    it 'should return correct all_currencies' do 
      expect(json_response[:all_currencies]).to eq ['XAF']
    end

    it 'should return correct global counts' do
      expect(json_response[:global_counts][:created_count]).to eq 1
      expect(json_response[:global_counts][:ready_count]).to eq 1
      expect(json_response[:global_counts][:paid_count]).to eq 2
      expect(json_response[:global_counts][:partially_paid_count]).to eq 0
      expect(json_response[:global_counts][:cancelled_count]).to eq 0
      expect(json_response[:global_counts][:failed_count]).to eq 0
      expect(json_response[:global_counts][:processed_count]).to eq 1
      expect(json_response[:global_counts][:expired_count]).to eq 0
      expect(json_response[:global_counts][:overdue_count]).to eq 0
      expect(json_response[:global_counts][:total_count]).to eq 5

      expect(json_response[:global_counts][:revenues]['XAF']).to_not be_nil
      expect(json_response[:global_counts][:revenues]['USD']).to be_nil
    end

    it 'should return correct monthly history' do 
      expect(json_response[:monthly_history].size).to eq 1
      expect(json_response[:monthly_history].first[:year]).to eq Date.today.year
      expect(json_response[:monthly_history].first[:month]).to eq Date.today.month
      expect(json_response[:monthly_history].first[:total_count]).to eq 5
      expect(json_response[:monthly_history].first[:paid_count]).to eq 3
      expect(json_response[:monthly_history].first[:partially_paid_count]).to eq 0

      expect(json_response[:monthly_history].first[:revenues]['USD']).to be_nil
      expect(json_response[:monthly_history].first[:revenues]['XAF']).to_not be_nil

      expect(json_response[:monthly_history].first[:traffic_fees]['USD']).to be_nil
      expect(json_response[:monthly_history].first[:traffic_fees]['XAF']).to_not be_nil

      expect(json_response[:monthly_history].first[:recurrent_fees]['USD']).to be_nil
      expect(json_response[:monthly_history].first[:recurrent_fees]['XAF']).to_not be_nil
    end

    it 'should have no top10' do 
      expect(json_response[:top10]).to be_nil
    end

    it 'should have merchants_current_month' do 
      expect(json_response[:merchants_current_month].size).to eq 1
      expect(json_response[:merchants_current_month].first[:merchant_name]).to eq merchant.username
    end

    it 'should have merchants_global' do 
      expect(json_response[:merchants_global].size).to eq 1
    end
  end

end