require 'spec_helper'

describe "/api/v1/mobileapp/mobiles" do

  let(:url) { "api/v1/mobileapp/mobiles" }
  let!(:merchant) { FactoryGirl.create(:merchant) }

  describe 'register' do 
    describe 'create Mobile' do 
      before do 
        api_post url, { imei: '23234234' }, basic_auth(merchant.username, merchant.password)
      end

      it { expect(response.status).to eq 201 }
      it { expect(json_response['mobile']['confirmed']).to eq false }
      it { expect(json_response['mobile']['profile_name']).to eq 'Merchant'}
      it { expect(json_response['mobile']['currency']).to eq merchant.currency }
      it { expect(json_response['mobile']['username']).to eq merchant.username }
      it { expect(json_response['mobile']['email']).to eq merchant.email }
      it { expect(json_response['mobile']['msisdn']).to eq merchant.msisdn }
    end

    describe 'when password not match' do 
      before do 
        api_post url, { imei: '23234277' }, basic_auth(merchant.username, 'invalid')
      end

      it { expect(response.status).to eq 401 }
      
    end

    describe 'when imei already exists' do 
      context 'when imei belongs to merchant' do 
        let(:imei) { '632324324' }
        before do 
          merchant.mobiles.create!(imei: imei)
          api_post url, { imei: imei }, basic_auth(merchant.username, merchant.password)
        end

        it { expect(response.status).to eq 423 }

      end

      context 'when imei belongs to other merchant' do 

        let(:merchant2) { FactoryGirl.create(:merchant, username: 'merchant2') }
        let(:imei) { '632324325' }

        before do 
          merchant2.mobiles.create!(imei: imei)
          api_post url, { imei: imei }, basic_auth(merchant.username, merchant.password)
        end

        it { expect(response.status).to eq 422 }

      end
    end

    describe 'when imei is empty' do 

      before do 
        api_post url, { imei: '' }, basic_auth(merchant.username, merchant.password)
      end

      it { expect(response.status).to eq 499 }
    end

  end

  describe 'index' do 
    before do 
      merchant.mobiles.create!(imei: '124343423')
      merchant.mobiles.create!(imei: '234234234')
      api_get url, {}, basic_auth(merchant.username, merchant.password)
    end

    it { expect(response.status).to eq 200 }
    it 'should have correct mobiles' do 
      expect(json_response[0]['imei']).to eq '124343423'
      expect(json_response[1]['imei']).to eq '234234234'
    end
  end

  describe 'verify' do 
    
    let(:verify_url) { "api/v1/mobileapp/mobiles/#{imei}/verify" } 

    context 'when valid' do 

      let(:imei) { '23234235' }

      before do 
        api_post url, { imei: imei }, basic_auth(merchant.username, merchant.password)

        otp = Mobile.last.otp

        api_put verify_url, { otp: otp }, basic_auth(merchant.username, merchant.password)
      end    

      it { expect(response.status).to eq 200 }
      it { expect(json_response[:mobile][:confirmed]).to eq true }
    end

    context 'when already expired' do

      let(:imei) { '23234255' }

      before do 
        api_post url, { imei: imei }, basic_auth(merchant.username, merchant.password)

        otp = Mobile.last.otp

        Timecop.travel(16.minutes)

        api_put verify_url, { otp: otp }, basic_auth(merchant.username, merchant.password)
      end    

      it { expect(response.status).to eq 422 }
      it { expect(json_response[:errors][:otp].first).to eq 'already expired' }
    end
    
  end

  describe 'update pin' do 

    let(:verify_url) { "api/v1/mobileapp/mobiles/#{imei}/verify" } 
    let(:update_url) { "api/v1/mobileapp/mobiles/#{imei}" }

    before do 
      api_post url, { imei: imei }, basic_auth(merchant.username, merchant.password)

      otp = Mobile.last.otp

      api_put verify_url, { otp: otp }, basic_auth(merchant.username, merchant.password)
    end

    context 'when valid' do 

      let(:imei) { '23234335' }

      before do 
        api_put update_url, { pin: '1234' }, basic_auth(merchant.username, merchant.password)
      end    

      it { expect(response.status).to eq 200 }
      it { expect(json_response[:mobile][:confirmed]).to eq true }
    end

    context 'when pin is not 4 digits' do 
      let(:imei) { '23234338' }

      before do 
        api_put update_url, { pin: 'abcd' }, basic_auth(merchant.username, merchant.password)
      end    

      it { expect(response.status).to eq 422 }
      it { expect(json_response[:errors][:pin].first).to eq 'must be 4 digits' }
    end

  end

  describe 'forget pin' do 
    let(:verify_url) { "api/v1/mobileapp/mobiles/#{imei}/verify" } 
    let(:update_url) { "api/v1/mobileapp/mobiles/#{imei}" }
    let(:forget_pin_url) { "api/v1/mobileapp/mobiles/#{imei}/forget_pin" }

    before do 
      api_post url, { imei: imei }, basic_auth(merchant.username, merchant.password)

      otp = Mobile.last.otp

      api_put verify_url, { otp: otp }, basic_auth(merchant.username, merchant.password)
      api_put update_url, { pin: '1234' }, basic_auth(merchant.username, merchant.password)
    end

    context 'when valid' do 

      let(:imei) { '23234335' }

      before do 
        api_put forget_pin_url, {}, basic_auth(merchant.username, merchant.password)
      end    

      it { expect(response.status).to eq 200 }
      it { expect(json_response[:mobile][:confirmed]).to eq false }
    end

  end

  describe 'destroy' do 
    let(:imei) { '23234235' }
    let(:delete_url) { "api/v1/mobileapp/mobiles/#{imei}" }
    
    before do 
      api_post url, { imei: imei }, basic_auth(merchant.username, merchant.password)

      api_delete delete_url, {}, basic_auth(merchant.username, merchant.password)
    end    

    it 'should not exist the imei' do 
      expect(merchant.mobiles.find_by(imei: imei)).to be_nil
    end

    it { expect(response.status).to eq 200 }
  end
  
end