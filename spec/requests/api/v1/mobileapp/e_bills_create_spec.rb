require 'spec_helper'

describe 'api/v1/mobileapp/e_bills' do 
  let!(:admin) { FactoryGirl.create(:admin) }
  let!(:service_desk) { FactoryGirl.create(:service_desk) }
  let!(:merchant) { FactoryGirl.create(:merchant) }
  let!(:merchant2) { FactoryGirl.create(:merchant2) }
  let(:url) { "/api/v1/mobileapp/e_bills" }

  describe "create" do
    it "should not be able to create if invalid authentication" do
      e_bill_attrs = FactoryGirl.attributes_for(:e_bill_for_api).merge!(payee_id: merchant.msisdn)
      post url, e_bill_attrs.to_json, json_headers("invalid", "invalid")

      puts "response code #{response.response_code} #{response.body}"
      puts "response: #{response.inspect}"
      expect(response.response_code).to eq 401
      json = JSON.parse(response.body)
      expect(json['error']).not_to be_nil
    end

    it "should not be able to create if authorization failed" do
      e_bill_attrs = FactoryGirl.attributes_for(:e_bill_for_api).merge!(payee_id: merchant.msisdn)
      post url, e_bill_attrs.to_json, json_headers(service_desk.username, service_desk.password)

      puts "response code #{response.response_code} #{response.body}"
      expect(response.response_code).to eq 403
      json = JSON.parse(response.body)
      expect(json['message']).to eq I18n.t("api.errors.no_right")
    end

    context "create valid ebill" do
      let(:e_bill_attrs) do
        attrs = FactoryGirl.attributes_for(:e_bill_for_api)
        attrs.merge!(:client_transaction_id => '001', :payee_id => merchant.msisdn) 
      end

      before do
        post url, e_bill_attrs.to_json, merchant_headers
        puts "request body: #{e_bill_attrs.to_json}"
      end

      it "should be able to create e_bill" do
        expect(response.response_code).to eq 201
      end

      it "should return a valid ebill" do
        puts "#{response.body}"
        json = JSON.parse response.body
        json['e_bill']["payer_email"].should == e_bill_attrs[:payer_email]
        json['e_bill']["currency"].should == e_bill_attrs[:currency]
        json['e_bill']["state"].should == "created"
        json['e_bill']['external_reference'].should == "internal ref001"
        json['e_bill']['payer_name'].should == e_bill_attrs[:payer_name]
        json['e_bill']['payer_address'].should == e_bill_attrs[:payer_address]
        json['e_bill']['payer_city'].should == e_bill_attrs[:payer_city]
        json["errors"].should be_nil
      end

      it "should add a new e_bill in db" do
        expect(EBill.all.count).to eq 1
      end

      it "should add a new success audit trail " do
        last_audit = AuditTrail.last_audit
        expect(last_audit.status).to eq "success"
      end

      it "should have an audit trail that the client_transaction_id is not nil" do
        last_audit = AuditTrail.last_audit
        expect(last_audit.client_transaction_id).to eq "001"
      end

      it "should not be nil for the params of audit trail" do
        last_audit = AuditTrail.last_audit
        expect(last_audit.params).not_to be_blank
      end

      it "should not be nil for the operator of audit trail" do
        last_audit = AuditTrail.last_audit
        expect(last_audit.operator).not_to be_blank
      end

    end

  end

  def merchant_headers
    json_headers merchant.username, merchant.password
  end

  # set the JSON headers include basic auth
  def json_headers username, password
    { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json', 
          'HTTP_AUTHORIZATION' =>  ActionController::HttpAuthentication::Basic.encode_credentials(username, password) }
  end
end