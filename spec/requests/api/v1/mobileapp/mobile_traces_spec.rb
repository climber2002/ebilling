require 'spec_helper'

describe "/api/v1/mobileapp/mobile_traces", type: :request do

  let(:url) { "api/v1/mobileapp/mobile_traces" }
  let!(:merchant) { FactoryGirl.create(:merchant) }
  let!(:merchant2) { FactoryGirl.create(:merchant, username: 'merchant2') }
  let(:imei) { '323424342' }
  let(:imei2) { '323424343' }
  let!(:mobile) { merchant.mobiles.create(imei: imei, otp_confirmed_at: Time.now) }
  let!(:mobile2) { merchant2.mobiles.create(imei: imei2, otp_confirmed_at: Time.now) }

  describe 'create' do 

    context 'valid' do 
      before do 
        api_post url, { device_id: imei, stack_trace: 'trace...' }
      end

      it { expect(response.status).to eq 201 }
      it do 
        expect(json_response[:mobile_trace][:device_id]).to eq imei
        expect(json_response[:mobile_trace][:stack_trace]).to eq 'trace...'
      end
    end

    context 'valid upcase' do 
      before do 
        api_post url, { DEVICE_ID: imei, STACK_TRACE: 'trace...' }
      end

      it { expect(response.status).to eq 201 }
      it do 
        expect(json_response[:mobile_trace][:device_id]).to eq imei
        expect(json_response[:mobile_trace][:stack_trace]).to eq 'trace...'
      end
    end

  end
end