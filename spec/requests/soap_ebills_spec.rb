require 'spec_helper'

describe "soap_ebills" do
  before :each do
    @admin = FactoryGirl.create(:admin)
    @payment = FactoryGirl.create(:payment)

  end

  describe "verify" do
    it "should be able to verify a ready ebill" do
      e_bill = FactoryGirl.create(:e_bill)
      e_bill.trigger

      params = { :e_bill => { :client_transaction_id => "12345", :payee_id => e_bill.user.msisdn,
               :payer_id => e_bill.payer_msisdn, :amount => e_bill.amount.to_s } }
      post "/soap_ebills/#{e_bill.bill_id}/verify", params.to_json, 
          { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json', 
            'HTTP_AUTHORIZATION' =>  ActionController::HttpAuthentication::Basic.encode_credentials(@payment.username, @payment.password) }

      expect(response).to be_success
      json = JSON.parse(response.body)
      expect(json['client_transaction_id']).to eq '12345'
      expect(json['server_transaction_id']).not_to be_nil
      expect(json['e_bill']['payer_id']).to eq e_bill.payer_msisdn

      last_audit = AuditTrail.last_audit
      expect(last_audit.status).to eq "success"
    end

    it "should fail if the ebill is not ready" do
      e_bill = FactoryGirl.create(:e_bill)
      expect(e_bill.state).to eq "created"

      params = { :e_bill => { :client_transaction_id => "12345", :payee_id => e_bill.user.msisdn,
               :payer_id => e_bill.payer_msisdn, :amount => e_bill.amount.to_s } }
      post "/soap_ebills/#{e_bill.bill_id}/verify", params.to_json, 
          { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json', 
            'HTTP_AUTHORIZATION' =>  ActionController::HttpAuthentication::Basic.encode_credentials(@payment.username, @payment.password) }

      expect(response.response_code).to eq 405
      puts "#{response.class}"
      json = JSON.parse(response.body)
      expect(json['errors']['error_code']).to eq SoapEbillsController::EBILLS_STATE_ERROR_CODES['created'][0]

    end

    it "should fail if the amount doesn't match" do
      e_bill = FactoryGirl.create(:e_bill)
      e_bill.trigger

      params = { :e_bill => { :client_transaction_id => "12345", :payee_id => e_bill.user.msisdn,
               :payer_id => e_bill.payer_msisdn, :amount => (e_bill.amount + 100).to_s } }

      post "/soap_ebills/#{e_bill.bill_id}/verify", params.to_json, 
          { 'CONTENT_TYPE' => 'application/json', 'ACCEPT' => 'application/json', 
            'HTTP_AUTHORIZATION' =>  ActionController::HttpAuthentication::Basic.encode_credentials(@payment.username, @payment.password) }

      expect(response.response_code).to eq 406
      json = JSON.parse(response.body)
      expect(json['errors']['error_code']).to eq "ERR-103"
    end

  end
end