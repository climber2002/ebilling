require 'spec_helper'

describe OutlineController do

  describe "GET #show" do
    context "When the user is not logged in" do
      it "redirects to login path" do
        get :show
        expect(response).to redirect_to new_user_session_path
      end
    end

    context "When the user is logged in" do
      before(:each) do
        @guest = FactoryGirl.create(:user)
        sign_in @guest

        get :show
      end

      it "assigns the current user to @user" do
        expect(assigns(:user)).to eq @guest
      end

      it "renders the :show template" do
        expect(response).to render_template :show
      end
    end
  end

  describe "GET #edit_password" do
    context "When the user is not logged in" do
      it "redirects to login path" do
        get :edit_password
        expect(response).to redirect_to new_user_session_path
      end
    end

    context "When the user is logged in" do
      before :each do
        @guest = FactoryGirl.create(:user)
        sign_in @guest

        get :edit_password
      end

      it "assigns the current user to @user" do
        expect(assigns(:user)).to eq @guest
      end

      it "renders the edit_password template" do
        expect(response).to render_template(:edit_password)
      end
    end
  end

  describe "PUT #update_password" do
    context "When the user is not logged in" do
      it "redirects to login page" do
        put :update_password, user: { current_password: "guest123",
                                    password: "guest1234",
                                    password_confirmation: "guest1234" }

        expect(response).to redirect_to new_user_session_path
      end
    end

    context "When the user is logged in" do
      before :each do
        @guest = FactoryGirl.create(:user)
        sign_in @guest
      end

      it "updates the user's password" do
        orig_encrypted_password = @guest.encrypted_password

        put :update_password, user: { current_password: "guest123",
                                    password: "guest1234",
                                    password_confirmation: "guest1234" }

        @guest.reload
        expect(flash[:notice]).to eq "Password updated successfully"
        expect(@guest.encrypted_password).to_not eq orig_encrypted_password
      end

      it "doesn't update user's password if password is blank" do
        orig_encrypted_password = @guest.encrypted_password

        put :update_password, user: { current_password: "guest123",
                                    password: "",
                                    password_confirmation: "" }

        @guest.reload

        expect(flash[:notice]).to eq "Password is not updated"
        expect(@guest.encrypted_password).to eq orig_encrypted_password
      end

      it "render edit password template if the password and password_confirmation doesn't match" do
        put :update_password, user: { current_password: "guest123",
                                    password: "guest1234",
                                    password_confirmation: "not_equal_password" }

        expect(response).to render_template :edit_password
      end
    end
  end
end
