require 'spec_helper'

describe DashboardController do
  describe "GET #index" do
    context "When the user is not logged in" do
      it "redirects to login path" do
        get :index
        expect(response).to redirect_to new_user_session_path
      end
    end
  end

end
