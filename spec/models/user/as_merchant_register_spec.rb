# == Schema Information
#
# Table name: users
#
#  id                             :integer          not null, primary key
#  username                       :string(255)      default(""), not null
#  encrypted_password             :string(255)      default(""), not null
#  email                          :string(255)      default("")
#  reset_password_token           :string(255)
#  reset_password_sent_at         :datetime
#  remember_created_at            :datetime
#  sign_in_count                  :integer          default(0)
#  current_sign_in_at             :datetime
#  last_sign_in_at                :datetime
#  current_sign_in_ip             :string(255)
#  last_sign_in_ip                :string(255)
#  created_at                     :datetime
#  updated_at                     :datetime
#  first_name                     :string(255)
#  last_name                      :string(255)
#  msisdn                         :string(255)
#  city                           :string(255)
#  address                        :string(255)
#  e_bills_count                  :integer          default(0)
#  notification_url               :string(255)
#  notification_params            :string(255)      default([]), is an Array
#  notification_post              :boolean          default(TRUE)
#  legal_info                     :string(255)
#  greeting_message               :string(255)
#  email_notification             :boolean          default(FALSE)
#  website                        :string(255)
#  logo                           :string(255)
#  signature                      :string(255)
#  additional_notification_params :string(255)
#  currency                       :string(255)
#  payment_system_id              :integer
#  search_field                   :text
#  shared_key                     :string(255)
#  tax_rate                       :float
#  zipcode                        :string(255)
#  charge_payer                   :boolean          default(FALSE)
#

require 'spec_helper'

describe User::AsMerchantRegister do

  let!(:profile_merchant) { FactoryGirl.create(:profile_merchant) }

  describe 'creation' do 

    it 'should be able to create a valid merchant' do 

      merchant = User::AsMerchantRegister.create(username: 'merchantest', email: 'climber2002@gmail.com',
        name: 'Merchant D', zipcode: '234234', msisdn: '2343423', currency: 'USD')

      expect(merchant).to be_valid
    end
  end
end
