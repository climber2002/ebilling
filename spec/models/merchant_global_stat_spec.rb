# == Schema Information
#
# Table name: merchant_global_stats
#
#  id                   :integer          not null, primary key
#  created_count        :integer          default(0)
#  ready_count          :integer          default(0)
#  paid_count           :integer          default(0)
#  partially_paid_count :integer          default(0)
#  cancelled_count      :integer          default(0)
#  failed_count         :integer          default(0)
#  processed_count      :integer          default(0)
#  expired_count        :integer          default(0)
#  merchant_id          :integer          not null
#  created_at           :datetime
#  updated_at           :datetime
#  overdue_count        :integer          default(0)
#

require 'spec_helper'

describe MerchantGlobalStat do
  pending "add some examples to (or delete) #{__FILE__}"
end
