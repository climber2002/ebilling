# == Schema Information
#
# Table name: merchant_payment_system_mappings
#
#  id                  :integer          not null, primary key
#  merchant_id         :integer
#  payment_system_id   :integer
#  merchant_ps_id      :string(255)
#  created_at          :datetime
#  updated_at          :datetime
#  ps_percentage       :float            default(0.0)
#  search_field        :string(255)
#  ebilling_percentage :float            default(0.0)
#

require 'spec_helper'

describe MerchantPaymentSystemMapping do
  let(:user) { FactoryGirl.create(:user) }
  let(:merchant) { FactoryGirl.create(:merchant) }
  let(:payment) { FactoryGirl.create(:payment) }

  describe 'validation' do 
    it 'should be able to create a valid mapping' do 
      mapping = MerchantPaymentSystemMapping.create(merchant_id: merchant.id, 
        payment_system_id: payment.id, merchant_ps_id: 'abcde')
      expect(mapping).to be_valid
    end

    it 'should be invalid if no merchant_id' do 
      mapping = MerchantPaymentSystemMapping.create(
        payment_system_id: payment.id, merchant_ps_id: 'wuwurwe')
      expect(mapping).to_not be_valid
    end

    it 'should be invalid if merchant_id does not belong to merchant' do 
      mapping = MerchantPaymentSystemMapping.create(merchant_id: user.id, 
        payment_system_id: payment.id, merchant_ps_id: 'abcdeswer')
      expect(mapping).to_not be_valid
    end
  end
end
