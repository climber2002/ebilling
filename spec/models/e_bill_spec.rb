# == Schema Information
#
# Table name: e_bills
#
#  id                          :integer          not null, primary key
#  user_id                     :integer
#  payer_email                 :string(255)
#  payer_msisdn                :string(255)
#  amount                      :integer
#  currency                    :string(255)
#  state                       :string(255)
#  expired_at                  :datetime
#  schedule_at                 :datetime
#  external_reference          :string(255)
#  additional_info             :string(255)
#  description                 :string(255)
#  reason                      :string(255)
#  sms                         :boolean          default(FALSE)
#  email                       :boolean          default(FALSE)
#  created_at                  :datetime
#  updated_at                  :datetime
#  sms_message                 :string(255)
#  merchant_internal_ref       :string(255)
#  sent_at                     :datetime
#  paid_at                     :datetime
#  cancelled_at                :datetime
#  processed_at                :datetime
#  failed_at                   :datetime
#  expire_at                   :datetime
#  short_description           :string(255)
#  due_date                    :date
#  bill_id                     :string(255)
#  batch_file_id               :integer
#  payer_name                  :string(255)
#  payer_address               :string(255)
#  payer_city                  :string(255)
#  accept_partial_payment      :boolean          default(FALSE)
#  minimum_amount              :integer
#  amount_paid                 :integer          default(0)
#  partially_paid_at           :datetime
#  overdue_at                  :datetime
#  payment_system_id           :integer
#  ps_transaction_id           :string(255)
#  traffic_fee_centimes        :integer          default(0), not null
#  traffic_fee_currency        :string(255)      default("XAF"), not null
#  search_field                :text
#  payer_id                    :string(255)
#  payer_code                  :string(255)
#  data0                       :string(255)
#  data1                       :string(255)
#  data2                       :string(255)
#  data3                       :string(255)
#  data4                       :string(255)
#  data5                       :string(255)
#  data6                       :string(255)
#  data7                       :string(255)
#  data8                       :string(255)
#  data9                       :string(255)
#  ps_transaction_fee_centimes :integer          default(0), not null
#  ps_transaction_fee_currency :string(255)      default("XAF"), not null
#  last_paid_amount_centimes   :integer          default(0), not null
#  last_paid_amount_currency   :string(255)      default("XAF"), not null
#

require 'spec_helper'

describe EBill do
  describe "Validation" do
    it "should have a valid factory" do
      expect(FactoryGirl.create(:e_bill)).to be_valid
    end

    it 'should be able to create ebill with description and additional info up to 200 characters' do 
      description = "1234567890" * 20
      additional_info = '1234567890' * 20
      expect(FactoryGirl.create(:e_bill, description: description, additional_info: additional_info)).to be_valid
    end

    it "should have sms notification set by default" do
      expect(FactoryGirl.build(:e_bill).sms).to be_true
    end

    it "should not be valid if description exceeds 250 characters" do
      expect(FactoryGirl.build(:e_bill, description: "a" * 251)).to have(1).errors_on(:description)
    end

    it "should not be valid if payer_email is not valid email" do
      expect(FactoryGirl.build(:e_bill, payer_email: "abcdef")).to have(1).errors_on(:payer_email)
    end

    it "should be valid to not set payer_email if email is not true" do
      expect(FactoryGirl.build(:e_bill, payer_email: nil, email: nil)).to be_valid
    end

    it "should not be valid to not set payer_email if email is true" do
      expect(FactoryGirl.build(:e_bill, payer_email: nil, email: true)).to have(1).errors_on(:payer_email)
    end

    it "should not be valid if the msisdn is invalid" do
      expect(FactoryGirl.build(:e_bill, payer_msisdn: "abcd")).to have(1).errors_on(:payer_msisdn)
    end

    it "should not be valid if user is nil" do
      expect(FactoryGirl.build(:e_bill, user: nil)).to have(1).errors_on(:user_id)
    end

    it "should not be valid if client_transaction_id exceeds 35" do
      expect(FactoryGirl.build(:e_bill, client_transaction_id: "12345678901234567123456789012345671234567890123456712345678901234567")).to have(1).errors_on(:client_transaction_id)
    end

    it "should not be valid if short_description is blank" do
      expect(FactoryGirl.build(:e_bill, short_description: "")).to have(1).errors_on(:short_description)
    end

    it "should be able to set the user by payee_id" do
      merchant = FactoryGirl.create(:merchant)
      e_bill = FactoryGirl.build(:e_bill)
      e_bill.payee_id = merchant.msisdn
      expect(e_bill.user).to eq merchant
    end

    it "should be invalid if the email is invalid" do
      e_bill = FactoryGirl.build(:e_bill, payer_email: "aaabbb")
      expect(e_bill).to have(1).errors_on(:payer_email)
    end

    it "should allow blank email" do
      expect(FactoryGirl.build(:e_bill, email: false, payer_email: "")).to be_valid
    end

    it 'reset minimum_amount if not accept_partial_payment' do
      bill = FactoryGirl.create(:e_bill, accept_partial_payment: false, minimum_amount: 100)
      expect(bill.minimum_amount).to eq nil
    end

    context 'when accept_partial_payment is true' do

      it 'should be valid when accept_partial_payment and minimum_amount valid' do
        ebill = FactoryGirl.create(:e_bill, accept_partial_payment: true, minimum_amount: 20, amount: 100)
        expect(ebill).to be_valid
      end

      it 'should be invalid if minimum_amount is 0' do
        ebill = FactoryGirl.build(:e_bill, accept_partial_payment: true, minimum_amount: 0, amount: 100)
        expect(ebill).to_not be_valid
        expect(ebill.errors_on(:minimum_amount)).to be_present
      end

      it 'should be invalid if minimum_amount is bigger than amount' do
        ebill = FactoryGirl.build(:e_bill, accept_partial_payment: true, minimum_amount: 101, amount: 100)
        expect(ebill).to_not be_valid
        expect(ebill.errors_on(:minimum_amount)).to be_present
      end

      it 'should be invalid if amount is not present' do 
        ebill = FactoryGirl.build(:e_bill, accept_partial_payment: true, minimum_amount: 101, amount: '')
        expect(ebill).to_not be_valid
        expect(ebill.errors_on(:amount)).to be_present
      end
    end
  end

  describe "bill_id" do
    it "should have a bill_id starts with 555 with 16 characters" do
      expect(FactoryGirl.create(:e_bill).bill_id).to start_with("555")
    end

    it "should have bill_id all with number characters" do
      expect(FactoryGirl.create(:e_bill).bill_id).to match /555(\d+)/
    end

    it "two ebill should have different bill_id" do
      ebill1 = FactoryGirl.create(:e_bill)
      ebill2 = FactoryGirl.create(:e_bill)
      
      expect(ebill1.bill_id).not_to eq ebill2.bill_id
    end

    it "should store the bill_id in db after create" do
      e_bill = FactoryGirl.create(:e_bill)
      e_bill.reload
      expect(e_bill.bill_id).to start_with("555")
    end

    it 'when no Setting bill id length  should be 10' do 
      Settings.bill_id_length = nil
      e_bill = FactoryGirl.create(:e_bill)
      e_bill.reload
      expect(e_bill.bill_id.length).to eq 10
    end

    it 'when have Setting bill id length  should be equal to setting' do 
      Settings.bill_id_length = '13'
      e_bill = FactoryGirl.create(:e_bill)
      e_bill.reload
      expect(e_bill.bill_id.length).to eq 13
    end
  end

  describe "Currency" do
    it 'the created e_bill should have same currency as merchant' do 
      e_bill = FactoryGirl.create(:e_bill)
      expect(e_bill.currency).to eq 'SGD'
    end
  end

  describe "merchant_name" do
    it "should return correct merchant name" do
      ebill = FactoryGirl.create(:e_bill)
      expect(ebill.merchant_name).not_to be_nil
    end
  end

  describe "notification" do
    it "should return correct param values for get_notification_params" do
      ebill = FactoryGirl.create(:e_bill)
      expected_param_values = { 'billingid' => ebill.bill_id, 'customerid' => ebill.payer_id.to_s, 'amount' => ebill.amount.to_s }
      expect(ebill.get_notification_params(['billingid', 'amount', 'customerid'])).to eq expected_param_values
    end

    it "should return additional param values if it is set on user" do
      ebill = FactoryGirl.create(:e_bill)
      ebill.user.additional_notification_params = "param1=value1&param2=value2;param3=value3"
      expected_param_values = { 'billingid' => ebill.bill_id, 'customerid' => ebill.payer_id.to_s,
       'amount' => ebill.amount.to_s, 'param1' => 'value1', 'param2' => 'value2', 'param3' => 'value3' }
      expect(ebill.get_notification_params(['billingid', 'amount', 'customerid'])).to eq expected_param_values
    end
  end

  describe "send receipts and invoice" do
    it "should not be able to send receipts for created ebill" do
      ebill = FactoryGirl.create(:e_bill)
      expect(ebill.state).to eq "created"
      expect(ebill.can_send_receipt?).to be_false
    end

    it "should not be able to send receipts for ready ebill" do
      ebill = FactoryGirl.create(:e_bill)
      ebill.trigger
      expect(ebill.can_send_receipt?).to be_false
    end

    it "should not be able to send receipt for cancelled ebill" do
      ebill = FactoryGirl.create(:e_bill)
      ebill.trigger
      ebill.cancel
      expect(ebill.state).to eq "cancelled"
      expect(ebill.can_send_receipt?).to be_false
    end

    it "should be able to send receipt when paid" do
      ebill = FactoryGirl.create(:e_bill)
      ebill.trigger
      ebill.pay
      expect(ebill.can_send_receipt?).to be_true
    end

  end

  describe "overdue" do
    before do
      @created1 = FactoryGirl.create(:e_bill, due_date: "31/10/2013")
      @created2 = FactoryGirl.create(:e_bill, due_date: DateTime.now.tomorrow.to_date)

      @paid1 = FactoryGirl.create(:e_bill, due_date: "31/10/2013")
      @paid1.trigger
      @paid1.pay
    end

    it "should only return overdue and state is created or ready" do 
      ebills = EBill.overdue
      expect(ebills.size).to eq 0
    end
  end

  describe "overdue" do 
    before do
      @ebill = FactoryGirl.create(:e_bill, due_date: "31/10/2013")
      @ebill.overdue
      expect(@ebill.state).to eq 'overdue'
    end

    it "should still can pay" do 
      expect(@ebill.can_pay?).to eq true
    end
  end

  describe "message" do
    # TODO should fix this later
    #it "should format correct message if no message set" do
    #  ebill = FactoryGirl.create(:e_bill)
    #  expect(ebill.message).to eq "Dear #{ebill.payer_id},You recently purchased #{ebill.amount} #{ebill.currency} on " +
    #                "#{ebill.payee_id} at #{ebill.created_at}, your e-Bill reference is #{ebill.bill_id}"
    #  Rails.logger.debug "message: #{ebill.message}"
    #end

  end

  describe "state machine" do
    before :each do
      @ebill = FactoryGirl.build(:e_bill)
      @ebill.create
    end

    context "when the EBill is created" do
      it "should be created at the start" do
        expect(@ebill.state).to eq "created"
      end

      it "should have trigger and expire state_event" do
        expect(@ebill.state_events).to match_array [:trigger, :overdue, :expire]
      end

      it "should have one audit trail" do
        expect(AuditTrail.count).to eq 1
        expect(AuditTrail.first.from).to be_blank
        expect(AuditTrail.first.to).to eq "created"
        expect(AuditTrail.first.server_transaction_id).not_to be_blank
        expect(AuditTrail.first.context).to be_blank
      end

      it "should be able to put trigger state_event in update attributes" do
        expect(@ebill.update_attributes(state_event: "trigger")).to be_true
      end

      it "should not be able to put pay state_event in update attributes" do
        expect(@ebill.update_attributes(state_event: "pay")).to be_false
      end
    end

    context "when the EBill is ready" do
      before :each do
        expect(@ebill.sent_at).to be_nil
        @ebill.trigger
      end

      it "should set the sent_at" do
        expect(@ebill.sent_at).not_to be_nil
      end

      it "should be ready after trigger" do
        expect(@ebill.state).to eq "ready"
      end

      it "should have cancel, invalidate, pay, expire state_event" do
        expect(@ebill.state_events).to match_array [:cancel, :invalidate, :overdue, :pay, :expire, :pay_partial, :resend]
      end

      it "should add one audit trail" do
        last_audit = AuditTrail.order('created_at').last
        expect(last_audit.from).to eq "created"
        expect(last_audit.to).to eq "ready"
        expect(last_audit.context).to match /Sent message:/
      end
    end


    it "should be paid after pay in ready state" do
      @ebill.trigger
      expect(@ebill.paid_at).to be_nil
      @ebill.pay

      expect(@ebill.state).to eq "paid"
      expect(@ebill.paid_at).not_to be_nil
    end

    it "should be cancelled after cancal in ready state" do
      @ebill.trigger
      expect(@ebill.cancelled_at).to be_nil
      @ebill.cancel

      expect(@ebill.state).to eq "cancelled"
      expect(@ebill.cancelled_at).not_to be_nil
    end
  end

  describe "scope" do
    before :each do
      @created1 = FactoryGirl.create(:e_bill)
      @created2 = FactoryGirl.create(:e_bill)

      @ready1 = FactoryGirl.create(:e_bill)
      @ready1.trigger
      @ready2 = FactoryGirl.create(:e_bill)
      @ready2.trigger
    end

    context "by_state" do
      it "should have 2 created" do
        created = EBill.by_state(:created)
        expect(created.count).to eq 2
        expect(created.include? @created1).to be_true
        expect(created.include? @created2).to be_true
      end

      it "should have 2 created by calling created" do
        created = EBill.created
        expect(created.count).to eq 2
        expect(created.include? @created1).to be_true
        expect(created.include? @created2).to be_true
      end

      it "should have 2 ready" do
        ready = EBill.by_state(:ready)
        expect(ready.count).to eq 2
        expect(ready.include? @ready1).to be_true
        expect(ready.include? @ready2).to be_true
      end

      it "should have 2 ready by calling ready" do
        ready = EBill.ready
        expect(ready.count).to eq 2
        expect(ready.include? @ready1).to be_true
        expect(ready.include? @ready2).to be_true
      end
    end
  end

  describe "create" do
    context "When the schedule_at is not set" do
      before do
        @e_bill = FactoryGirl.build(:e_bill, :schedule_at => nil)
        @e_bill.create
      end

      it "should be sent automatically" do
        # the sent is sent in background now
        expect(@e_bill.state).to eq "created"
      end

      it "should generate an audit trail" do
        last_audit = AuditTrail.last_audit
        expect(last_audit.status).to eq "success"
        expect(last_audit.action).to eq "createEBill"
      end
    end

    context "When the schedule_at is set" do
      before do
        @e_bill = FactoryGirl.build(:e_bill, :schedule_at => 3.days.from_now)
        @e_bill.create
      end

      it "should only be created" do
        expect(@e_bill.state).to eq "created"
      end

    end

    context "When create an invalid ebill and log failure" do
      before do
        @e_bill = FactoryGirl.build(:e_bill, :schedule_at => nil, :payer_msisdn => nil)
        @created = @e_bill.create true
      end

      it "should not be created" do
        expect(@created).to be_false
      end

      it "should generate an audit trail" do
        last_audit = AuditTrail.last_audit
        expect(last_audit.status).to eq "failed"
        expect(last_audit.action).to eq "createEBill"
        expect(last_audit.bill_id).to be_blank
      end
    end

    context "When create an invalid ebill and not log failure" do
      before do
        @e_bill = FactoryGirl.build(:e_bill, :schedule_at => nil, :payer_msisdn => nil)
        @created = @e_bill.create false
      end

      it "should not be created" do
        expect(@created).to be_false
      end

      it "should generate an audit trail" do
        last_audit = AuditTrail.last_audit
        expect(last_audit).to be_nil
      end
    end
  end

  describe "e_bills counter_cache" do
    it "should updates e_bills counter_cache" do
      merchant = FactoryGirl.create(:merchant)
      expect(merchant.e_bills_count).to eq 0

      merchant.e_bills.create(FactoryGirl.attributes_for(:e_bill, user: nil))
      merchant.reload
      expect(merchant.e_bills_count).to eq 1
    end
  end

  describe "find_by_bill_id" do
    it "should be able to find by bill_id" do
      e_bill = FactoryGirl.create(:e_bill)
      bill_id = e_bill.bill_id

      by_bill_id = EBill.find_by_bill_id bill_id
      expect(by_bill_id).to eq e_bill
    end
  end

end
