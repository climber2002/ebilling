# == Schema Information
#
# Table name: roles
#
#  id           :integer          not null, primary key
#  name         :string(255)      not null
#  description  :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#  role_type_id :integer
#  display_name :string(255)
#

require 'spec_helper'

describe Role do
  describe "Validation" do
    it "has a valid factory" do
      expect(FactoryGirl.build(:role_user_viewer)).to be_valid
    end

    it "the display name is eq name if display_name not set" do
      role = FactoryGirl.build(:role_user_viewer)
      expect(role.display_name).to eq role.name
    end

    it "should display display_name if display_name is set" do
      expect(FactoryGirl.build(:role_user_viewer, display_name: "display").display_name).to eq "display"
    end

    it "is invalid if role name already exists" do
      FactoryGirl.create(:role_user_viewer)

      expect(Role.create(name: "UserViewer")).to have(1).errors_on(:name)
    end

    it "is invalid if the role name is blank" do
      role = FactoryGirl.build(:role_user_viewer, name: "")
      expect(role).to have(1).errors_on(:name)
    end

  end

end
