# == Schema Information
#
# Table name: payment_system_monthly_stats
#
#  id                :integer          not null, primary key
#  payment_system_id :integer          not null
#  year              :integer          not null
#  month             :integer          not null
#  created_at        :datetime
#  updated_at        :datetime
#

require 'spec_helper'

describe PaymentSystemMonthlyStat do
  let(:payment_system) { FactoryGirl.create(:payment) } 

  describe 'creation' do 
    it 'should be able to create a valid PaymentSystemMonthlyStat' do 
      expect(PaymentSystemMonthlyStat.create(payment_system: payment_system, 
        year: 2015, month: 10)).to be_valid
    end

    it 'is invalid if year does not exist' do 
      expect(PaymentSystemMonthlyStat.create(payment_system: payment_system, 
        month: 10)).to_not be_valid
    end
  end
  
end
