# == Schema Information
#
# Table name: role_types
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

require 'spec_helper'

describe RoleType do
  describe "Creation" do
    it "should have a valid factory" do
      expect(FactoryGirl.build(:role_type)).to be_valid
    end
  end
end
