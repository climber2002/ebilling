# == Schema Information
#
# Table name: fee_strategies
#
#  id                      :integer          not null, primary key
#  type                    :string(255)
#  fixed_amount_centimes   :integer          default(0), not null
#  fixed_amount_currency   :string(255)      default("XAF"), not null
#  percentage              :float
#  maximum_amount_centimes :integer          default(0), not null
#  maximum_amount_currency :string(255)      default("XAF"), not null
#  merchant_id             :integer
#  created_at              :datetime
#  updated_at              :datetime
#  disable_recurrent       :boolean          default(FALSE)
#

require 'spec_helper'

describe FeeStrategy do
  pending "add some examples to (or delete) #{__FILE__}"
end
