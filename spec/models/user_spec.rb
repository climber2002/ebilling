# == Schema Information
#
# Table name: users
#
#  id                             :integer          not null, primary key
#  username                       :string(255)      default(""), not null
#  encrypted_password             :string(255)      default(""), not null
#  email                          :string(255)      default("")
#  reset_password_token           :string(255)
#  reset_password_sent_at         :datetime
#  remember_created_at            :datetime
#  sign_in_count                  :integer          default(0)
#  current_sign_in_at             :datetime
#  last_sign_in_at                :datetime
#  current_sign_in_ip             :string(255)
#  last_sign_in_ip                :string(255)
#  created_at                     :datetime
#  updated_at                     :datetime
#  first_name                     :string(255)
#  last_name                      :string(255)
#  msisdn                         :string(255)
#  city                           :string(255)
#  address                        :string(255)
#  e_bills_count                  :integer          default(0)
#  notification_url               :string(255)
#  notification_params            :string(255)      default([]), is an Array
#  notification_post              :boolean          default(TRUE)
#  legal_info                     :string(255)
#  greeting_message               :string(255)
#  email_notification             :boolean          default(FALSE)
#  website                        :string(255)
#  logo                           :string(255)
#  signature                      :string(255)
#  additional_notification_params :string(255)
#  currency                       :string(255)
#  payment_system_id              :integer
#  search_field                   :text
#  shared_key                     :string(255)
#  tax_rate                       :float
#  zipcode                        :string(255)
#  charge_payer                   :boolean          default(FALSE)
#

require 'spec_helper'

describe User do

  describe "Validation" do
    it "has a valid factory" do
      expect(FactoryGirl.build(:user)).to be_valid
    end

    it 'should have shared key' do 
      user = FactoryGirl.create(:user)
      expect(user.shared_key).to_not be_nil
    end

    it "is invalid to have no username" do
      expect(FactoryGirl.build(:user, username: nil)).to have(1).errors_on(:username)
    end

    it "is invalid to have no password" do
      expect(FactoryGirl.build(:user, password: nil)).to have(1).errors_on(:password)
    end

    it "is valid if have no first_name" do
      expect(FactoryGirl.build(:user, first_name: "")).to be_valid
    end

    it "is invalid if have no last_name" do
      expect(FactoryGirl.build(:user, last_name: "")).to have(1).errors_on(:last_name)
    end

    it "is invalid if have no msisdn" do
      expect(FactoryGirl.build(:user, msisdn: "")).to have(1).errors_on(:msisdn)
    end

    it "is invalid if msisdn is not valid" do
      expect(FactoryGirl.build(:user, msisdn: "abc2343242")).to have(1).errors_on(:msisdn)
    end

    it "is invalid if email is not valid" do
      user = FactoryGirl.build(:user, email: "aaa")
      expect(user).to have(1).errors_on(:email)
      Rails.logger.debug "user for email: #{user.errors[:email]}"
    end

    it 'is invalid if a merchant has no currency' do 
      merchant = FactoryGirl.build(:merchant, currency: nil)
      expect(merchant.has_profile?(Profile::MERCHANT)).to be_true
      expect(merchant).to_not be_valid
    end

    it "should not be valid if currency has 4 characters" do
      expect(FactoryGirl.build(:merchant, currency: "AAAA")).to have(1).errors_on(:currency)
    end
  end

  describe 'payment_system_operator profile' do 
    it 'should be able to create a valid payment_system_operator' do 
      expect(FactoryGirl.create(:payment_system_operator)).to be_valid
    end

    it 'is invalid if payment_system_operator has no payment system' do 
      ps_operator = FactoryGirl.build(:payment_system_operator, payment_system: nil)
      expect(ps_operator).to_not be_valid
      expect(ps_operator.errors_on(:payment_system_id).size).to eq 1
    end

    it 'clear payment_system if the user is not a payment_system_operator' do 
      payment = FactoryGirl.create(:payment)
      merchant = FactoryGirl.create(:merchant, payment_system: payment)
      expect(merchant.payment_system).to be_nil
    end
  end

  describe "notification" do
    it "should set notification_post to true when new an instance" do
      expect(User.new.notification_post).to be_true
    end
  end

  describe "Profile" do
    before(:each) do
      @user = FactoryGirl.build(:user)
      @profile_user_manager = FactoryGirl.create(:profile_user_manager)
      @user.profiles << @profile_user_manager
      @user.save
    end

    it "can add profile" do
      expect(@user.profiles).to include(@profile_user_manager)
    end

    it "can get roles after set profiles" do
      expect(@user.roles).to include(Role.find_by(name: "UserManager"))
      expect(@user.has_role?("UserManager"))
    end

    it "can check if has_profile?" do
      service_desk = FactoryGirl.create(:service_desk)
      expect(service_desk.has_profile?(Profile::SERVICE_DESK)).to be_true
      expect(service_desk.has_profile?(Profile::ADMIN)).to be_false
    end
  end

  describe "Ability" do
    before do
      @admin = FactoryGirl.create(:admin)
      @merchant = FactoryGirl.create(:merchant)
      @merchant2 = FactoryGirl.create(:merchant2)
      @service_desk = FactoryGirl.create(:service_desk)
    end

    context "Merchant" do
      before do
        @ability = Ability.new @merchant
      end

      it "the merchant should be able to create e_bills" do
        expect(@ability.can?(:create, EBill)).to be_true
      end

      it "the merchant should be able to view its own e_bill" do
        e_bill = FactoryGirl.create(:e_bill, user: @merchant)
        expect(@ability.can?(:read, e_bill)).to be_true
        expect(@ability.can?(:trigger, e_bill)).to be_true
      end

      it "the merchant should not be able to view other e_bill" do
        e_bill = FactoryGirl.create(:e_bill, user: @merchant2)
        expect(@ability.can?(:read, e_bill)).to be_false
        expect(@ability.can?(:trigger, e_bill)).to be_false
      end
    end

    context "Admin" do
      before do
        @ability = Ability.new @admin
      end

      it "the admin should be able to view other e_bill" do
        e_bill = FactoryGirl.create(:e_bill, user: @merchant)
        expect(@ability.can?(:read, e_bill)).to be_true
      end

      it "the admin should be able to trigger other e_bill" do
        e_bill = FactoryGirl.create(:e_bill, user: @merchant)
        expect(@ability.can?(:trigger, e_bill)).to be_true
      end
    end

    context "ServiceDesk" do
      before do
        @ability = Ability.new @service_desk
      end

      it "should not be able to create EBill" do
        expect(@ability.can?(:create, EBill)).to be_false
      end

      it "should be able to see other's ebills" do
        e_bill = FactoryGirl.create(:e_bill, user: @merchant)
        expect(@ability.can?(:read, e_bill)).to be_true
        expect(@ability.can?(:trigger, e_bill)).to be_false
      end
    end
  end

  describe "scope" do
    before :each do
      @admin = FactoryGirl.create(:admin)
      @service_desk = FactoryGirl.create(:service_desk)
      @merchant = FactoryGirl.create(:merchant)
      @merchant2 = FactoryGirl.create(:merchant, username: "merchant2",
                                    email: "merchant2@example.com")
    end

    context "by_profile" do
      it "should return 2 users when get by Merchant profile" do
        users = User.by_profile(Profile::MERCHANT).to_a
        expect(users.count).to eq 2
        expect(users.include? @merchant).to be_true
        expect(users.include? @merchant2).to be_true
      end
    end

    context "order by username" do
      it "should order by username" do
        users = User.order_by_username.to_a
        expect(users).to match_array [@admin, @merchant, @merchant2, @service_desk]
      end
    end
  end

  describe "can_provision?" do
    it "should be able to provision for admin" do
      expect(FactoryGirl.create(:admin).can_provision?).to be_true
    end

    it "should be able to provision for merchant" do
      expect(FactoryGirl.create(:merchant).can_provision?).to be_true
    end

    it "should not be able to provision for service desk" do
      expect(FactoryGirl.create(:service_desk).can_provision?).to be_false
    end
  end

end
