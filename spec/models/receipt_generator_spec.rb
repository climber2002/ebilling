require 'spec_helper'

describe ReceiptGenerator do
  
  let(:ebill) do
    ebill = FactoryGirl.create(:e_bill)
    ebill.trigger
    ebill.pay
    ebill.process

    ebill.reload
    expect(ebill.state).should eq 'processed'
    ebill
  end

  it "should generate a valid file" do
    generator = ReceiptGenerator.new(ebill, File.join(Rails.root, "/spec/fixtures/template.odt"))
    generator.generate do |pdf_filepath|
      puts "pdf : #{pdf_filepath}"
      expect(File.exists?(pdf_filepath)).to be_true

      FileUtils.cp pdf_filepath, Pathname.new("~/rails_projects").expand_path
    end
    expect(File.exists?(generator.odf_filepath)).to be_false
    expect(File.exists?(generator.pdf_filepath)).to be_false
  end

end