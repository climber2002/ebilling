require 'spec_helper'

describe ExportRequest do

  describe 'creation' do 
    it 'should be able to create valid export request' do
      er = ExportRequest.new(from: Date.yesterday, to: Date.today, state: 'paid', export_type: 'excel')
      expect(er).to be_valid
    end

    it 'is invalid if to is empty' do 
      er = ExportRequest.new(from: Date.yesterday, state: 'paid', export_type: 'excel')
      expect(er).to_not be_valid
    end
  end
end