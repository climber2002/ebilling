# == Schema Information
#
# Table name: merchant_monthly_stats
#
#  id                          :integer          not null, primary key
#  created_count               :integer          default(0)
#  ready_count                 :integer          default(0)
#  paid_count                  :integer          default(0)
#  partially_paid_count        :integer          default(0)
#  cancelled_count             :integer          default(0)
#  failed_count                :integer          default(0)
#  processed_count             :integer          default(0)
#  expired_count               :integer          default(0)
#  merchant_id                 :integer          not null
#  year                        :integer          not null
#  month                       :integer          not null
#  created_at                  :datetime
#  updated_at                  :datetime
#  overdue_count               :integer          default(0)
#  traffic_fee_centimes        :integer          default(0), not null
#  traffic_fee_currency        :string(255)      default("XAF"), not null
#  revenue_centimes            :integer          default(0), not null
#  revenue_currency            :string(255)      default("XAF"), not null
#  recurrent_fee_centimes      :integer          default(0), not null
#  recurrent_fee_currency      :string(255)      default("XAF"), not null
#  ps_transaction_fee_centimes :integer          default(0), not null
#  ps_transaction_fee_currency :string(255)      default("XAF"), not null
#

require 'spec_helper'

describe MerchantMonthlyStat::MonthlyHistoryForAdmin do
  let!(:merchant_xaf) { FactoryGirl.create(:merchant, currency: 'XAF') }
  let!(:merchant2_xaf) { FactoryGirl.create(:merchant2, currency: 'XAF') }
  let!(:merchant3_usd) { FactoryGirl.create(:merchant3, currency: 'USD') }

  describe '.get_stats_for' do 
    before do 
      populate_for_merchant_xaf
      populate_for_merchant2_xaf
      populate_for_merchant3_usd

      @stats = described_class.get_stats_for(Date.new(2015, 8), 3)
    end

    it 'should have 3 instances' do 
      expect(@stats.count).to eq 3
    end

    it 'the first stat should be correct' do 
      stat = @stats[0]
      expect(stat.year).to eq 2015
      expect(stat.month).to eq 8
      expect(stat.created_count).to eq 3 + 5 + 5
      expect(stat.ready_count).to eq 7 + 7 + 7
      expect(stat.paid_count).to eq 9 + 3 + 3
      expect(stat.partially_paid_count).to eq 10 + 10 + 10
      expect(stat.cancelled_count).to eq 5 + 5 + 4
      expect(stat.failed_count).to eq 8 + 7 + 8
      expect(stat.processed_count).to eq 2 + 2 + 2
      expect(stat.expired_count).to eq 6 + 6 + 6
      expect(stat.overdue_count).to eq 7 + 7 + 7

      expect(stat.revenues.size).to eq 2
      expect(stat.revenues['XAF']).to eq Money.new(349900, 'XAF')
      expect(stat.revenues['USD']).to eq Money.new(73400, 'USD')

      expect(stat.recurrent_fees.size).to eq 2
      expect(stat.recurrent_fees['XAF']).to eq Money.new(1100, 'XAF')
      expect(stat.recurrent_fees['USD']).to eq Money.new(500, 'USD')

      expect(stat.traffic_fees.size).to eq 2
      expect(stat.traffic_fees['XAF']).to eq Money.new(123400 + 1300, 'XAF')
      expect(stat.traffic_fees['USD']).to eq Money.new(123400, 'USD')
    end

    it 'the second stat should be correct' do 
      stat = @stats[1]
      expect(stat.year).to eq 2015
      expect(stat.month).to eq 7
      expect(stat.created_count).to eq 5 + 2
      expect(stat.ready_count).to eq 3 + 3
      expect(stat.paid_count).to eq 2 + 32
      expect(stat.partially_paid_count).to eq 5 + 5
      expect(stat.cancelled_count).to eq 15 + 15
      expect(stat.failed_count).to eq 1 + 1
      expect(stat.processed_count).to eq 5 + 5
      expect(stat.expired_count).to eq 8 + 7
      expect(stat.overdue_count).to eq 7 + 7

      expect(stat.revenues.size).to eq 2
      expect(stat.revenues['XAF']).to eq Money.new(24500, 'XAF')
      expect(stat.revenues['USD']).to eq Money.new(35350, 'USD')

      expect(stat.recurrent_fees.size).to eq 2
      expect(stat.recurrent_fees['XAF']).to eq Money.new(1000, 'XAF')
      expect(stat.recurrent_fees['USD']).to eq Money.new(1000, 'USD')

      expect(stat.traffic_fees.size).to eq 2
      expect(stat.traffic_fees['XAF']).to eq Money.new(10400, 'XAF')
      expect(stat.traffic_fees['USD']).to eq Money.new(10400, 'USD')
    end

    it 'the third stat should be correct' do 
      stat = @stats[2]
      expect(stat.year).to eq 2015
      expect(stat.month).to eq 6
      expect(stat.created_count).to eq 2 + 2 + 2
      expect(stat.ready_count).to eq 3 + 3 + 3
      expect(stat.paid_count).to eq 2 + 20 + 12
      expect(stat.partially_paid_count).to eq 5 + 5 + 5
      expect(stat.cancelled_count).to eq 15 + 15 + 15
      expect(stat.failed_count).to eq 1 + 1 + 1
      expect(stat.processed_count).to eq 5 + 5 + 5
      expect(stat.expired_count).to eq 8 + 8 + 8
      expect(stat.overdue_count).to eq 7 + 7 + 7

      expect(stat.revenues.size).to eq 2
      expect(stat.revenues['XAF']).to eq Money.new(246800, 'XAF')
      expect(stat.revenues['USD']).to eq Money.new(54200, 'USD')

      expect(stat.recurrent_fees.size).to eq 2
      expect(stat.recurrent_fees['XAF']).to eq Money.new(1000 + 1500, 'XAF')
      expect(stat.recurrent_fees['USD']).to eq Money.new(1500, 'USD')

      expect(stat.traffic_fees.size).to eq 2
      expect(stat.traffic_fees['XAF']).to eq Money.new(10400 + 18900, 'XAF')
      expect(stat.traffic_fees['USD']).to eq Money.new(81900, 'USD')
    end
  end

  def populate_for_merchant_xaf
    MerchantMonthlyStat.create(created_count: 3, ready_count: 7, paid_count: 9, 
        partially_paid_count: 10, cancelled_count: 5, failed_count: 8, processed_count: 2, 
        expired_count: 6, merchant_id: merchant_xaf.id, year: 2015, month: 8, overdue_count: 7,
        traffic_fee_centimes: 123400, traffic_fee_currency: 'XAF', revenue_centimes: 324300,
        revenue_currency: 'XAF', recurrent_fee_centimes: 500, recurrent_fee_currency: 'XAF')

    MerchantMonthlyStat.create(created_count: 5, ready_count: 3, paid_count: 2, 
      partially_paid_count: 5, cancelled_count: 15, failed_count: 1, processed_count: 5, 
      expired_count: 8, merchant_id: merchant_xaf.id, year: 2015, month: 7, overdue_count: 7,
      traffic_fee_centimes: 10400, traffic_fee_currency: 'XAF', revenue_centimes: 24500,
      revenue_currency: 'XAF', recurrent_fee_centimes: 1000, recurrent_fee_currency: 'XAF')

    MerchantMonthlyStat.create(created_count: 2, ready_count: 3, paid_count: 2, 
      partially_paid_count: 5, cancelled_count: 15, failed_count: 1, processed_count: 5, 
      expired_count: 8, merchant_id: merchant_xaf.id, year: 2015, month: 6, overdue_count: 7,
      traffic_fee_centimes: 10400, traffic_fee_currency: 'XAF', revenue_centimes: 23400,
      revenue_currency: 'XAF', recurrent_fee_centimes: 1000, recurrent_fee_currency: 'XAF')

    MerchantMonthlyStat.create(created_count: 2, ready_count: 3, paid_count: 2, 
      partially_paid_count: 5, cancelled_count: 15, failed_count: 1, processed_count: 5, 
      expired_count: 8, merchant_id: merchant_xaf.id, year: 2015, month: 5, overdue_count: 7,
      traffic_fee_centimes: 10400, traffic_fee_currency: 'XAF', revenue_centimes: 24500,
      revenue_currency: 'XAF', recurrent_fee_centimes: 1000, recurrent_fee_currency: 'XAF')
  end

  def populate_for_merchant2_xaf
    MerchantMonthlyStat.create(created_count: 5, ready_count: 7, paid_count: 3, 
        partially_paid_count: 10, cancelled_count: 5, failed_count: 7, processed_count: 2, 
        expired_count: 6, merchant_id: merchant2_xaf.id, year: 2015, month: 8, overdue_count: 7,
        traffic_fee_centimes: 1300, traffic_fee_currency: 'XAF', revenue_centimes: 25600,
        revenue_currency: 'XAF', recurrent_fee_centimes: 600, recurrent_fee_currency: 'XAF')

    # no stats for month 7

    MerchantMonthlyStat.create(created_count: 2, ready_count: 3, paid_count: 20, 
      partially_paid_count: 5, cancelled_count: 15, failed_count: 1, processed_count: 5, 
      expired_count: 8, merchant_id: merchant2_xaf.id, year: 2015, month: 6, overdue_count: 7,
      traffic_fee_centimes: 18900, traffic_fee_currency: 'XAF', revenue_centimes: 223400,
      revenue_currency: 'XAF', recurrent_fee_centimes: 1500, recurrent_fee_currency: 'XAF')

    MerchantMonthlyStat.create(created_count: 2, ready_count: 3, paid_count: 20, 
      partially_paid_count: 5, cancelled_count: 15, failed_count: 1, processed_count: 5, 
      expired_count: 8, merchant_id: merchant2_xaf.id, year: 2015, month: 5, overdue_count: 7,
      traffic_fee_centimes: 18900, traffic_fee_currency: 'XAF', revenue_centimes: 24200,
      revenue_currency: 'XAF', recurrent_fee_centimes: 1500, recurrent_fee_currency: 'XAF')
  end

  def populate_for_merchant3_usd
    MerchantMonthlyStat.create(created_count: 5, ready_count: 7, paid_count: 3, 
        partially_paid_count: 10, cancelled_count: 4, failed_count: 8, processed_count: 2, 
        expired_count: 6, merchant_id: merchant3_usd.id, year: 2015, month: 8, overdue_count: 7,
        traffic_fee_centimes: 123400, traffic_fee_currency: 'USD', revenue_centimes: 73400,
        revenue_currency: 'USD', recurrent_fee_centimes: 500, recurrent_fee_currency: 'USD')

    MerchantMonthlyStat.create(created_count: 2, ready_count: 3, paid_count: 32, 
      partially_paid_count: 5, cancelled_count: 15, failed_count: 1, processed_count: 5, 
      expired_count: 7, merchant_id: merchant3_usd.id, year: 2015, month: 7, overdue_count: 7,
      traffic_fee_centimes: 10400, traffic_fee_currency: 'USD', revenue_centimes: 35350,
      revenue_currency: 'USD', recurrent_fee_centimes: 1000, recurrent_fee_currency: 'USD')

    MerchantMonthlyStat.create(created_count: 2, ready_count: 3, paid_count: 12, 
      partially_paid_count: 5, cancelled_count: 15, failed_count: 1, processed_count: 5, 
      expired_count: 8, merchant_id: merchant3_usd.id, year: 2015, month: 6, overdue_count: 7,
      traffic_fee_centimes: 81900, traffic_fee_currency: 'USD', revenue_centimes: 54200,
      revenue_currency: 'USD', recurrent_fee_centimes: 1500, recurrent_fee_currency: 'USD')

    MerchantMonthlyStat.create(created_count: 2, ready_count: 3, paid_count: 12, 
      partially_paid_count: 5, cancelled_count: 15, failed_count: 1, processed_count: 5, 
      expired_count: 8, merchant_id: merchant3_usd.id, year: 2015, month: 5, overdue_count: 7,
      traffic_fee_centimes: 81900, traffic_fee_currency: 'USD', revenue_centimes: 54200,
      revenue_currency: 'USD', recurrent_fee_centimes: 1500, recurrent_fee_currency: 'USD')
  end  
end
