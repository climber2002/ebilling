# == Schema Information
#
# Table name: mobile_traces
#
#  id          :integer          not null, primary key
#  device_id   :string(255)
#  stack_trace :text
#  user_id     :integer
#  created_at  :datetime
#  updated_at  :datetime
#

require 'spec_helper'

describe MobileTrace do
  
  describe 'creation' do 
    it 'should create a valid mobile trace' do 
      expect(FactoryGirl.create(:mobile_trace)).to be_valid
    end

    it 'is invalid if no device_id' do 
      expect(FactoryGirl.build(:mobile_trace, device_id: '')).to_not be_valid
    end
  end
end
