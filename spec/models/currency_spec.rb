# == Schema Information
#
# Table name: currencies
#
#  id         :integer          not null, primary key
#  code       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

require 'spec_helper'

describe Currency do
  
  describe 'creation' do 
    it 'should be able to create a valid currency' do 
      expect(Currency.create(code: 'XAF')).to be_valid
    end

    it 'is invalid if code not present' do 
      expect(Currency.create(code: '')).to_not be_valid
    end

    it 'is invalid if code duplicates' do 
      Currency.create(code: 'SGD')
      expect(Currency.create(code: 'SGD')).to_not be_valid
    end

    it 'is invalid if code format is not correct' do 
      expect(Currency.create(code: 'SGDD')).to_not be_valid
    end
  end

end
