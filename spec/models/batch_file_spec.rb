# == Schema Information
#
# Table name: batch_files
#
#  id          :integer          not null, primary key
#  file        :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#  user_id     :integer
#  state       :string(255)
#  provisioned :integer          default(0)
#  succeeded   :integer          default(0)
#  failed      :integer          default(0)
#

require 'spec_helper'

describe BatchFile do
  before :each do
    @merchant = FactoryGirl.create(:merchant)
    @merchant2 = FactoryGirl.create(:merchant2)
    @admin = FactoryGirl.create(:admin)
    @service_desk = FactoryGirl.create(:service_desk)
  end

  context "for admin" do
    before do
      @batch_file = @admin.batch_files.new
      @batch_file.file = File.open(File.join(Rails.root, "/spec/fixtures/Facture.xlsx"))
      @batch_file.save!
    end

    it "should be created at first" do
      expect(@batch_file.state).to eq "created"
    end

    it "should have counters reset" do
      expect(@batch_file.succeeded).to eq 0
      expect(@batch_file.failed).to eq 0
      expect(@batch_file.provisioned).to eq 0
    end

    it "should be able to store a csv file" do
      puts "batch_file path: #{@batch_file.file.current_path}"
      expect(@batch_file.file.current_path).not_to be_nil
    end

  end

  context "provision" do

    before do
      @batch_file = @admin.batch_files.new
      @batch_file.file = File.open(File.join(Rails.root, "/spec/fixtures/Facture.xlsx"))
      @batch_file.save!
      @batch_file.provision
    end

    it "should finish the batch file" do
      expect(@batch_file.state).to eq 'finished'
    end

    it "should be able to provision e_bills" do
      expect(EBill.all.count).to eq 3
    end

    it "should provision customer name" do
      expect(EBill.first.payer_name).to eq "Andy"
    end

    it "should provision customer address" do
      expect(EBill.first.payer_address).to eq "address"
    end

    it "should provision customer city" do
      expect(EBill.first.payer_city).to eq "Singapore"
    end

    it "should have 3 e_bills in the batch file" do
      @batch_file.reload
      expect(@batch_file.e_bills.size).to eq 3
    end

    it "should have 3 successful audits" do
      audits = AuditTrail.all
      expect(audits.count).to eq 3

      audits.each do |audit|
        expect(audit.status).to eq "success"
        expect(audit.operator).to eq "admin"
        expect(audit.params).not_to be_nil
      end
    end 

  end

  context "provision file which contains error" do
    before :each do 
      @batch_file = @admin.batch_files.new
      @batch_file.file = File.open(File.join(Rails.root, "/spec/fixtures/facture_error.xlsx"))
      @batch_file.save!

      @batch_file.provision
    end

    it "should finish the batch file" do
      expect(@batch_file.state).to eq 'finished'
    end

    it "should provision 2 e_bills" do
      expect(EBill.all.count).to eq 2
    end
    
    it "should have 3 audits" do
      expect(AuditTrail.all.count).to eq 3
    end

    it "should have 2 success audits" do
      expect(AuditTrail.where(status: "success").count).to eq 2
    end

    it "should have 1 failed audits" do
      expect(AuditTrail.where(status: "failed").count).to eq 1
    end

    it "should have error messages for the failed audit" do
      expect(AuditTrail.where(status: "failed").first.error_messages).not_to be_blank
    end
  end

  context "provsion Felicien's file" do
    before :each do
      @batch_file = @admin.batch_files.new
      @batch_file.file = File.open(File.join(Rails.root, "/spec/fixtures/E-Billing_Batch.xlsx"))
      @batch_file.save!

      @batch_file.provision
    end

    it "should finish the batch file" do
      expect(@batch_file.state).to eq "finished"
    end

    it "should not provsiion any e_bills" do
      expect(EBill.all.count).to eq 0
    end

    it "should have 2 failed audits" do
      expect(AuditTrail.where(status: "failed").count).to eq 2
    end
  end

end
