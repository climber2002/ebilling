# == Schema Information
#
# Table name: ebilling_payment_accounts
#
#  id                :integer          not null, primary key
#  payment_system_id :integer
#  payment_id        :string(255)
#  created_at        :datetime
#  updated_at        :datetime
#

require 'spec_helper'

describe EbillingPaymentAccount do
  let(:payment) { FactoryGirl.create(:payment) }

  it 'should create valid EbillingPaymentAccount' do 
    expect(EbillingPaymentAccount.create(payment_system: payment, payment_id: '123423434')).to be_valid
  end

  it 'should not be able to create twice for same payment system' do 
    EbillingPaymentAccount.create(payment_system: payment, payment_id: '123423434')

    expect(EbillingPaymentAccount.create(payment_system: payment, payment_id: '12342343')).to_not be_valid
  end
end
