# == Schema Information
#
# Table name: fee_strategies
#
#  id                      :integer          not null, primary key
#  type                    :string(255)
#  fixed_amount_centimes   :integer          default(0), not null
#  fixed_amount_currency   :string(255)      default("XAF"), not null
#  percentage              :float
#  maximum_amount_centimes :integer          default(0), not null
#  maximum_amount_currency :string(255)      default("XAF"), not null
#  merchant_id             :integer
#  created_at              :datetime
#  updated_at              :datetime
#  disable_recurrent       :boolean          default(FALSE)
#

require 'spec_helper'

describe FeeStrategy::VariableStrategy do

  describe 'creation' do 
    it 'should be able to create a new variable strategy' do 
      strategy = FeeStrategy::VariableStrategy.new(maximum_amount: 10, percentage: 1)
      expect(strategy).to be_valid
    end

    it 'is invalid if the maximum_amount is 0' do 
      strategy = FeeStrategy::VariableStrategy.new(maximum_amount: 0.0, percentage: 1)
      expect(strategy).to_not be_valid
    end

    it 'is invalid if the percentage is 0' do 
      strategy = FeeStrategy::VariableStrategy.new(maximum_amount: 10.0, percentage: 0)
      expect(strategy).to_not be_valid
    end
  end

  describe '#calculate_traffic_fee_for_e_bill' do 
    let(:merchant) { FactoryGirl.create(:merchant) }
    let(:ebill) { FactoryGirl.create(:e_bill, amount: 200, amount_paid: 100, user_id: merchant.id) }
    subject { FeeStrategy::VariableStrategy.new(maximum_amount: 10, percentage: 1, merchant_id: merchant.id) }

    it 'should calculate based on amount' do 
      expect(subject.calculate_traffic_fee_for_e_bill(ebill).to_f).to eq 2.0
    end
  end  
end
