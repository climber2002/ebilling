# == Schema Information
#
# Table name: fee_strategies
#
#  id                      :integer          not null, primary key
#  type                    :string(255)
#  fixed_amount_centimes   :integer          default(0), not null
#  fixed_amount_currency   :string(255)      default("XAF"), not null
#  percentage              :float
#  maximum_amount_centimes :integer          default(0), not null
#  maximum_amount_currency :string(255)      default("XAF"), not null
#  merchant_id             :integer
#  created_at              :datetime
#  updated_at              :datetime
#  disable_recurrent       :boolean          default(FALSE)
#

require 'spec_helper'

describe FeeStrategy::FixedStrategy do
  
  it 'should be able to create a new fixed strategy' do 
    strategy = FeeStrategy::FixedStrategy.new(fixed_amount: 0.01)
    expect(strategy).to be_valid
  end

  it 'is invalid if the fixed_amount is 0' do 
    strategy = FeeStrategy::FixedStrategy.new(fixed_amount: 0.0)
    expect(strategy).to_not be_valid
    expect(strategy.errors_on(:fixed_amount).size).to eq 1
  end
end
