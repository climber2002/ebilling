# == Schema Information
#
# Table name: audit_trails
#
#  id                    :integer          not null, primary key
#  client_transaction_id :string(255)
#  from                  :string(255)
#  to                    :string(255)
#  bill_id               :string(255)
#  payer_id              :string(255)
#  context               :text
#  created_at            :datetime
#  updated_at            :datetime
#  status                :string(255)      default("success")
#  action                :string(255)
#  params                :text
#  operator              :string(255)
#  error_messages        :text
#  search_field          :text
#

require 'spec_helper'

describe AuditTrail do

  describe "Validation" do
    it "should be able to create one without client transaction ID" do
      audit_trail = AuditTrail.create :payer_id => "2234234",
                    :to => "created", :bill_id => "5550000000001"
      expect(audit_trail).to be_valid

    end
  end
end
