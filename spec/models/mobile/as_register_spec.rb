# == Schema Information
#
# Table name: mobiles
#
#  id               :integer          not null, primary key
#  imei             :string(255)
#  user_id          :integer
#  otp              :string(255)
#  otp_last_sent_at :datetime
#  otp_confirmed_at :datetime
#  pin_hash         :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#

require 'spec_helper'

describe Mobile::AsRegister do

  let(:user) { FactoryGirl.create(:merchant) }

  describe 'creation' do

    context 'valid' do
      subject { Mobile::AsRegister.create(user: user, imei: '234324234') }

      it { expect(subject).to be_valid }
      it 'should have otp set' do
        expect(subject.otp.length).to eq 6
      end
      it 'should have otp_last_sent_at set' do
        expect(subject.otp_last_sent_at).to_not be_nil
      end
    end

  end
end
