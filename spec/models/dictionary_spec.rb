require 'spec_helper'
require 'rake'
Ebilling::Application.load_tasks

describe "dictionary provision" do

  describe "provision" do
    before :each do
      Rake::Task['db:provision'].reenable
      Rake::Task['db:provision'].invoke
    end

    it "should have Admin profile" do
      Rails.logger.debug("test admin profile")
      admin_profile = Profile.find_by(name: Profile::ADMIN)
      expect(admin_profile).not_to be_nil

      expect(admin_profile.has_role?("UserViewer")).to be_true
      expect(admin_profile.has_role?("UserManager")).to be_true

      expect(admin_profile.has_role?("ProfileViewer")).to be_true
      expect(admin_profile.has_role?("ProfileManager")).to be_true

      expect(admin_profile.has_role?("CreateEBill")).to be_true
      expect(admin_profile.has_role?("ViewEBill")).to be_true
      expect(admin_profile.has_role?("ManageEBill")).to be_true

      expect(admin_profile.has_role?("ViewAuditTrail")).to be_true

      expect(admin_profile.has_role?("ManageOwnEBill")).to be_false
      expect(admin_profile.has_role?("ViewOwnEBill")).to be_false

    end

    it "should have Merchant profile" do
      Rails.logger.debug("test merchant profile")
      merchant_profile = Profile.find_by(name: Profile::MERCHANT)
      expect(merchant_profile).not_to be_nil

      expect(merchant_profile.has_role?("CreateEBill")).to be_true
      expect(merchant_profile.has_role?("ViewOwnEBill")).to be_true
      expect(merchant_profile.has_role?("ViewEBill")).to be_false
      expect(merchant_profile.has_role?("ManageEBill")).to be_false
      expect(merchant_profile.has_role?("ManageOwnEBill")).to be_true
      expect(merchant_profile.has_role?("ViewAuditTrail")).to be_false
    end

    it "should have ServiceDesk profile" do
      Rails.logger.debug("test service desk profile")
      service_desk_profile = Profile.find_by(name: Profile::SERVICE_DESK)
      expect(service_desk_profile).not_to be_nil

      expect(service_desk_profile.has_role?("CreateEBill")).to be_false
      expect(service_desk_profile.has_role?("ViewEBill")).to be_true
      expect(service_desk_profile.has_role?("ViewOwnEBill")).to be_false
      expect(service_desk_profile.has_role?("ViewAuditTrail")).to be_true
    end
  end

end