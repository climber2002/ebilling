# == Schema Information
#
# Table name: payment_system_monthly_items
#
#  id                             :integer          not null, primary key
#  paid_count                     :integer          default(0)
#  partially_paid_count           :integer          default(0)
#  processed_count                :integer          default(0)
#  revenue_centimes               :integer          default(0), not null
#  revenue_currency               :string(255)      default("XAF"), not null
#  payment_system_monthly_stat_id :integer
#  merchant_id                    :integer
#  created_at                     :datetime
#  updated_at                     :datetime
#  transaction_fee_centimes       :integer          default(0), not null
#  transaction_fee_currency       :string(255)      default("XAF"), not null
#

require 'spec_helper'

describe PaymentSystemMonthlyItem do
  let(:payment_system) { FactoryGirl.create(:payment) }
  let(:merchant) { FactoryGirl.create(:merchant) }
  let(:payment_system_monthly_stat) do
    PaymentSystemMonthlyStat.create(payment_system_id: payment_system.id, year: 2015, month: 9) 
  end

  describe 'creation' do 
    it 'should be able to create an item' do 
      item = payment_system_monthly_stat.payment_system_monthly_items.create(merchant: merchant)
      expect(item).to be_valid
    end
  end
end
