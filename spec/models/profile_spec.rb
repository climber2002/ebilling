# == Schema Information
#
# Table name: profiles
#
#  id          :integer          not null, primary key
#  name        :string(255)      not null
#  description :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

require 'spec_helper'

describe Profile do
  describe "Validation" do
    it "has a valid factory" do
      expect(FactoryGirl.create(:profile)).to be_valid
    end

    it "is invalid if the profile name already exists" do
      FactoryGirl.create(:profile)
      expect(FactoryGirl.build(:profile)).to have(1).errors_on(:name)
    end

    it "is invalid if the profile name is blank" do
      expect(FactoryGirl.build(:profile, name: "")).to have(1).errors_on(:name)
    end
  end

  describe "Manager roles" do
    it "is able to add roles" do
      profile = FactoryGirl.create(:profile_user_manager)
      origin_role_number = profile.roles.size

      profile.roles << FactoryGirl.create(:role_user_viewer)
      profile.roles << FactoryGirl.create(:role_user_manager)

      expect(profile.roles.size - origin_role_number).to eq 2
    end

    it "is able to assign roles by role_ids" do
      role_user_viewer = FactoryGirl.create(:role_user_viewer)
      role_user_manager = FactoryGirl.create(:role_user_manager)

      profile = FactoryGirl.create(:profile_user_manager)
      profile.role_ids = [role_user_viewer.id, role_user_manager.id, ""]
      expect(profile).to be_valid
      expect(profile.roles.size).to eq 2
    end
  end

  describe "has_role?" do
    before do
      @profile_admin = FactoryGirl.create(:profile_admin)
    end

    it "should have ViewEBill role" do
      expect(@profile_admin.has_role? "ViewEBill").to be_true
    end

    it "should not have ViewOwnEBill role" do
      expect(@profile_admin.has_role? "ViewOwnEBill").to be_false
    end
  end
end
