require 'spec_helper'

feature 'Logon' do
  background do
    @admin = FactoryGirl.create(:admin)
  end

  scenario 'logon as a valid user' do
    visit root_path
    fill_in 'user[username]', with: @admin.username
    fill_in 'user[password]', with: @admin.password
    click_button 'Sign in'

    expect(current_path).to eq root_path
    expect(page).to have_content 'Signed in successfully.'
  end

  scenario 'logon using wrong password' do
    visit root_path
    fill_in 'user[username]', with: @admin.username
    fill_in 'user[password]', with: "wrong_password"
    click_button 'Sign in'

    expect(current_path).to eq new_user_session_path
    expect(page).to have_content 'Invalid username or password'
  end

end