require 'spec_helper'

feature "Create EBill" do
  background do
    @admin = FactoryGirl.create(:admin)
    @merchant = FactoryGirl.create(:merchant)
    @merchant2 = FactoryGirl.create(:merchant2)

  end

  scenario "Create a valid EBill" do
    visit root_path
    fill_in 'user[username]', with: @merchant2.username
    fill_in 'user[password]', with: @merchant2.password
    click_button 'Sign in'

    click_link 'EBills'
    click_link 'create-e-bill'

    fill_in 'Payer Email', with: "payer@example.com"
    fill_in 'Payer MSISDN', with: "123456789"
    fill_in 'Amount', with: "200"
    fill_in 'Additional info', with: "Additional_Information"
    fill_in 'Short description', with: "short_description"
    fill_in 'Description', with: "New_Description"
    fill_in "Merchant internal ref", with: "MER000001"
    fill_in "Due date", with: "31/10/2014"
    find(:css, "#e_bill_email").set(true)

    click_button 'Save changes'

    expect(page).to have_content 'The Ebill is created'

    expect(page).to have_content 'merchant2'
    expect(page).to have_content 'State'
    expect(page).to have_content 'created'
    expect(page).to have_content '200'
    expect(page).to have_content 'payer@example.com'
    expect(page).to have_content 'Additional_Information'
    expect(page).to have_content 'New_Description'
    expect(page).to have_content 'MER000001'

    visit e_bills_path
    expect(page).to have_content "123456789"
  end

  scenario "Create a valid EBill for merchant" do
    visit root_path
    fill_in 'user[username]', with: @merchant.username
    fill_in 'user[password]', with: @merchant.password
    click_button 'Sign in'

    click_link 'EBills'
    click_link 'create-e-bill'

    fill_in 'Payer Email', with: "payer@example.com"
    fill_in 'Payer MSISDN', with: "123456789"
    fill_in 'Amount', with: "200"
    fill_in 'Short description', with: "short_description"
    fill_in 'Additional info', with: "Additional_Information"
    fill_in 'Description', with: "New_Description"
    fill_in "Merchant internal ref", with: "MER000001"
    fill_in "Due date", with: "31/10/2014"
    find(:css, "#e_bill_email").set(true)

    click_button 'Save changes'
    expect(page).to have_content 'The Ebill is created'

    page.driver.submit :delete, destroy_user_session_path, {}
    fill_in 'user[username]', with: @merchant2.username
    fill_in 'user[password]', with: @merchant2.password
    click_button 'Sign in'

    click_link 'EBills'

    expect(page).not_to have_content "123456789"
  end

end