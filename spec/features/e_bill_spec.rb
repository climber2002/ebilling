require 'spec_helper'

feature "Create EBill" do
  background do
    @admin = FactoryGirl.create(:admin)
    @merchant = FactoryGirl.create(:merchant)
    @merchant2 = FactoryGirl.create(:merchant2)

    visit root_path
    fill_in 'user[username]', with: @admin.username
    fill_in 'user[password]', with: @admin.password
    click_button 'Sign in'

    expect(current_path).to eq root_path
  end

  scenario "Create a valid EBill" do
    click_link 'EBills'
    click_link 'create-e-bill'

    select 'merchant2', from: 'Merchant'
    fill_in 'Payer Email', with: "payer@example.com"
    fill_in 'Payer MSISDN', with: "123456789"
    fill_in 'Amount', with: "200"
    fill_in 'Additional info', with: "Additional_Information"
    fill_in 'Short description', with: "short_description"
    fill_in 'Description', with: "New_Description"
    fill_in "Merchant internal ref", with: "MER000001"
    fill_in "Due date", with: "31/10/2014"
    find(:css, "#e_bill_email").set(true)

    click_button 'Save changes'

    expect(page).to have_content 'The Ebill is created'

    expect(page).to have_content 'merchant2'
    expect(page).to have_content 'State'
    expect(page).to have_content 'created'
    expect(page).to have_content '200'
    expect(page).to have_content 'payer@example.com'
    expect(page).to have_content 'Additional_Information'
    expect(page).to have_content 'New_Description'
    expect(page).to have_content 'MER000001'


    visit e_bills_path
    expect(page).to have_content "123456789"

    visit audit_trails_path
    expect(page).to have_content "created"

  end

  scenario "Create a valid EBill and set schedule_at" do
    click_link 'EBills'
    click_link 'create-e-bill'

    fill_in 'Payer MSISDN', with: "123456789"
    fill_in 'Payer Email', with: "payer@example.com"
    fill_in 'Amount', with: "200"
    fill_in 'Short description', with: "short_description"
    fill_in 'Schedule at', with: 3.days.from_now.to_s
    fill_in "Due date", with: "31/10/2014"
    find(:css, "#e_bill_email").set(true)

    click_button 'Save changes'

    expect(page).to have_content 'The Ebill is created'
    expect(page).to have_content 'State'
    expect(page).to have_content 'created'

  end

  scenario "Create a EBill, select email but doesn't specify Payee email" do
    click_link 'EBills'
    click_link 'create-e-bill'

    fill_in 'Payer MSISDN', with: "123456789"
    fill_in 'Amount', with: "200"
    find(:css, "#e_bill_email").set(true)

    click_button 'Save changes'
    expect(page).to have_content 'Must specify Email as you select the email notification'
  end

  scenario "Create a EBill but doesn't specify Payee MSISDN" do
    click_link 'EBills'
    click_link 'create-e-bill'

    fill_in 'Amount', with: "200"
    find(:css, "#e_bill_sms").set(true)

    click_button 'Save changes'
    expect(page).to have_content "can't be blank"
  end

end