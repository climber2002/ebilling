# == Schema Information
#
# Table name: ebilling_payment_accounts
#
#  id                :integer          not null, primary key
#  payment_system_id :integer
#  payment_id        :string(255)
#  created_at        :datetime
#  updated_at        :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :ebilling_payment_account do
  end
end
