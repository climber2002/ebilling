# == Schema Information
#
# Table name: payment_system_monthly_stats
#
#  id                :integer          not null, primary key
#  payment_system_id :integer          not null
#  year              :integer          not null
#  month             :integer          not null
#  created_at        :datetime
#  updated_at        :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :payment_system_monthly_stat do
  end
end
