# == Schema Information
#
# Table name: mobiles
#
#  id               :integer          not null, primary key
#  imei             :string(255)
#  user_id          :integer
#  otp              :string(255)
#  otp_last_sent_at :datetime
#  otp_confirmed_at :datetime
#  pin_hash         :string(255)
#  created_at       :datetime
#  updated_at       :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :mobile do
  end
end
