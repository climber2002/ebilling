# == Schema Information
#
# Table name: merchant_payment_system_mappings
#
#  id                  :integer          not null, primary key
#  merchant_id         :integer
#  payment_system_id   :integer
#  merchant_ps_id      :string(255)
#  created_at          :datetime
#  updated_at          :datetime
#  ps_percentage       :float            default(0.0)
#  search_field        :string(255)
#  ebilling_percentage :float            default(0.0)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :merchant_payment_system_mapping do
  end
end
