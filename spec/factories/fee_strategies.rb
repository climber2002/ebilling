# == Schema Information
#
# Table name: fee_strategies
#
#  id                      :integer          not null, primary key
#  type                    :string(255)
#  fixed_amount_centimes   :integer          default(0), not null
#  fixed_amount_currency   :string(255)      default("XAF"), not null
#  percentage              :float
#  maximum_amount_centimes :integer          default(0), not null
#  maximum_amount_currency :string(255)      default("XAF"), not null
#  merchant_id             :integer
#  created_at              :datetime
#  updated_at              :datetime
#  disable_recurrent       :boolean          default(FALSE)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :fee_strategy do
    
  end
end
