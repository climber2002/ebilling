# == Schema Information
#
# Table name: batch_files
#
#  id          :integer          not null, primary key
#  file        :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#  user_id     :integer
#  state       :string(255)
#  provisioned :integer          default(0)
#  succeeded   :integer          default(0)
#  failed      :integer          default(0)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :batch_file do
  end
end
