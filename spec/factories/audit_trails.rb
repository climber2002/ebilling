# == Schema Information
#
# Table name: audit_trails
#
#  id                    :integer          not null, primary key
#  client_transaction_id :string(255)
#  from                  :string(255)
#  to                    :string(255)
#  bill_id               :string(255)
#  payer_id              :string(255)
#  context               :text
#  created_at            :datetime
#  updated_at            :datetime
#  status                :string(255)      default("success")
#  action                :string(255)
#  params                :text
#  operator              :string(255)
#  error_messages        :text
#  search_field          :text
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :audit_trail do
  end
end
