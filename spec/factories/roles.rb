# == Schema Information
#
# Table name: roles
#
#  id           :integer          not null, primary key
#  name         :string(255)      not null
#  description  :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#  role_type_id :integer
#  display_name :string(255)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :role do
    name "Role"
    description "Role"

    factory :role_user_viewer do
      name "UserViewer"
      description "UserViewer"
    end

    factory :role_user_manager do
      name "UserManager"
      description "UserManager"
    end

    factory :role_profile_viewer do
      name "ProfileViewer"
      description "ProfileViewer"
    end

    factory :role_profile_manager do
      name "ProfileManager"
      description "ProfileManager"
    end

    factory :role_view_e_bill do
      name "ViewEBill"
      description "ViewEBill"
    end

    factory :role_create_e_bill do
      name "CreateEBill"
      description "CreateEBill"
    end

    factory :role_view_own_e_bill do
      name "ViewOwnEBill"
      description "ViewOwnEBill"
    end

    factory :role_manage_e_bill do
      name "ManageEBill"
      description "ManageEBill"
    end

    factory :role_manage_own_e_bill do
      name "ManageOwnEBill"
      description "ManageOwnEBill"
    end

    factory :role_view_audit_trail do
      name "ViewAuditTrail"
      description "ViewAuditTrail"
    end

    factory :role_verify_e_bill do
      name "VerifyEBill"
      description "VerifyEBill"
    end

    factory :role_view_payment_system_e_bill do
      name "ViewPaymentSystemEBill"
      description "ViewPaymentSystemEBill"
    end

    factory :role_update_e_bill_to_pay do
      name "UpdateEBillToPay"
      description "UpdateEBillToPay"
    end

    initialize_with { Role.find_or_create_by(name: name) }

    after(:build) do |role|
      role.role_type = FactoryGirl.create(:role_type)
    end
  end
end
