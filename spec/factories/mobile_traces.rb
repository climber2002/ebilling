# == Schema Information
#
# Table name: mobile_traces
#
#  id          :integer          not null, primary key
#  device_id   :string(255)
#  stack_trace :text
#  user_id     :integer
#  created_at  :datetime
#  updated_at  :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :mobile_trace do
    device_id { Faker::Number.number(10).to_s }
    stack_trace { "stack trace......" }
    user 
  end
end
