# == Schema Information
#
# Table name: fee_buckets
#
#  id               :integer          not null, primary key
#  currency_id      :integer
#  min_transactions :integer
#  max_transactions :integer
#  fee_value        :integer
#  created_at       :datetime
#  updated_at       :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :fee_bucket do
  end
end
