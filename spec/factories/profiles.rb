# == Schema Information
#
# Table name: profiles
#
#  id          :integer          not null, primary key
#  name        :string(255)      not null
#  description :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :profile do
    name "Profile"
    description "Profile"

    factory :profile_user_manager do
      name "UserManager"
      description "UserManager"

      after(:build) do |profile|
        profile.roles << FactoryGirl.create(:role_user_manager)
      end
    end

    factory :profile_admin do
      name Profile::ADMIN
      description "Can manage everything"
      after(:build) do |profile|
        [:role_user_viewer, :role_user_manager,
          :role_profile_viewer, :role_profile_manager,
          :role_view_e_bill, :role_create_e_bill, :role_manage_e_bill,
          :role_view_audit_trail].each do |role|
          profile.roles << FactoryGirl.create(role)
        end
      end
    end

    factory :profile_merchant do
      name Profile::MERCHANT
      description Profile::MERCHANT
      after(:build) do |profile|
        [:role_create_e_bill, :role_view_own_e_bill, :role_manage_own_e_bill].each do |role|
          profile.roles << FactoryGirl.create(role)
        end
      end
    end

    factory :profile_service_desk do
      name Profile::SERVICE_DESK
      description Profile::SERVICE_DESK
      after(:build) do |profile|
        [:role_view_e_bill, :role_view_audit_trail].each do |role|
          profile.roles << FactoryGirl.create(role)
        end
      end
    end

    factory :profile_payment_system do
      name Profile::PAYMENT_SYSTEM
      description Profile::PAYMENT_SYSTEM
      after(:build) do |profile|
        [:role_verify_e_bill, :role_update_e_bill_to_pay].each do |role|
          profile.roles << FactoryGirl.create(role)
        end
      end
    end

    factory :profile_payment_system_operator do
      name Profile::PAYMENT_SYSTEM_OPERATOR
      description Profile::PAYMENT_SYSTEM_OPERATOR
      after(:build) do |profile|
        [:role_view_payment_system_e_bill].each do |role|
          profile.roles << FactoryGirl.create(role)
        end
      end
    end

    factory :profile_ebill_creator do
      name Profile::EBILL_CREATOR
      description Profile::EBILL_CREATOR
      after(:build) do |profile|
        [:role_create_e_bill].each do |role|
          profile.roles << FactoryGirl.create(role)
        end
      end
    end

    after(:build) do |profile|
      profile.roles << FactoryGirl.create(:role)
    end
  end
end
