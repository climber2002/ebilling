# == Schema Information
#
# Table name: e_bills
#
#  id                          :integer          not null, primary key
#  user_id                     :integer
#  payer_email                 :string(255)
#  payer_msisdn                :string(255)
#  amount                      :integer
#  currency                    :string(255)
#  state                       :string(255)
#  expired_at                  :datetime
#  schedule_at                 :datetime
#  external_reference          :string(255)
#  additional_info             :string(255)
#  description                 :string(255)
#  reason                      :string(255)
#  sms                         :boolean          default(FALSE)
#  email                       :boolean          default(FALSE)
#  created_at                  :datetime
#  updated_at                  :datetime
#  sms_message                 :string(255)
#  merchant_internal_ref       :string(255)
#  sent_at                     :datetime
#  paid_at                     :datetime
#  cancelled_at                :datetime
#  processed_at                :datetime
#  failed_at                   :datetime
#  expire_at                   :datetime
#  short_description           :string(255)
#  due_date                    :date
#  bill_id                     :string(255)
#  batch_file_id               :integer
#  payer_name                  :string(255)
#  payer_address               :string(255)
#  payer_city                  :string(255)
#  accept_partial_payment      :boolean          default(FALSE)
#  minimum_amount              :integer
#  amount_paid                 :integer          default(0)
#  partially_paid_at           :datetime
#  overdue_at                  :datetime
#  payment_system_id           :integer
#  ps_transaction_id           :string(255)
#  traffic_fee_centimes        :integer          default(0), not null
#  traffic_fee_currency        :string(255)      default("XAF"), not null
#  search_field                :text
#  payer_id                    :string(255)
#  payer_code                  :string(255)
#  data0                       :string(255)
#  data1                       :string(255)
#  data2                       :string(255)
#  data3                       :string(255)
#  data4                       :string(255)
#  data5                       :string(255)
#  data6                       :string(255)
#  data7                       :string(255)
#  data8                       :string(255)
#  data9                       :string(255)
#  ps_transaction_fee_centimes :integer          default(0), not null
#  ps_transaction_fee_currency :string(255)      default("XAF"), not null
#  last_paid_amount_centimes   :integer          default(0), not null
#  last_paid_amount_currency   :string(255)      default("XAF"), not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :e_bill do
    payer_email { Faker::Internet.email }
    payer_msisdn { Faker::Number.number(10).to_s }
    amount { Faker::Number.number(3) }
    short_description "short_description"
    user { FactoryGirl.create(:merchant, username: Faker::Name.name, currency: 'SGD') }
    email      true
    sms        true
    due_date   "31/10/2013"
    payer_name { Faker::Name.name }
    payer_address { Faker::Address.street_address }
    payer_city { Faker::Address.city }
    accept_partial_payment false

    factory :e_bill_for_api do
      payer_msisdn { Faker::Number.number(10).to_s }
      payer_id { 'payer_id' }
      payer_email { Faker::Internet.email }
      short_description "short_description"
      amount 100
      currency "XAF"
      sms "1"
      email "1"
      expire_at "2013-07-24T13:51:29-04:00"
      due_date   "2013-10-13" 
      external_reference "internal ref001"
      description "description"
    end
  end

  
end
