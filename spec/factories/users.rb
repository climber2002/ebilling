# == Schema Information
#
# Table name: users
#
#  id                             :integer          not null, primary key
#  username                       :string(255)      default(""), not null
#  encrypted_password             :string(255)      default(""), not null
#  email                          :string(255)      default("")
#  reset_password_token           :string(255)
#  reset_password_sent_at         :datetime
#  remember_created_at            :datetime
#  sign_in_count                  :integer          default(0)
#  current_sign_in_at             :datetime
#  last_sign_in_at                :datetime
#  current_sign_in_ip             :string(255)
#  last_sign_in_ip                :string(255)
#  created_at                     :datetime
#  updated_at                     :datetime
#  first_name                     :string(255)
#  last_name                      :string(255)
#  msisdn                         :string(255)
#  city                           :string(255)
#  address                        :string(255)
#  e_bills_count                  :integer          default(0)
#  notification_url               :string(255)
#  notification_params            :string(255)      default([]), is an Array
#  notification_post              :boolean          default(TRUE)
#  legal_info                     :string(255)
#  greeting_message               :string(255)
#  email_notification             :boolean          default(FALSE)
#  website                        :string(255)
#  logo                           :string(255)
#  signature                      :string(255)
#  additional_notification_params :string(255)
#  currency                       :string(255)
#  payment_system_id              :integer
#  search_field                   :text
#  shared_key                     :string(255)
#  tax_rate                       :float
#  zipcode                        :string(255)
#  charge_payer                   :boolean          default(FALSE)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    username { Faker::Name.first_name }
    password "guest123"
    password_confirmation "guest123"
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    msisdn { Faker::Number.number(10).to_s }

    factory :admin do
      username "admin"
      password "admin123"
      password_confirmation "admin123"

      after :build do |user|
        user.profiles << (Profile.find_by(:name => Profile::ADMIN) ||
                          FactoryGirl.create(:profile_admin))
      end
    end

    factory :ebill_creator do 
      username "ebill_creator"
      currency "XAF"
      password "ebill_creator123"
      password_confirmation "ebill_creator123"

      after :build do |user|
        user.profiles << (Profile.find_by(:name => Profile::EBILL_CREATOR) ||
                          FactoryGirl.create(:profile_ebill_creator))
      end
    end

    factory :merchant do
      username "merchant"
      currency "XAF"
      password "merchant123"
      password_confirmation "merchant123"

      after :build do |user|
        user.profiles << (Profile.find_by(:name => Profile::MERCHANT) ||
                          FactoryGirl.create(:profile_merchant))
      end
    end

    factory :merchant2 do
      username "merchant2"
      password "merchant123"
      currency "SGD"
      password_confirmation "merchant123"

      after :build do |user|
        user.profiles << (Profile.find_by(:name => Profile::MERCHANT) ||
                          FactoryGirl.create(:profile_merchant))
      end
    end

    factory :merchant3 do
      username "merchant3"
      password "merchant123"
      currency "USD"
      password_confirmation "merchant123"

      after :build do |user|
        user.profiles << (Profile.find_by(:name => Profile::MERCHANT) ||
                          FactoryGirl.create(:profile_merchant))
      end
    end

    factory :service_desk do
      username "servicdesk"
      password "servicedesk123"
      password_confirmation "servicedesk123"

      after :build do |user|
        user.profiles << (Profile.find_by(name: Profile::SERVICE_DESK) ||
                          FactoryGirl.create(:profile_service_desk))
      end
    end

    factory :payment do
      username "payment"
      password "payment123"
      password_confirmation "payment123"

      after :build do |user|
        user.profiles << (Profile.find_by(name: Profile::PAYMENT_SYSTEM) ||
                          FactoryGirl.create(:profile_payment_system))
      end
    end

    factory :payment_system_operator do
      username "payment_system_operator"
      password "payment123"
      password_confirmation "payment123"
      payment_system { FactoryGirl.create(:payment, username: "payment_#{rand(100)}") }

      after :build do |user|
        user.profiles << (Profile.find_by(name: Profile::PAYMENT_SYSTEM_OPERATOR) ||
                          FactoryGirl.create(:profile_payment_system_operator))
      end
    end
  end
end
