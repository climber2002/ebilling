# == Schema Information
#
# Table name: merchant_global_stats
#
#  id                   :integer          not null, primary key
#  created_count        :integer          default(0)
#  ready_count          :integer          default(0)
#  paid_count           :integer          default(0)
#  partially_paid_count :integer          default(0)
#  cancelled_count      :integer          default(0)
#  failed_count         :integer          default(0)
#  processed_count      :integer          default(0)
#  expired_count        :integer          default(0)
#  merchant_id          :integer          not null
#  created_at           :datetime
#  updated_at           :datetime
#  overdue_count        :integer          default(0)
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :merchant_global_stat do
  end
end
