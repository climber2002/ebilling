# == Schema Information
#
# Table name: role_types
#
#  id          :integer          not null, primary key
#  name        :string(255)
#  description :string(255)
#  created_at  :datetime
#  updated_at  :datetime
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :role_type do
    name "role_type"
    description "role_type"

    initialize_with { RoleType.find_or_create_by(name: name) }
  end
end
