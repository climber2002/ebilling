# == Schema Information
#
# Table name: merchant_monthly_stats
#
#  id                          :integer          not null, primary key
#  created_count               :integer          default(0)
#  ready_count                 :integer          default(0)
#  paid_count                  :integer          default(0)
#  partially_paid_count        :integer          default(0)
#  cancelled_count             :integer          default(0)
#  failed_count                :integer          default(0)
#  processed_count             :integer          default(0)
#  expired_count               :integer          default(0)
#  merchant_id                 :integer          not null
#  year                        :integer          not null
#  month                       :integer          not null
#  created_at                  :datetime
#  updated_at                  :datetime
#  overdue_count               :integer          default(0)
#  traffic_fee_centimes        :integer          default(0), not null
#  traffic_fee_currency        :string(255)      default("XAF"), not null
#  revenue_centimes            :integer          default(0), not null
#  revenue_currency            :string(255)      default("XAF"), not null
#  recurrent_fee_centimes      :integer          default(0), not null
#  recurrent_fee_currency      :string(255)      default("XAF"), not null
#  ps_transaction_fee_centimes :integer          default(0), not null
#  ps_transaction_fee_currency :string(255)      default("XAF"), not null
#

# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :merchant_monthly_stat do
  end
end
