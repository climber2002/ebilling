source 'https://rubygems.org'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.0.0'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.0'

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
# gem 'therubyracer', platforms: :ruby

# Use jquery as the JavaScript library
gem 'jquery-rails'

gem 'jquery-fileupload-rails'

# Turbolinks makes following links in your web application faster. Read more: https://github.com/rails/turbolinks
gem 'turbolinks'

gem 'jquery-turbolinks'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

# Devise
gem 'devise', '= 3.0.2'

# CanCan
gem 'cancan', '= 1.6.10'

# state_machine
gem 'state_machine', '1.2.0'

# money
gem 'money', '5.1.1'

gem 'countries'

# postgresql
gem 'pg', '0.17.0'

# will_paginate
gem 'will_paginate', '3.0.3'

gem 'bootstrap-will_paginate', '0.0.6'

# delay job
gem 'delayed_job_active_record', '4.0.0'

gem 'font-awesome-rails'

gem 'therubyracer'

gem 'daemons'

gem 'carrierwave'

gem 'mini_magick'

gem 'smarter_csv'

gem "rails-settings-cached", "0.3.1"

gem 'uuidtools'

gem 'rest-client'

gem 'roo'

gem 'odf-report', :git => 'git://github.com/sandrods/odf-report'

gem 'annotate'

gem 'rails_kindeditor'

gem 'data_migrate'

gem 'rabl', '~> 0.9.4.pre1'
gem 'oj'
gem 'versioncake', '~> 2.3.1'

gem 'money-rails'

gem 'newrelic_rpm'

gem 'axlsx_rails'

gem 'active_type'

gem 'rubyzip', '~> 0.9.4'

gem 'activerecord-nulldb-adapter'

# rspec
group :development, :test do
  gem 'rspec-rails', '~> 2.14.0'
  gem 'factory_girl_rails', '~> 4.2.1'
  gem 'pry-rails'
  gem 'pry-byebug'
end

group :test do
  gem 'faker', '~> 1.2.0'
  gem 'capybara', '~> 2.1.0'
  gem 'database_cleaner', '~> 1.0.1'
  gem 'launchy', '~> 2.3.0'
  gem 'selenium-webdriver', '~> 2.33.0'
  gem 'timecop'
  gem 'test-unit'
end



group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

# Use ActiveModel has_secure_password
# gem 'bcrypt-ruby', '~> 3.0.0'

# Use unicorn as the app server
gem 'unicorn'

# gem 'capistrano', '~> 3.1.0', group: :development

# # rails specific capistrano funcitons
# gem 'capistrano-rails', '~> 1.1.0', group: :development

# # integrate bundler with capistrano
# gem 'capistrano-bundler', group: :development

# # if you are using RBENV
# gem 'capistrano-rbenv', "~> 2.0", group: :development

gem 'puma', '~> 3.0'

gem 'whenever', :require => false

# Use Capistrano for deployment
# gem 'capistrano', group: :development

# Use debugger
# gem 'debugger', group: [:development, :test]

# json
gem 'json', '1.8.3'
