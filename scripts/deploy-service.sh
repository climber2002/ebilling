#! /bin/bash

set -euo pipefail

BASEDIR=$(dirname $0)/..

## Fetch dependencies requirement of service
echo "--- fetch specifications"
specification=$(<${BASEDIR}/service-deployment/${ENVIRONMENT}-specification.json)

service_priority=$(echo $specification | jq -r '.Priority')
zone_name=$(echo $specification | jq -r '.Route53ZoneName')
domain_name=$(echo $specification | jq -r '.DomainName')
scheme=$(echo $specification | jq -r '.Scheme')
with_background_task=$(echo $specification | jq -r '.WithBackgroundTask')
docker_image=$(echo $specification | jq -r '.DockerImage')
desired_count=$(echo $specification | jq -r '.DesiredCount')
background_desired_count=$(echo $specification | jq -r '.BackgroundDesiredCount')
with_background_task_health_check=$(echo $specification | jq -r '.WithBackgroundTaskHealthCheck')
[ $with_background_task_health_check = null ] && with_background_task_health_check='false'
[ $desired_count = null ] && desired_count=1
[ $background_desired_count = null ] && background_desired_count=1
exposed_host=$(echo $specification | jq -r '.ExposedHost')
[ $exposed_host = null ] && exposed_host=${APP_NAME}.${zone_name}
exposed_host_cert=$(echo $specification | jq -r '.ExposedHostCertificate')
[ $exposed_host_cert = null ] && exposed_host_cert=none
max_percent=$(echo $specification | jq -r '.MaximumPercent')
[ $max_percent = null ] && max_percent=200
min_percent=$(echo $specification | jq -r '.MinimumHealthyPercent')
[ $min_percent = null ] && min_percent=100
memory=$(echo $specification | jq -r '.Memory')
[ $memory = null ] && memory=1000
memory_reservation=$(echo $specification | jq -r '.MemoryReservation')
[ $memory_reservation = null ] && memory_reservation=750
background_memory=$(echo $specification | jq -r '.BackgroundMemory')
[ $background_memory = null ] && background_memory=1000
background_memory_reservation=$(echo $specification | jq -r '.BackgroundMemoryReservation')
[ $background_memory_reservation = null ] && background_memory_reservation=750
class=$(echo $specification | jq -r '.Class')
[ $class = null ] && class="ecs_service"
maintenance_mode=$(echo $specification | jq -r '.MaintenanceMode')
[ $maintenance_mode = null ] && maintenance_mode="off"
healthcheck_path=$(echo $specification | jq -r '.HealthCheckPath')
[ $healthcheck_path = null ] && healthcheck_path="/"
auto_scale_mode=$(echo $specification | jq -r '.AutoScaleMode')
[ $auto_scale_mode = null ] && auto_scale_mode="none"

database_password=$(eval "aws ssm get-parameter --with-decryption  --name "ebilling-db-password" | jq -r .Parameter | jq -r .Value")
secret_key_base=$(eval "aws ssm get-parameter --with-decryption  --name "ebilling-secret-key-base" | jq -r .Parameter | jq -r .Value")


# if [ "${BUILDKITE}" == "true" ]
# then
#   echo "--- assuming account [${AWS_ACCOUNT}]"
#   [ -n "${AWS_ACCOUNT}" ] && $(stsassume ${AWS_ACCOUNT}) || exit 1
# fi

## Compile the docker image name
docker_image="${DOCKER_IMAGE}:${COMMIT}"
echo "Deploy docker image ${docker_image}"

set +e

echo "--- :cloudformation: :aws: deploy cloudformation stack"
aws cloudformation deploy \
  --no-fail-on-empty-changeset \
  --stack-name ${PROJECT}-${ENVIRONMENT}-${APP_NAME}-service \
  --template-file service-deployment/ecs-service.yml \
  --parameter-overrides "AppName=${APP_NAME}"  \
    "Environment=${ENVIRONMENT}" \
    "DockerImage=${docker_image}" "Priority=${service_priority}" \
    "Project=${PROJECT}" "ZoneName=${zone_name}" \
    "WithBackgroundTask=${with_background_task}" \
    "DesiredCount"=${desired_count} \
    "BackgroundDesiredCount"=${background_desired_count} \
    "WithBackgroundTaskHealthCheck"=${with_background_task_health_check} \
    "ExposedHost"=${exposed_host} \
    "ExposedHostCert"=${exposed_host_cert} \
    "MaximumPercent"=${max_percent} \
    "MinimumHealthyPercent"=${min_percent} \
    "Memory=${memory}" "MemoryReservation=${memory_reservation}" \
    "BackgroundMemory=${background_memory}" "BackgroundMemoryReservation=${background_memory_reservation}" \
    "Class=${class}" "MaintenanceMode=${maintenance_mode}" \
    "HealthCheckPath=${healthcheck_path}" \
    "AutoScaleMode=${auto_scale_mode}" \
    "DatabasePassword=${database_password}" \
    "SecretKeyBase=${secret_key_base}" \
    "DomainName=${domain_name}" \
   --tags "application=${APP_NAME}" "environment=${ENVIRONMENT}" \
    "commit=${COMMIT}" "project=${PROJECT}" \
  --no-fail-on-empty-changeset --capabilities CAPABILITY_IAM

# if [ $? -eq 0 ]; then
#   echo "creation/update of stack succeed."
# else
#   aws cloudformation describe-stack-events --stack-name ${PROJECT}-${ENVIRONMENT}-${APP_NAME} --query 'StackEvents[*].{EventID:EventId,ResourceStatus:ResourceStatus,ResourceType:ResourceType,Timestamp:Timestamp,ResourceStatusReason:ResourceStatusReason,PhysicalResourceId:PhysicalResourceId}' >&2
#   echo "creation/update of stack failed." >&2
#   exit 1
# fi
