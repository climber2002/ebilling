#!/usr/bin/env bash

set -euo pipefail

echo "Build image"

## Construct the docker image with tag
DOCKER_IMAGE=${DOCKER_IMAGE:-${APP_NAME}:${COMMIT}}

echo "Build ${DOCKER_IMAGE}"

# mkdir -p target
# tar czf target/app.tgz app bin config db lib config.ru Rakefile Gemfile Gemfile.lock
docker build --compress --force-rm --no-cache -t "${DOCKER_IMAGE}" -f Dockerfile .
