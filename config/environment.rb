# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Ebilling::Application.initialize!

APP_CONFIG = YAML.load_file("#{Rails.root}/config/application.yml")

begin
  ActionMailer::Base.smtp_settings = {
    :address              => Settings.smtp_address,
    :port                 => Settings.smtp_port.to_i,
    :domain               => Settings.smtp_domain,
    :user_name            => Settings.smtp_username,
    :password             => Settings.smtp_password,
    :authentication       => 'plain',
    :enable_starttls_auto => true
  }
rescue StandardError => e
  Rails.logger.warn("Can't load Settings, it's run in db migration, #{e}")
end

