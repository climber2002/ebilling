# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
# set :output, "/path/to/my/cron_log.log"
#
# every 2.hours do
#   command "/usr/bin/some_great_command"
#   runner "MyModel.some_method"
#   rake "some:great:rake:task"
# end
#
# every 4.days do
#   runner "AnotherModel.prune_old_records"
# end

# Learn more: http://github.com/javan/whenever

ENV.each { |k, v| env(k, v) }

set :output, "#{path}/log/cron_log.log"

every 10.minutes do
  runner "MerchantGlobalStatsPopulator.new.populate"
end

every 1.day do 
  runner "MerchantMonthlyStatsPopulator.new.populate_for_yesterday"
  runner "PaymentSystemMonthlyStatsPopulator.new.populate_for_yesterday"
end

every 1.day do 
  runner "OverduePopulator.new.populate_overdue"
end
