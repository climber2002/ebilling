set :stage, :production
set :branch, "master"

# This is used in the Nginx VirtualHost to specify which domains
# the app should appear on. If you don't yet have DNS setup, you'll
# need to create entries in your local Hosts file for testing.
set :server_name, "lab.billing-easy.net"

# used in case we're deploying multiple versions of the same
# app side by side. Also provides quick sanity checks when looking
# at filepaths
set :full_app_name, "#{fetch(:application)}_#{fetch(:stage)}"

# server '188.166.96.137', user: 'deploy', roles: %w{web app db}, primary: true
server '51.254.223.86', user: 'deploy', roles: %w{web app db}, primary: true

set :deploy_to, "/data/lab.billing-easy.net"

# dont try and infer something as important as environment from
# stage name.
set :rails_env, :production

# number of unicorn workers, this will be reflected in
# the unicorn.rb and the monit configs
set :unicorn_worker_count, 1

# whether we're using ssl or not, used for building nginx
# config file
set :enable_ssl, false

set :delayed_job_args, "-n 1"
