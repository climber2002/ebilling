working_directory File.expand_path("../..", __FILE__)
worker_processes 10
listen "/tmp/unicorn.sock"
timeout 30
pid "/tmp/unicorn_xhoppe.pid"
stdout_path "#{Rails.root}/log/unicorn.log"
stderr_path "#{Rails.root}/log/unicorn.log"