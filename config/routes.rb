Ebilling::Application.routes.draw do
  devise_for :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root 'dashboard#index'

  resources :e_bills do
    member do
      get :pre_trigger
      put :trigger
      put :proces, as: :process
      put :send_receipt
      put :send_invoice
      get :export
    end
  end
  get "/diagnostic", controller: "diagnostic", action: "index"
  get "/locale/:locale", controller: "locale", action: "update", as: :locale
  resources :audit_trails, only: [:index, :show]
  resources :profiles
  resources :users do 
    resource :fee_strategy
  end
  resource  :settings
  resource  :system_configs do
    collection do
      post :regenerate_stats 
    end
  end
  resources :countries
  resource  :e_bill_exports
  resource  :shared_keys
  resources :batch_files, only: [:new, :create, :index]
  resource :outline, controller: :outline, only: [:edit, :update, :show] do
    member do
      get :edit_password
      put :update_password
    end
  end
  resources :mobiles
  resources :mobile_traces
  resources :monthly_stats
  resources :currencies do 
    resources :fee_buckets
  end
  resources :ebilling_payment_accounts
  resources :merchant_payment_system_mappings
  resources :anonymous_searches, only: [:new, :create]
  
  resources :payment_system_operator_merchants, only: [:index]
  resources :merchant_payment_systems, only: [:index]

  namespace :merchant do 
    resources :registrations, only: [:new, :create]
  end

  namespace :api do
    scope '/v:api_version' do
      namespace :mobileapp do
        resources :mobiles do 
          member do 
            put :verify
            put :forget_pin
          end
        end

        resources :mobile_traces
        resources :e_bills, only: [:create] do 
          collection do 
            get :search
            get :history
            get :by_external_reference
            get :download_receipt
          end
        end
      end
    end

    namespace :v1 do
      namespace :merchant do
        resources :e_bills do
          member do
            put :send, action: :trigger
            put :process, action: :handle
          end
          collection do 
            post :bulk_create
          end
        end
      end

      namespace :payment_gateway do
        resources :e_bills do
          member do
            put :pay
            put :verify
          end
        end
      end
    end
  end

  post 'soap_ebills/:id/update' => 'soap_ebills#update'
  post 'soap_ebills/:id/verify' => 'soap_ebills#verify'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
