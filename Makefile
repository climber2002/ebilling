.PHONY: deploy-%

export PROJECT := ebilling
export APP_NAME := ebilling
export IMAGE_NAME := ebilling
export AWS_DEFAULT_REGION := us-east-1
export DOCKER_URL := 094446193982.dkr.ecr.us-east-1.amazonaws.com
# export DOCKER_URL := 660283961017.dkr.ecr.us-east-1.amazonaws.com
export DOCKER_IMAGE = $(DOCKER_URL)/$(PROJECT)
export COMMIT := $(shell git rev-parse --short HEAD)


%-staging: env := staging
%-production: env := production

build:
	@./scripts/build.sh

ecr-login:
	@eval ``

package:
	@./scripts/push_image.sh

deploy-%: ecr-login
	@docker-compose run --rm \
		-e ENVIRONMENT=$(env) -e OWNER -e PROJECT \
		-e APP_NAME -e COMMIT -e DOCKER_IMAGE \
		build-helper scripts/deploy-service.sh




